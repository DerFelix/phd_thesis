\noindent
In this chapter, we introduce the spaces $\SDsstarN_g(m,1) \subset \SDsstar_g(m,1)$ which are
the spaces of Sullivan diagrams of genus $g$ with $m$ (un)parametrized enumerated normalized outgoing boundaries.
Using the homotopy equivalence of \cite{EgasKupers2014}, see also Proposition \ref{section:inclusion_of_fM_into_SD_combinatorial},
the spaces $\SDsstarN_g(m,1)$ are regarded as the harmonic compactification of the moduli spaces with normalized outgoing boundaries, i.e.,
$\ov{N\fM}^\star_g(m,1) \simeq \SDsstarN_g(m,1)$.
Consequently, all results on the homotopy type of $\SDsstarN_g(m,1)$ also hold true for the harmonic compactification $\ov{N\fM}^\star_g(m,1)$.
Let us state the main result of this chapter.
\begin{theoremwithfixednumber}{\ref{theorem:stable_homotopy_type_of_harm_normalized}}
  Let $m\ge 1$ and $g \ge 0$.
  There are maps of $m$-dimensional torus fibrations.
  \begin{align*}
    \begin{tikzcd}[ampersand replacement=\&]
        \RadpaenN_g(m,1) \ar{r} \ar{d}{\pi^\paen_\unen} \& \SDspaenN_g(m,1) \ar[hook]{r}{\ov\varphi_g} \ar{d}{\pi^\paen_\unen} \& \SDspaenN_\infty(m,1) \ar{r}{\Pol_{\SDs}} \ar{d}{\pi^\paen_\unen} \& \PolspcpaenN(m,1) \simeq EU(1)^m \ar{d}{\pi^\paen_\unen} \\
        \RadunenN_g(m,1) \ar{r}                         \& \SDsunenN_g(m,1) \ar[hook]{r}{\ov\varphi_g}                         \& \SDsunenN_\infty(m,1) \ar{r}{\Pol_{\SDs}}                         \& \PolspcunenN(m,1) \simeq BU(1)^m
    \end{tikzcd}
  \end{align*}
  The composition $\RadpaenN_g(m,1) \to \PolspcpaenN(m,1)$ respectively $\RadunenN_g(m,1) \to \PolspcunenN(m,1)$ is the restriction to the outgoing boundaries
  (see Definition \ref{definition:restriction_to_the_outgoing_boundaries}).
  The stabilization maps $\ov\varphi_g$ are $(g-2)$-connected and the maps $\Pol_{\SDs}$ are homotopy equivalences.
\end{theoremwithfixednumber}

The chapter is organized as follows.
In Section \ref{section:harmonic_compacitification_normalized},
we introduce the spaces of Sullivan diagrams with normalized outgoing boundaries, denoted by $\SDsstarN_g(m,1)$, and
relate these spaces to the spaces of normalized polygons.
Using methods developed in Sections
\ref{section:polygon_spaces_normalized}, \ref{section:poly_spaces_cellular_structures}, \ref{section:desuspension_theorem_pol} and \cite[Section 4]{BoesEgas2017},
we construct an $m$-fold multi-cyclic structure on $\SDspaenN_g(m,1)$, see Proposition \ref{proposition:multicyclic_structure_on_NSD},
we obtain the desuspension theorem for the spaces of Sullivan diagrams, see Theorem \ref{theorem:desuspension_theorem_SD}, and
we show that the spaces $\SDsstarN_g(m,1)$ admit homological stability with respect to the genus, see Proposition \ref{proposition:homological_stability_NSD}.

In Section \ref{section:homotopy_type_of_harmonic_compacitification_normalized},
we study the homotopy type of the spaces $\SDsstarN_g(m,1)$ using the ``unreduced cluster spectral sequence''.
The main result of this chapter is Theorem \ref{theorem:stable_homotopy_type_of_harm_normalized} stated above.
