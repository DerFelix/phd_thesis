\section{On the homotopy type of the spaces \texorpdfstring{$\SDsstarN_g(m,1)$}{NSD\_g(m,1)}}
\label{section:homotopy_type_of_harmonic_compacitification_normalized}
In this section, we study the homotopy type of the spaces of Sullivan diagram with normalized boundaries.
Let us state our main result.
\begin{theoremwithfixednumber}{\ref{theorem:stable_homotopy_type_of_harm_normalized}}
  Let $m\ge 1$, $g \ge 0$ and consider the following maps of $m$-dimensional torus fibrations.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
        \SDspaenN_g(m,1) \ar[hook]{r}{\ov\varphi} \ar{d}{\pi^\paen_\unen} \& \SDspaenN_\infty(m,1) \ar{r}{\Pol} \ar{d}{\pi^\paen_\unen} \& \PolspcpaenN(m,1) \simeq EU(1)^m \ar{d}{\pi^\paen_\unen} \\
        \SDsunenN_g(m,1) \ar[hook]{r}{\ov\varphi}                         \& \SDsunenN_\infty(m,1) \ar{r}{\Pol}                         \& \PolspcunenN(m,1) \simeq BU(1)^m
    \end{tikzcd}
  \end{align}
  The stabilization maps $\ov\varphi$ are $(g-2)$-connected and the maps $\Pol$ are homotopy equivalences.
\end{theoremwithfixednumber}
We obtain Theorem \ref{theorem:stable_homotopy_type_of_harm_normalized} by a careful study of the ``unreduced cluster filtration'' which is denoted by $Cl^-_p\SDspaenN_g(m,1)$ of $\SDspaenN_g(m,1)$.
Our cluster filtration, introduced in Section \ref{section:cluster_filtration_of_SD}, is an extension of Bödigheimer's filtration of the moduli spaces.
The associated cluster spectral sequence is studied in Section \ref{section:cluster_spectral_sequence_of_NSDpaen} and we show that
the inclusion $Cl^-_1\SDspaenN_g(m,1) \subset \SDspaenN_g(m,1)$ induces homology isomorphisms in the range $0 \le \ast \le g-2$,
see Proposition \ref{proposition:cluster_spectral_sequence_NSD}.
Then, in Section \ref{section:homotopy_type_of_NSDpaen},
we show that this inclusion and the map $\Pol \colon Cl^-_1\SDspaenN_g(m,1) \to \PolspcpaenN_g(m,1)$ are both $(g-2)$ connected.
This will imply Theorem \ref{theorem:stable_homotopy_type_of_harm_normalized}.


\subsection{The (unreduced) cluster filtration of the spaces of Sullivan diagrams}
\label{section:cluster_filtration_of_SD}
In the section, we introduce the \defn{cluster filtration} and the \defn{unreduced cluster filtration}.
\begin{definition}
  \label{definition:SD_trivial_surface}
  Let $\Sigma = (\lambda, S_1, \ldots, S_v)$ be a combinatorial Sullivan diagram.
  A ghost surface $S_i$ is a \defn{non-essential cluster} if there exists $0 < r \le \deg(\Sigma)$ and (in the parametrized case) a leaf $L_j$ such that $S_i$ is one of the following:
  \begin{align}
    S_i = (0,0,\{ \cycle{r} \})
    \;\text{ or }\;
    S_i = (0,0, \{\cycle{L_j\ r}\})\,.
  \end{align}
  A ghost surface $S_i$ is an \defn{essential cluster} if it not a non-essential cluster.
\end{definition}

\begin{remark}
  Observe that the ghost surfaces $(0,0,\{ \cycle{0} \})$ and $(0,0,\{ \cycle{L_i\ 0} \})$ are essential clusters.
  This is necessary in order to make the stabilization map and the torus action compatible with the cluster filtration, see below.
\end{remark}

\begin{definition}
  \label{definition:SD_cluster_filtration}
  Let $\Sigma = (\lambda, S_1, \ldots, S_v)$ be a combinatorial Sullivan diagram.
  The \defn{cluster number of $\Sigma$} is its number of essential clusters:
  \begin{align}
    c(\Sigma) = \# \{ 1 \le i \le v \mid S_i \text{ essential cluster} \} \,.
  \end{align}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  On the spaces of Sullivan diagrams (with normalized boundaries), the \defn{cluster filtration} is
  \begin{align}
    Cl_p\SDsstar_g(m,1)  &= \{ \Sigma \in \SDsstar_g(m,1) \mid c(\Sigma) \le p \}
    \intertext{respectively}
    Cl_p\SDsstarN_g(m,1) &= \{ \Sigma \in \SDsstarN_g(m,1) \mid c(\Sigma) \le p \} \,.
  \end{align}
\end{definition}

\begin{remark}
  Note that the cluster filtration of the spaces of Sullivan diagrams
  is induced by a cellular respectively multi-cyclic filtration of the underlying cellular respectively multi-cyclic structure.
  In particular, the torus action on $\SDspaenN_g(m,1)$ restricts to each stratum of the cluster filtration.
\end{remark}

\begin{remark}
  \label{remark:NRad_to_NSD_cluster_filtration}
  Forgetting the parametrizations,
  the inclusion of the moduli space into its harmonic compactification and
  the stabilization maps respect the cluster filtration.
  More precisely, let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$ and
  consider the following commutative diagram whose vertical maps are $m$-dimensional torus fibrations.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&, row sep=3ex, column sep={18ex,between origins}]
      \RadpaenN_g(m,1)         \ar{rr}                \ar{dd}                               \ar[hook]{rd}{\varphi}  \&\& \SDspaenN_g(m,1)      \ar{dd} \ar[hook]{rd}{\ov\varphi}  \\
      \& \RadpaenN_{g+1}(m,1)  \ar[crossing over]{rr}                                                               \&\& \SDspaenN_{g+1}(m,1)  \ar{dd}                            \\
      \RadunenN_g(m,1)         \ar{rr}                                                      \ar[hook]{rd}{\varphi}  \&\& \SDsunenN_g(m,1)              \ar[hook]{rd}{\ov\varphi}  \\
      \& \RadunenN_{g+1}(m,1)  \ar{rr}                \ar[swap, hookleftarrow, crossing over]{uu}                   \&\& \SDsunenN_{g+1}(m,1)
    \end{tikzcd}
  \end{align}
  All maps in the diagram are compatible with the cluster filtration of radial slit configurations respectively Sullivan diagrams.
\end{remark}

By construction, the cluster filtration behaves well with respect to the torus action.
Clearly this is favorable for geometric arguments.
However, in the study of the associated spectral sequence it leads to algebraic inconveniences.
Therefore, we introduce the ``unreduced'' cluster filtration which is incompatible with the torus action but behaves well with our study of the spectral sequence.

\begin{definition}
  \label{definition:SD_cluster_filtration_unreduced}
  Let $\Sigma = (\lambda, S_1, \ldots, S_v)$ be a combinatorial Sullivan diagram.
  The \defn{unreduced cluster number of $\Sigma$} is its number of ghost surfaces:
  \begin{align}
    c^-(\Sigma) = \# \{ S_1, \ldots, S_v \} = v \,.
  \end{align}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  On the spaces of Sullivan diagrams (with possibly normalized boundaries), the \defn{unreduced cluster filtration} is
  \begin{align}
    Cl^-_p\SDsstar_g(m,1)  &= \{ \Sigma \in \SDsstar_g(m,1) \mid c^-(\Sigma) \le p \}
    \intertext{respectively}
    Cl^-_p\SDsstarN_g(m,1) &= \{ \Sigma \in \SDsstarN_g(m,1) \mid c^-(\Sigma) \le p \} \,.
  \end{align}
\end{definition}

\begin{remark}
  \label{remark:NSD_and_reduced_cluster}
  Forgetting the parametrizations and the stabilization maps respect the unreduced cluster filtration.
  More precisely, given $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$, all maps in the commutative diagrams below are compatible with the unreduced cluster filtration.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      \SDspaenN_g(m,1) \ar{r}{\ov\varphi}  \ar{d} \& \SDspaenN_{g+1}(m,1)  \ar{d} \\
      \SDsunenN_g(m,1) \ar{r}{\ov\varphi}         \& \SDsunenN_{g+1}(m,1)
    \end{tikzcd}
    \quad\quad
    \begin{tikzcd}[ampersand replacement=\&]
      \SDspaen_g(m,1) \ar{r}{\ov\varphi}  \ar{d} \& \SDspaen_{g+1}(m,1)  \ar{d} \\
      \SDsunen_g(m,1) \ar{r}{\ov\varphi}         \& \SDsunen_{g+1}(m,1)
    \end{tikzcd}
  \end{align}
\end{remark}

\subsection{The cluster spectral sequence of \texorpdfstring{$\SDspaenN_g(m,1)$}{NSD\_g(m,1)}}
\label{section:cluster_spectral_sequence_of_NSDpaen}
In this section, we investigate the unreduced cluster spectral sequence of $\SDspaenN_g(m,1)$ and obtain the following result.
\begin{proposition}
  \label{proposition:cluster_spectral_sequence_NSD}
  Let $m \ge 1$, $g \ge 0$ and consider the following diagrams of inclusions of subspaces.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
        Cl^-_1\SDspaenN_g(m,1)  \ar[hook]{r}{Cl^-_1\ov\varphi_g} \ar[hook]{d}{\alpha_g}  \& Cl^-_1\SDspaenN_\infty(m,1) \ar[hook]{d}{\alpha_\infty}   \\
        \SDspaenN_g(m,1)        \ar[hook]{r}{\ov\varphi_g}                               \& \SDspaenN_\infty(m,1)
    \end{tikzcd}
  \end{align}
  The maps $\alpha_g$, $\ov\varphi_g$ and $Cl^-_1\ov\varphi_g$ induce isomorphisms in homology in the range $\ast \le g-2$.
  The map $\alpha_\infty$ is a quasi-isomorphism.
\end{proposition}

\begin{proof}
  Recall that, by Proposition \ref{proposition:homological_stability_NSD}, the stabilization map $\ov\varphi_g$ induces homology isomorphisms in the claimed range.
  Using the definition of a combinatorial Sullivan diagram with exactly one ghost surface, note that
  $Cl^-_1\ov\varphi \colon Cl^-_1\SDspaenN_g(m,1) \xhr{} Cl^-_1\SDspaenN_\infty(m,1)$ induces a map of cellular chain complexes
  that is an isomorphism in the claimed range.
  It remains to show that $\alpha_\infty$ is a quasi isomorphism.
  
  By $E^r_{p,q}$ we denote the spectral sequence associated to the unreduced cluster filtration of $\SDspaenN_\infty(m,1)$.
  Note that $E^0_{1,\ast} = (Cl^-_1\SDpaenN_\infty(m,1))_{\ast+1}$.
  For every $p \neq 1$, the $p$-th row $E^0_{p,\ast}$ is acyclic by Lemma \ref{lemma:acyclicity_of_the_rows_of_the_cluster_spectral_sequce}.
  Consequently $\alpha_\infty$ induces an isomorphism:
  \begin{align}
    H_{\ast+1}(Cl^-_1\SDpaenN_\infty(m,1); \bZ) \cong E^1_{1,\ast} = E^\infty_{1,\ast} \cong H_{\ast+1}( \SDpaenN_\infty(m,1); \bZ) \,.
  \end{align}
\end{proof}

\begin{definition}
  Let $m \ge 1$ and $N_\ast := \SDpaenN_\infty(m,1)$.
  By $E^r_{p,q}$ we denote the \defn{cluster spectral sequence} associated to the unreduced cluster filtration of $\SDpaenN_\infty(m,1)$ with
  zero-th page $E^0_{p,q} = Cl^-_pN_{p+q} / Cl^-_{p-1}N_{p+q}$.
\end{definition}

\begin{lemma}
  \label{lemma:acyclicity_of_the_rows_of_the_cluster_spectral_sequce}
  For every $p > 1$, the chain complex $E^0_{p,\ast}$ is acyclic .
\end{lemma}

\begin{proof}
  We construct a homotopy $h$ from the identity to zero.
  Let $\Sigma = (\lambda, S_1, \ldots, S_p)$ be a Sullivan diagram in $E^0_{p,\ast}$ such that
  $S_1$ is attached to the vertex zero.
  Note that $S_1$ is the unique ghost surface with infinite genus.
  Since $p \ge 2$, there is an attaching point $i(\Sigma)$ of minimal index such that $S_1$ is attached to $i(\Sigma)$ but not to $i(\Sigma) + 1$.
  See the left hand side of Figure \ref{figure:surface_next_to_the_stable_surface} for a local picture.
  \begin{figure}[ht]
    \centering
    \inkpic[.95\textwidth]{surface_next_to_the_stable_surface}
    \caption{
      \label{figure:surface_next_to_the_stable_surface}
      On the left, we show the local picture around the attaching points $i = i(\Sigma)$ and $i+1$.
      Hereby, $i+1$ is the first vertex to which the ghost surface of infinite genus is not attached to.
      On the right, we show the local picture around the attaching points $i$, $i+1$ and $i+2$ of $\Sigma'$.
      By construction, we have $\Sigma = d_i(\Sigma')$.
    }
  \end{figure}
  Then, since $S_1$ has infinite genus, $\Sigma$ is the face of the Sullivan diagram $h'(\Sigma)$ seen
  on the right hand side of Figure \ref{figure:surface_next_to_the_stable_surface}.
  Algebraically, $h'(\Sigma) = (\lambda', S'_1, \ldots S'_p)$ is defined as follows.
  The ribbon structure $\lambda'$ is obtained from $\lambda$ by increasing all symbols $j > i(\Sigma)$ by one and then introducing the new fixed point $\cycle{i(\Sigma)+1}$.
  This induces a canonical injection $\Phi \colon \cycles(\lambda) \xhr{} \cycles(\lambda')$ and we define the ghost surfaces of $\Sigma'$ by
  \begin{align}
    S'_1 &= (0, \infty, \Phi(A_1) \cup \{ \cycle{i(\Sigma) + 1} \})
    \intertext{and for $j > 1$ it is}
    S'_j &= (0, g_i, \Phi(A_j)) \,.
  \end{align}
  In order to define the homotopy $h(\Sigma) = \pm h'(\Sigma)$, we need to introduce the correct signs.
  Using Proposition \ref{proposition:NSD_and_suspensions}, we can assume that the sign of the $j$-th face of a given Sullivan diagram is $(-1)^j$.
  With this sign convention, the homotopy is defined as
  \begin{align}
    h(\Sigma) = (-1)^{i(\Sigma)} h'(\Sigma) \,.
  \end{align}
  Now, for a Sullivan diagram $\Sigma$ of degree $k$ with $i = i(\Sigma)$ we compute:
  \begin{align}
    dh(\Sigma)
      &= \sum_{0 \le j \le k+1} (-1)^{j + i} d_jh'\Sigma \\
      &= \sum_{j < i} (-1)^{j + i} d_jh'\Sigma + \Sigma + \sum_{j > i} (-1)^{j + i} d_jh'\Sigma \\
      &= \Sigma + \sum_{j < i} (-1)^{j + i} d_jh'\Sigma + \sum_{j > i} (-1)^{j + i} d_jh'\Sigma
    \intertext{
      By definition of $h'\Sigma$ we have $d_jh'\Sigma = h'd_j\Sigma$ if $j < i$.
      By definition of the cluster spectral sequence, we delete all faces that merge two distinct surfaces.
      Therefore $d_{i+1}h'\Sigma = 0$, $h'd_i\Sigma = 0$ and $d_jh'\Sigma = h'd_{j-1}\Sigma$ for $j > i+1$.
    }
      &= \Sigma + \sum_{j < i} (-1)^{j + i} h'd_j\Sigma + \sum_{j > i} (-1)^{j + i} h'd_{j-1}\Sigma \\
      &= \Sigma + \sum_{j < i} (-1)^{j + i} h'd_j\Sigma + \sum_{j \ge i} (-1)^{j + 1 + i} h'd_j\Sigma
    \intertext{By definition of $hd_j\Sigma$, the sign of $h'd_j\Sigma$ is $(-1)^{i-1}$ if $j < i$ and $(-1)^i$ for $j \ge i$.}
      &= \Sigma + \sum_{j < i} (-1)^{j + 1} hd_j\Sigma + \sum_{j \ge i} (-1)^{j + 1} hd_j\Sigma \\
      &= \Sigma - hd(\Sigma)
  \end{align}
  This finishes the proof.
\end{proof}


\subsection{The homotopy type of \texorpdfstring{$\SDspaenN_\infty(m,1)$ and $\SDsunenN_\infty(m,1)$}{NSD\_infty(m,1)}}
\label{section:homotopy_type_of_NSDpaen}
In this section, we detect the (stable) homotopy types of $\SDspaenN_g(m,1)$ and $\SDsunenN_g(m,1)$.

\begin{theorem}
  \label{theorem:stable_homotopy_type_of_harm_normalized}
  Let $m\ge 1$, $g \ge 0$ and consider the following maps of $m$-dimensional torus fibrations.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
        \SDspaenN_g(m,1) \ar[hook]{r}{\ov\varphi} \ar{d}{\pi^\paen_\unen} \& \SDspaenN_\infty(m,1) \ar{r}{\Pol} \ar{d}{\pi^\paen_\unen} \& \PolspcpaenN(m,1) \simeq EU(1)^m \ar{d}{\pi^\paen_\unen} \\
        \SDsunenN_g(m,1) \ar[hook]{r}{\ov\varphi}                         \& \SDsunenN_\infty(m,1) \ar{r}{\Pol}                         \& \PolspcunenN(m,1) \simeq BU(1)^m
    \end{tikzcd}
  \end{align}
  The stabilization maps $\ov\varphi$ are $(g-2)$-connected and the maps $\Pol$ are homotopy equivalences.
\end{theorem}

\begin{proof}
  To begin with, consider the following restiction of upper row of the diagram above:
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
        Cl^-_1\SDspaenN_g(m,1) \ar[hook]{r}{Cl^-_1\ov\varphi_g} \ar[hook]{d}{\alpha_g} \& Cl^-_1\SDspaenN_\infty(m,1) \ar{r}{Cl^-_1\Pol_\infty} \ar[hook]{d}{\alpha_\infty} \& \PolspcpaenN(m,1) \simeq EU(1)^m \ar[equal]{d} \\
              \SDspaenN_g(m,1) \ar[hook]{r}{\ov\varphi_g}                              \&       \SDspaenN_\infty(m,1) \ar{r}{\Pol_\infty}                                   \& \PolspcpaenN(m,1) \simeq EU(1)^m
    \end{tikzcd}
  \end{align}
  Recall that, by Proposition \ref{proposition:cluster_spectral_sequence_NSD},
  the maps $\alpha_g$, $\ov\varphi_g$ and $Cl^-_1\ov\varphi_g$ induce isomorphisms in homology in the range $\ast \le g-2$ and $\alpha_\infty$ is a quasi-isomorphism.
  For $g \ge 2$, the inclusions $\alpha_g$ and $\alpha_\infty$ induces a surjection on fundamental groups by
  Lemma \ref{lemma:cl1_inclusion_induces_surjection_on_fundamental_groups_NSD}.
  But, $Cl^-_1\SDspaenN_g(m,1)$ and $Cl^-_1\SDspaenN_\infty(m,1)$ are simply connected by Lemma \ref{lemma:cl1_to_pol_is_highly_connected}.
  It follows that $\alpha_g$ is $(g-2)$-connected and that $\alpha_\infty$ is a homotopy equivalence by the relative Hurewicz theorem.
  
  By commutativity of the above diagram and by Theorem \ref{theorem:classifying_map_from_NSD} we have:
  \begin{align}
    Cl^-_1\Pol_\infty \circ Cl^-_1\ov\varphi_g
      = (\Pol_\infty \circ \ov\varphi_g) \circ \alpha_g
      = \Pol_g \circ \alpha_g \,.
  \end{align}
  Now, by Lemma \ref{lemma:cl1_to_pol_is_highly_connected}, $\Pol_g \circ \alpha_g$ is $(g-1)$-connected and $Cl^-_1\Pol_\infty = \Pol_\infty \circ \alpha_\infty$ is a homotopy equivalence.
  In the preceding paragraph, we have seen that $\alpha_\infty$ is a homotopy equivalence and we conclude that $\Pol_\infty$ is also a homotopy equivalence.
  Till this point, we have seen that $\alpha_g$ and $Cl^-_1\Pol_\infty \circ Cl^-_1\ov\varphi_g$ are $(g-2)$-connected and that $\Pol_\infty$ is a homotopy equivalence.
  It follows that $\ov\varphi_g$ is also $(g-2)$-connected.
  
  The unparametrized case follows from the study of the long exact sequences of the $m$-dimensional torus fibrations $\pi^\paen_\unen$.
\end{proof}

\begin{lemma}
  \label{lemma:cl1_inclusion_induces_surjection_on_fundamental_groups_NSD}
  Let $1 \le g \le \infty$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  The inclusion
  \begin{align}
    Cl^-_1\SDsstarN_g(m,1) \xhr{} \SDsstarN_g(m,1)
  \end{align}
  induces a surjective homomorphism on fundamental groups.
\end{lemma}

\begin{proof}
  Note that $Cl^-_1\SDsstarN_g(m,1) \subset \SDsstarN_g(m,1)$ is a cellular subcomplex.
  The one-dimensional cells not in $Cl^-_1\SDsstarN_g(m,1)$ are given by Sullivan diagrams $\Sigma$ with exactly two ghost surfaces,
  see Figure \ref{figure:one_dimensional_cell_not_in_cl1_NSD}.
  \begin{figure}[ht]
    \centering
    \inkpic[.35\textwidth]{one_dimensional_cell_not_in_cl1_NSD}
    \caption{
      \label{figure:one_dimensional_cell_not_in_cl1_NSD}
      We show a 1-cell $\Sigma = (\lambda, S_1, S_2)$ in the space of Sullivan diagram with normalized boundaries while
      suppressing the leaves and the genus of the two ghost surfaces.
      The cycles of $\rho$ are all transpositions $\rho_r = \cycle{L_r\ i_r}$ with the exception of a single three-cycle $\rho_v = \cycle{L_v\ i\ j}$ or $\rho_v = \cycle{L_v\ j\ i}$.
    }
  \end{figure}
  We denote the ghost surface attached to the vertex $0$ by $S_1$ and the other ghost surfaces is $S_2$.
  Assume that $S_1$ has positive genus.
  Then, the faces of the 2-cell seen on the left hand side of Figure \ref{figure:two_cell_with_two_faces_in_cl1_NSD}
  are $\Sigma$ and two 1-cells in $Cl^-_1\SDsstarN_g(m,1)$.
  \begin{figure}[ht]
    \centering
    \inkpic[.7\textwidth]{two_cell_with_two_faces_in_cl1_NSD}
    \caption{
      \label{figure:two_cell_with_two_faces_in_cl1_NSD}
      We show two 2-cells in the space of Sullivan diagram with normalized boundaries while
      suppressing the leaves and the genus of the two ghost surfaces.
      On the left hand side, we show a Sullivan diagram whose right ghost surface has two boundary components and whose left ghost surface has one boundary component.
      On the right hand side, we show such a Sullivan diagram whose right ghost surface has one boundary components and whose left ghost surface has two boundary component.
    }
  \end{figure}
  Otherwise, if $S_1$ has genus zero, $S_2$ has to have positive genus (since $g \ge 1$).
  As before, the faces of the 2-cell seen on the right hand side of Figure \ref{figure:two_cell_with_two_faces_in_cl1_NSD}
  are $\Sigma$ and two 1-cells in $Cl^-_1\SDsstarN_g(m,1)$.
  This is enough to proof the Lemma.
\end{proof}

\begin{lemma}
  \label{lemma:cl1_to_pol_is_highly_connected}
  Let $1 \le g \le \infty$ and $m \ge 1$.
  The composition
  \begin{align}
    Cl^-_1\Pol_g \colon Cl^-_1\SDspaenN_g(m,1) \xhr{\alpha_g} \SDspaenN_g(m,1) \xr{\Pol_g} \PolspcpaenN(m,1) \simeq EU(1)^m
  \end{align}
  is $(g-1)$-connected.
  In particular, $Cl^-_1\SDspaenN_g(m,1)$ is $(g-1)$-connected.
\end{lemma}

\begin{proof}
  Recall that $\Pol \colon \SDspaenN_g(m,1) \to \PolspcpaenN(m,1)$
  is defined by sending $\Sigma$ to its outgoing boundary cycles $\rho$.
  
  Observe that $Cl^-_1\SDspaenN_g(m,1)$ is the realization of a semi-multi-simplicial subset of the multi-simplicial set underlying $\SDspaenN_g(m,1)$:
  The multi-simplices in $Cl^-_1\SDspaenN_g(m,1)$ are Sullivan diagrams with one surface and this condition is clearly closed under taking faces.
  However, every degeneracy map increase the number of ghost surfaces by one.
  Observe further that, $\Pol \colon Cl^-_1\SDspaenN_g(m,1) \to \PolspcpaenN(m,1)$ factors through the fat realization $\FatPolspcpaenN_g(m,1)$ of $\PolspcpaenN(m,1)$:
  \begin{align}
    Cl^-_1\Pol_g \colon Cl^-_1\SDspaenN_g(m,1) \to \FatPolspcpaenN_g(m,1) \simeq \PolspcpaenN_g(m,1) \,.
  \end{align}
  Given a combinatorial Sullivan diagrams with non-degenerate boundaries and a single surface, i.e., $\Sigma = (\lambda, S_1)$,
  note that $S_1 = (0, g_1, A_1)$ is uniquely determined by $\rho$ and the genus of the Sullivan diagram.
  Therefore, $Cl^-_1\Pol \colon Cl^-_1\SDspaenN_g(m,1) \to \FatPolspcpaenN_g(m,1)$ is the realization of the injective semi-multi-simplicial map
  \begin{align}
    \Phi \colon \Sigma = (\lambda, S_1) \mapsto \rho \,.
  \end{align}
  It is enough to show that $\Phi$ is bijective in total degrees $0 \le \ast \le g$.
  Given a permutation $\rho$ in $\FatPolspcpaenN_g(m,1)$ of total degree $k \le g$ there is a unique Sullivan diagram $\Sigma = (\lambda, S_1)$
  of some genus $g'$, with $m$ normalized boundaries, with outgoing boundary cycles $\rho$ and with ghost surface $S_1 = (0,0,\cycles(\lambda))$.
  The genus $g'$ of $\Sigma$ is determined by taking $k$ (multi-)faces of $\Sigma$ leading to a Sullivan diagram $\Sigma' = (\lambda', S'_1)$ of total degree zero.
  Then $g'$ is the genus of $S'_1$.
  In this process of taking $k$ faces of $\Sigma$, the genus of the unique ghost surface can only increase by one under a face map, i.e., $g' \le k$.
  Therefore, the Sullivan diagram $(\lambda, (0, k-g', \cycles(\lambda))$ in $Cl^-1\SDspaenN_g(m,1)$ is mapped to $\rho$ in $\FatPolspcpaenN_g(m,1)$.
\end{proof}
