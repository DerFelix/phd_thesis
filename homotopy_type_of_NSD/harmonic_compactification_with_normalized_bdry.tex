\section{The space of Sullivan diagrams with normalized boundaries}
\label{section:harmonic_compacitification_normalized}
Using the ideas of Chapter \ref{chapter:polygon_spaces}, we introduce the spaces $\SDsstarN_g(m,1)$ which are called
the space of Sullivan diagrams of genus $g$ with $m$ (un)parametrized enumerated normalized outgoing boundaries,
see Definition \ref{definition:SD_normalized_boundaries}.
The spaces $\SDsstarN_g(m,1)$ and $\PolspcstarN(m,1)$ share important structural properties:
The spaces $\SDspaenN_g(m,1)$ admit an $m$-fold multi-cyclic structure,
see Proposition \ref{proposition:multicyclic_structure_on_NSD}.
There is a canonical map $\Pol_{\SD} \colon \SDspaenN_g(m,1) \to \PolspcpaenN(m,1)$ of $m$-fold multi-cyclic spaces, that extends the restriction to the outgoing boundaries
$\Pol_{\Rad} \colon \RadpaenN_g(m,1) \to \PolspcpaenN(m,1)$, see Theorem \ref{theorem:classifying_map_from_NSD}.
The map $\Pol_{\SD}$ is investigated further in Section \ref{section:homotopy_type_of_harmonic_compacitification_normalized}.
Moreover, there is a desuspension theorem for spaces of Sullivan diagrams, see Theorem \ref{theorem:desuspension_theorem_SD}.
Furthermore, using the methods of \cite[Section 4]{BoesEgas2017}, we show that the spaces of Sullivan diagrams with normalized boundaries admit homological stability,
see Proposition \ref{proposition:homological_stability_NSD}.


\subsection{Spaces of Sullivan diagrams with non-degenerate outgoing boundaries}
\label{section:SD_normalized_boundaries}
Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
Given a Sullivan diagram $(\Sigma; t) \in \SDsstar_g(m,1)$, each of its admissible edges has a length (determined by $t$) and is part of one of the $m$ outgoing boundaries.
We define the length of a fixed outgoing boundary as the sum of the lengths of the admissible edges belonging to this boundary cycle,
see Definition \ref{definition:length_of_a_SDpaen} and Definition \ref{definition:length_of_a_SDunen}.
The length of the outgoing boundaries defines a continuous map to the $(m-1)$-dimensional simplex,
see Definition \ref{definition:length_function_SD_enumerated}.
An outgoing boundary cycle is said to be degenerate if its length is zero.
This leads to a stratification of $\SDsstar_g(m,1)$ by the number of non-degenerate outgoing boundaries, see Definition \ref{definition:SD_stratification_by_bdry}.
The space of Sullivan diagrams with non-degenerate outgoing boundaries $B\SDsstar_g(m,1)$ is the top stratum.
It is open and dense.
We define $\SDsstarN_g(m,1) \subset B\SDsstar_g(m,1)$ the space of Sullivan diagrams with normalized boundaries as the closed subspace where all outgoing boundaries have equal lengths,
see Definition \ref{definition:SD_normalized_boundaries}.
Similarly to the spaces of polygons, the normalization of the outgoing boundaries of $B\SDsstar_g(m,1)$ defines a deformation retraction onto $\SDsstarN_g(m,1)$.
By Proposition \ref{proposition:non_degenerate_SD_as_NSD_times_delta}, the retraction and the length of the outgoing boundaries induce a homeomorphism
$B\SDsstar_g(m,1) \cong \SDsstarN_g(m,1) \times \interior{\Delta^{m-1}}$.
This homeomorphism will lead to an $m$-fold multi-cyclic structure on $\SDspaenN_g(m,1)$, see Proposition \ref{proposition:multicyclic_structure_on_NSD}.
Moreover, it is used to deduce the desuspension theorem for the spaces of Sullivan diagrams, see Theorem \ref{theorem:desuspension_theorem_SD}.

\begin{definition}
  \label{definition:length_of_a_SDpaen}
  Let $g \ge 0$, $m \ge 1$ and $(\Sigma; t) \in \SDspaen_g(m,1)$ with $t \in \interior{\Delta^k}$.
  Denote the labels of $\Sigma$ by $L = \{ L_1, \ldots, L_m \}$.
  Its outgoing boundary cycles are $\rho$ and we denote its set of cycles by $\cycles( \rho ) = \{ \sigma_1, \ldots, \sigma_m \}$ such that $L_i \in \sigma_i$.
  The \defn{length of the $i$-th outgoing boundary} is $l_i = l_i(\Sigma; t) = \sum_{j \in \sigma_i} t_j$ and
  the \defn{length of $(\Sigma; t)$} is the tuple $l(\Sigma; t) = (l_1, \ldots, l_m)$.
\end{definition}

\begin{remark}
  Observe that $l_i > 0$ if and only if $\rho_i \neq \cycle{L_i}$ and that $\sum_{1 \le i \le m} l_i = 1$.
\end{remark}

\begin{definition}
  \label{definition:length_of_a_SDunen}
  Let $g \ge 0$, $m \ge 1$ and $(\Sigma; t) \in \SDsunen_g(m,1)$ with $t \in \interior{\Delta^k}$.
  Denote the enumeration data of $\Sigma$ by $\beta_1$ and $\beta_2$.
  Its non-degenerate boundary is $\rho$.
  The \defn{length of the $i$-th outgoing boundary} is $l_i = l_i(\Sigma; t) = \sum_{j \in \beta_1^{-1}(i)} t_j$ and
  the \defn{length of $(\Sigma; t)$} is the tuple $l(\Sigma; t) = (l_1, \ldots, l_m)$.
\end{definition}

\begin{remark}
  Observe that $l_i > 0$ if and only if $i \in \imag(\beta_1)$ and that $\sum_{1 \le i \le m} l_i = 1$.
\end{remark}

\begin{definition}
  \label{definition:length_function_SD_enumerated}
  On the space of Sullivan diagrams of genus $g$ with $m$ enumerated (un)parametrized outgoing boundaries, the \defn{length of the outgoing boundaries} is the continuous map
  \begin{align}
    l \colon& \SDspaen_g(m,1) \to \Delta^{m-1}, \quad (\Sigma; t) \mapsto l(\Sigma; t)
    \intertext{respectively}
    l \colon& \SDsunen_g(m,1) \to \Delta^{m-1}, \quad (\Sigma; t) \mapsto l(\Sigma; t) \,.
  \end{align}
\end{definition}

\begin{remark}
  The length of the outgoing boundaries $l$ commutes with the forgetful map $\pi^\paen_\unen \colon \SDspaen_g(m,1) \to \SDsunen_g(m,1)$.
\end{remark}

\begin{remark}
  In Definition \ref{definition:degenerate_SD}, we introduced the notion of degenerate (outgoing) boundary cycles of a Sullivan diagram.
  Given a point $(\Sigma; t) \in \SDspaen_g(m,1)$ or $\SDsunen_g(m,1)$ with $t \in \interior{\Delta^k}$,
  observe that the $i$-th boundary cycle of $\Sigma$ is degenerate if and only if $l_i(\Sigma; t) = 0$.
  This leads to the following filtration.
\end{remark}

\begin{definition}
  \label{definition:SD_stratification_by_bdry}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  The space of Sullivan diagrams of genus $g$ with $m$ (un)parametrized and enumerated outgoing boundaries is filtered by
  the closed subspaces of those Sullivan diagrams with at least $m-k$ degenerate outgoing boundaries
  \begin{align}
    D_k\SDsstar_g(m,1) &:= l^{-1}( \skel_{k}\Delta^{m-1}) \quad\text{where } 0 \le k \le m \,.
  \end{align}
  The \defn{space of Sullivan diagrams of genus $g$ with $m$ (un)parametrized, enumerated, partially degenerate outgoing boundaries} is the closed subspace
  \begin{align}
    D\SDsstar_g(m,1) &:= l^{-1}( \del \Delta^{m-1}) = D_{m-1}\SDsstar_g(m,1) \,.
  \end{align}
  The \defn{space of Sullivan diagrams of genus $g$ with $m$ (un)parametrized enumerated, non-degenerate outgoing boundaries} is the open and dense subspace
  \begin{align}
    B\SDsstar_g(m,1) &:= l^{-1}( \interior{\Delta^{m-1}}) = \SDsstar_g(m,1) - D\SDsstar_g(m,1) \,.
  \end{align}
\end{definition}

\begin{remark}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  For $k = 0$, the space $D_k\SDsstar_g(m,1)$ is empty,
  for $k = 1$, it consists of exactly $m$ discrete points and
  for $k > 1$, it is connected.
\end{remark}

\begin{definition}
  \label{definition:SD_normalized_boundaries}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  The \defn{space of Sullivan diagrams of genus $g$ with $m$ (un)parametrized enumerated normalized boundaries} is
  the closed subspace of all Sullivan diagrams whose outgoing boundaries have equal length:
  \begin{align}
    \SDspaenN_g(m,1) &:= l^{-1}( {\textstyle \frac{1}{m}, \ldots, \frac{1}{m}} ) \subset \SDspaen_g(m,1) \\
    \SDsunenN_g(m,1) &:= l^{-1}( {\textstyle \frac{1}{m}, \ldots, \frac{1}{m}} ) \subset \SDsunen_g(m,1) \,.
  \end{align}
\end{definition}

On the space of Sullivan diagrams with non-degenerate outgoing boundaries,
we introduce the normalization of the lengths of the outgoing boundaries analogously to the normalization of polygons,
see Definition \ref{definition:Pol_normalization_of_a_simplex},
Definition \ref{definition:Pol_normalization} and
Proposition \ref{proposition:Pol_as_NPol_times_delta}.
To this end, we need a slight change in our notation for combinatorial Sullivan diagrams with non-degenerate boundary cycles.

\begin{remark}
  \label{remark:regarding_the_non_degenerate_boundaries_as_permutations_in_SymGrstarn}
  Given a point $(\Sigma; t) \in B\SDspaen_g(m,1)$, denote the outgoing boundary cycles of $\Sigma$ by $\rho$.
  It is clear that $\rho$ has exactly $m$ cycles, $\rho$ does not have fixed points and each cycle permutes exactly one leaf in $L = \{ L_1, \ldots, L_m \}$ non-trivially.
  Therefore, each cycle
  $\rho_i = \cycle{L_i\ \rho_{i,0}\ \rho_{i,1}\ \ldots } \in \SymGr[n\sqcup L]$
  describes a parametrized cyclic permutation
  $\tilde\rho_i = \cycle{ \rho_{i,0}\ \rho_{i,1} \ \ldots }$.
  Using an enumeration of the leaves, $\rho$ describes a parametrized enumerated permutation $\tilde\rho \in \SymGrpaenn$
  with cycle decomposition $\tilde\rho = \tilde\rho_1 \cdots \tilde\rho_m$.
  Given two Sullivan diagrams $(\Sigma; t)$, $(\Sigma'; t') \in B\SDspaen_g(m,1)$ with outgoing boundary cycles $\rho \neq \alpha$
  it is clear that these give different parametrized enumerated permutations $\tilde \rho \neq \tilde \alpha$.
  
  Given a point $(\Sigma; t) \in \SDsunenN_g(m,1)$ and denoting the outgoing boundary cycles of $\Sigma$ by $\rho$.
  It is clear that the enumeration data $\beta_1 \colon \cycles(\rho) \xhr{} \{ 1, 2, \ldots, m \}$ is a bijection.
  Therefore, $\rho$ describes an unparametrized enumerated permutation $\tilde \rho \in \SymGrunenn$.
  As in the parametrized case, two outgoing boundary cycles $\rho \neq \alpha$ give different unparametrized enumerated permutations $\tilde \rho \neq \tilde \alpha$.
\end{remark}

\begin{notation}
    Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
    Given a Sullivan diagram $(\Sigma;t) \in B\SDsstar_g(m,1)$, we identify its outgoing boundary cycles $\rho$ with
    its (un)parametrized enumerated permutation $\tilde \rho \in \SymGrstarn$,
    see Remark \ref{remark:regarding_the_non_degenerate_boundaries_as_permutations_in_SymGrstarn}.
\end{notation}

\begin{definition}
  \label{definition:SD_normalization_of_a_simplex}
  Let $g \ge 0$, $m \ge 1$, $\star \in \{ \paen, \unen \}$ and $(\Sigma; t) \in B\SDsstar_g(m,1)$ with $t \in \interior{\Delta^n}$.
  By $\rho = \rho_1 \cdots \rho_m$ we denote its outgoing boundary cycles.
  Each symbol $0 \le i \le n$ belongs to exactly one cycle of $\rho$, say $\rho_{j(i)}$, and
  we denote the length of the $j(i)$-th outgoing boundary by
  $l(i) := l_{j(i)}(\Sigma; t) = \sum_{k \in \rho_{j(i)}} t_k$.
  The \defn{normalization of $(\Sigma; t)$} is
  \begin{align}
    r(\Sigma; t) := (\Sigma; t'), \quad t'_i = \frac{t_i}{m \cdot l(i)} \text{ for } 0 \le i \le n\,.
  \end{align}
\end{definition}

\begin{definition}
  \label{definition:SD_normalization}
  Let $g \ge 0$, $m \ge 1$, $\star \in \{ \paen, \unen \}$.
  The \defn{normalization} is the continuous retraction
  \begin{align}
    r \colon B\SDsstar(m,1) \to \SDsstarN(m,1)
  \end{align}
  induced by the normalization of simplices, see Definition \ref{definition:SD_normalization_of_a_simplex}.
\end{definition}

\begin{proposition}
  \label{proposition:non_degenerate_SD_as_NSD_times_delta}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen\}$.
  The normalization and the length of the outgoing boundaries induce a homeomorphism
  \begin{align}
    r \times l \colon B\SDsstar_g(m,1) \cong \SDsstarN_g(m,1) \times \interior{\Delta^{m-1}} \,.
  \end{align}
\end{proposition}

\begin{proof}
  Analogously to the proof of Proposition \ref{proposition:Pol_as_NPol_times_delta},
  the inverse of $r \times l$ is
  \begin{align}
    ((\Sigma; t'), l') \mapsto (\Sigma; t), \quad t_i = t'_i \cdot m \cdot l'_{j(i)}
  \end{align}
  where $j(i)$ denotes the number of the cycle to which $i$ belongs, i.e., $i \in \rho_{j(i)}$.
\end{proof}

\begin{remark}
  The $m$-dimensional torus $U(1)^m$ acts on $B\SDspaen_g(m,1)$ by rotating the leaves in their corresponding boundary cycles and
  is is clear that the orbit space is $B\SDsunen_g(m,1)$, see Section \ref{section:multicyclic_structure_on_NSD}.
  Observe that the normalization map $B\SDspaen_g(m,1) \to \SDspaenN_g(m,1)$ is equivariant with respect to this action and
  that the length of the outgoing boundaries is invariant under the action.
  Consequently, the homeomorphism $r \times l$ is an equivariant map (where $U(1)^m$ acts trivially on $\interior{\Delta^{m-1}}$).
\end{remark}


\subsection{Multi-cyclic structures and torus bundles}
\label{section:multicyclic_structure_on_NSD}
Recall that the $m$-dimensional torus acts on $\PolspcpaenN(m,1)$ by rotating the $m$ parametrization points.
Moreover, the $m$ parametrization points gave canonical barycentric coordinates in $m$ enumerated simplices and
this gave $\PolspcpaenN(m,1)$ the structure of an $m$-fold multi-cyclic space, see Section \ref{section:poly_spaces_cellular_structures}.
Regarding the leaves in a Sullivan diagram as parametrization point in its outgoing boundary,
the methods used in Section \ref{section:poly_spaces_cellular_structures} are used here to equip $\SDspaenN_g(m,1)$ with the structure of an $m$-fold multi-cyclic space,
see Proposition \ref{proposition:multicyclic_structure_on_NSD} and Discussion \ref{discussion:multicyclic_structure_on_NSD_geometrically}.
The $m$-dimensional torus action if free by Proposition \ref{proposition:multicyclic_torus_action_on_NSD_is_free} and
we have a canonical cell decomposition of the orbit space is $\SDsunenN_g(m,1)$, see Proposition \ref{proposition:cellular_decomposition_of_NSD}.
The restriction of a radial slit configuration to its outgoing boundaries factors through the spaces of Sullivan diagram with normalized boundaries,
see Theorem \ref{theorem:classifying_map_from_NSD}.

\begin{proposition}
  \label{proposition:multicyclic_structure_on_NSD}
  Let $g \ge 0$ and $m\ge 1$.
  The space $\SDspaenN(m,1)$ is homeomorphic to the realization of an $m$-fold multi-cyclic set whose
  multi-simplices of total degree $k$ are the combinatorial Sullivan diagrams in
  \begin{align}
      \SDpaen(m,1) / D\SDpaen(m,1)
  \end{align}
  of degree $k+m-1$.
  Such a Sullivan diagram $\Sigma$ has multi-degree $\underline{k} = (k_1, \ldots, k_m)$ where
  $k_i$ is the norm of the $i$-th cycle of the outgoing boundary cycles $\rho \in \SymGrpaenn[k+m-1]$.

  The face maps $d_{j,i}$ collapse the $i$-th admissible edge from the $j$-th outgoing boundary cycle, i.e.,
  \begin{align}
    d_{j,i}(\Sigma) = d_{\rho_{j,i}}(\Sigma) \,,
  \end{align}
  the degeneracy maps $s_{j,i}$ are
  \begin{align}
    s_{j,i}(\Sigma) = s_{\rho_{j,i}}(\Sigma)
  \end{align}
  and the cyclic operator in the $j$-th component is induced by cyclically permuting the leaf $L_j$ in its cycle.
\end{proposition}

\begin{proof}
  The proof is similar to the proof of Proposition \ref{proposition:multicyclic_structure_on_NPol}.
  Let us denote the interior of an $k$-simplex by $\Delta^k$ for the sake of readability.
  The image of an open $k$-dimensional cell $\Sigma \times \Delta^k$ under the normalization map is the open multi-simplex
  \begin{align}
    \label{proof:proposition:multicyclic_structure_on_NSD:canonical_cell_decompisition}
    \Delta(\Sigma) := r(\Sigma \times \Delta^k) \cong \Sigma \times \Delta^{N(\rho_1)} \times \ldots \times \Delta^{N(\rho_m)}
  \end{align}
  of dimension $k-m+1$ where we regard $\rho = \cycle{ \rho_{1,0}\ \ldots} \cdots \cycle{\rho_{m,0}\ \ldots}$ as permutation in $\SymGrpaenn[k]$.
  Using the distinguished symbol of each cycle, we have canonical barycentric coordinates $(t_{j,0}, \ldots, t_{j,N(\rho_j)})_{j=1,\ldots,m}$.
  From the construction it is clear that the $i$-th face in the $j$-coordinate of the multi-simplex $\Delta(\Sigma)$ is the multi-simplex $\Delta(D_{\rho_{j,i}}(\Sigma))$.
  With this at hand, it is easy to see that $\SDspaenN(m,1)$ is the realization of the multi-simplicial space described in the Proposition.
  
  Let $\Sigma = (\lambda, S_1, \ldots, S_v) \in \SDpaen(m,1) / D\SDpaen(m,1)$ be a combinatorial Sullivan diagram and
  let $1 \le j \le m$.
  We define $t^j\Sigma := \tilde \Sigma := (\tilde \lambda, \tilde S_1, \ldots \tilde S_v)$ algebraically.
  To this end, denote the outgoing boundary cycles of $\Sigma$ by $\rho \in \SymGr([k] \cup L)$.
  The cycle decomposition of $\rho$ is denoted by $\rho = \cycle{ L_1\ \rho_{1,0}\ \ldots\ \rho_{1,k_1} } \cdots \cycle{L_m\ \rho_{m,0}\ \ldots\ \rho_{m,k_m} }$.
  
  The outgoing boundary cycles of $\tilde \Sigma$ are defined as $\tilde\rho = \tilde\rho_1 \cdots \tilde\rho_m$ with $\tilde\rho_i = \rho_i$ for $i \neq j$ and
  $\tilde \rho_j = \cycle{\rho_{j,0}\ L_j\ \rho_{j,1}\ \ldots\ \rho_{j,k_j} }$.
  The ribbon structure of $\tilde\Sigma$ is defined as
  \begin{align}
    \tilde\lambda = \cycle{0\ \ldots\ k} \tilde \rho^{-1} \,.
  \end{align}
  
  We define the ghost surfaces of $\tilde\Sigma$ as follows.
  Since
  \begin{align}
    \lambda^{-1}       &= \cycle{L_1\ \rho_{1,0}\ \ldots\ } \cdots \cycle{L_j\ \rho_{j,0}\ \rho_{j,1}\ \ldots\ \rho_{j,k_j}} \cdots \cycle{L_m\ \rho_{m,0}\ \ldots\ } \cdot \cycle{k\ \ldots\ 0}
    \intertext{and}
    \tilde\lambda^{-1} &= \cycle{L_1\ \rho_{1,0}\ \ldots\ } \cdots \cycle{\rho_{j,0}\ L_j\ \rho_{j,1}\ \ldots\ \rho_{j,k_j}} \cdots \cycle{L_m\ \rho_{m,0}\ \ldots\ } \cdot \cycle{k\ \ldots\ 0} 
  \end{align}
  differ only by the position of $L_j$ in its cycle, deleting all leaves from $\lambda$ and $\tilde\lambda$ gives the same permutation $\lambda'$.
  Observe that the permutation $\lambda'$ has the same number of cycles as $\lambda$ and $\tilde\lambda$:
  This is because $\rho(L_i) \neq L_i$ for any leaf $L_i$, since $\Sigma \notin D\SDpaen_g(m,1)$, and this implies $\lambda(L_i) \neq L_i$ for any leaf $L_i$.
  Therefore, deleting the leaves from $\lambda$ and $\tilde \lambda$ induces a bijection
  \begin{align}
    \Phi \colon \cycles(\lambda) \cong \cycles(\lambda') \cong \cycles(\tilde\lambda) \,.
  \end{align}
  Denoting the ghost surfaces of $\Sigma = (\lambda, S_1, \ldots, S_v)$ by $S_i = (g_i, m_i, A_i)$, the ghost surfaces of $\tilde\Sigma = (\tilde \lambda, \tilde S_1, \ldots, \tilde S_v)$ are defined as
  \begin{align}
    \tilde S_i = (g_i, m_i, \{ \Phi(\alpha) \mid \alpha \in A_i \}) \,.
  \end{align}
  
  A straight forward computation shows that these data equip $\SDspaenN_g(m,1)$ with the structure of an $m$-fold multi-cyclic space.
\end{proof}

\begin{discussion}
  \label{discussion:multicyclic_structure_on_NSD_geometrically}
  Given a Sullivan diagram with parametrized enumerated normalized boundaries $\Sigma \in \SDspaenN_g(m,1)$,
  each leaf $L_i$ belongs to exactly one boundary cycle say $\rho_i$ and each
  outgoing boundary cycle has length $\frac{1}{m}$, see Figure \ref{figure:SD_torus_action_pre} for an example.
  Moving the $m$ leaves clockwise in their respective boundary defines a free action by the $m$-dimensional torus on $\SDspaenN_g(m,1)$.
  See Figure \ref{figure:SD_torus_action_post} for an example:
  The ribbon structure of $\Sigma$ is
  $\lambda = \cycle{0\ L_2}\cycle{1\ 4}\cycle{2\ 6\ 5}\cycle{3\ L_1}$
  and the outgoing boundary cycles are
  $\rho = \cycle{L_1\ 3\ 1\ 5\ 2}\cycle{L_2\ 0\ 4\ 6}$.
  The ghost surfaces are
  $S_1 = (0,0,\{\cycle{0\ L_2}, \cycle{1\ 4}\})$,
  $S_2 = (0,1,\{\cycle{2\ 6 \ 5})$ and
  $S_3 = (0,0,\{\cycle{3\ L_1}\})$.
  Given an angle $\alpha = e^{\frac{3}{4}\pi i } \in U(1)$, we move the leaf $L_1$ on its boundary cycle proportional to $\alpha$.
  We denote the resulting Sullivan diagram by $\Sigma_1$.
  It is shown on the left of Figure \ref{figure:SD_torus_action_post}.
  
  Regarding the $i$-th leaf as a parametrization point in its boundary cycle
  leads to an $m$-fold multi-cyclic structure on $\SDspaenN_g(m,1)$,
  see Proposition \ref{proposition:multicyclic_structure_on_NSD}.
  The torus action described above agrees with the geometric realization of the multi-cyclic operation.
  Let us discuss the action of $\alpha = e^{\frac{3}{4}\pi i} \in U(1)$ on the first leaf of the Sullivan diagram
  $\Sigma$ seen in Figure \ref{figure:SD_torus_action_pre} in some detail.
  Using the multi-cyclic structure given in Proposition \ref{proposition:multicyclic_structure_on_NSD} and
  the formula in Proposition \ref{proposition:multicyclic_circle_action_in_terms_of_representatives},
  we obtain the Sullivan diagram $\alpha.\Sigma$ by in two steps:
  At first we split the admissible edge $e_5$ of $\Sigma$ into two pieces
  (by using a degeneracy map $s_5$ which ties a disc to the splitting point).
  The outgoing boundary cycles of this Sullivan diagram $s_5(\Sigma)$ are
  $\cycle{L_1\ 3\ 1\ 5\ 6\ 2}\cycle{L_2\ 0\ 4\ 7}$.
  Rotating the leaf $L_1$ in its boundary cycle, the outgoing boundary cycles of $\alpha.\Sigma$ are
  $\cycle{L_1\ 6\ 2\ 3\ 1\ 5\ }\cycle{L_2\ 0\ 4\ 7}$.
  This Sullivan diagram is denoted $\Sigma_2$ and it is shown on the right hand side of Figure \ref{figure:SD_torus_action_post}.
  
  Observe that the Sullivan diagrams $\Sigma_1$ and $\Sigma_2$ represent the same point in $\SDspaenN_g(m,1)$ because $\Sigma_2 = s_2(\Sigma_1)$.
  \begin{figure}[!p]
    \centering
    \inkpic[.45\textwidth]{SD_torus_action_pre}
    \caption{
      \label{figure:SD_torus_action_pre}
      The ribbon structure of this Sullivan diagram $\Sigma$ is
      $\lambda = \cycle{0\ L_2}\cycle{1\ 4}\cycle{2\ 6\ 5}\cycle{3\ L_1}$
      and the outgoing boundary cycles are
      $\rho = \cycle{L_1\ 3\ 1\ 5\ 2}\cycle{L_2\ 0\ 4\ 6}$.
      The ghost surfaces are
      $S_1 = (0,0,\{\cycle{0\ L_2}, \cycle{1\ 4}\})$,
      $S_2 = (0,1,\{\cycle{2\ 6 \ 5} \})$ and
      $S_3 = (0,0,\{\cycle{3\ L_1}\})$.
      We highlight the boundary cycle $\cycle{L_1\ 3\ 1\ 5\ 2}$.
    }
    \vspace{6ex}
    \inkpic[\textwidth]{SD_torus_action_post}
    \caption{
      \label{figure:SD_torus_action_post}
      Given the Sullivan diagram $\Sigma = (\lambda, S_1, S_2, S_3)$ seen in Figure \ref{figure:SD_torus_action_pre} and
      the angle $\alpha = e^{\frac{3}{4}\pi i} \in U(1)$,
      we move the leaf $L_1$ (sitting on a disc between the admissible edges $e_2$ and $e_3$) in its boundary cycle
      (consisting of the admissible edges $e_3$, $e_1$, $e_5$ and $e_2$ in this order)
      by $\frac{3}{4}$ of the total length of the boundary cycle.
      The resulting Sullivan diagram $\Sigma_1 = \alpha.\Sigma$ is seen on the left.
      \\
      Using the multi-cyclic structure and Proposition \ref{proposition:multicyclic_circle_action_in_terms_of_representatives} instead,
      we obtain the Sullivan diagram $\Sigma_2 = \alpha.\Sigma$ seen on the right.
      In $\SDspaenN_g(m,1)$ they represent the same point, because $\Sigma_2 = s_2(\Sigma_1)$.
    }
  \end{figure}
\end{discussion}

\begin{proposition}
  \label{proposition:multicyclic_torus_action_on_NSD_is_free}
  Let $g \ge 0$ and $m \ge 1$.
  The induced action of the $m$-fold torus on the space $\SDspaenN_g(m,1)$ is free.
\end{proposition}

\begin{proof}
  The proof is similar to the proof of Proposition \ref{proposition:multicyclic_torus_action_on_poly_is_free}.
  Using Proposition \ref{proposition:multicyclic_circle_action_in_terms_of_representatives},
  the action of the $i$-th factor of the $m$-dimensional torus on $\SDspaenN_g(m,1)$ can be carried out explicitly:
  Let $\alpha \in U(1)$ be an arbitrary angle and let $\Sigma \in \SDspaenN_g(m,1)$.
  Then, $\alpha.\Sigma$ is obtained from $\Sigma$ by moving the $i$-th leaf in its outgoing boundary cycle proportional to $\alpha$.
  This is enough to proof the proposition.
\end{proof}

\begin{proposition}
  \label{proposition:cellular_decomposition_of_NSD}
  Let $g \ge 0$ and $m\ge 1$.
  The space of Sullivan diagrams $\SDsunenN_g(m,1)$ admits a cellular decomposition whose $k$-dimensional cells are
  in one-to-one correspondence with the cellular generators of $\SDunen_g(m,1) / D\SDunen_g(m,1)$ of degree $k+m-1$.
  Forgetting the parametrizations defines a cellular quotient map $\SDspaenN_g(m,1) \to \SDsunenN_g(m,1)$.
\end{proposition}

\begin{proof}
  The proof is similar to the proof of Proposition \ref{proposition:cellular_decomposition_of_normalized_polygons}.
  The image of an open $k$-dimensional cell $\Sigma \times \Delta^k$ of $\SDsunen_g(m,1)$ under the normalization map is the open cell
  \begin{align}
    \Delta(\Sigma) := r(\Sigma \times \Delta^k) \cong \Sigma \times \Delta^{N(\rho_1)} \times \ldots \times \Delta^{N(\rho_m)}
  \end{align}
  of dimension $k-m+1$.
  Since $\rho$ is an unparametrized enumerated permutation, the enumeration of the $m$ simplices is determined by $\rho$ but
  the barycentric coordinates in each simplex are only determined up to cyclic permutation.
  After choosing parametrizations for all open cells, the attaching maps of these cells are uniquely determined.
  
  Forgetting the leaves defines the quotient map to the orbit space of the torus action and
  it is clear that this map sends (multi-simplicially non-degenerate) cells of $\SDspaenN_g(m,1)$ to (possibly multi-simplicially degenerate) cells of $\SDsunenN_g(m,1)$.
\end{proof}

Given a radial slit configuration, the restriction to the outgoing boundaries defines a map to the spaces of polygons,
see Definition \ref{definition:restriction_to_the_outgoing_boundaries} and
Theorem \ref{theorem:classifying_map_from_rad}.
This map extends to the spaces of Sullivan diagrams with normalized boundaries.

\begin{theorem}
  \label{theorem:classifying_map_from_NSD}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  Sending a Sullivan diagram $\Sigma \in \SDsstarN_g(m,1)$ to its outgoing boundary cycles $\rho \in \SymGrpaenn[k]$ defines an $m$-fold multi-simplicial map
  \begin{align}
    \Pol_{\SD} \colon \SDsstarN_g(m,1) \to \PolspcstarN(m,1) \,.
  \end{align}
  It is an extension of the restriction to the outgoing boundaries $\Pol_{\Rad} \colon \RadstarN_g(m,1) \to \PolspcstarN(m,1)$, i.e.,
  the following diagram of $m$-dimensional torus fibrations is commutatitive.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      \Pol_{\Rad} \colon \RadpaenN_g(m,1) \ar{r} \ar{d}{\pi^\paen_\unen} \& \SDspaenN_g(m,1) \ar{r}{\Pol_{\SD}} \ar{d}{\pi^\paen_\unen} \& \PolspcpaenN(m,1) \simeq EU(1)^m \ar{d}{\pi^\paen_\unen} \\
      \Pol_{\Rad} \colon \RadunenN_g(m,1) \ar{r}                         \& \SDsunenN_g(m,1) \ar{r}{\Pol_{\SD}}                         \& \PolspcunenN(m,1) \simeq BU(1)^m
    \end{tikzcd}
  \end{align}
  Moreover, denoting the stabilization map of Sullivan diagrams by $\ov\varphi$, it holds $\Pol_{\SD} \circ \ov\varphi = \Pol_{\SD}$.
\end{theorem}

\begin{proof}
  It is clear that sending $\Sigma = (\lambda, S_1, \ldots)$ of total degree $k$ to $\rho \in \SymGrstarn[k+m-1]$ defines a map of $m$-fold multi-simplicial spaces,
  see Proposition \ref{proposition:multicyclic_structure_on_NSD} and Proposition \ref{proposition:multicyclic_structure_on_NPol}.
  Moreover, $\Pol_{\SD} \circ \ov\varphi = \Pol_{\SD}$ follows from the definition of the stabilization map, see Definition \ref{definition:stabilization_map_SD}.
  
  In order to show that the diagram is commutatitive,
  recall that the restriction to the outgoing boundaries sends a radial slit configuration $(\sigma_0, \ldots, \sigma_q, \sigma^\star)$ to $\sigma^\star$.
  Moreover, by Section \ref{section:inclusion_of_fM_into_SD_combinatorial},
  the radial projection of $(\sigma_0, \ldots, \sigma_q, \sigma^\star)$ is a Sullivan diagram with outgoing boundary cycles $\rho = \sigma^\star$.
\end{proof}

In the next Sections, we investigate the map $\Pol_{\SD} \colon \SDsstarN_g(m,1) \to \PolspcstarN(m,1)$ further and show that it is $(g-2)$-connected,
see Theorem \ref{theorem:stable_homotopy_type_of_harm_normalized}.
This has interesting implications to the study of string topology,
see Section \ref{section:string_topology}.


\subsection{The desuspension theorem for spaces of Sullivan diagrams}
\label{section:desuspension_theorem_SD}
Recall that, for all $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$, the homeomorphism
\begin{align}
  \Phi \colon \SDsstar(m,1) - D\SDsstar(m,1) \cong \SDsstarN(m,1) \times \interior{\Delta^{m-1}}
\end{align}
lead to the cellular decomposition of $\SDsstarN(m,1)$.
The $k$-dimensional cells of $\SDsstarN(m,1)$ are in canonical one-to-one correspondence to the $(k+m-1)$-dimensional cellular generators of $\SDstar_g(m,1) / D\SDpaen_g(m,1)$.
Extending the homeomorphism $\Phi$ to $\SDsstar(m,1) / D\SDsstar(m,1)$ yields our desuspension theorem for the spaces of Sullivan diagrams.
\begin{theorem}
  \label{theorem:desuspension_theorem_SD}
  There are isomorphisms
  \begin{align}
    H_\ast( \SDspaenN(m,1); \bZ) &\cong \Sigma^{-m+1} H_\ast( \SDspaen(m,1), D\SDspaen(m,1); \bZ) \\
    H_\ast( \SDsunenN(m,1); \bF_2) &\cong \Sigma^{-m+1} H_\ast( \SDsunen(m,1), D\SDsunen(m,1); \bF_2)
  \end{align}
  Moreover, these isomorphisms commute with the stabilization maps $\ov\varphi$ and, with coefficients in $\bF_2$, they commute with the forgetful map $\pi^\paen_\unen$.
\end{theorem}

\begin{proof}
  Given a combinatorial Sullivan diagram with non-degenerate boundaries,
  it is it has the same faces in $\SDpaenN_g(m,1)$ and $\Sigma^{-m+1} (\SDpaen_g(m,1) / D\SDpaen_g(m,1))$ up to signs.
  This proofs this desuspension theorem over $\bF_2$.
  The integral desuspension theorem follows from introducing the correct signs,
  see Proposition \ref{proposition:NSD_and_suspensions}.
\end{proof}

\begin{proposition}
  \label{proposition:NSD_and_suspensions}
  Let $g \ge 0$ and $m \ge 1$.
  The signs in \eqref{definition:block_and_set_of_symbols_left_of_i:sign_of_a_multisimplex} define an isomorphism of cellular chain complexes.
  \begin{align}
    \Phi \colon \SDpaenN_g(m,1) &
      \to \Sigma^{-m+1} (\SDpaen_g(m,1) / D\SDpaen_g(m,1)) \\
    (\lambda, S_1, \ldots, S_k) &
      \mapsto \sign(\rho) \cdot (\lambda, S_1, \ldots, S_k) \,.
  \end{align}
\end{proposition}

\begin{proof}
  The proof of this proposition is analogous to the proof of Proposition \ref{proposition:NPol_and_suspensions}.
\end{proof}


\subsection{Homological stability for the spaces \texorpdfstring{$\SDsstarN_g(m,1)$}{NSD\_g(m,1)}}
\label{section:homological_stability_NSD}
Using a variation of the discrete Morse flows in \cite{BoesEgas2017}, we obtain homological stability for the spaces of Sullivan diagrams with normalized boundaries.
\begin{proposition}
  \label{proposition:homological_stability_NSD}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  The restriction of the stabilization map to the spaces of Sullivan diagrams with normalized boundaries
  \begin{align}
    \ov\varphi \colon \SDsstarN_g(m,1) \xhr{} \SDsstarN_{g+1}(m,1)
  \end{align}
  induces isomorphisms in homology in degrees $0 \le \ast \le g-2$.
\end{proposition}

\begin{proof}
  The proof is analogous to the proof of the stability theorem of Sullivan diagrams, see Theorem \ref{theorem:boes_egas_stability}.
  Using the same recipe as in \cite[Section 4.4, Definition 4.27]{BoesEgas2017}
  we obtain a discrete Morse flow on $\SDstarN_{g+1}(m,1) / \SDstarN_g(m,1)$ such that all cells in degrees $0 \le \ast \le g-2$ are either collapsible or redundant.
  More precisely, using the notations introduced in \cite[Section 4]{BoesEgas2017},
  a cell $\Sigma$ is \defn{collapsible} if has a surface $S$ ending with an odd fence and every other surface $S'$ of $\Sigma$ with $ft(S) \neq 0$ and $end(S') > end(S) > 0$ has genus zero.
  Among the surfaces of $\Sigma$ satisfying this condition, we pick $S$ with $end(S)$ maximal.
  Then, the \defn{redundant partner} of $\Sigma$ is $d_{end(S)-1}(\Sigma)$.
  The arguments given in \cite[Section 6.2]{BoesEgas2017}, show that this defines a discrete Morse flow on $\SDstarN_g(m,1)$ that is compatible with the stabilization map.
  From the construction it follows that $\SDstarN_{g+1}(m,1) / \SDstarN_g(m,1)$ does not have essential cells in degrees $0 \le \ast \le g-1$, i.e.,
  \begin{align}
    H_\ast( \SDstarN_{g+1}(m,1) / \SDstarN_g(m,1) ) = 0
      \quad\text{ for } 0 \le \ast \le g-1 \,.
  \end{align}
  Now, the claim follows the long exact sequence of $\ov\varphi \colon \SDstarN_g(m,1) \xhr{} \SDstarN_{g+1}(m,1)$.
\end{proof}

\begin{remark}
  We study the homomotopy type of these spaces of Sullivan diagrams in the subsequent sections.
  We will show that the stabilization maps are $(g-2)$-connected,
  see Theorem \ref{theorem:stable_homotopy_type_of_harm_normalized}.
\end{remark}

Note that the stabilization maps induces an inclusion of subspaces or subcomplexes.
We define the spaces of Sullivan diagrams with stable genus as follows.

\begin{definition}
  \label{definition:SD_stable_genus}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  The \defn{spaces of Sullivan diagrams of stable genus with $m$ (un)parametrized enumerated (normalized) boundaries} are
  \begin{align}
    \SDsstar_\infty(m,1)  &= \cup_{g \ge 0} \SDsstar_g(m,1)
    \intertext{respectively}
    \SDsstarN_\infty(m,1) &= \cup_{g \ge 0} \SDsstarN_g(m,1)
  \end{align}
  Its cellular chain complex is denoted by
  \begin{align}
    \SDstar_\infty(m,1)   &= \cup_{g \ge 0} \SDstar_g(m,1)
    \intertext{respectively}
    \SDstarN_\infty(m,1)  &= \cup_{g \ge 0} \SDstarN_g(m,1)
  \end{align}
  Abusing notation, the canonical inclusion into the (cellular complex of the) space of Sullivan diagrams with stable genus is denoted by $\ov\varphi$.
\end{definition}
