\chapter*{Summary}
By $\fM^\paen_g(m,n)$ we denote the moduli space of conformal structures on an oriented compact cobordism $S_g(m,n)$ of genus $g \ge 0$ and with $m+n \ge 0$ enumerated, parametrized boundary components
of which $n$ are ``incoming'' and $m$ are ``outgoing''.
The study of these spaces reflects a strong relationship between geometry, topology and mathematical physics.
In this thesis, we study
(1) the homotopy type of Bödigheimer's harmonic compactification of these moduli spaces (and of variations of these) and
(2) the unstable homology of these moduli spaces (and of variations of these).
Some of our results are from a joint-project with Daniela Egas-Santander, see \cite{BoesEgas2017}.

\subsection*{On the homotopy type of the harmonic compactifications}
The free loop space of a simply connected, closed manifold $M$ is denoted by $LM$.
Its homology is an algebra over the homology of the operad of framed discs $H_\ast(\cD_2)$.
As an algebra over this operad, it is isomorphic to the Hochschild cohomology of the cochains $C^\bullet(M)$, see \cite{CohenJones2002}.
There are numerous extensions of the action of $H_\ast(\cD_2)$ on the loop homology and
on the Hochschild cohomology of an $A_\infty$-algebra to an action of the homology of the moduli spaces $\fM^\paen_g(m,n)$ and their harmonic compactifications $\ov\fM^\paen_g(m,n)$,
see e.g.
\cite{
  CohenGodin2004,
  TradlerZeinalian2006,
  Costello2007Graphs,
  Costello2007Frobenius,
  KaufmannModSpcActions1,KaufmannModSpcActions2,
  KontsevichSoibelman2009,
  Kaufmann2010,PoirierRounds2011,
  Egas2014,
  WahlWesterland2016}.
Our main result is the following:
\begin{theoremwithfixednumber}{\ref{theorem:stable_homotopy_type_of_harm_paen}}[B.]
  The harmonic compactification $\ov\fM^\paen_g(m,1)$ is $(g+m-2)$-connected for every $g \ge 2$ and $m \ge 1$ or $g \ge 0$ and $m \ge 3$.
\end{theoremwithfixednumber}
As usual, the $n+m$ enumerated parametrized boundary curves of the cobordism $S_g(m,n)$ are partitioned into $n$ ``incoming boundaries'' and $m$ ``outgoing boundaries''.
Note that the $m$-dimensional torus acts on $\fM^\paen_g(m,n)$ by reparametrization of the $m$ outgoing boundary components.
The orbit space is denoted by $\fM^\unen_g(m,n)$.
\begin{theoremwithfixednumber}{\ref{theorem:boes_egas_stability}}[B., Egas]
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  The stabilization maps of the moduli spaces $\varphi_g \colon \fM^\star_g(m,1) \to \fM^\star_{g+1}(m,1)$ extend to the harmonic compactification,
  i.e., there is a map $\varphi_g \colon \ov{\fM}^\star_g(m,1) \to \ov{\fM}^\star_{g+1}(m,1)$
  making the following diagram commutative.
  \begin{align*}
    \begin{tikzcd}[ampersand replacement=\&]
      \fM^\star_g(m,1)      \ar{r}{\varphi_g} \ar{d} \& \fM^\star_{g+1}(m,1) \ar{d} \\
      \ov{\fM}^\star_g(m,1) \ar{r}{\ov\varphi_g}     \& \ov{\fM}^\star_{g+1}(m,1)
    \end{tikzcd}
  \end{align*}
  The stabilization map $\ov\varphi_g$ induces an isomorphism in homology in degrees $\ast \le g + m - 2$.
  Moreover, if $\star = \paen$ or if $m > 2$, the stabilization map $\ov\varphi_g$ is $(g+m-2)$-connected.
\end{theoremwithfixednumber}
The orbit projection $\fM^\paen_g(m,n) \to \fM^\unen_g(m,n)$ is an $m$-dimensional torus bundle.
Unfortunately, the extension of this map to the harmonic compactification is not even a fibration.
In this thesis, we study the following retract of the moduli spaces $\fM^\star_g(m,n)$.
By $N\fM^\star_g(m,n)$ we denote the subspace of $\fM^\star_g(m,n)$ where all outgoing boundary curves have a fixed circumference, say $1$;
Its harmonic compactification $\ov{N\fM}^\star_g(m,n)$ is a closed subspace of $\ov{\fM}^\star_g(m,n)$.
\begin{theoremwithfixednumber}{\ref{theorem:stable_homotopy_type_of_harm_normalized}}[B.]
  Let $m \ge 1$, $g \ge 0$.
  Forgetting the parametrization of the outgoing boundaries defines $m$-dimensional torus fibrations.
  \begin{align*}
    \begin{tikzcd}[ampersand replacement=\&]
        N\fM^\paen_g(m,1) \ar[hook]{r} \ar{d} \& \ov{N\fM}^\paen_g(m,1) \ar{d} \\
        N\fM^\unen_g(m,1) \ar[hook]{r}        \& \ov{N\fM}^\unen_g(m,1)
    \end{tikzcd}
  \end{align*}
  The forgetful maps are compatible with the stabilization maps $\varphi_g$ and $\ov\varphi_g$.
  The stabilization maps $\ov\varphi_g$ and
  the classifying map $\ov{N\fM}^\unen_g(m,1) \to BU(1)^m \simeq (\CP^\infty)^m$ are $(g-1)$-connected.
  In particular, $\ov{N\fM}^\paen_g(m,1)$ is $(g-1)$-connected.
\end{theoremwithfixednumber}
Moreover, the homology of the harmonic compactifications $\ov\fM^\star_g(m,n)$ and $\ov{N\fM}^\star_g(m,n)$ is related by our desuspension Theorem \ref{theorem:desuspension_theorem_SD}.
Finally, we construct infinite families of (unstable) homology classes of infinite order in the harmonic compactification $\ov{\fM}^\paen_g(m,1)$.
All of these classes correspond to non-trivial higher string operations.

\subsection*{On the unstable homology of the moduli spaces}
The homology of the moduli spaces of surfaces is independent of the genus in a range of homological degrees and, in this range, it is known for coefficients in the rationals or in finite fields.
Below this range and with coefficients in the integers, the homology of the moduli spaces is almost unknown besides computations for small genera or in low homological degrees.
In this thesis, we detect three non-trivial generators, called $f$, $s$ and $w_3$.
They live in the homology of the moduli spaces $\fM^m_{g,1}$
of surfaces of genus $g$, with $1$ parametrized boundary curve and with $m$ permutable punctures.
\begin{theoremwithfixednumber}{\ref{theorem:unstable_homology_for_small_g_and_m}}[B., Bödigheimer, Ehrenfried, Mehner]
  The integral homology of the moduli space $\fM_{2,1}$ and $\fM^2_{1,1}$ is as follows.
  \begin{align*}
    H_\ast( \fM_{2,1}; \bZ) =
    \begin{cases}
      \bZ\cycle{c^2} & \ast = 0\\
      \bZ_{10}\cycle{cd} & \ast = 1\\
      \bZ_2\cycle{d^2} & \ast = 2\\
      \bZ\cycle{\lambda s} \oplus \bZ_2\cycle{T(e)} & \ast = 3\\
      \bZ_2 \cycle{?} \oplus \bZ_3\cycle{w_3} & \ast = 4 \\
      0 & \ast \ge 5
    \end{cases}
    \quad
    H_\ast( \fM_{1,1}^2; \bZ) =
    \begin{cases}
      \bZ\cycle{a^2c} & \ast = 0\\
      \bZ\cycle{a^2 d} \oplus \bZ_2\cycle{bc} & \ast = 1\\
      \bZ_2\cycle{a^2 e} \oplus \bZ_2\cycle{bd} & \ast = 2\\
      \bZ_2\cycle{f} & \ast = 3 \\
      0 & \ast \ge 4
    \end{cases}
  \end{align*}
  Here, the known generators $a$,$b$,$c$,$d$,$e$,$t$ and their products have been described in \cite[Kapitel 1]{Mehner201112}, see also \cite{BoedigheimerBoes2018}.
  The only generator remaining unkown is denoted by $?$.
\end{theoremwithfixednumber}
The non-trivial generator $f \in H_3(\fM^2_{1,1}; \bZ)$ is an embedded $3$-dimensional torus.
The non-trivial generator $s \in H_3( \fM^1_{2,1}; \bQ)$ is the image of the (rational) fundamental class $[U]$ of the fibration $U \xhr{} \fM_{2,1} \to \fM_{2,0}$,
where $U$ is the unit tangent bundle of the closed oriented surface of genus two.
In \cite{SongTillmann2007,SegalTillmann2008}, Song--Tillmann and Segal--Tillman construct geometric maps from the braid groups to mapping class groups.
More precisely, they send the canonical generators of the $2k$-th braid group $Br_{2k}$ to
a $2k$ Dehn twists along interlocking simply-closed curves in an oriented surface of genus $k-1$.
The non-trivial generator $w_3 \in H_4( \fM^1_{2,1}; \bQ)$ is the image of the generator in $H_4(Br_6; \bZ) \cong \bZ_3$ under their map.

\subsection*{Acknowledgements}
First and foremost I thank Carl-Friedrich Bödigheimer for his constant encouragement, for many valuable and illuminating conversations and for discussing various drafts of my thesis with me.
I am grateful to Daniela Egas Santander for introducing me to various aspects of string topology and for plenty detailed and extensive discussions on different models of moduli spaces and related topics.
I thank Nathalie Wahl for her interest in my project and for fruitful discussions on string operations.
I am thankful to Andrea Bianchi and Martin Palmer for helpful discussions on various details of my thesis.
My thanks go to Oscar Randal-Williams for useful conversations on methods to prove homological stability.
I thank Alexander Kupers for conversations on homological stability and for answering my questions on the history of string topology.
Furthermore, I thank Benson Farb for discussing methods to find unstable homology classes in the moduli spaces and the harmonic compactifications.
Finally, I am thankful to David Sprehn for discussing methods to detect certain unstable homology classes.
