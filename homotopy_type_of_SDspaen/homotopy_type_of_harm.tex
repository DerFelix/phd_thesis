\noindent
In this chapter, we show that the spaces of Sullivan diagrams with parametrized enumerated boundaries $\SDspaen_g(m,1)$
are highly connected with respect to the genus and the number of outgoing boundary curves.
Using the homotopy equivalence of \cite{EgasKupers2014}, see also Proposition \ref{section:inclusion_of_fM_into_SD_combinatorial},
we regard the spaces $\SDspaen_g(m,1)$ as the harmonic compactification of the moduli spaces, i.e.,
$\ov{\fM}^\paen_g(m,1) \simeq \SDspaen_g(m,1)$.
Consequently, the harmonic compactifications $\ov{\fM}^\paen_g(m,1)$ are highly connected with respect to the genus and the number of outgoing boundary curves.

\begin{theorem}
  \label{theorem:stable_homotopy_type_of_harm_paen}
  Let $g \ge 2$ and $m \ge 1$ or $g \ge 0$ and $m \ge 3$.
  The spaces of Sullivan diagrams of genus $g$ with $m$ parametrized enumerated boundaries is $(g+m-2)$-connected.
  In particular, $\SDspaen_\infty(m,1)$ and $\ov{\fM}^\paen_\infty(m,1)$ are contractible.
\end{theorem}

\begin{proof}
  The space $\SDspaen_\infty(m,1)$ is simply connected by Proposition \ref{proposition:SDpaen_is_1_connected}.
  By Proposition \ref{proposition:cluster_spectral_sequence}, the inclusion
  \begin{align}
    Cl^-_1\SDspaen_\infty(m,1) \xhr{} \SDspaen_\infty(m,1)
  \end{align}
  is a quasi isomorphism.
  But $Cl^-_1\SDspaen_\infty(m,1)$ is acyclic by Lemma \ref{lemma:cl1_by_degeneration_filtration}.
  We conclude that $\SDspaen_\infty(m,1)$ is contractible.
  Finally, the inclusion $\SDspaen_g(m,1) \to \SDspaen_\infty(m,1) \simeq \ast$ is $(g+m-2)$-connected by
  Theorem \ref{theorem:boes_egas_stability}, Proposition \ref{proposition:SDpaen_is_1_connected} and the relative Hurewicz theorem.
\end{proof}

The remainder of this chapter is devoted to the Lemmas and Propositions used in the proof of Theorem \ref{theorem:stable_homotopy_type_of_harm_paen}.

\begin{lemma}
  \label{lemma:cl1_inclusion_induces_surjection_on_fundamental_groups}
  Let $1 \le g \le \infty$, $m \ge 1$ and $\star \in \{ \paen, \unen \}$.
  The inclusion
  \begin{align}
    Cl^-_1\SDsstar_g(m,1) \xhr{} \SDsstar_g(m,1)
  \end{align}
  induces a surjective homomorphism on fundamental groups.
\end{lemma}

\begin{proof}
  The proof is similar to the proof of Lemma \ref{lemma:cl1_inclusion_induces_surjection_on_fundamental_groups_NSD}.
  The 1-cells of $\SDsstar_g(m,1)$ that are not in $Cl^-_1\SDsstar_g(m,1)$ are the Sullivan diagrams with two ghost surfaces,
  see the left hand side of Figure \ref{figure:two_cell_with_two_faces_in_cl1}.
  Given such a Sullivan diagram $\Sigma = (\lambda, S_1, S_2)$, one of the two ghost surfaces has to have positive genus since $g \ge 1$.
  Therefore, $\Sigma$ is a face of a Sullivan diagram $\Sigma'$ seen in the middle or on the right hand side of Figure \ref{figure:two_cell_with_two_faces_in_cl1}.
  The other two faces of $\Sigma'$ are 1-cells in $Cl^-_1\SDsstar_g(m,1)$.
  This is enough to proof the Lemma.
  \begin{figure}[ht]
    \centering
    \inkpic[.9\textwidth]{two_cell_with_two_faces_in_cl1}
    \caption{
      \label{figure:two_cell_with_two_faces_in_cl1}
      We show three cells with two ghost surfaces while suppressing the leaves and the genus of the ghost surfaces.
      On the left, we show a 1-cell $\Sigma$. One of the ghost surfaces has to have positive genus since $g \ge 1$.
      On the middle and on the right, we show cofaces $\Sigma'$ of $\Sigma$.
    }
  \end{figure}
\end{proof}

\begin{proposition}
  \label{proposition:SDpaen_is_1_connected}
  Let $2 \le g \le \infty$ and $m \ge 1$ or $0 \le g \le \infty$ and $m \ge 3$.
  Then, the space of Sullivan diagrams $\SDspaen_g(m,1)$ is simply connected.
\end{proposition}

\begin{proof}
  For $m = 1$, we have $\SDspaen_g(m,1) = \SDspaenN_g(m,1)$ and the claim follows from
  Lemma \ref{lemma:cl1_inclusion_induces_surjection_on_fundamental_groups_NSD} and
  Lemma \ref{lemma:cl1_to_pol_is_highly_connected}.
  For $m > 2$, the claim follows from Theorem \ref{theorem:boes_egas_connectivity}.
  
  It remains to study the case $m = 2$.
  Using Lemma \ref{lemma:cl1_inclusion_induces_surjection_on_fundamental_groups},
  it enough to show that $Cl^-_1\SDpaen_g(m,1)$ has trivial fundamental group.
  To this end, note that a Sullivan diagram with a single ghost surface is completely determined by its outgoing boundary cycles $\rho$.
  This leads to the following partition of the 1-cells of $Cl^-_1\SDpaen_g(m,1)$ into three groups, see Figure \ref{figure:one_cells_of_cl1}.
  A 1-cell $\Sigma \in Cl^-_1\SDpaen_g(m,1)$ is of type $\alpha_1$, $\alpha_2$ respectively $\beta$ if its outgoing boundary cycles $\rho$ are
  $\rho(\alpha_1) = \cycle{L_i\ 0\ 1} \cycle{L_j}$,
  $\rho(\alpha_2) = \cycle{L_i\ 1\ 0} \cycle{L_j}$ respectively
  $\rho(\beta) = \cycle{L_i\ 0} \cycle{L_j\ 1}$.
  \begin{figure}[ht]
    \centering
    \inkpic[.9\textwidth]{one_cells_of_cl1}
    \caption{
      \label{figure:one_cells_of_cl1}
      We show the three types of 1-cells in $Cl^-_1\SDpaen_g(m,1)$ while suppressing the genus of the ghost surface.
      We denote the types by $\alpha_1$, $\alpha_2$ and $\beta$ and the corresponding outgoing boundary cycles are
      $\rho(\alpha_1) = \cycle{L_i\ 0\ 1} \cycle{L_j}$,
      $\rho(\alpha_2) = \cycle{L_i\ 1\ 0} \cycle{L_j}$ respectively
      $\rho(\beta) = \cycle{L_i\ 0} \cycle{L_j\ 1}$.
    }
  \end{figure}
  
  Every cell of type $\alpha_1$ is a loop in $Cl^-_1\SDpaen_g(m,1)$ because
  $D_0(\rho(\alpha_1)) = D_1(\rho(\alpha_1)) = \cycle{L_i\ 0}\cycle{L_j}$.
  This loop is filled by the 2-cell seen on the left of Figure \ref{figure:two_cells_of_cl1}:
  Its outgoing boundary cycles are $\tilde\rho = \cycle{L_i\ 0\ 1\ 2}\cycle{L_j}$ and we have
  $D_0(\tilde\rho) = D_1(\tilde\rho) = D_2(\tilde\rho) = \cycle{L_i\ 0\ 1}\cycle{L_j} = \rho(\alpha_1)$.
  \begin{figure}[ht]
    \centering
    \inkpic[.9\textwidth]{two_cells_of_cl1}
    \caption{
      \label{figure:two_cells_of_cl1}
      We show three 2-cells in $Cl^-_1\SDpaen_g(m,1)$ while suppressing the genus of the ghost surface.
      On the left, the outgoing boundary cycles are $\cycle{L_i\ 0\ 1\ 2}\cycle{L_j}$.
      In the middle, the outgoing boundary cycles are $\cycle{L_i\ 2\ 1\ 0}\cycle{L_j}$.
      On the right, the outgoing boundary cycles are $\cycle{L_1\ 0\ 2}\cycle{L_2\ 1}$.
    }
  \end{figure}
  
  Similarly, $\alpha_2$ is a loop in $Cl^-_1\SDpaen_g(m,1)$ that is filled by the 2-cell seen in the middle of Figure \ref{figure:two_cells_of_cl1}.
  
  Observe that there are exactly two 1-cells of type $\beta$:
  The outgoing boundary cycles of $\beta_1$ are $\rho(\beta_1) = \cycle{L_1\ 0}\cycle{L_2\ 1}$ and
  the outgoing boundary cycles of $\beta_2$ are $\rho(\beta_2) = \cycle{L_1\ 1}\cycle{L_2\ 0}$.
  Observe further that both $\beta_1$ and $\beta_2$ are edges joining the two 0-cells.
  Therefore, the fundamental group is generated by the loop $\beta_2 \star \beta_1$.
  But the 2-cell on the right of Figure \ref{figure:two_cells_of_cl1} show that $\beta_2 \star \beta_1$ is homotopic to a loop of type $\alpha_1$, which is null-homotopic.
\end{proof}

\begin{proposition}
  \label{proposition:cluster_spectral_sequence}
  Let $m \ge 1$, $g \ge 0$ and consider the following diagrams of inclusions of subspaces.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
        Cl^-_1\SDspaen_g(m,1)  \ar[hook]{r}{Cl^-_1\ov\varphi_g} \ar[hook]{d}{\alpha_g} \& Cl^-_1\SDspaen_\infty(m,1) \ar[hook]{d}{\alpha_\infty} \\
        \SDspaen_g(m,1)        \ar[hook]{r}{\ov\varphi_g}                              \& \SDspaen_\infty(m,1)
    \end{tikzcd}
  \end{align}
  The maps $\alpha_g$, $\ov\varphi_g$ and $Cl_1\ov\varphi_g$ induce isomorphisms in homology in the range $\ast \le m+g-2$.
  The map $\alpha_\infty$ is a quasi-isomorphisms.
\end{proposition}

\begin{proof}
  The proof is similar to the proof of Proposition \ref{proposition:cluster_spectral_sequence_NSD}.
  Recall that, by Theorem \ref{theorem:boes_egas_stability}, the stabilization map $\ov\varphi$ induces homology isomorphisms in the claimed range.
  From the definition of a combinatorial Sullivan diagram with exactly one ghost surface and the discrete Morse flow constructed in \cite[Section 4.4]{BoesEgas2017},
  it is clear that $Cl^-_1\ov\varphi \colon Cl^-_1\SDspaen_g(m,1) \xhr{} Cl^-_1\SDspaen_\infty(m,1)$ induces a map of Morse complexes
  that is an isomorphism in the claimed range.
  It remains to show that $\alpha_\infty$ is a quasi isomorphism.
  This will follow from studying of the cluster spectral sequence $E^r_{p,q}$ of $\SDspaen_\infty(m,1)$.
  Using the same recipe as in the proof of Lemma \ref{lemma:acyclicity_of_the_rows_of_the_cluster_spectral_sequce},
  the rows of $E^0_{p,q}$ are acyclic for $p > 0$.
  We leave this to the reader.
\end{proof}

\begin{definition}
  Let $0 \le g \le \infty$ and $m \ge 1$.
  The space $Cl^-_1\SDspaen_g(m,1)$ is filtered by the Sullivan diagrams with at least $m-k$ degenerate outgoing boundaries, i.e.,
  for $0 \le k \le m$ it is
  \begin{align}
    D_kCl^-_1\SDspaen_\infty(m,1)
      &= Cl^-_1\SDspaen_\infty(m,1) \cap D_k\SDspaen_\infty(m,1) \\
      &= \{ \Sigma \in Cl^-_1\SDspaen_\infty(m,1) \mid l(\Sigma) \in \skel_{k}\Delta^{m-1} \} \,.
  \end{align}
\end{definition}

\begin{remark}
  Let $g \ge 0$ and $m \ge 1$.
  For $k = 0$, the space $D_kCl^-_1\SDspaen_g(m,1)$ is empty,
  for $k = 1$, it consists of exactly $m$ discrete points and
  for $k > 1$, it is connected.
\end{remark}

\begin{remark}
  A Sullivan diagram $\Sigma \in D_kCl^-_1\SDspaen_\infty(m,1)$ is always of the form $\Sigma = (\lambda, (0, \infty, \cycles{\lambda}))$.
  Therefore, it is uniquely determined by its ribbon structure $\lambda$ or, equivalently, its outgoing boundary cycles $\rho$.
\end{remark}

\begin{lemma}
  \label{lemma:cl1_by_degeneration_filtration}
  Let $m \ge 1$ and let $E^r_{p,q}$ be the spectral sequence associated to the filtration $D_pCl^-_1\SDspaen_\infty(m,1)$.
  The spectral sequence collapses on the second page and is concentrated in $E^2_{1,-1} \cong \bZ$.
  In particular $Cl^-_1\SDspaen_\infty(m,1)$ is acyclic.
\end{lemma}

\begin{proof}
  We denote the set of $m$ leaves by $L = \{ L_1, \ldots, L_m\}$.
  By $C_\ast$, we denote the cellular chain complex $Cl^-_1\SDpaen_\infty(m,1)$ and
  by $E^r_{p,q}$ we denote the spectral sequence associated to the filtration $D_pCl^-_1\SDpaen_\infty(m,1)$.
  The 0-th page is
  \begin{align}
    E^0_{p,q} = D_pC_{p+q} / D_{p-1}C_{p+q} \,.
  \end{align}
  By definition, $E^0_{p,q}$ is generated by Sullivan diagrams $\Sigma = (\lambda, S_1)$
  whose non-degenerate boundary cycle $\rho$ has exactly $p$ cycles not of the form $\cycle{L_i}$ for some leaf $L_i \in L$.
  This gives a partition of $L$ into $L = L' \sqcup L''$ with
  \begin{align}
    L' = \{ L_i \in L \mid \cycle{L_i} \notin \cycles(\rho) \} \,.
  \end{align}
  Note that the partition is invariant under the face maps in $E^0_{p,q}$.
  Therefore, $E^0_{p,\ast}$ is a direct sum of $\binom{m}{p}$ chain complexes
  \begin{align}
    E^0_{p,\ast} \cong \bigoplus_{L' \subset L, \#L' = p} K(L')_\ast \,.
  \end{align}
  For each $p \ge 1$, we will show now that each $K(L')$ is isomorphic to $Cl^-_1\SDpaenN_\infty(p,1)$.
  Given $L' \subset L$ the enumeration of $L$ induces an enumeration of $L'$ and we are safe to assume that $L' = \{ L_1, \ldots, L_p \} \subset \{ L_1, \ldots, L_m \} = L$.
  Given a Sullivan diagram $\Sigma \in K(L')$ having a single ghost surface and with outgoing boundary cycles
  \begin{align}
    \rho = \rho_1 \cdots \rho_p \cdot \cycle{L_{p+1}} \cdots \cdot \cycle{L_m} \in \SymGr[k \sqcup L]
  \end{align}
  we define $\Phi(\Sigma)$ to be the Sullivan diagram with a single ghost surface and with outgoing boundary cycles
  \begin{align}
    \Phi(\rho) = \rho_1 \ldots \rho_p  \in \SymGr[k \sqcup L'] \,.
  \end{align}
  From Discussion \ref{discussion:faces_algebraically} it is clear that this defines a chain map
  \begin{align}
    \Phi \colon K(L') \to Cl^-_1\SDpaen_\infty(p,1) / Cl^-_1D\SDpaen_\infty(p,1) \,.
  \end{align}
  Observe that $\Phi$ is bijective:
  Given a Sullivan diagram in $Cl^-_1\SDpaen_\infty(p,1) / Cl^-_1D\SDpaen_\infty(p,1)$, adding the $m-p$ degenerate cycles $\cycle{L_{p+1}}, \ldots, \cycle{L_m}$ to
  the unique ghost surface defines the inverse chain map.
  By our desuspension theorem for the spaces of Sullivan diagrams and Proposition \ref{proposition:NSD_and_suspensions} we have an isomorphism
  \begin{align}
    \SDpaen_\infty(p,1) / D\SDpaen_\infty(p,1) \cong \Sigma^{p-1}\SDpaenN_\infty(p,1)
  \end{align}
  that sends a Sullivan diagram $(\lambda, S)$ to $\pm (\lambda, S)$.
  In particular, the isomorphism is compatible with the corresponding unreduced cluster filtrations, i.e.,
  \begin{align}
    Cl^-_1\SDpaen_\infty(p,1) / Cl^-_1D\SDpaen_\infty(p,1) \cong \Sigma^{p-1}Cl^-_1\SDpaenN_\infty(p,1) \,.
  \end{align}
  The chain complexes $Cl^-_1\SDpaenN_\infty(p,1)$ are contractible by
  Proposition \ref{proposition:cluster_spectral_sequence_NSD} and Theorem \ref{theorem:stable_homotopy_type_of_harm_normalized}.
  Therefore,
  \begin{align}
    E^1_{p,q} = \bigoplus_{L' \subset L, \#L' = p} H_{p+q}(K(L'); \bZ) \cong \bigoplus_{L' \subset L, \#L' = p} H_{p+q}(\bZ[p-1]); \bZ)
      \begin{cases}
        \bZ^{\binom{m}{p}}  & q    = -1 \\
        0                   & q \neq -1
      \end{cases} \,.
  \end{align}
  
  Denote the single non-trivial row by $G_\ast = E^1_{\ast, -1}$.
  From the above it clear that
  \begin{align}
    G_p \cong \bZ \langle L' \subset L \mid \#L' = p\rangle
  \end{align}
  where we identify $L' = \{ L_{i_1} < \ldots < L_{i_p} \} \subset \{ L_1 < \ldots < L_m \} = L$ with the Sullivan diagram $\Sigma(L')$ having outgoing boundary cycles
  \begin{align}
    \rho(L') = \cycle{0\ L_{i_1}} \cdots \cycle{p-1\ L_{i_p}} \cdots \,.
  \end{align}
  With this identification, the differential $d \colon G_p \to G_{p-1}$ is
  \begin{align}
    d(L') = \sum_{1 \le j \le p} (-1)^j \{ L_{i_1} < \ldots < \widehat{L_{i_j}} < \ldots < L_{i_p} \} \,.
  \end{align}
  We conclude that $G_{\ast-1}$ is the cellular chain complex of an $(m-1)$-dimensional simplex.
  In particular, $E^2_{p,q}$ is concentrated in degree $E^2_{1, -1} \cong \bZ$.
\end{proof}
