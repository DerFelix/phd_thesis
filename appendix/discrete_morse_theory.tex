\noindent
Here, we review the elements of discrete Morse theory which was introduced by Forman in \cite{Forman1998}.
We briefly recall the geometric motivation in a simplified setting.
Consider a finite, regular simplicial complex $X$ and
assume we have a top dimensional simplex $\Delta$ of $X$ with a free face $d_i\Delta$, i.e.,
$d_i\Delta$ appears exactly once as a face of a top dimensional simplex.
Retracting $\Delta$ onto $\Lambda^i\Delta = \del \Delta - d_i\Delta$ results in a finite, regular simplicial complex $X' \simeq X$ having two simplicies fewer then $X$.
The cell $\Delta$ is called \defn{collapsible} and $d_i\Delta$ is \defn{redundant}.
Repeating this process results in a sequence of pairs of collapsibles and redundants.
This set of (disjoint) pairs is called \defn{discrete Morse flow} and all non-paired simplices are called \defn{essential}.
Performing the collapses one gets a smaller simplicial complex $M \simeq X$ and $M$ has as many simplices as there are essentials in $X$.

We need a more general version where a non-regular cell complex $X$ and non-free faces are allowed.
We only describe this in the algebraic setting.


\begin{definition}[Cellular Graph]
  Let $R$ be a coefficient ring.
  A directed graph $C = (V,E,d,\theta)$ with \defn{vertices $V$}, \defn{edges $E \subseteq V \times V$}, \defn{degree $d \colon V \to \mathbb Z$} and
  \defn{coefficients} $\theta \colon E \to R$ is said to be \defn{cellular}
  if it has only finitely many vertices in each grading and every edge $(v,w) \in E$ decreases the grading by one and has non-vanishing coefficient, i.e.,
  \begin{align}
    d(v) = d(w) + 1 \mspc{and}{20} \theta(v,w) \neq 0 \,.
  \end{align}
  Let $K_\bullet$ be a chain complex of finite type that is free in each degree and has a distinguished choice of basis, e.g., the cellular complex of a CW complex of finite type.
  Its associated cellular graph has vertices given by the basis elements and edges given by the differential.
  The grading of the vertices is given by the degree of the basis elements in the chain complex and the coefficients of the edges by their coefficient in the differential.
\end{definition}

\begin{example}
  Consider the integral chain complex $K_\bullet$ concentrated in degrees $0$ and $1$ with $K_0 = \mathbb Z \langle e_1, e_2, e_3 \rangle$, $K_1 = \mathbb Z \langle f_1, f_2, f_3 \rangle$;
  the only non-trivial differential is
  \begin{align}
    \left( \begin{array}{rrr}
      1 & -1 & 0  \\
      2 & 2  & 1  \\
      0 & 5  & -1 
    \end{array} \right) \,.
  \end{align}
  The cellular graph is as follows:
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&, column sep=15ex, row sep=10ex]
      e_1 \ar{d}{1} \ar{dr}[very near start]{2} \& e_2 \ar{dl}[very near start, swap]{-1} \ar{d}{2} \ar{dr}[very near start]{5} \& e_3 \ar{dl}[very near start, swap]{1} \ar{d}{-1} \& d=1 \\
      f_1                                       \& f_2                                                                          \& f_3                                              \& d=0
    \end{tikzcd}
  \end{align}
\end{example}

\begin{definition}[Matchings]
  Let $C = (V,E,d,\theta)$ be a cellular graph.
  A collection of edges $F \subseteq E$ is a \defn{matching} if any two edges in $F$ have disjoint vertices and
  if $\theta(v,w)$ is invertible for every edge $(v,w) \in F$.
  In this case, a vertex $v \in V$ is called \defn{collapsible} if there exists an edge $(v,w) \in F$,
  it is called \defn{redundant} if there exists an edge $(u,v) \in F$ and
  it is called \defn{essential} otherwise.
\end{definition}

\begin{definition}[$F$-inverted graph]
  Let $C = (V,E,d,\theta)$ be a cellular graph and $F \subseteq E$ a matching.
  The \defn{$F$-inverted graph} $\finv = (V, E_F, d, \theta_F)$ is obtained from $C$ by inverting the edges in $F$, i.e.,
  \begin{align}
    E_F &=  (E-F) \sqcup \{ (w,v) \mid (v,w) \in F \} \\
    \intertext{and inverting their coefficient in $R$, i.e.,}
    \theta_F(x,y) &=
      \begin{cases}
        \theta(x,y) & (x,y) \in E - F \\
        - \theta(y,x)^{-1} & (y,x) \in F
      \end{cases}
  \end{align} 
  Every edge in the $F$-inverted graph either decreases or increases the grading by one and so we write $v \ad w$ if $(v,w) \in E - F$ and $v \au w$ else.
  
  A path in $\finv$ from $x$ to $y$ is denoted by $\gamma \colon x \leadsto y$.
  The set of paths in the $F$-inverted graph is
  \begin{align}
    P_F(y, x) = \{ \gamma \colon x \leadsto y \text{ in } \finv \} \,.
  \end{align}
  The \defn{coefficient} $\theta_F(\gamma)$ of a path $\gamma \in P_F(y, x)$ is the product of the coefficients $\theta_F$ of its edges.
\end{definition}

\begin{definition}[Discrete Morse flow]
  Let $C$ be a cellular graph.
  A matching $F$ is a \defn{discrete Morse flow} if the $F$-inverted graph $\finv$ is \defn{acyclic}, i.e., it does not contain oriented loops.
\end{definition}

The following Lemma turns out to be helpful to deduce the acyclicity of a matching.

\begin{lemma}
  \label{lemma:dmt_acyclicity}
  Let $C$ be a cellular graph and $F$ a matching.
  The $F$-inverted graph is acyclic if and only if there exists a partial order $\le$ on the collapsibles such that
  the existence of a path
  \begin{align}
    c_1 \ad r_2 \au c_2 \mspc{with}{20} \text{$c_i$ collapsible}
  \end{align}
  implies $c_1 \lneqq c_2$.
\end{lemma}

\begin{proof}
  If $\finv$ is acyclic, then the relation
  \begin{align}
    c_1 \precneqq c_2 \;\; \text{if and only if there exists a non-empty path from $c_1$ to $c_2$}
  \end{align}
  is a partial order.
  The converse is clear.
\end{proof}

\begin{definition}[Morse Complex]
  Let $C = (V,E,d,\theta)$ be a cellular graph with Morse flow $F$.
  The \defn{Morse complex} $M_\bullet= M(C, F)_\bullet$ of $C$ and $F$ is a chain complex freely generated by the essential vertices in each grading.
  The coefficient of an essential cell $y \in M_{n-1}$ in the boundary of an essential cell $x \in M_n$ is
  \begin{align}
    \del_{y, x} = \sum_{\gamma \in P_F(y, x)} \theta_F(\gamma) \,.
  \end{align}
\end{definition}

\begin{theorem}[Forman]
  \label{theorem:dmt_homology}
  Let $C$ be the cellular graph of a chain complex $K_\bullet$.
  Let $F$ be a discrete Morse flow on $C$.
  Then, the Morse complex $M(C,F)_\bullet$ is a homotopy retract of $K_\bullet$.
  The inclusion $\iota \colon M(C,F)_\bullet \to K_\bullet$ is given by sending essential cells $x$ to
  \begin{align}
    \iota(x) =  \sum_{\gamma \in P_x} \theta_F(\gamma) t(\gamma)
  \end{align}
  where $P_x = \{ \gamma \in P_F(y, x) \mid \deg(y) = \deg(x), y \text{ not redundant} \}$ and
  $t(\gamma)$ denotes the endpoint of a path $\gamma$.
\end{theorem}

For a proof of Theorem \ref{theorem:dmt_homology}, we refer the reader to \cite[Chapter 11.3]{Kozlov2008}.
