\noindent
In what follows, we denote by $\bK$ a field of characteristic different from two.
We give three equivalent definitions of symmetric Frobenius algebra.
A detailed discussion of Frobenius algebras and their connections to two-dimensional field theories is found in \cite{KockFrobeniusAlgs}.

\begin{definition}
  \label{definition:frobenius_algebra_bilin}
  A \defn{symmetric Frobenius algebra} is a finite dimensional, unital, associative algebra $A$ over $\bK$ together with
  a non-degenerate, symmetric bilinear form $\sigma \colon A \otimes A \to A$ with $\sigma(xy,z) = \sigma(x,yz)$ for all $x,y,z \in A$.
\end{definition}

\begin{definition}
  \label{definition:frobenius_algebra_lin_func}
  A \defn{symmetric Frobenius algebra} is a finite dimensional, unital, associative algebra $A$ over $\bK$ together with
  a non-degenerate, symmetric linear functional $\epsilon \colon A \to \bK$, i.e.,
  $\{ x \in A \mid \epsilon(x) = 0 \}$ does not contain a proper left ideal and $\epsilon(xy) = \epsilon(yx)$ for all $x,y \in A$.
\end{definition}

\begin{definition}
  \label{definition:frobenius_algebra_mul_and_comul}
  A \defn{symmetric Frobenius algebra} is a finite dimensional vector space $A$ over $\bK$ together with
  \begin{enumerate}
    \item a multiplication $\mu \colon A \otimes A \to A$ and unit $\eta \colon \bK \to A$ making $A$ a unital, associative algebra
    \item a comultiplication $\delta \colon A \to A \otimes A$ and counit $\epsilon \colon A \to \bK$ making $A$ a counital, coassociative coalgebra
  \end{enumerate}
  such that $\sigma := \epsilon\mu \colon A \otimes A \to \bK$ is symmetric and such that the Frobenius relation
  \begin{align}
    (\id \otimes \mu)(\delta \otimes \id) = \delta\mu = (\mu \otimes \id)(\id \otimes \delta)
  \end{align}
  is satisfied.
\end{definition}

\begin{remark}
  Let us say a few words why the three definitions give the same object.
  Consider a symmetric Frobenius algebra as defined in Definition \ref{definition:frobenius_algebra_bilin} the non-degenerate symmetric bilinear form $\sigma$ defines a non-degenerate symmetric linear functional $\epsilon$ via
  \begin{align}
    \epsilon(x) = \sigma(x,1) \,.
  \end{align}
  Vice versa, the non-degenerate symmetric linear functional $\epsilon$ defines the non-degenerate symmetric bilinear form via
  \begin{align}
    \sigma(x,y) = \epsilon(xy) \,.
  \end{align}
  
  Given a symmetric Frobenius algebra $A = (A, \mu, \eta, \delta, \epsilon)$ as in Definition \ref{definition:frobenius_algebra_mul_and_comul},
  the counit $\epsilon$ is a non-degenerate, symmetric linear functional.
  In order to construct the comultiplication and the counit from a given non-degenerate bilinear form, we refer the reader to \cite[Section 2.3]{KockFrobeniusAlgs}.
\end{remark}

\begin{remark}
  Using the Kozul-sign-convention, the definition of a graded symmetric Frobenius algebra agrees with the above Definitions.
\end{remark}

Let us discuss two well known examples of Frobenius algebras.

\begin{example}
  Consider the cohomology ring $A := H^\ast( M; \bQ)$ of a connected, closed, oriented, $n$-dimensional manifold $M$ with fundamental class $[M]$.
  We denote the Kronecker pairing by $\langle - , - \rangle$.
  The \defn{Frobenius pairing of $A$} is
  \begin{align}
    \sigma(\alpha, \beta) := \langle \alpha \cup \beta, [M] \rangle \in \bQ
  \end{align}
  This makes the cohomology ring $A$ into a graded symmetric Frobenius algebra by the Poincaré-duality Theorem.
  
  If, in addition, all odd-dimensional cohomology groups vanish, we obtain an ungraded symmetric Frobenius algebra.
  In particular, the cohomology ring of an even-dimensional sphere is an ungraded symmetric Frobenius algebra.
  Let us spell out its structure in the sense of Definition \ref{definition:frobenius_algebra_mul_and_comul}.
  As an algebra, it is an exteriour algebra $A = \Lambda[t]$ in one generator $t$ in degree $2n$.
  The counit is given by $\epsilon(1) = 0$ and $\epsilon(t) = 1$.
  The comultiplication is given by $\delta(1) = 1 \otimes t + t \otimes 1$ and $\delta(t) = t \otimes t$.
\end{example}

\begin{example}
  Let $G$ be a finite group with $n+1$ elements $G = \{ 1, s_1, \ldots, s_n \}$.
  On its group ring $\bC[G]$ we define the \defn{Frobenius form} by $\epsilon(1) = 1$ and $\epsilon(s_i) = 0$.
  The associated bilinear form $\sigma(x,y) := \epsilon(xy)$ is non-degenerate since $\sigma(g, g^{-1}) = 1$ for all group elements $g \in G$.
  The Frobenius form is clearly symmetric.
\end{example}

