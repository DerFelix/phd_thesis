\noindent
Here, we make the relation between the bar compactification and the (norm filtration of the reduced) bar resolution of varying symmetric groups explicit.
To this end, we concentrate on the bar compactification $R := Bar(\fM^\unun_g(m,1))$ of a fixed moduli space with unparametrized and unenumerated outgoing boundaries.
As before, we denote the complement of $\Rad \subset R$ by $R'$ which is a subcomplex of $R$.
The homology of $\fM$ is Poincaré dual to the cohomology of $(R,R')$ with certain orientation coefficients.
The orientation coefficients will not play a role in the subsequent discussion and are therefore suppressed in the notation.

Computing the horizontal faces of the reduced%
\footnote{The subcomplex generated by all simplicially degenerated bi-simplices is acyclic. The quotient by this subcomplex is the called the reduced bi-complex.}
 bi-complex $(R,R')$ gives rise to a spectral sequence $E^r_{p,q}$.
Using the inhomogeneous notation, the $q$-th row of $E^0_{p,q}$ is the chain complex of all tuples $\Sigma = ( \tau_1 \mid \ldots \mid \tau_q)$ such that
$\tau_i \neq 1$, $N(\tau_1) + \ldots + N(\tau_q) = h = 2g+m-2$ and $m(\Sigma) = \ncyc(\tau_q \cdots \tau_1 \cdot \omega_p) = m$.
Observe that $d'_0\Sigma = 0 = d'_q\Sigma$ and that $m(\Sigma) = m(d'_i\Sigma)$ whenever $d'_i\Sigma \neq 0$.
This chain complex is a direct summand of a row of a spectral sequence that computes the homology of the reduced bar resolution of the symmetric group $\SymGr_p$:
Denote the reduced bar resolution of $\SymGr_p$ by $B_\ast\SymGr_p$.
The set of all transpositions is a generating set $\cE$ of $\SymGr_p$.
The corresponding world length norm is denoted by $N^\cE$.
It induces a filtration $F_s$ of $B_\ast\SymGr_p$.
The associated shifted spectral sequence is denoted by $\cN^r_{s,t}$ with $\cN^0_{s,t} := F_sB_t\SymGr_p / F_{s-1}B_t\SymGr_p$.
It is immediate that $(E^0_{p,\ast}, d') \subset (\cN^0_{h,\ast}, d^0)$ is a direct summand and therefore, $E^1_{p,q} \subset \cN^1_{h,q}$ is a direct summand.

The theory of factorable groups, introduced by Visy in \cite{Visy201011} and generalized by Alexander Heß and Rui Wang in \cite{Hess2012} and \cite{Wang201102},
is used to show that $\cN^1_{h,\ast}$ is concentrated in degree $\cN^1_{h,h} = \ker(d^0_{h,h})$.
By \cite[Theorem 5.2.1]{Visy201011}, the symmetric group $\SymGr_p$ is \defn{factorable} with respect to the generating set of all transpositions $\cE$, i.e.,
there is a factorization map $\eta \colon \SymGr_p \to \SymGr_p \times \cE$ that splits off a generator while
respecting the word length norm and such that a certain compatibility with the multiplication is achieved.
More precisely, denote the multiplication by $\mu \colon \SymGr_p \times \SymGr_p \to \SymGr_p$ and consider the diagram below.
\begin{align}
  \begin{tikzcd}[ampersand replacement=\&]
      \SymGr_p \times \SymGr_p  \arrow{d}{\mu} \arrow{r}{\eta \times \id} \& \SymGr_p \times \SymGr_p \times \SymGr_p \arrow{r}{\id \times \mu}    \& \SymGr_p \times \SymGr_p \arrow{r}{\id \times \eta}   \& \SymGr_p \times \SymGr_p \times \SymGr_p \arrow{d}{\mu \times \id} \\
      \SymGr_p \arrow{rrr}{\eta}                                          \&                                                                       \&                                                       \& \SymGr_p \times \SymGr_p
  \end{tikzcd}
\end{align}
Then, the upper right composition of maps preserves the norm if and only if the lower left composition does and, in this case, the diagram commutes.
Visualizing the multiplication as two strings that are merged and the factorization map as a single string that is split,
the lower left and upper right composition of the diagram is visualized by Figure \ref{figure:factorability_visualized}.
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[line width=1pt, yscale=.25, xscale=.8]
      \draw (1,7) -- (1,6) -- (.5,5) -- (.5,2) -- (1,1) -- (1,0);
      \draw (0,7) -- (0,6) -- (.5,5) -- (.5,2) -- (0,1) -- (0,0);
  \end{tikzpicture}
  \hspace{3cm}
  \begin{tikzpicture}[line width=1pt, yscale=.25, xscale=.8]
      \draw (0.5,7) -- (0.5,6) -- (0,5)                                         -- (0,2) -- (0.5,1) -- (0.5,0);
      \draw (0.5,7) -- (0.5,6) -- (1,5) -- (1.5,4) -- (1.5,3) -- (1,2) -- (0.5,1);
      \draw (2,7)   -- (2,5)                     -- (1.5,4) -- (1.5,3) -- (2,2)                     -- (2,0);
  \end{tikzpicture}
  \caption{
    \label{figure:factorability_visualized}
    The left picture visualizes $\eta\circ \mu$ and the right picture visualizes
    $\mu \times \id \circ \id \times \eta \circ \id \times \mu \circ \eta \times \id$.
  }
\end{figure}

Let us discuss another example of factorable groups, see \cite[Example 1.2.4]{Visy201011}.
Consider the free group $\Fr_n$ on $n$ generators $\{ s_1, \ldots, s_n \}$.
Taking the generating set $\cE = \{ s_1, \ldots s_n, s_1^{-1}, \ldots, s_n^{-1} \}$,
any element $x \in \Fr_n$ is uniquely represented by a unique word of minimal length $x = s_{i_N}^{\epsilon_N} \cdots s_{i_1}^{\epsilon_1}$ with $s_j \in \cE$.
The factorization map $\eta$ is defined by splitting of the right most generator of the unique word.

In general, for a factorable group (or monoid), the factorability structure leads to a discrete Morse flow on the complex $(\cN^0_{h,\ast}, d^0)$ such that
all basis elements in degree $\ast < h$ are either collapsible or redundant, see \cite[Chapter 3]{Hess2012}.
The norm filtration $\cN^0_{h,\ast}$ of the reduced bar resolution does not have elements of degree $\ast > h$ by construction.
\begin{theorem}[{\cite[Theorem 4.1.1]{Visy201011}}]
  The homology of the norm filtration $(\cN^0_{h,\ast}, d^0)$ is concentrated in degree $h$ and $\cN^1_{h,\ast} = \ker(d^0_{h,h})$.
\end{theorem}
Moreover, using the discrete Morse flow, a canonical basis for $\cN^1_{h,\ast}$ is constructed,
see \cite[Remark 4.3.2]{Visy201011} and also \cite[Proposition 2.3.36]{Hess2012} or \cite[Definition 2.8.5]{BoesHermann2014}.
