\section{Multi-cyclic sets}
\label{appendix:multicyclic_sets}
The upcoming definitions are well known and discussed in great detail in \cite[Chapter~6.1]{LodayCyclicHomology}.

\begin{definition}
  \defn{Connes cyclic category} $\CC$ has objects $[n] = \{ 0, \ldots, n \}$ for $n \in \bN$ and the following morphisms:
  \begin{enumerate}
    \item $\delta_i \colon [n-1] \to [n]$, $i=0, \ldots, n$ called faces,
    \item $\sigma_i \colon [n+1] \to [n]$, $i=0, \ldots, n$ called degeneracies and
    \item $\tau_n \colon [n] \to [n]$ called cyclic operators.
  \end{enumerate}
  These satisfy the following relations.
  \begin{enumerate}
    \item $\begin{aligned}[t]
        \delta_j \delta_i &= \delta_i \delta_{j-1} & \text{for } i < j, \\
        \sigma_j \sigma_i &= \sigma_i \sigma_{j+1} & \text{for } i \le j, \\
        \sigma_j \delta_i &=
          \begin{cases}
            \delta_i \sigma_{j-1} & i < j, \\
            \id_{[n]}   & i = j, i = j+1, \\
            \sigma_j \delta_{i-1} & i > j+1.
          \end{cases}
      \end{aligned}$
    \item $\begin{aligned}[t]
        \tau_n \delta_i &= \delta_{i-1} \tau_{n-1} & \text{for } 1 \le i \le n, \\
        \tau_n \delta_0 &= \delta_n, \\
        \tau_n \sigma_i &= \sigma_{i-1} \tau_{n+1} & \text{for } 1 \le i \le n, \\
        \tau_n \sigma_0 &= \sigma_n \tau_{n+1}^2.
      \end{aligned}$
    \item $\begin{aligned}[t]
        \tau_n^{n+1} &= \id_{[n]}.
      \end{aligned}$
  \end{enumerate}
\end{definition}

\begin{definition}
  The category $\CCop$ has objects $[n] = \{0, \ldots, n\}$ for $n \in \bN$ and the following morphisms:
  \begin{enumerate}
    \item $d_i \colon [n] \to [n-1]$, $i=0, \ldots, n$ called faces,
    \item $s_i \colon [n] \to [n+1]$, $i=0, \ldots, n$ called degeneracies and
    \item $t_n \colon [n] \to [n]$  called cyclic operators.
  \end{enumerate}
  These satisfy the following relations.
  \begin{enumerate}
    \item $\begin{aligned}[t]
        d_i d_j &= d_{j-1} d_i & \text{for } i < j, \\
        s_i s_j &= s_{j+1} s_i & \text{for } i \le j, \\
        d_i s_j &=
          \begin{cases}
            s_{j-1} d_i & i < j, \\
            \id_{[n]}   & i = j, i = j+1, \\
            s_j d_{i-1} & i > j+1.
          \end{cases}
      \end{aligned}$
    \item $\begin{aligned}[t]
        d_i t_n &= t_{n-1} d_{i-1} & \text{for } 1 \le i \le n, \\
        d_0 t_n &= d_n, \\
        s_i t_n &= t_{n+1} s_{i-1} & \text{for } 1 \le i \le n, \\
        s_0 t_n &= t_{n+1}^2 s_n.
      \end{aligned}$
    \item $\begin{aligned}[t]
        t_n^{n+1} &= \id_{[n]}.
      \end{aligned}$
  \end{enumerate}
\end{definition}

In this thesis, we are working with multi-simplicial sets that come with a cyclic action in each factor.

\begin{definition}
  The \defn{multi-cyclic category}, denoted by $\kCC$, is the $k$-fold product of categories $\kCC := \CC \times \ldots \times \CC$.
  The objects of $\kCC$ are denoted $[n_1, \ldots, n_k] := [n_1] \times \ldots \times [n_k]$.
  
  The category $\kCCop$ is the $k$-fold product of categories $\kCCop := \CCop \times \ldots \times \CCop$ which is canonically isomorphic to $(\CC \times \ldots \times \CC)^{op}$.
  The objects of $\kCCop$ are denoted $[n_1, \ldots, n_k] := [n_1] \times \ldots \times [n_k]$.
\end{definition}

Note that Proposition \ref{proposition:multicyclic_cat_multisimplicial_cat_and_automorphismgroups},
Proposition \ref{proposition:multicyclic_torus} and
Theorem \ref{theorem:geometric_realization_of_a_multicyclic_set}
justify our definition above.
The proof of the case $k=1$ can be found in \cite[Chapter 6 and Chapter 7]{LodayCyclicHomology}.
The general case is proven analogously because all identities and relations that have to be satisfied can be verified in $\Set$.

\begin{proposition}
  \label{proposition:multicyclic_cat_multisimplicial_cat_and_automorphismgroups}
  The category $\kCCop$ contains $\kDop = \Dop \times \ldots \times \Dop$ as a subcategory.
  Moreover:
  \begin{enumerate}
    \item There are canonical isomorphisms $Aut_{\kCCop}([n_1, \ldots, n_k]) \cong \times_{i=1}^k\bZ_{n_i + 1}$.
    \item In $\kCCop$, any morphism $\varphi \in \Hom_{\kCCop}([m_1, \ldots, m_k], [n_1, \ldots, n_k])$ admits a unique decomposition $\varphi = \sigma \circ f$ with
      $f \in \Hom_{\kDop}([m_1, \ldots, m_k], [n_1, \ldots, n_k])$ and $\sigma \in \Aut_{\kCCop}([n_1, \ldots, n_k])$.
  \end{enumerate}
\end{proposition}

\begin{proposition}
  \label{proposition:multicyclic_torus}
  The family of cyclic groups $C_n := \Aut_{\CCop}([n])$ of order $n+1$ forms a cyclic set $C_\bullet \colon \CCop \to \Set$.
  Its geometric realization is homeomorphic to the circle $U(1)$.
  
  For $k \ge 1$, the family of multi-cyclic groups is defined to be the composition of functors
  \begin{align}
    kC_\bullet \colon \CCop \times \ldots \times \CCop \xr{C_\bullet \times \ldots \times C_\bullet} \Set \times \ldots \times \Set \xhr{} \Set \,.
  \end{align}
  It is a multi-cyclic set.
  Its geometric realization is homeomorphic to the torus $U(1)^k$.
\end{proposition}

\begin{theorem}
  \label{theorem:geometric_realization_of_a_multicyclic_set}
  Let $X$ be a multi-cyclic set and let $|X|$ be the geometric realization of its underlying simplicial set.
  Then
  \begin{enumerate}
    \item the space $|X|$ is endowed with a canonical action of the torus $U(1)^k$ and
    \item the geometric realization is a functor from the category of multi-cyclic spaces to the category of spaces with $U(1)^k$ action.
  \end{enumerate}
\end{theorem}

\begin{proposition}
  \label{proposition:multicyclic_circle_action_in_terms_of_representatives}
  Let $X$ be a cyclic set and let $|X|$ be its geometric realization.
  Consider a point $x = (\sigma; u_0, \ldots, u_n) \in |X|$ with $\sigma$ not degenerate and with $0 < u_i$ for all $i$.
  For $z \in \bS^1 = [0,1]/\sim$ with $0 \le z \le u_n$ the point $z.x$ is represented by
  \begin{align}
    ( t_{n+1} s_n\sigma; z, u_0, \ldots, u_{n-1}, u_n - z ) \,.
  \end{align}
\end{proposition}

\begin{proof}
  The proof uses notations and facts from \cite[Chapters 6.1, 7.1]{LodayCyclicHomology}.
  There is a functor $F \colon \Fun( \Dop, \Set ) \to \Fun( \CCop, \Set )$ that is left adjoint to the forgetful functor.
  For a simplicial set $Y$ it is defined as the simplicial set $F(Y)_n = C_n \times Y_n$.
  Here, we do not spell out the cyclic structure on $F(Y)$.
  For a cyclic space $X$, the evaluation $\ev \colon F(X) \to X$, $(g,x) = g.x$ is a morphism of cyclic spaces.
  The realization $|F(X)|$ is homeomorphic to $|C| \times |X|$ via $p_1 \times p_2$ for maps
  \begin{align}
    p_1(g,x;u) = (g;u) \quad \text{and} \quad p_2(g, x; u) = (x; g^\ast u)
  \end{align}
  where $g^\ast u$ is the cocylic action defined by $\tau_n( u_0, \ldots, u_n) := (u_1, \ldots, u_n, u_0)$.
  The action of $\bS^1 \cong |C|$ on $|X|$ is $\ev \circ (p_1 \times p_2)^{-1}$.
  
  With these facts at hand, the claim is readily checked:
  A point $x = (\sigma; u_0, \ldots, u_n)$ is also represented by $(s_n\sigma; u_0, \ldots, u_{n-1}, u_n - z, z)$ for $0 \le z \le u_n$.
  The point $z \in \bS^1$ is represented by $(t_1, z, 1-z)$ which is also $( s_n \circ \ldots \circ s_1( t_1 ); z, u_0, \ldots, u_{n-1}, u_n - z)$.
  By \cite{LodayCyclicHomology}, we know that $s_n \circ \ldots \circ s_1( t_1 ) = t_{n+1}$.
  The point $(z, x) \in \bS^1 \times |X| $ is hit by $(t_{n+1}, s_n\sigma; z, u_0, \ldots, u_{n-1}, u_n - z)$ under the homeomorphism $p_1 \times p_2$:
  \begin{align}
    (z,x) = p_1 \times p_2 (t_{n+1}, s_n\sigma; z, u_0, \ldots, u_{n-1}, u_n - z) \,.
  \end{align}
  Evaluating yields
  \begin{align}
    \ev (t_{n+1}, s_n\sigma; z, u_0, \ldots, u_{n-1}, u_n - z) = (t_{n+1} s_n \sigma; z, u_0, \ldots, u_{n-1}, u_n - z) \,.
  \end{align}
\end{proof}
