\noindent
Here, we collect most of what is known about the unstable homology of the moduli spaces in question.
For the convenience of the reader, we include some results on the stable homology.

\section{The virtual cohomological dimension}
\label{appendix:unstable_homology_vcd}
The virtual cohomological dimension of the moduli spaces has been determined by Harer in \cite[Theorem 4.1]{Harer1986}.
\begin{theorem}[Harer]
  Let $g \ge 0$, $m \ge 0$, $n \ge 0$ and $2g+m+n > 2$.
  The virtual cohomological dimension of the moduli space $\fM^\paen_g(m,n)$ is $d(g,m,n)$ with
  \begin{align}
    d(g,m,n) =
      \begin{cases}
        4(g-1) + 2n + m & \text{if } g > 0 \text{ and } n + m > 0 \\
        4g - 5          & \text{if } g > 0 \text{ and } n = m = 0 \\
        2n + m - 3      & \text{else}
      \end{cases} \,.
  \end{align}
\end{theorem}
Moreover, by \cite{ChurchFarbPutman2012}, the rational homology vanishes on its virtual cohomological dimension in the following cases.
\begin{theorem}[Church--Farb--Putman]
  For $g \ge 2$, $m+n \le 1$ it is
  \begin{align}
    H^{d(g,m,n)}( \fM^\unen_g(m,n); \bQ ) = 0 \,.
  \end{align}
\end{theorem}


\section{Results on the first homology}
\label{appendix:unstable_homology_H1}
Here, we spell out the results of
\cite[Theorem 4.5]{Birman1974},
\cite[Theorem 1]{Powell1978},
\cite{Ehrenfried1997},
\cite[Theorem 3.12]{KorkmazMcCarthy2000},
\cite[Proposition 1.6]{KorkmazStipsicz2003} and
\cite[Proposition 11]{Godin2007}
on the first homology of the mapping class group.
We denote by $\Gamma^\star_g(m,n)$ the mapping class group of a surface of genus $g$ with $n+m$ boundary components among which
$n$ are enumerated and parametrized whereas the other
$m$ component are (un)enumerated and unparametrized depending on $\star \in \{\unen, \unun\}$.
\begin{theorem}[Birman, Powell, Ehrenfried, Korkmaz--McCarthy, Godin]
  It is
  \begin{align}
    H_1( \Gamma^\unun_g(m,0); \bZ ) &\cong
      \begin{cases}
        0                      & g=0, m=0,1 \\
        \bZ_{m-1}              & g=0, m \ge 2, m \text{ odd} \\
        \bZ_{2m-2}             & g=0, m \ge 2, m \text{ even} \\
        \bZ_{12}               & g=1, m = 0,1 \\
        \bZ_{12} \oplus \bZ_2  & g=1, m \ge 2 \\
        \bZ_{10}               & g=2, m = 0,1 \\
        \bZ_{10} \oplus \bZ_2  & g=2, m \ge 2 \\
        0                      & g \ge 3, m = 0,1 \\
        \bZ_2                  & g \ge 3, m \ge 2
      \end{cases}
    \intertext{and}
    H_1( \Gamma^\unen_g(m,n); \bZ ) &\cong
      \begin{cases}
        \bZ^2        & g = 1, m = 0, n = 2 \\
        \bZ_{10}     & g \ge 2, m \ge 0, n \ge 0 \\
        0            & g \ge 3, m \ge 0, n \ge 0
      \end{cases} \,.
  \end{align}
\end{theorem}

Recall that, for $n \ge 1$, the moduli space $\fM^\star_g(m,n)$ is a classifying space of $\Gamma^\star_g(m,n)$ and,
for $n=0$, $\fM^\star_g(m,n)$ is still rationally a classifying space.

\begin{corollary}
  Let $g \ge 1$, $m \ge 0$, $n \ge 1$ and $\star \in \{ \unen, \unun \}$.
  Then
  \begin{align}
    H_1( \fM^\unen_g(m,n); \bZ ) &\cong
      \begin{cases}
        \bZ^2        & g = 1, m = 0, n = 2 \\
        \bZ_{10}     & g \ge 2, m \ge 0, n \ge 1 \\
        0            & g \ge 3, m \ge 0, n \ge 1
      \end{cases}
      \intertext{and}
      H_1( \fM^\star_g(m,0); \bQ ) &\cong 0 \,.
  \end{align}
\end{corollary}

Moreover, we would like to add the following proposition which we believe is known.
A proof of this fact will follow from the study of the spectral sequence of the fibration $\fM^\unun_g(m,1) \to \fM^\unun_g(0,1)$.

\begin{proposition}
  Let $g \ge 0$ and $m \ge 0$.
  It is
  \begin{align}
    H_1( \fM^\unun_g(m,1); \bZ ) &\cong
      \begin{cases}
        \bZ                   & g = 0,1, m=0,1 \\
        \bZ \oplus \bZ_2      & g = 0,1, m \ge 2 \\
        \bZ_{10}              & g = 2,   m = 0,1 \\
        \bZ_{10} \oplus \bZ_2 & g = 2,   m \ge 2 \\
        0                     & g \ge 3, m =0,1 \\
        \bZ_2                 & g \ge 3, m \ge 2
      \end{cases} \,.
  \end{align}
\end{proposition}


\section{Results on the second homology}
\label{appendix:unstable_homology_H2}
Let us spell out the results of
\cite{Harer1983},
\cite{Harer1991},
\cite{BensonCohen1991},
\cite{Ehrenfried1997},
\cite{KorkmazStipsicz2003},
\cite[Proposition 11]{Godin2007},
\cite{Mehner201112} and
\cite[Theorem 4.9, Corollary 4.10]{Sakasai2012}
on the second homology of the mapping class group $\Gamma^\unen_g(m,n)$.
\begin{theorem}[Harer, Benson--Cohen, Ehrenfried, Korkmaz--Stipsicz, Godin, Mehner, Sakasai]
  It is
  \begin{align}
    H_2( \Gamma^\unen_g(m,n); \bZ ) &\cong
      \begin{cases}
        \bZ \oplus \bZ_2    & g = 1, m = 0, n = 2 \\
        \bZ_2               & g = 2, m = 0,1, n = 0,1 \\
        \bZ \oplus \bZ_2    & g = 3, m = 0, n = 0,1 \\
        \bZ^2 \oplus \bZ_2  & g = 3, m = 1, n = 0 \\
        \bZ^{n+1}           & g \ge 4, m \ge 0, n \ge 0
      \end{cases} \,.
  \end{align}
\end{theorem}

Recall that, for $n \ge 1$, the moduli space $\fM^\unen_g(m,n)$ is a classifying space of $\Gamma^\unen_g(m,n)$ and,
for $n=0$, $\fM^\unen_g(m,n)$ is still rationally a classifying space.

\begin{corollary}
  It is
  \begin{align}
    H_2( \fM^\unen_g(m,n); \bZ ) &\cong
      \begin{cases}
        \bZ \oplus \bZ_2    & g = 1, m = 0, n = 2 \\
        \bZ_2               & g = 2, m = 0,1, n = 1 \\
        \bZ \oplus \bZ_2    & g = 3, m = 0, n = 1 \\
        \bZ^{n+1}           & g \ge 4, m \ge 0, n \ge 1
      \end{cases}
    \intertext{and}
    H_2( \fM^\unen_g(m,n); \bQ ) &\cong
      \begin{cases}
        \bQ                 & g = 1, m = 0, n = 2 \\
        0                   & g = 2, m = 0,1, n = 0,1 \\
        \bQ                 & g = 3, m = 0, n = 0,1 \\
        \bQ^2               & g = 3, m = 1, n = 0 \\
        \bQ^{n+1}           & g \ge 4, m \ge 0, n \ge 0
      \end{cases} \,.
  \end{align}
\end{corollary}


\section{Results on the homology of a single moduli space}
\label{appendix:unstable_homology_of_a_single_moduli_space}

The moduli spaces $\fM^\unun_0(m,1)$ are classifying spaces for the braid groups $Br_m$.
Their homology is well known by the results of \cite{Arnold1970}, \cite{Fuks1970} and \cite{CohenLadaMay1976}.


For coefficients in a field $\bF$ with $\characteristic(\bF) \neq 2$, $3$ or $5$,
the mapping class group $\Gamma^\unun_2(0,0)$ is a classifying space for $\fM^\unun_2(0,0)$.
In this case, we have $H^\ast( \fM^\unun_2(0,0); \bF ) = 0$ by \cite[Corollary 5.2.3]{LeeWeintraub1985}.
For coefficients in the field $\bF$ with $\characteristic(\bF) = 2$, $3$ or $5$ elements, the Poincaré series of $\Gamma^\unun_2(0,0)$ has been determined by
\cite[Theorem I.1.2, Theorem II.1.1, Theorem III.1.1]{BensonCohen1991}.
\begin{theorem}[Lee--Weintraub, Benson--Cohen]
  Let $p$ be a prime and denote the finite field with $p$ elements by $\bF_p$.
  The Poincaré series of $\Gamma^\unun_2(0,0)$ are as follows.
  \begin{align}
    \sum_{n \ge 0} \dim_{\bF_p} H^n(\Gamma^\unun_2(0,0); \bF_p) \cdot t^n =
      \begin{cases}
        (1 + t^2 + 2t^3 + t^4 + t^5)/(1 - t)(1 - t^4)  & p = 2\\
        (1 + t^3 + t^4 + t^5)/(1 - t^4)                & p = 3\\
        1 / (1 - t)                                    & p = 5\\
        1                                              & \text{else}
      \end{cases}
  \end{align}

\end{theorem}


The rational homology of the moduli spaces $\fM^\unun_2(3,0)$ respectively $\fM^\unun_4(0,0)$ has been computed by Tommasi in
\cite[Theorem 1.4]{Tommasi2005} respectively \cite[Theorem 1.1]{Tommasi2007}.
We summarize her results as follows.
\begin{theorem}[Tommasi]
  The rational Betti numbers of the moduli space $\fM^\unun_2(3,0)$ respectively $\fM^\unun_4(0,0)$ are
  \begin{align}
    \beta(\fM^\unun_4(0,0)) &= (1,0,1,0,1,1)
    \intertext{respectively}
    \beta(\fM^\unun_2(3,0)) &= (1,0,3,0,2,1,1,0,2) \,.
  \end{align}
\end{theorem}


Adding a point near the boundary of a given surface induces an inclusion of moduli spaces
$a \colon \fM^\unun_g(m,n) \to \fM^\unun_g(m+1,n)$.
The induced map in homology is split injective by \cite[Theorem 1.3]{BoedigheimerTillmann2001}.
Using Bödigheimer's combinatorial models for the moduli spaces,
\cite{Ehrenfried1997},
\cite{Abhau200501},
\cite{Wang201102},
\cite{Mehner201112},
\cite{BoesHermann2014} and the author
compute the rational and integral homology of $\fM^\unun_g(m,1)$ for small parameters $m$.
Moreover, \cite{Mehner201112} and the author identified some of the generators.
\begin{theorem}[Bödigheimer, Ehrenfried, Abhau, Wang, Mehner, B.--Hermann, B.]
  The integral homology of the moduli space $\fM^\unun_1(3,1)$ respectively $\fM^\unun_2(2,1)$ is
  as follows.
  \begin{align}
    H_\ast(\fM^\unun_1(3,1);\bZ) &\cong
      \begin{cases}
        \bZ\cycle{a^3c} & \ast=0 \\
        \bZ\cycle{a^3d} \oplus \bZ_2\cycle{abc} & \ast=1 \\
        \bZ_2\cycle{a^2e} \oplus \bZ_2\cycle{abd} & \ast=2 \\
        \bZ\cycle{?} \oplus \bZ_2\cycle{be} \oplus \bZ_2\cycle{af} & \ast=3 \\
        \bZ\cycle{?} \oplus \bZ\cycle{?} & \ast=4 \\
        \bZ\cycle{?} & \ast=5 \\
        0 & \text{else}
      \end{cases}
    \intertext{respectively}
    H_\ast(\fM^\unun_2(1,1);\bZ) &\cong
      \begin{cases}
        \bZ\cycle{ac^2} & \ast=0 \\
        \bZ_2\cycle{acd} & \ast=1 \\
        \bZ\cycle{?} \oplus \bZ_2\cycle{ad^2} & \ast=2 \\
        \bZ\cycle{\lambda a s} \oplus \bZ\cycle{?} \oplus \bZ_2\cycle{at} \oplus \bZ_2\cycle{de} & \ast=3 \\
        \bZ_3\cycle{aw_3} \oplus \bZ_2\cycle{a?} \oplus \bZ_6\cycle{?} & \ast=4 \\
        \bZ\cycle{?} & \ast=5 \\
        \bZ\cycle{?} & \ast=6 \\
        0 & \text{else}
      \end{cases}
  \end{align}
  Here, the known generators $a$,$b$,$c$,$d$,$e$,$t$ and their products are described in \cite[Kapitel 1]{Mehner201112} or \cite[Chapter 4]{BoesHermann2014}, see also \cite{BoedigheimerBoes2018}.
  The generators $s \in H_3( \fM^\unun_2(1,1); \bQ)$, $w_3 \in H_4( \fM^\unun_2(1,1); \bZ)$ respectively $f \in H_3(\fM^\unun_1(2,1); \bZ)$ are described in
  Section \ref{section:the_generator_s_is_non_trivial}, Section \ref{section:the_generator_w_3_is_non_trivial} respectively Section \ref{section:the_generator_f_is_non_trivial}.
  The other generators are denoted by the symbol $?$.
  Moreover, a generator $x \in H_\ast(\fM^\unun_g(m,1); \bZ)$ is in image of the split injective map $a^k \colon H_\ast( \fM^\unun_g(m-k, 1); \bZ ) \to H_\ast( \fM^\unun_g(m,1); \bZ)$
  if and only if it is denoted by $x = a^ky$ in the above description.
  
  Furthermore, the rational Betti numbers of the moduli space $\fM^\unun_1(4,1)$, $\fM^\unun_2(2,1)$ respectively $\fM^\unun_3(0,1)$ are
  \begin{align}
    \beta(\fM^\unun_1(4,1)) &= (1,1,0,2,3,2,1) \,,\\
    \beta(\fM^\unun_2(2,1)) &= (1,0,1,3,1,2,2) \\
    \intertext{respectively}
    \beta(\fM^\unun_3(0,1)) &= (1,0,1,1,0,1,1,0,0,1) \,. \\
  \end{align}
\end{theorem}


Using a chain complex of ribbon graphs as a model for mapping class groups, Godin computes the integral homology of $\Gamma^\unun_g(m,n)$ for small parameters $g$, $m$ and $n$ in \cite{Godin2007}.
Her results match the results of Ehrenfried.
\begin{theorem}[Godin]
  The integral homology of the mapping class group $\Gamma^\unun_1(0,2)$, $\Gamma^\unun_1(1,0)$ respectively $\Gamma^\unun_2(1,0)$ is as follows.
  \begin{align}
    H_\ast(\Gamma^\unun_1(0,2);\bZ) &\cong
      \begin{cases}
        \bZ & \ast=0 \\
        \bZ^2 & \ast=1 \\
        \bZ \oplus \bZ_2 & \ast=2 \\
        \bZ_2 & \ast=3 \\
        0 & \text{else}
      \end{cases}
    \intertext{respectively}
    H_\ast(\Gamma^\unun_1(1,0);\bZ) &\cong
      \begin{cases}
        \bZ & \ast=0 \\
        \bZ_{12} & \ast=2k+1 \\
        0 & \ast=2k+2
      \end{cases}
    \intertext{respectively}
    H_\ast(\Gamma^\unun_2(1,0);\bZ) &\cong
      \begin{cases}
        \bZ & \ast=0 \\
        \bZ_{10} & \ast=1 \\
        \bZ \oplus \bZ_2 & \ast=2 \\
        \bZ_{120} \oplus \bZ_{10} \oplus \bZ_2 & \ast=2k+3 \\
        \bZ_6 \oplus \bZ_2 & \ast=2k+4
      \end{cases}
  \end{align}
\end{theorem}

Recall that, for $n \ge 1$, the moduli space $\fM^\unun_g(m,n)$ is a classifying space of $\Gamma^\unun_g(m,n)$ and,
for $n=0$, $\fM^\unun_g(m,n)$ is still rationally a classifying space.

\begin{corollary}
  It is
    \begin{align}
    H_\ast(\fM^\unun_1(0,2);\bZ) &\cong
      \begin{cases}
        \bZ & \ast=0 \\
        \bZ^2 & \ast=1 \\
        \bZ \oplus \bZ_2 & \ast=2 \\
        \bZ_2 & \ast=3 \\
        0 & \text{else}
      \end{cases}
    \intertext{respectively}
    H_\ast(\fM^\unun_1(1,0);\bQ) &\cong
      \begin{cases}
        \bQ & \ast=0 \\
        0 & \text{else}
      \end{cases}
    \intertext{respectively}
    H_\ast(\fM^\unun_2(1,0);\bQ) &\cong
      \begin{cases}
        \bQ & \ast=0 \\
        0 & \ast=1 \\
        \bQ & \ast=2 \\
        0 & \text{else}
      \end{cases} \,.
  \end{align}
\end{corollary}

