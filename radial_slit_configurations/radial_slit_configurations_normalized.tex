\section{Radial slit configurations with normalized boundaries}
\label{section:radial_slit_configurations_normalized}
In this section, we discuss the spaces of radial slit configurations with normalized boundaries $\RadstarN_g(m,n) \subset \Radstar_g(m,n)$.
The study of their harmonic compactifications will lead to new insights on the (usual) harmonic compactification of the moduli spaces,
see Chapter \ref{chapter:homotopy_type_of_NSD} and Chapter \ref{chapter:homotopy_type_of_SDspaen}.
Here, we define the restriction to the outgoing boundaries $\Pol \colon \Radstar_g(m,1) \to \Polspcstar(m,1)$, see Definition \ref{definition:restriction_to_the_outgoing_boundaries}, 
and we measure the lengths of the outgoing boundaries, see Definition \ref{definition:length_of_the_boundaries_rad}.
Then, by Definition \ref{definition:rad_normalized_boundaries},
the spaces $\RadstarN_g(m,1) \subset \Radstar_g(m,1)$ consist of all radial slit configurations (or moduli) whose outgoing boundary curves all share the same length.
Then, we will prove Theorem \ref{theorem:classifying_map_from_rad}.

\begin{proposition}
  Let $m \ge 1$, $g \ge 0$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
  The bar compactification of $\Radstar_g(m,1)$ is $Bar^\star_g(m,1)$.
  The projection to the last factor
  \begin{align}
    S_{p,q} = (\SymGrununn[p])^{q} \times \SymGrstarn[p] \to \SymGrstarn[p]
  \end{align}
  induces a map of bi-simplicial sets whose geometric realization restricts to the following diagram.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      \Radstar_g(m,1)  \ar{r}{\Pol} \ar[hook]{d} \& \Polspcstar(m,1) \ar[hook]{d} \\
      Bar^\star_g(m,1) \ar{r}{\Pol}              \& \Polspcstar
    \end{tikzcd}
  \end{align}
\end{proposition}

\begin{proof}
  Regarding $\SymGrstarn$ as a bi-simplicial set that is constant in one direction,
  the projection to the last factor $S_{p,q} = (\SymGrununn[p])^{q} \times \SymGrstarn[p] \to \SymGrstarn[p]$ is clearly a map of bi-simplicial sets.
  Its geometric realization is the map $\Pol \colon Bar^\star_g(m,1) \to \Polspcstar$.
  
  The space of radial slit configurations $\Rad_g(m,1)$ is an open subspace in $Bar^\star_g(m,1)$ consisting of all open simplices $(\omega_p; \sigma_0, \ldots, \sigma_q; \sigma^\star)$ that are proper.
  In particular, the number of non-degenerate outgoing boundaries is
  \begin{align}
    m(\omega_p; \sigma_0, \ldots, \sigma_q; \sigma^\star) = \ncyc(\sigma^\star) = m \,.
  \end{align}
  Therefore, by definition of $\Polspcstar(m,1)$, the map $\Pol \colon Bar^\star_g(m,1) \to \Polspcstar$ restricts to $\Pol \colon \Radstar_g(m,1) \to \Polspcstar(m,1)$.
\end{proof}

Having the geometric meaning of the map $\Pol$ in mind, it is called the restriction to the outgoing boundaries.

\begin{definition}
  \label{definition:restriction_to_the_outgoing_boundaries}
  The map $\Pol$ is the \defn{restriction to the outgoing boundaries}.
\end{definition}

Having the lengths of a polygon and the space of normalized polygons at hand,
the definition of the length of the outgoing boundaries and the space of radial slit configurations with normalized outgoing boundaries is straightforward.

\begin{definition}
  \label{definition:length_of_the_boundaries_rad}
  Let $n \ge 1$, $m \ge 1$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
  The \defn{length of the boundaries} is the composition
  \begin{align}
    l \colon \Radstar_g(m,1) \xr{\Pol} \Polspcstar(m,1) \xr{l} \interior{\Delta^{m-1}}
  \end{align}
  for $\star \in \{ \paen, \unen \}$ respectively
  \begin{align}
    l \colon \Radstar_g(m,1) \xr{\Pol} \Polspcstar(m,1) \xr{l} \interior{\Delta^{m-1}} /_{\SymGrn[m-1]}
  \end{align}
  for $\star \in \{ \paun, \unun \}$.
\end{definition}

\begin{definition}
  \label{definition:rad_normalized_boundaries}
  Let $m \ge 1$, $g \ge 0$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
  The \defn{space of radial slit configurations of genus $g$ with (un)parametrized (un)enumerated normalized boundaries} is the subspace
  \begin{align}
    \RadstarN_g(m,1) := l^{-1}( {\textstyle \frac{1}{m}, \ldots, \frac{1}{m}} ) \subset \Radstar_g(m,1)
  \end{align}
\end{definition}

\begin{remark}
  Our notation agrees with the definition in \cite[Section 9]{Boedigheimer2006}.
\end{remark}

Restricting the maps $\Pol$ to the spaces of normalized polygons gives the following corollary.

\begin{corollary}
  The maps $\Pol$ restrict as follows.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      \RadstarN_g(m,1) \ar{r}{\Pol} \ar[hook]{d} \& \PolspcstarN(m,1) \ar[hook]{d} \\
      \Radstar_g(m,1)  \ar{r}{\Pol}              \& \Polspcstar(m,1)
    \end{tikzcd}
  \end{align}
\end{corollary}

\begin{proposition}
  Let $g \ge 0$ and $m \ge 1$.
  In the parametrized case, the restriction to the outgoing boundaries defines a map of operads $\Pol \colon \Radpaen_g(m,1) \to \Polspcpaen(m,1)$.
\end{proposition}

\begin{proof}
  It is well known that glueing surfaces along their parametrized boundaries defines the surface operad $\sqcup_{g,m} \fM^\paen_g(m,1)$.
  Recall that the space of parametrized and enumerated polygons is a space of local isometries from one parametrized circle to many parametrized circles.
  The composition of local isometries defines an operad structure on $\sqcup_m \Polspcpaen(m,1)$.
  Using Bödigheimer's spaces of radial slit configurations, it is clear that glueing surfaces along boundaries agrees with composing the corresponding local isometries.
\end{proof}

\begin{theorem}
  \label{theorem:classifying_map_from_rad}
  Let $m \ge 1$, $g \ge 0$.
  Forgetting the parametrization of the outgoing boundaries defines an $m$-dimensional torus bundle $\pi^\paen_\unen \colon \Radpaen_g(m,1) \to \Radunen_g(m,1)$ that is classified by
  the restriction to the outgoing boundaries:
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      \Radpaen_g(m,1) \ar{r}{\Pol} \ar{d}{\pi^\paen_\unen} \& \Polspcpaen(m,1) \simeq EU(1)^m \ar{d}{\pi^\paen_\unen} \\
      \Radunen_g(m,1) \ar{r}{\Pol}                         \& \Polspcunen(m,1) \simeq BU(1)^m
    \end{tikzcd}
  \end{align}
  Moreover, denoting the stabilization map by $\varphi$, it is $\Pol = \Pol \circ \varphi$.
\end{theorem}

\begin{proof}
  By construction, it is clear that $\Pol \colon \Radpaen_g(m,1) \to \Polspcpaen(m,1)$ is equivariant with respect to the torus action.
  Consequently, $\Radpaen_g(m,1) \to \Radunen_g(m,1)$ is classified by $\Pol \colon \Radunen_g(m,1) \to \Polspcunen(m,1) \simeq BU(1)^m$.
  
  Observe that $\sqcup_{g,m} \Radunen_g(m,1)$ resp.\ $\sqcup_m \Polspcunen(m,1)$ is an algebra over $\sqcup_{g,m} \Radpaen_g(m,1)$ resp.\ $\sqcup_m \Polspcpaen(m,1)$ and
  that the restriction to the outgoing boundaries is a map of algebras.
  The stabilization map is defined by glueing a surface of genus one with two parametrized boundary components to a given surface, i.e.,
  it is defined as the operadic action of a single point $S \in \Radpaen_1(1,1)$ on $\sqcup_{g,m} \Radunen_g(m,1)$.
  \begin{figure}[ht]
    \centering
    \begin{tikzpicture}[line width=1pt, scale=0.8]
      % draw grid
      \foreach \i in {2,...,3} \draw[color=black!50, dashed] (0, 0) circle (\i);
      \draw[color=black!50, dashed] (  0 : 1) -- (  0 : 4);
      \draw[color=black!50, dashed] (120 : 1) -- (120 : 2);
      \draw[color=black!50, dashed] (240 : 1) -- (240 : 2);
      
      % draw inner and outer circles
      \draw[color=black, line width=2pt] (0, 0) circle (1);
      \draw[color=black, line width=2pt] (0, 0) circle (4);
      
      % draw parametrization points
      \filldraw[color=green] (0 : 1) circle (4pt);
      \filldraw[color=blue]  (0 : 4) circle (4pt);
      
      % draw slits
      \draw[color=black] (119 : 3) -- (119 : 4);
      \draw[color=black] (121 : 2) -- (121 : 4);
      \draw[color=black] (239 : 3) -- (239 : 4);
      \draw[color=black] (241 : 2) -- (241 : 4);
      \filldraw (119 : 3) circle (2pt);
      \filldraw (121 : 2) circle (2pt);
      \filldraw (239 : 3) circle (2pt);
      \filldraw (241 : 2) circle (2pt);
    \end{tikzpicture}
    \caption{
      \label{figure:stabilization_map_via_composition_of_radial_slit_configurations}
      The radial slit configuration $S$.
      The parametrization point of the incoming boundary is green and the parametrization on the outgoing boundary is blue.
    }
  \end{figure}
  Here, we regard $S$ as the radial slit picture corresponding to the barycenter of the $(2,2)$-simplex with combinatorial type
  $(\omega_2; \cycle{ 0\ 1\ 2 }: \cycle{ 0\ 2 }\cycle{ 1 }: \cycle{ 0\ 1\ 2 }; \cycle{ 0\ 1\ 2})$,
  shown in Figure \ref{figure:stabilization_map_via_composition_of_radial_slit_configurations}.
  Observe that $\Pol(S) = (\frac{1}{3}, \frac{1}{3}, \frac{1}{3}) \in \cycle{ 0\ 1\ 2 } \times \Delta^2 \subset \Polspcpaen(1,1)$ represents the local isometry $\id \colon \bS^1 \to \bS^1$.
  Since $\Pol$ is a map of algebras, it follows that
  \begin{align}
    \Pol(\varphi(F)) = \Pol( F \cup S ) = \Pol(F) \circ \Pol(S) = \Pol(F) \circ \id = \Pol(F) \,.
  \end{align}
\end{proof}


\begin{remark}
  Recall that, by Loojienga, there is a graded isomorphism of algebras over $H^\ast( \fM^\paen_\infty(m,n); \bZ )$
  \begin{align}
    H^\ast( \fM^\paen_\infty(m,n); \bZ )[ u_1, \ldots, u_m] \cong H^\ast( \fM^\unen_\infty(m,n); \bZ )
  \end{align}
  where $u_i$ denote the characteristic classes of the $m$-dimensional torus bundle.
  Using the explicit cellular descriptions of $\Rad$ and $\Pol$, the above Theorem allows explicit computations in low degrees.
\end{remark}
