\noindent
In \cite{Boedigheimer19901, Boedigheimer2006} Bödigheimer uses the theory of harmonic potentials to construct models for the moduli spaces in question.
% \footnote{For technical reasons, we exclude moduli spaces of surfaces with non-negative Euler characteristic.}.
For the convenience of the reader and to fix our notation, we recall some of the details of the construction of one of his models.
It is called the \defn{space of radial slit configurations}.
In Section \ref{section:the_space_of_radial_slit_configurations_unun}, we discuss the construction of the model $\Rad^\unun_g(m,1) \simeq \fM^\unun_g(m,1)$.
The construction of the models
$\Rad^\unun_g(m,1) \simeq \fM^\unun_g(m,1)$, $\Rad^\paen_g(m,1) \simeq \fM^\paen_g(m,1)$, $\Rad^\paun_g(m,1) \simeq \fM^\paun_g(m,1)$ and $\Rad^\unen_g(m,1) \simeq \fM^\unen_g(m,1)$
is similar and is discusses in fewer details in Section \ref{section:the_space_of_radial_slit_configurations_other}.
These spaces of radial slit configurations admit at least two compactifications, see below.

Bödigheimer introduces the \defn{bar compactification} in \cite{Boedigheimer2007}.
In this thesis, we denote the bar compactification of $\fM^\star_g(m,1)$ by $Bar^\star_g(m,1)$.
It is seen as the completion of the combinatorial aspects of radial slit configurations and as such it is a space made from subcomplexes of bar complexes of all symmetric groups,
see Section \ref{section:compactification_bar}, Appendix \ref{appendix:factorable_groups} and also \cite{Visy201011} and \cite{BoesHermann2014}.
The bar compactification has been used by various authors to compute the unstable homology of the moduli spaces of Riemann surfaces for small parameters $m \ge 1$ and $g \ge 0$,
see \cite{Ehrenfried1997,Abhau200501,AbhauBoedigheimerEhrenfried2008,Mehner201112,Wang201102,BoesHermann2014} and Section \ref{section:the_generator_f_is_non_trivial}.

The \defn{harmonic compactification} is introduced by Bödigheimer in \cite{Boedigheimer2006}.
In this thesis, we denote the harmonic compactification of $\fM^\star_g(m,1)$ by $\ov{\fM}^\star_g(m,1)$.
It is seen as the completion of the geometric aspects of the radial slit configurations,
see Section \ref{section:compactification_harmonic} and also \cite{Boedigheimer2006} and \cite{EgasKupers2014}.
The study of the harmonic compactification has important applications to the study of string topology,
see Section \ref{section:string_topology} and Section \ref{section:on_the_homotopy_type_of_the_space_of_sullivan_diagrams}.
We study the homotopy type of the harmonic compactification in Chapters \ref{chapter:sullivan_diagrams}, \ref{chapter:homotopy_type_of_NSD} and \ref{chapter:homotopy_type_of_SDspaen}.

In Section \ref{section:radial_slit_configurations_normalized}, we use the bar compactification and the spaces of polygons to classify the $m$-dimensional torus bundle $\Radpaen_g(m,1) \to \Radunen_g(m,1)$.
\begin{theoremwithfixednumber}{\ref{theorem:classifying_map_from_rad}}
  Let $m \ge 1$, $g \ge 0$.
  Forgetting the parametrization of the outgoing boundaries defines an $m$-dimensional torus bundle $\pi^\paen_\unen \colon \Radpaen_g(m,1) \to \Radunen_g(m,1)$ that is classified by
  the restriction to the outgoing boundaries, see Definition \ref{definition:restriction_to_the_outgoing_boundaries}:
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      \Radpaen_g(m,1) \ar{r}{\Pol^\paen_g} \ar{d}{\pi^\paen_\unen} \& \Polspcpaen(m,1) \simeq EU(1)^m \ar{d}{\pi^\paen_\unen} \\
      \Radunen_g(m,1) \ar{r}{\Pol^\unen_g}                         \& \Polspcunen(m,1) \simeq BU(1)^m
    \end{tikzcd}
  \end{align}
  Denoting the stabilization map by $\varphi_g \colon \Radstar_g(m,1) \to \Radstar_{g+1}(m,1)$, it is $\Pol^\star_g = \Pol^\star_{g+1} \circ \varphi_g$.
\end{theoremwithfixednumber}
Furthermore, in Section \ref{section:radial_slit_configurations_normalized}, we discuss the space of radial slit configurations with normalized boundaries ---
which is a model for the moduli space of Riemann surfaces whose outgoing boundaries have equal circumferences.
Their harmonic compactifications will be studied in Chapter \ref{chapter:homotopy_type_of_NSD} and Chapter \ref{chapter:homotopy_type_of_SDspaen}.
