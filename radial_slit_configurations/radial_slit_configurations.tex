\section{The space of radial slit configurations for \texorpdfstring{$\fM^\unun_g(m,1)$}{M\_g(m,1)}}
\label{section:the_space_of_radial_slit_configurations_unun}
In this section, we discuss the construction of Bödigheimer's radial model $\Rad^\unun_g(m,1) \simeq \fM^\unun_g(m,1)$.
Given a Riemann surface $F \in \fM^\unun_g(m,1)$, we use the flow lines of a unique harmonic potential function $f \colon F \to [0,1]$ to
associate a configuration of radial slits on a annulus with $F$,
see Subsection \ref{section:the_space_of_radial_slit_configurations_unun:from_RS_to_slit_configuration}.
The combinatorics of such a radial slit configuration are encoded by a sequence of permutations that are subject to certain conditions,
see Subsection \ref{section:the_space_of_radial_slit_configurations_unun:combinatorial_type}.
The space of all radial slit configurations satisfying these conditions constitute the space of radial slit configurations which is
homeomorphic to the moduli space via the Hilbert uniformization map,
see Subsection \ref{section:the_space_of_radial_slit_configurations_unun:the_space_of_radial_slit_configurations_unun}.
For a topologist, there are two canonical compactifications of the space of radial slit configurations.
Most of their features are described at the end of Subsection \ref{section:the_space_of_radial_slit_configurations_unun:the_space_of_radial_slit_configurations_unun} and
in Sections \ref{section:compactification_bar} and \ref{section:compactification_harmonic}.
The harmonic compactification is studied in greater detail via spaces of Sullivan diagrams in
Chapter \ref{chapter:sullivan_diagrams}, Chapter \ref{chapter:homotopy_type_of_NSD} and Chapter \ref{chapter:homotopy_type_of_SDspaen}.


\subsection{From a Riemann surface to a radial slit configuration}
\label{section:the_space_of_radial_slit_configurations_unun:from_RS_to_slit_configuration}
Given a Riemann surface $F$ with $n \ge 1$ parametrized and enumerated incoming and $m \ge 1$ unparametrized and unenumerated outgoing boundaries,
we choose a harmonic function $f \colon F \to [0,1]$ that is equal to $0$ on the incoming boundaries and one equal to $1$ on the outgoing boundaries.
The harmonic potential $f$ is a solution to a Dirichlet problem.
It always exists and is uniquely determined by $F$ and its values on the boundary components,
see e.g.\ \cite[Theorem 22.17]{Forster1981} or \cite[Theorem I.25]{Tsuji1959}.
Moreover, the harmonic potential has finitely many critical points and each of them is in the interior of $F$.
The union of all critical points and of all gradient flow lines that leave a critical point define a possibly disconnected graph $K \subset F$,
which is called the \defn{unstable critical graph}.
It is a subgraph of the \defn{complete critical graph}%
\footnote{In Bödigheimer's original definition of the critical graph \cite[Section 6.2]{Boedigheimer2006},
the flow lines entering or leaving a parametrization point were not included in the complete critical graph.}
which consists of all critical points and of all flow lines that leave or enter a critical point or a parametrization point $P \in \del_{in}F$.
The preimage of a critical resp.\ regular value is a \defn{critical level} resp.\ \defn{regular level}.
Observe that the union of the incoming boundaries is precisely $f^{-1}(0)$ whereas the union of the outgoing boundaries is precisely $f^{-1}(1)$ and
for our purposes it is useful to call these levels also critical levels.
The critical levels are ordered by their values, i.e., a critical level $f^{-1}(x)$ is smaller than a critical level $f^{-1}(y)$ if and only if $x < y$.
Let us denote the number of critical levels by $q+2 \ge 2$ and index the critical levels by increasing order, i.e., the critical levels are $\fl_0 < \ldots < \fl_{q+1}$.
By construction $\fl_0$ is the union of all incoming boundaries and $\fl_{q+1}$ is the union of all outgoing boundaries.
We say that two critical levels $\fl_i < \fl_j$ are \defn{consecutive} if and only if $j = i+1$.
Given two consecutive critical levels $\fl_i = f^{-1}(x_i)$ and  $\fl_{i+1} = f^{-1}(x_{i+1})$, the $i$-th intermediate (regular) level is $\fr_i := f^{-1}( \frac{x_i + x_{i+1}}{2} )$.

Observe that the region between two consecutive critical levels is a disjoint union of open cylinders.
Observe further that the region between the levels $\fl_0$ and $\fr_i$ for some $0 < i \le q$ is a closed submanifold of $F$
with Riemann structure, with $n$ enumerated and parametrized incoming boundaries and
with some number of outgoing boundaries that do not come with a natural parametrization or enumeration.

After removing the unstable critical graph, we are left with an open submanifold of $F$.
The process of ``pulling the remaining flow lines straight'' defines a biholomorphic map whose image is a disjoint union of $n$ annuli $A_1, \ldots, A_n$ with radial slits removed.
More precisely, the inner radius of each annulus is $1 = \exp(0)$ and each outer radius is $\exp(1)$.
Every flow line is mapped on a radiant, each incoming boundary $C^-_i \subset \del_{in}F$ is mapped to the inner boundary of the annuli $A_i$ and
the parametrization point $P^-_i \in C^-_i$ is send to the complex number $1 \in A_i$.
See Figure \ref{figure:harmonic_function_intro} for an example.
\begin{figure}[ht]
  \centering
  \inkpic[.7\columnwidth]{harmonic_function}
  \caption{\label{figure:harmonic_function_intro}
    On the left, we sketch the flow lines of a harmonic function defined on a Riemann surface $F \in \fM^\unun_1(1,1)$ of genus one with one incoming and one outgoing boundary curve.
    The critical points are black, the paramatrization point on the incoming boundary is green, the flow lines leaving a critical point are red and all other flow lines are blue.
    On the right, we sketch the image of $F$ under the process described above.
    It is an annulus with red pairs of radial slit removed and one pair is shorter than the other.}
\end{figure}


\subsection{The combinatorial type of a radial slit configuration}
\label{section:the_space_of_radial_slit_configurations_unun:combinatorial_type}
Now, we discuss the combinatorics of the distribution of the pairs of radial slits over the annuli.
For the sake of simplicity, we treat the case $F \in \fM^\unun_g(m,1)$ at first, the other cases are treated later.
By construction, a harmonic potential on a Riemann surface $F$ induces a conformal map between $F - K$ and an annulus with pairs of radial slits removed.
The complete critical graph and the critical levels give a grid of radial and concentric lines on this annulus.
Recall that, by our definition of the complete critical graph, the positive real axis%
\footnote{The intersection of the positive real axis with the annulus corresponds to the flow line that leaves the parametrization point of the incoming boundary.}
is part of the grid.
The radial lines give $p+1$ radial segments.
Using the clockwise orientation of the annulus, we number these segments by $0, \ldots, p$ starting with the unique segment that begins on the positive real line.
The critical levels give $q+1$ annuli of increasing size.
We number the annuli by $0, \ldots, q$ starting with the smallest one.
The (bended) rectangle in the $i$-th sector of the $j$-th annulus is denoted by $R_{i,j}$.
These rectangles correspond to (metric) rectangles on the surface such that the top edge of $R_{i,j}$ coincides with the bottom edge of $R_{i+1,j}$ and
the right edge of $R_{i,j}$ coincides with the left edge of $R_{i,\sigma_i(j)}$ for some permutation $\sigma_i \in \SymGrn[p]$.
The sequence of permutations $(\sigma_0: \ldots: \sigma_q)$ is the \defn{combinatorial type} of such a configuration of radial slits.
In Figure \ref{figure:combinatorial_type}, a radial slit configuration of combinatorial type
$\Sigma = (\langle 0\ 1\ 2\ 3\ 4\rangle: \langle 0\ 3\ 4\rangle \langle 1\ 2\rangle: \langle 0\ 3\ 2\ 1\ 4\rangle)$ is shown.
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[line width=1pt, scale=0.8]
  % draw grid
  \foreach \i in {2,...,3} \draw[color=black!50, dashed] (0, 0) circle (\i);
  \foreach \i in {0,...,4} \draw[color=black!50, dashed] (\i * 72 : 1) -- (\i * 72 : 4);

  % draw inner and outer circles
  \draw[color=black, line width=2pt] (0, 0) circle (1);
  \draw[color=black, line width=2pt] (0, 0) circle (4);
  
  % highlight some of the rectangles
  \draw node at (350 : 5) {$R_{0,0}$};
  \draw[color=black!50] (350 : 4.5) -- (350 : 1.5);
  \draw node at (330 : 5) {$R_{1,0}$};
  \draw[color=black!50] (330 : 4.5) -- (330 : 2.5);
  \draw node at (310 : 5) {$R_{2,0}$};
  \draw[color=black!50] (310 : 4.5) -- (310 : 3.5);
  \draw node at (130 : 5) {$R_{0,3}$};
  \draw[color=black!50] (130 : 4.7) -- (130 : 1.5);
  \draw node at (110 : 5) {$R_{1,3}$};
  \draw[color=black!50] (110 : 4.7) -- (110 : 2.5);
  \draw node at (90 : 5) {$R_{2,3}$};
  \draw[color=black!50] (90 : 4.7) -- (90 : 3.5);

  % draw slits
  \draw[color=black] (72 : 3) -- (72 : 4);
  \draw[color=black] (2*72 : 2) -- (2*72 : 4);
  \draw[color=black] (3*72 : 3) -- (3*72 : 4);
  \draw[color=black] (4*72 : 2) -- (4*72 : 4);
  \filldraw (72 : 3) circle (2pt);
  \filldraw (2*72 : 2) circle (2pt);
  \filldraw (3*72 : 3) circle (2pt);
  \filldraw (4*72 : 2) circle (2pt);
  \end{tikzpicture}
  \caption{
    \label{figure:combinatorial_type}
    A radial slit configuration whose grid is highlighted by dashed gray lines.
    Some of the (bended) rectangles are indicated.
    The combinatorial type of this radial slit configuration is
    $\Sigma = (\sigma_0: \sigma_1: \sigma_2)$ with
    $\sigma_0 = \langle 0\ 1\ 2\ 3\ 4\rangle$, $\sigma_1 = \langle 0\ 3\ 4\rangle \langle 1\ 2\rangle$ and $\sigma_2 = \langle 0\ 3\ 2\ 1\ 4\rangle$.
  }
\end{figure}

The combinatorial type is subject to three conditions:
The parametrized incoming and the outgoing boundaries are encoded correctly,
see \eqref{radial_slit_configuration:combinatorial_type:incoming} and \eqref{radial_slit_configuration:combinatorial_type:outgoing};
The combinatorial type encodes the  Euler characteristic of the surface correctly,
see \eqref{radial_slit_configuration:combinatorial_type:Euler_characteristic};
The configuration is non-degenerate in the simplicial sense,
see \eqref{radial_slit_configuration:combinatorial_type:simplicially_non_degenerate_vertical},
\eqref{radial_slit_configuration:combinatorial_type:simplicially_non_degenerate_horizontal} and the subsequent paragraphs.
More precisely:
The region between the incoming boundary and the subsequent critical level $\fl_1$ is a cylinder (that comes with a parametrization of its incoming boundary), i.e.,
$\sigma_0$ is the long cycle
\begin{align}
  \label{radial_slit_configuration:combinatorial_type:incoming}
  \sigma_0 = \omega_p = \langle 0\ 1\ \ldots\ p\rangle \,;
\end{align}
The region between the outgoing boundaries and the preceding critical level $\fl_q$ is a disjoint union of $m$ cylinders (that do not come with a natural ordering or a parametrization), i.e.,
$\sigma_q$ has exactly $m$ cycles
\begin{align}
  \label{radial_slit_configuration:combinatorial_type:outgoing}
  \ncyc(\sigma_q) = m \,.
\end{align}
Using the ``triangulation'' of $F$ in terms of the rectangles $R_{i,j}$, observe that the curvature of $F$ is concentrated in the corners of these rectangles.
Observe further that the corner points in $F$ are in one to one correspondence to the cycles of the permutations
$\tau_i := \sigma_i \sigma_{i-1}^{-1}$ and each cycle $c$ of length $l$ contributes with an angle $l\cdot 2\pi$.
Consequently, each cycle $c$ of length $l$ contributes to the Euler characteristic with $-(l-1) = -N(c)$, where $N(c)$ denotes the norm%
\footnote{The norm of a permutation is the minimal word length in the generating set of all transpositions.}
of $c$.
Therefore, the negative of the Euler characteristic of $F$ is exactly
\begin{align}
  \label{radial_slit_configuration:combinatorial_type:Euler_characteristic}
  - \chi(F) = N(\tau_1) + \ldots + N(\tau_q) = 2g + m + n - 2 \,.
\end{align}
By construction, neither does our grid give superflous annuli, i.e.,
\begin{align}
  \label{radial_slit_configuration:combinatorial_type:simplicially_non_degenerate_vertical}
  \text{$\sigma_{i} \neq \sigma_{i+1}$ for all $i$}
\end{align}
nor does our grid give superflous radial segments, i.e.,
\begin{align}
  \label{radial_slit_configuration:combinatorial_type:simplicially_non_degenerate_horizontal}
  \text{for each $1 \le j+1 \le p$ there is an $i$ with $\sigma_i(j) \neq j+1$.}
\end{align}
The first permutation $\sigma_0$ is always the long cycle by \eqref{radial_slit_configuration:combinatorial_type:incoming}.
Therefore, the sequence of permutations $(\sigma_0: \ldots: \sigma_q)$ carries the same information as the sequence
\begin{align}
  (\tau_1 \mid \ldots \mid \tau_q)
  \quad\quad\text{with}\quad\quad
  \tau_i := \sigma_i \sigma_{i-1}^{-1} \,.
\end{align}
The former is called the \defn{homogeneous notation} and the latter is called the \defn{inhomogeneous notation} of a given combinatorial type.


\subsection{The spaces of radial slit configurations \texorpdfstring{$\Rad^\unun_g(m,1)$}{Rad\_g(m,1)}}
\label{section:the_space_of_radial_slit_configurations_unun:the_space_of_radial_slit_configurations_unun}
Using the endpoints of the slits of a radial slit configuration as (bi-simplicial) barycentric coordinates,
it is immediate that the subspace of all Riemann surfaces whose associated radial slit configuration has a fixed combinatorial type
$(\sigma_0: \ldots: \sigma_q)$ forms an open bi-simplex $\interior{\Delta^p \times \Delta^q} \subset \fM^\unun_g(m,1)$.

Given a radial slit configuration $C$ of combinatorial type $\Sigma = (\sigma_0: \ldots: \sigma_q)$ with bi-simplicial coordinates $(s,t) \in \interior{\Delta^p \times \Delta^q}$,
we write $C = (\Sigma; s; t)$.
Letting the $i$-th coordinate in $s$ tend to zero, the limit point is called the \defn{$i$-th horizontal face} of $(\Sigma;s;t)$ if it exists.
Similarly, letting the $j$-th coordinate in $t$ tend to zero, the limit point is called the \defn{$j$-th vertical face} of $(\Sigma;s;t)$ if it exists.
Assuming that the $j$-th vertical face of $(\Sigma; s;t)$ exists,
the resulting radial slit configuration is obtained by contracting the $j$-th annulus of $(\Sigma;s;t)$ and the combinatorial type is
\begin{align}
  \label{radial_slit_configuration:combinatorial_type:vertical_face}
  d'_j(\sigma_0: \ldots: \sigma_q)  &= (\sigma_0: \ldots: \hat{\sigma_j}: \ldots: \sigma_q) \,.\\
  \intertext{
    Assuming that the $i$-th horizontal face of $(\Sigma;s;t)$ exists,
    the resulting radial slit configuration is obtained by contracting the $i$-th radial segment and its combinatorial type is
  }
  \label{radial_slit_configuration:combinatorial_type:horizontal_face}
  d''_i(\sigma_0: \ldots: \sigma_q) &= (D_i\sigma_0: \ldots: D_i\sigma_q) \,.
\end{align}
It is clear that not all faces of a given radial slit configuration exist in $\fM^\unun_g(m,1)$
(e.g., the combinatorial type of the faces $d'_0$ or $d'_q$ will violate condition
\eqref{radial_slit_configuration:combinatorial_type:incoming} or \eqref{radial_slit_configuration:combinatorial_type:outgoing}).
The radial slit configurations whose combinatorial type satisfies the conditions
\eqref{radial_slit_configuration:combinatorial_type:incoming},
\eqref{radial_slit_configuration:combinatorial_type:outgoing},
\eqref{radial_slit_configuration:combinatorial_type:Euler_characteristic},
\eqref{radial_slit_configuration:combinatorial_type:simplicially_non_degenerate_vertical} and
\eqref{radial_slit_configuration:combinatorial_type:simplicially_non_degenerate_horizontal}
constitute the \defn{space of radial slit domains} denoted by $\Rad^\unun_g(m,1)$.
Its topology is such that sending a Riemann surface to its radial slit configuration defines a homeomorphism
\begin{align}
  \Phi \colon \fM^\unun_g(m,1) \cong \Rad^\unun_g(m,1) \,.
\end{align}
This homeomorphism is called the \defn{Hilbert uniformization map}, c.f.\ \cite[Theorem 1.1]{Boedigheimer2006}.

For a topologist, there are at least two canonical completions of the space of radial slit domains as a \defn{relative multi-simplicial manifold}, i.e.,
there are two canonical choices of a space $R := R_g(m,1)$, such that
(1) the space $R$ is the realization of a (finite) multi-simplicial set and
(2) $\Rad \subset R$ is open and dense and the complement $R' := R - \Rad$ is a codimension one subcomplex.
Then, (3) $H_\ast( \Rad; \bZ)$ is Poincaré dual to $H^\ast( R, R'; \cO)$ for an orientation system $\cO$.
The two completions are called the \defn{harmonic compactification} and the \defn{bar compactification} of the moduli space.
The most important aspects of these compactifications are discussed in Section \ref{section:compactification_bar} and Section \ref{section:compactification_harmonic}.
Let us state their most important features here before discussing their details in the subsequent subsections.

The harmonic compactification, introduced in \cite{Boedigheimer1993Harmonic} see also \cite[Section 8]{Boedigheimer2006},
is obtained from $\Rad^\unun_g(m,1)$ by adding specific degenerate surfaces which admit a real function that is harmonic on its smooth points.
The study of the harmonic compactification has important applications to the study of string topology, see Section \ref{section:string_topology}.

The bar compactification, introduced in \cite{Boedigheimer2007}, admits a more elegant presentation.
Adding all iterated vertical and horizontal faces of all combinatorial types that occur in
$\Rad^\unun_g(m,1)$ yields a multi-simplicial space with finitely many non-degenerate cells.
We call this space the bar compactification of the moduli space because the vertical faces
\eqref{radial_slit_configuration:combinatorial_type:vertical_face} make a connection to bar resolutions of all symmetric groups immediate.
The bar compactification and its relations to group homology are used by several authors to compute the integral homology of
the moduli spaces $\fM^\unun_g(m,1)$ for small parameters $g$ and $m$, see Section \ref{section:unstable_homology}.


\section{The space of radial slit configurations for \texorpdfstring{$\fM^\unen_g(m,1)$}{M\_g(m,1)}, \texorpdfstring{$\fM^\paun_g(m,1)$}{M\_g(m,1)} and \texorpdfstring{$\fM^\paen_g(m,1)$}{M\_g(m,1)}}
\label{section:the_space_of_radial_slit_configurations_other}
Considering the moduli spaces $\fM^\unen_g(m,n)$, $\fM^\paun_g(m,n)$ resp.\ $\fM^\paen_g(m,n)$ with enumerated or parametrized outgoing boundaries,
the theory of harmonic potentials is used to establish a homeomorphism between the moduli space and a space of radial slit configurations
$\Rad^\unen_g(m,n)$, $\Rad^\paun_g(m,n)$ resp.\ $\Rad^\paen_g(m,n)$.
As before, each point in the respectively space of radial slit configurations has a combinatorial type and the points of the same combinatorial type form an open multi-simplex.
We discuss the combinatorial types occuring in $\Rad^\unen_g(m,1)$, $\Rad^\paun_g(m,1)$ resp.\ $\Rad^\paen_g(m,1)$ seperately.


\subsection{The space of radial slit configurations \texorpdfstring{$\Rad^\unen_g(m,1)$}{Rad\_g(m,1)}}
\label{section:the_space_of_radial_slit_configurations_unen}
Using the same techniques as before, we associate to a Riemann surface $F \in \fM^\unen_g(m,1)$ with harmonic potential a radial slit configuration as follows.
The union of all critical points and of all gradient flow lines that leave a critical point constitute the \defn{unstable critical graph} $K$ and 
the \defn{complete critical graph} consists of all critical points and of all flow lines that leave or enter a critical point or a parametrization point $P \in \del_{in}F$.
To $F - K$, we associate a radial slit configuration by pulling the remaining flow lines straight.
The complete critical graph and the critical levels give a decomposition of the radial slit configuration (or the surface) into (bended) rectangles.
The identifications of the lower and upper edges of these rectangles give a sequence of permutations $(\sigma_0: \ldots: \sigma_q)$ with $\sigma_0, \ldots, \sigma_q \in \SymGrn[p]$.
The enumerated outgoing boundaries correspond to an enumeration of the cycles of $\sigma_q$ which is an enumerated permutation
$\sigma^\unen \in \SymGrunenn[p]$ with $\sigma_q = \pi^\unen_\unun(\sigma^\unen)$.
The combinatorial type is $(\sigma_0: \ldots: \sigma_q; \sigma^\unen)$ and $\Rad^\unen_g(m,1)$ consists of all radial slit configurations whose combinatorial type
$(\sigma_0: \ldots: \sigma_q; \sigma^\unen)$ is subject to the conditions
\eqref{radial_slit_configuration:combinatorial_type:incoming},
\eqref{radial_slit_configuration:combinatorial_type:outgoing},
\eqref{radial_slit_configuration:combinatorial_type:Euler_characteristic},
\eqref{radial_slit_configuration:combinatorial_type:simplicially_non_degenerate_vertical} and
\eqref{radial_slit_configuration:combinatorial_type:simplicially_non_degenerate_horizontal}.
Its topology is such that the points in $\Rad^\unen_g(m,1)$ with the same combinatorial type $(\sigma_0: \ldots: \sigma_q; \sigma^\unen)$ form an open bi-simplex.
If the $j$-th vertical face resp.\ the $i$-th horizontal face of a bi-simplex of combinatorial type $(\sigma_0: \ldots: \sigma_q; \sigma^\unen)$ exsits in $\Rad^\unen_g(m,1)$,
the combinatorial type of the resulting face is as follows.
\begin{align}
  \label{radial_slit_configuration_unen:combinatorial_type:vertical_face}
  d'_j(\sigma_0: \ldots: \sigma_q; \sigma^\unen)  &= (\sigma_0: \ldots: \hat{\sigma_j}: \ldots: \sigma_q; \sigma^\unen) \\
  \label{radial_slit_configuration_unen:combinatorial_type:horizontal_face}
  d''_i(\sigma_0: \ldots: \sigma_q; \sigma^\unen) &= (D_i\sigma_0: \ldots: D_i\sigma_q; D_i\sigma^\unen)
\end{align}
Sending a Riemann surface to its radial slit configuration defines a homeomorphism
\begin{align}
  \Phi \colon \fM^\unen_g(m,1) \cong \Rad^\unen_g(m,1) \,.
\end{align}
This homeomorphism is called the \defn{Hilbert uniformization map}.
Moreover, $\Rad^\unen_g(m,1)$ admits a bar compactification and an harmonic compactification.


\subsection{The space of radial slit configurations \texorpdfstring{$\Rad^\paun_g(m,1)$}{Rad\_g(m,1)}}
\label{section:the_space_of_radial_slit_configurations_paun}
Again, we associate to a Riemann surface $F \in \fM^\unen_g(m,1)$ with harmonic potential a radial slit configuration as follows.
The union of all critical points and of all gradient flow lines that leave a critical point constitute the \defn{unstable critical graph} $K$ and 
the \defn{complete critical graph} consists of all critical points and
of all flow lines that leave or enter a critical point or a parametrization point $P \in \del_{in}F \cup \del_{out}F$.
To $F - K$, we associate a radial slit configuration by pulling the remaining flow lines straight.
The complete critical graph and the critical levels give a decomposition of the radial slit configuration (or the surface) into (bended) rectangles.
The identifications of the lower and upper edges of these rectangles give a sequence of permutations
$(\sigma_0: \ldots: \sigma_q)$ with $\sigma_0, \ldots, \sigma_q \in \SymGrn[p]$.
The parametrization of the unenumerated outgoing boundaries correspond to an parametrization of the cycles of $\sigma_q$ which
is an parametrized permutation $\sigma^\paun \in \SymGrpaunn[p]$ with $\sigma_q = \pi^\paun_\unun(\sigma^\paun)$.
The combinatorial type is $(\sigma_0: \ldots: \sigma_q; \sigma^\paun)$
In order to deduce the conditions the combinatorial type satisfies, observe that the case, where all parametrization points are disjoint from the critical graph, is generic.
In this case, whenever a parametrization point is inbetween the radial segments $j$ and $j+1$,
the parametrized permutation has $j+1$ as the distinguished element in its cycle and all permutation $\sigma_i$ satisfy $\sigma_i(j) = j+1$.
Now it is straight forward to check that $\Rad^\paun_g(m,1)$ consists of all radial slit configurations whose combinatorial type
$(\sigma_0: \ldots: \sigma_q; \sigma^\paun)$ is subject to the conditions
\eqref{radial_slit_configuration:combinatorial_type:incoming},
\eqref{radial_slit_configuration:combinatorial_type:outgoing},
\eqref{radial_slit_configuration:combinatorial_type:Euler_characteristic},
\eqref{radial_slit_configuration:combinatorial_type:simplicially_non_degenerate_vertical} and
\eqref{radial_slit_configuration_paun:combinatorial_type:simplicially_non_degenerate_horizontal}.
\begin{align}
  \label{radial_slit_configuration_paun:combinatorial_type:simplicially_non_degenerate_horizontal}
  \text{If $j+1$ is not distinguished in $\sigma^\paun$, then there is an $i$ with $\sigma_i(j) \neq j+1$.}
\end{align}
Its topology is such that the points in $\Rad^\paun_g(m,1)$ with the same combinatorial type $(\sigma_0: \ldots: \sigma_q; \sigma^\paun)$ form an open bi-simplex.
If the $j$-th vertical face resp.\ the $i$-th horizontal face of a bi-simplex of combinatorial type $(\sigma_0: \ldots: \sigma_q; \sigma^\paun)$ exsits in $\Rad^\paun_g(m,1)$,
the combinatorial type of the resulting face is as follows.
\begin{align}
  \label{radial_slit_configuration_paun:combinatorial_type:vertical_face}
  d'_j(\sigma_0: \ldots: \sigma_q; \sigma^\paun)  &= (\sigma_0: \ldots: \hat{\sigma_j}: \ldots: \sigma_q; \sigma^\paun) \\
  \label{radial_slit_configuration_paun:combinatorial_type:horizontal_face}
  d''_i(\sigma_0: \ldots: \sigma_q; \sigma^\paun) &= (D_i\sigma_0: \ldots: D_i\sigma_q; D_i\sigma^\paun)
\end{align}
Sending a Riemann surface to its radial slit configuration defines a homeomorphism
\begin{align}
  \Phi \colon \fM^\paun_g(m,1) \cong \Rad^\paun_g(m,1) \,.
\end{align}
This homeomorphism is called the \defn{Hilbert uniformization map}.
Moreover, $\Rad^\paun_g(m,1)$ admits a bar compactification and an harmonic compactification.


\subsection{The space of radial slit configurations \texorpdfstring{$\Rad^\paen_g(m,1)$}{Rad\_g(m,1)}}
\label{section:the_space_of_radial_slit_configurations_paen}
Finally, as a combination of $\Rad^\unen_g(m,1)$ and $\Rad^\paun_g(m,1)$,
the combinatorial type of a radial slit domain in $\Rad^\paen_g(m,1)$ is a sequence of permutations
$(\sigma_0: \ldots: \sigma_q; \sigma^\paen)$ with $\sigma_0, \ldots, \sigma_q \in \SymGrn[p]$, $\sigma^\paen \in \SymGrpaen[p]$ and $\sigma_q = \pi^\paen_\unun(\sigma^\paen)$ and
the space of radial slit domains $\Rad^\paen_g(m,1)$ consists of all radial slit configurations whose combinatorial type
$(\sigma_0: \ldots: \sigma_q; \sigma^\paen)$ is subject to the conditions
\eqref{radial_slit_configuration:combinatorial_type:incoming},
\eqref{radial_slit_configuration:combinatorial_type:outgoing},
\eqref{radial_slit_configuration:combinatorial_type:Euler_characteristic},
\eqref{radial_slit_configuration:combinatorial_type:simplicially_non_degenerate_vertical} and
\eqref{radial_slit_configuration_paen:combinatorial_type:simplicially_non_degenerate_horizontal}.
\begin{align}
  \label{radial_slit_configuration_paen:combinatorial_type:simplicially_non_degenerate_horizontal}
  \text{If $j+1$ is not distinguished in $\sigma^\paen$, then there is an $i$ with $\sigma_i(j) \neq j+1$.}
\end{align}
Its topology is such that the points in $\Rad^\paen_g(m,1)$ with the same combinatorial type $(\sigma_0: \ldots: \sigma_q; \sigma^\paen)$ form an open bi-simplex.
If the $j$-th vertical face resp.\ the $i$-th horizontal face of a bi-simplex of combinatorial type $(\sigma_0: \ldots: \sigma_q; \sigma^\paen)$ exsits in $\Rad^\paen_g(m,1)$,
the combinatorial type of the resulting face is as follows.
\begin{align}
  \label{radial_slit_configuration_paen:combinatorial_type:vertical_face}
  d'_j(\sigma_0: \ldots: \sigma_q; \sigma^\paen)  &= (\sigma_0: \ldots: \hat{\sigma_j}: \ldots: \sigma_q; \sigma^\paen) \\
  \label{radial_slit_configuration_paen:combinatorial_type:horizontal_face}
  d''_i(\sigma_0: \ldots: \sigma_q; \sigma^\paen) &= (D_i\sigma_0: \ldots: D_i\sigma_q; D_i\sigma^\paen)
\end{align}
Sending a Riemann surface to its radial slit configuration defines a homeomorphism
\begin{align}
  \Phi \colon \fM^\paen_g(m,1) \cong \Rad^\paen_g(m,1) \,.
\end{align}
This homeomorphism is called the \defn{Hilbert uniformization map}.
Moreover, $\Rad^\paen_g(m,1)$ admits a bar compactification and an harmonic compactification.

The rotation of the parametrization points of the $m$ enumerated outgoing boundaries defines a free action of an $m$-dimensional torus $U(1)^m$.
This action is induced by a multi-cyclic structure on an $U(1)^m$-equivariant retract of $\Rad^\paen_g(m,1)$, see Section \ref{section:radial_slit_configurations_normalized}.

