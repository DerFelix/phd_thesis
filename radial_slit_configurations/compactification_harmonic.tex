\section{The harmonic compactification of the moduli space}
\label{section:compactification_harmonic}
For the remainder of this section, we fix $g \ge 0$, $n = 1$, $m \ge 1$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
Given a Riemann surface $F \in \fM^\star_g(m,1)$, the complete critical graph and the critical levels of the harmonic potential give a decomposition of $F$:
The complement of the complete critical graph is a disjoint union of open rectangles (that correspond to the radial segments of the radial slit configuration associated to $F$).
The open region between two consecutive critical levels is a disjoint union of cylinders.
Collapsing some of these regions eventually gives surfaces with the following degeneracies:
\begin{enumerate}
  \item Handles degenerate to a line segment;
  \item Outgoing boundaries collide or degenerate to a point, called degenerate point;
  \item Non-separating closed curves degenerate to a point, also called degenerate point;
  \item Degenerate points merge.
\end{enumerate}
We advice the reader to study the pretty drawings of these degeneracies in \cite{Boedigheimer2006}.
By construction, each degenerate surface comes with a continuous real function that is harmonic on the smooth points and so the surface corresponds to a radial slit configuration.
Adding these surfaces to the moduli space yields its \defn{harmonic compactification} $\ov{\fM}^\star_g(m,n)$.
Using the space of radial slit domains $\Radstar_g(m,n)$ as a model for the moduli space $\fM^\star_g(m,n)$ we have the following theorem due to Bödigheimer.
\begin{theorem}[Bödigheimer]
  Let $g \ge 0$, $n \ge 1$, $m \ge 1$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
  The moduli space $\fM^\star_g(m,n)$ is an open and dense subspace of the harmonic compactification $\ov{\fM}^\star_g(m,n)$.
  The harmonic compactification is a finite cell complex such that
  \begin{enumerate}
    \item the points in $\fM^\star_g(m,n)$ with common combinatorial type form an open cell inside $\ov{\fM}^\star_g(m,n)$ and
    \item the complement $\ov{\fM}^\star_g(m,n) - \fM^\star_g(m,n)$ is a subcomplex of $\ov{\fM}^\star_g(m,n)$ of codimension one.
  \end{enumerate}
\end{theorem}

In the subsequent chapters,
we study the strong deformation retract $\ov{U\fM}^\star_g(m,1) \subset \ov\fM^\star_g(m,1)$ that is defined below.
Most importantly, the combinatorics of $\ov{U\fM}^\star_g(m,n)$ are more accessible and
the space $\ov{U\fM}^\star_g(m,n)$ is cellularly homeomorphic to a space of Sullivan diagrams that is used in the study of string topology,
see Section \ref{section:string_topology} and Chapter \ref{chapter:sullivan_diagrams}.

Recall that each Riemann surface $F \in \fM^\star_g(m,n)$ comes with a unique harmonic potential $u \colon F \to [0,1]$ and
the number of critical values of the harmonic potential gives a stratification of the moduli space $\fM^\star_g(m,n)$.
Equivalently, we have a stratification of the corresponding space of radial slit configurations $\Radstar_g(m,n)$ by the number of critical levels of a radial slit configuration.
The bottom-stratum is the subspace of all radial slit configurations on a single critical level.
It is denoted by $U\fM^\star_g(m,n) \subset \fM^\star_g(m,n)$ respectively $U\Radstar_g(m,n) \subset \Radstar_g(m,n)$.
By construction, the Hilbert uniformization restricts to these subspaces.
Applying Bödigheimer's construction of the harmonic compactification to the subspace $U\fM^\star_g(m,n)$,
we obtain a compactification which is canonically homeomorphic to the closure of $U\fM^\star_g(m,n)$ in the harmonic combination $\ov{\fM}^\star_g(m,n)$.
Therefore, $\ov{U\fM}^\star_g(m,n) \subset \ov{\fM}^\star_g(m,n)$ is seen as closed subspace without ambiguity.

The subspace $\ov{U\fM}^\star_g(m,n) \subset \ov{\fM}^\star_g(m,n)$ is a strong deformation retract.
Roughly speaking, the retraction $r \colon \ov{\fM}^\star_g(m,1) \to \ov{U\fM}^\star_g(m,1)$ is defined as follows.
A point $x$ in $\ov{\fM}^\star_g(m,n)$ is a (possibly degenerate) configuration of pairs of radial slits on one annulus $\bA$.
The endpoints of these slits define critical levels in $\bA$ and the critical levels give $q \ge 1$ subannuli of $\bA$ of increasing size.
The contraction of all but one subannus gives a (possibly degenerate) configuration of pairs of slits $r(x)$ whose endpoints share a common radius, i.e., $r(x) \in \ov{U\fM}^\star_g(m,1)$.
A complete proof is found in \cite[Lemma 2.23]{EgasKupers2014}.

By \cite[Theorem 1.1]{EgasKupers2014} the harmonic compactification $\ov{U\fM}^\star_g(m,n)$ is cellularly homeomorphic to the space of Sullivan diagrams $\SDs^\star_g(m,n)$.
In the literature, a Sullivan diagram is defined as a certain equivalence class of decorated ribbon graphs, see \cite[Section 3]{EgasKupers2014} or Section \ref{section:ribbon_graphs_and_sullivan_diagrams}.
In this thesis, we use our combinatorial description of Sullivan diagrams, introduced in \cite{BoesEgas2017}, see also Section \ref{section:combinatorial_SD}.
The space of Sullivan diagrams is defined as the realization of a simplicial set made from these Sullivan diagrams, see \cite[Section 4]{EgasKupers2014} or Section \ref{section:the_space_of_sullivan_diagrams}.
