\section{The cluster filtration}
\label{section:cluster_filtration}
In this section we recall the construction of the cluster filtration introduced in \cite{BoedigheimerBoes2018cluster}, see also \cite[Chapter 3]{BoesHermann2014}.
The associated spectral sequence is used in \cite{BoesHermann2014}, \cite{BoedigheimerBoes2018} and in this thesis,
to compute the homology of $\fM^\unun_g(m,1)$ for small parameters $g$ and $m$, see Section \ref{section:the_generator_f_is_non_trivial} and Appendix \ref{appendix:unstable_homology_of_a_single_moduli_space}.
In \cite{BoesEgas2017} the cluster filtration is used to make the cellular inclusion of the moduli space into its harmonic compactification explicit, see also Section \ref{section:inclusion_of_fM_into_SD_combinatorial}.

Let $g \ge 0$, $m\ge 1$, $n \ge 1$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
Let $F \in \fM^\star_g(m,1)$ and consider the unstable critical graph $K$ of (the harmonic potential $f$ of) $F$, see Section \ref{section:the_space_of_radial_slit_configurations_unun:from_RS_to_slit_configuration}.
The graph $K$ is possibly disconnected.
By $c(F)$ we denote the \defn{cluster number of $F$} which is the number of the connected components of $K$.
This gives a filtration of the moduli spaces called the \defn{cluster filtration}:
\begin{align}
 Cl_c\fM^\star_g(m,n) = \{ F \in \fM^\star_g(m,n) \mid c(F) \le c \} \,.
\end{align}
Note that this filtration is bi-simplicial.

\begin{theorem}[Bödigheimer]
  Let $g \ge 0$, $m\ge 1$, $n \ge 1$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
  The cluster filtration gives a first quadrant spectral sequence
  \begin{align}
    E^r_{c,t} \Rightarrow H_{c+t}( \fM^\star_g(m,n); \bZ)
  \end{align}
  that collapses on the second page.
\end{theorem}
In terms of radial slit configurations, the cluster number is defined as follows.
Let $\Sigma = (\tau_1 \mid \ldots \mid \tau_q)$ denote the combinatorial type of a cell of $\Radstar_g(m,1)$ of bi-degree $(p,q)$.
On $[p]$ we declare the equivalence relation $\sim_{CL}$ as the transitive closure of the relation
\begin{align}
  s \sim t \quad\text{if $s$ and $t$ are in the same cycle of $\tau_i$ for some $1 \le i \le q$.}
\end{align}
The equivalence classes of $\sim_{CL}$ are the \defn{clusters of $\Sigma$}.
The \defn{number of clusters of $\Sigma$} is the number of equivalence classes of $\sim_{CL}$.

\begin{proposition}[Bödigheimer]
  The number of clusters give a filtration of the spaces $\fM^\star_g(m,1)$ and $\Radstar_g(m,1)$.
  The Hilbert uniformization $\fM^\star_g(m,1) \cong \Radstar_g(m,1)$ induces a homeomorphism of the associated stratified spaces.
\end{proposition}

The following Lemma is used to make the inclusion of the moduli space into its harmonic compactification explicit in Section \ref{section:inclusion_of_fM_into_SD_combinatorial}.
\begin{lemma}
  \label{lemma:partition_of_tau}
  Let $\Sigma = (\tau_1 \mid \ldots \mid \tau_q)$ denote the combinatorial type of a cell of $\Radstar_g(m,1)$ of bi-degree $(p,q)$.
  Let $\tau := \tau_q \cdots \tau_1$.
  On the set of cycles of $\tau$, the relation
  \begin{align}
    \alpha \sim \beta \text{ if there exists $\alpha \ni i \sim_{CL} j \in \beta$}
  \end{align}
  is an equivalence relation.
\end{lemma}

\begin{proof}
  Being in the same cycle of $\tau$ defines an equivalence relation $\sim_{cyc}$ on $[p]$.
  Moreover, two elements $i,j \in [p]$ are in the same cycle of $\tau$ only if they are in the same cycle of some $\tau_r$.
  Therefore, $\sim_{CL}$ is coarser than $\sim_{cyc}$.
  This finishes the proof.
\end{proof}
