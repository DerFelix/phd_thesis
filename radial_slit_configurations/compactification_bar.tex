\section{The bar compactification of the moduli space}
\label{section:compactification_bar}
In this section, we introduce our notation and recall some of the details of the construction of the bar compactification of a given moduli space.
In Section \ref{section:the_generator_f_is_non_trivial}, we use the bar compactification to detect a generator in the unstable homology of the moduli space $\fM^\unun_1(2,1)$.
For more details and proofs, we refer the reader to \cite{Boedigheimer19901, Boedigheimer1993Harmonic, Boedigheimer2007, Mueller1996, Mehner201112, BoesHermann2014}.

For the remainder of this section, we fix $g \ge 0$, $n = 1$, $m \ge 1$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
Consider the multi-simplicial set $S$ defined as follows.
\begin{enumerate}
  \item $S_{p,q} := \{ (\omega_p; \sigma_0: \ldots: \sigma_q; \sigma^\star) \mid \omega_p = \langle 0\ \ldots\ p \rangle \in \fS^\unun_p; \sigma_0, \ldots, \sigma_q \in \fS^\unun_p; \sigma^\star \in \SymGrstar_p \}$
  \item $d'_j \colon S_{p,q} \to S_{p,q-1}$, $d'_j( \omega_p; \sigma_0: \ldots: \sigma_j: \ldots \sigma_q; \sigma^\star ) = (\omega_p; \sigma_0: \ldots: \hat \sigma_j: \ldots: \sigma_q; \sigma^\star)$
  \item $s'_j \colon S_{p,q-1} \to S_{p,q}$, $s'_j( \omega_p; \sigma_0: \ldots: \sigma_j: \ldots \sigma_q; \sigma^\star ) = (\omega_p; \sigma_0: \ldots: \sigma_j: \sigma_j: \ldots: \sigma_q; \sigma^\star)$
  \item $d''_i \colon S_{p,q} \to S_{p-1,q}$, $d''_i( \omega_p; \sigma_0: \ldots: \sigma_q; \sigma^\star ) = (D_i\omega_p; D_i\sigma_0: \ldots: D_i\sigma_q; D_i\sigma^\star)$
  \item $s''_i \colon S_{p-1,q} \to S_{p,q}$, $s''_i( \omega_p; \sigma_0: \ldots: \sigma_q; \sigma^\star ) = (S_i\omega_p; S_i\sigma_0: \ldots: S_i\sigma_q; S_i\sigma^\star)$
\end{enumerate}
The \defn{norm}
\begin{align}
  N(\omega_p; \sigma_0: \ldots: \sigma_q; \sigma^\star) := N(\sigma_q \sigma_{q-1}^{-1}) + \ldots + N(\sigma_1 \sigma_0^{-1})
\end{align}
induces a filtration on the multi-simplicial set $S$ that is weakly decreasing along the faces and constant along the degeneracies.
Furthermore, the \defn{number of non-degenerate outgoing boundaries}
\begin{align}
  m(\omega_p; \sigma_0: \ldots: \sigma_q; \sigma^\star) := \ncyc(\sigma^\star)
\end{align}
induces another filtration that is weakly decreasing along the horizontal faces and constant along the vertical faces and along all degeneracies.

Denote the Euler characteristic of the surface of genus $g$ with $n+m$ boundaries by $h = 2g + m + n - 2$.
In order to distinguish multi-simplicially degenerated radial slit configurations from those giving a degenerate surface,
a multi-simplex $\Sigma = (\omega_p; \sigma_0: \ldots: \sigma_q; \sigma^\star)$ is called \defn{proper} (with respect to $g$, $n$, $m$ and the boundary conditions)
if the following conditions are satisfied:
\begin{align}
  N(\Sigma) &= h =2g + m + n - 2 \\
  m(\Sigma) &= m \\
  \sigma_0  &= \omega_p \\
  \sigma_q  &= \pi^\star_\unun(\sigma^\star)
\end{align}
The \defn{bar compactification} $Bar^\star_g(m,1)$ of the moduli space $\fM^\star_g(m,1)$ is the realization of the multi-simplicial subset of $S$ generated by all proper multi-simplices.

Sending the open cells of $\Radstar_g(m,1)$ to the corresponding multi-simplices in $Bar^\star_g(m,1)$ defines an embedding.
By construction, all faces and degeneracies of a non-proper multi-simplex are also non-proper.
Therefore $DBar^\star_g(m,1) := Bar_g(m,1) - \Radstar_g(m,1)$ is a subcomplex of $Bar^\star_g(m,1)$.

The orientation system on the bar compactification is constant if the outgoing boundaries are enumerated.
For the unenumerated cases, we refer the reader to \cite{Mueller1996}.

Carrying out the details carefully leads to the following theorem.
\begin{theorem}[Bödigheimer]
  Let $g \ge 0$, $n \ge 1$, $m \ge 1$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
  \begin{enumerate}
    \item The Hilbert uniformization $\fM^\star_g(m,n) \to \Radstar_g(m,n)$ is a homeomorphism between the moduli space $\fM^\star_g(m,n)$ and
      its corresponding space of radial slit domains $\Radstar_g(m,n)$.
    \item The space of radial slit domains is a relative multi-simplicial manifold inside the bar compactification $Bar^\star_g(m,n)$, i.e.,
      the space $Bar^\star_g(m,n)$ is the realization of a (finite) multi-simplicial set;
      $\Radstar_g(m,n) \subset Bar^\star_g(m,n)$ is open and dense and the complement $DBar^\star_g(m,n) = Bar^\star_g(m,n) - \Radstar_g(m,n)$ is a subcomplex of codimension one;
    \item $H_\ast( \Radstar_g(m,n); \bZ)$ is Poincaré dual to $H^\ast( Bar^\star_g(m,n), DBar^\star_g(m,n); \cO)$ for an orientation system $\cO$.
      The orientation system is constant in the enumerated cases.
  \end{enumerate}
\end{theorem}

The relation between the bar compactification and the bar resolution of varying symmetric groups is discussed in Appendix \ref{appendix:factorable_groups}.
This relation leads to the Ehrenfried complex $\bE^\star_g(m,n)$
which is chain homotopy equivalent to the (total complex of the) relative multi-simplicial complex$(Bar^\star_g(m,n),DBar^\star_g(m,n))$.
Its number of cells is considerably smaller and it is used by several authors to
compute the integral homology of the moduli spaces in question, provided the genus and number of boundary components is small,
see Section \ref{section:unstable_homology}, Section \ref{section:the_generator_f_is_non_trivial} and Appendix \ref{appendix:unstable_homology_of_a_single_moduli_space}.
