# Dissertation

## Prerequisites
I wrote and compiled my thesis on Debian sid using the following packets (at least):
> biber ghostscript inkscape latexmk make texlive-base texlive-bibtex-extra texlive-binaries texlive-extra-utils texlive-font-utils texlive-fonts-extra texlive-fonts-extra-links texlive-fonts-recommended texlive-generic-extra texlive-generic-recommended texlive-lang-english texlive-lang-german texlive-latex-base texlive-latex-extra texlive-latex-recommended texlive-pictures texlive-plain-generic texlive-pstricks texlive-publishers texlive-science

## Building the thesis
The thesis is build using
> make

First, it compiles the pictures (*.svg) into includeable pdfs using inkscape.
These includable pdfs might have technical issues with some version of pdflatex.
We solve these problems by converting the pdfs into ps and then back to pdf.

Second, it compiles the thesis (*.tex) into a pdf.