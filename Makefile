LATEX=latexmk
LATEX_OPTS=-pdf
BIBTEX=biber
TITEL=dissertation

PICTURES_DIR=./pictures
PICTURES_SVG=$(wildcard $(PICTURES_DIR)/*.svg)
PICTURES_PDF=$(patsubst $(PICTURES_DIR)/%, $(PICTURES_DIR)/%, $(PICTURES_SVG:.svg=.pdf)) 

all: %: $(PICTURES_PDF)
	$(LATEX) $(LATEX_OPTS) $(TITEL)

pictures: %: $(PICTURES_PDF)

./pictures/%.pdf: ./pictures/%.svg 
	inkscape -z -D --file=$< --export-pdf=$@ --export-latex;
	pdf2ps $@ $@.ps;
	ps2pdf $@.ps $@;
	rm -f $@.ps;


.PHONY: clean
clean:
	rm -f $(TITEL).aux $(TITEL).bbl $(TITEL).blg $(TITEL).fdb_latexmk $(TITEL).fls $(TITEL).log $(TITEL).out $(TITEL).pdf $(TITEL).synctex.gz $(TITEL).toc

.PHONY: cleanpics
cleanpics:
	rm -f ./pictures/*.pdf ./pictures/*.pdf_tex

