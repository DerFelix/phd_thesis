\noindent
In this chapter, we summarize the results of a joint-research project with Daniela Egas Santander that resulted in the article \cite{BoesEgas2017}.
By \cite{EgasKupers2014}, the spaces of Sullivan diagrams, denoted by $\SDsstar_g(m,n)$, have the same homotopy type as the harmonic compactifications, i.e.,
$\SDsstar_g(m,1) \simeq \ov{\fM}^\star_g(m,1)$.
Usually, these spaces of Sullivan diagrams are defined as spaces of equivalence classes of combinatorial graphs,
see Section \ref{section:ribbon_graphs_and_sullivan_diagrams} and Section \ref{section:the_space_of_sullivan_diagrams}.
Inspired by the combinatorics of the Bödigheimer's spaces of radial slit configurations,
we introduce an equivalent description of these spaces of Sullivan diagrams using bi-weighted partitions of permutations, see Section \ref{section:combinatorial_SD}.
With our notation of combinatorial Sullivan diagrams, we obtain results on homological stability and construct infinite families of non-trivial homology classes,
see Section \ref{section:on_the_homotopy_type_of_the_space_of_sullivan_diagrams}.
Let us state the two most important results of \cite{BoesEgas2017} here.
\begin{theoremwithfixednumber}{\ref{theorem:boes_egas_stability}}[B., Egas]
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \paun, \unen, \unun \}$.
  The stabilization map $\varphi \colon \fM^\star_g(m,1) \to \fM^\star_{g+1}(m,1)$ extends to the harmonic compactifications, i.e.,
  there is a map $\ov\varphi \colon \ov{\fM}^\star_g(m,1) \to \ov{\fM}^\star_{g+1}(m,1)$ making the following diagram commutative.
  \begin{align*}
    \begin{tikzcd}[ampersand replacement=\&]
      \fM^\star_g(m,1)      \ar{r}{\varphi} \ar{d} \& \fM^\star_{g+1}(m,1) \ar{d} \\
      \ov{\fM}^\star_g(m,1) \ar{r}{\ov\varphi}     \& \ov{\fM}^\star_{g+1}(m,1)
    \end{tikzcd}
  \end{align*}
  The stabilization map $\ov{\varphi}$ induces an isomorphism in homology in the degrees $\ast \le g + m - 2$.
  Moreover, if $m > 2$, the stabilization map $\ov\varphi$ is $(g+m-2)$ connected.
\end{theoremwithfixednumber}

Moreover, the harmonic compactification is highly connected with respect to $m$.
\begin{theoremwithfixednumber}{\ref{theorem:boes_egas_connectivity}}[B., Egas]
  Let $g \ge 0$ and $m > 2$.
  Then
  \begin{align*}
   \pi_\ast( \ov{\fM}^\paen_g(m,1) ) = 0 \mspc{and}{20} \pi_\ast( \ov{\fM}^\unen_g(m,1) ) = 0 \mspc{for}{20} \ast \le m-2
  \end{align*}
  and
  \begin{align*}
    \pi_\ast( \ov{\fM}^\paun_g(m,1) ) = 0 \mspc{and}{20} \pi_\ast( \ov{\fM}^\unun_g(m,1) ) = 0 \mspc{for}{20} \ast \le m'
  \end{align*}
  where $m'$ is the largest even number strictly smaller than $m$.
\end{theoremwithfixednumber}
In Chapter \ref{chapter:homotopy_type_of_SDspaen}, we will show that the harmonic compactification $\ov{\fM}^\paen_g(m,1)$ is also highly connected with respect to $g$,
see Theorem \ref{theorem:stable_homotopy_type_of_harm_paen}.
