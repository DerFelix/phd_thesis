\section{A combinatorial description for Sullivan diagrams}
\label{section:combinatorial_SD}
By Definition \ref{definition:SD_eq_relation}, the cells of the space of 1-Sullivan diagrams are given by equivalence classes of ribbon graphs.
Here, we give a combinatorial description of the cells of $\SDspaen_g(m,1)$ and $\SDsunen_g(m,1)$,
see Definition \ref{definition:combinatorial_1_SD_paen}, Definition \ref{definition:combinatorial_1_SD_unen} and Proposition \ref{proposition:combinatorial_1_SD}.
For combinatorial representatives of the cells of the spaces $\SDspaun_g(m,1)$ and $\SDsunun_g(m,1)$ see \cite[Section 3]{BoesEgas2017}.
In Section \ref{section:combinatorial_SD_simplicial_structure}, we discuss the faces and degeneracies of our combinatorial representatives geometrically and algebraically.
Using our combinatorial description,
we describe the inclusion of the moduli space into its harmonic compactification followed by the homotopy equivalence to the space of Sullivan diagrams
in Section \ref{section:inclusion_of_fM_into_SD_combinatorial}.
The stabilization map is described in Section \ref{section:stabilization_map_on_SD_combinatorial}.

The informal idea is that any Sullivan diagram is uniquely described by attaching onto a ``ground circle" topological types of surfaces with decorations.
These surfaces are determined by their genus, number of punctures, boundary components and the combinatorial data of marked points at the boundary.
Inspired by \cite{Boedigheimer19901, Boedigheimer19902, Boedigheimer2006},
we describe the combinatorial data of the marked points at the boundary and the way in which these surfaces are attached to the ground circle by permutations.
Thereafter, we encode the genus and number of punctures as weights.
Therefore, we give a presentation of Sullivan diagrams in terms of bi-weighted permutations subject to certain conditions.
There are similarities to the the concept of stable graphs of \cite{Kontsevich1992} and \cite{Looijenga1995}.

For better readability we give separately the definition of combinatorial diagrams for the two cases in Definitions
\ref{definition:combinatorial_1_SD_paen} and \ref{definition:combinatorial_1_SD_unen}.
In Section \ref{section:inclusion_of_fM_into_SD_combinatorial}, we make the relation between the combinatorics of Bödigheimer's model for the moduli spaces and
our description of 1-Sullivan diagrams explicit.

\begin{definition}
  \label{definition:combinatorial_1_SD_paen}
  Let $n \ge 0, k \ge 1$ and let $L = \{ L_1, \ldots, L_m \}$ be a non-empty finite set.
  A \emph{parametrized enumerated combinatorial $1$-Sullivan diagram} $\Sigma$ of degree $n$ with leaves $L$ is a pair $(\lambda, \{ S_1, \ldots, S_k \})$ consisting of the following data:
  \begin{enumerate}[label={(\arabic*)}]
    \item the \emph{ribbon structure} $\lambda\in \SymGr([n] \sqcup L)$,
    \item the set of cycles of $\lambda$ is denoted by $\Lambda := \cycles(\lambda)$,
    \item the \emph{outgoing boundary cycles} given by the permutation
      $\rho = \lambda^{-1} \langle 0\ 1\ \ldots\ n \rangle$,
    \item the \emph{ghost surfaces} given by triples $S_i = (g_i, 0, A_i)$ where
      $A_i \subset \Lambda$ and $g_i \in \bN$.
  \end{enumerate}
  Subject to the following conditions:
  \begin{enumerate}[label={(\roman*)}]
    \item \label{data_partition_paen}            The $A_i$'s form a partition of $\Lambda$, i.e., $A_i\cap A_j = \emptyset$ if $i\neq j$ and $\cup_i A_i = \Lambda$.
    \item \label{data_attach_all_surfaces_paen}  For each $i$ the set $A_i$ contains at least one cycle with an element in $[n]$.
    \item \label{data_conditions_leaves_paen}    Each cycle of $\rho$ permutes exactly one leaf non-trivially or it is a fixed point $\cycle{L_i}$ with $L_i \in L$.
  \end{enumerate}
    We denote by  $C^\paen_J(n)$, the \defn{set of parametrized enumerated combinatorial 1-Sullivan diagrams of degree $n$ with leaves $L$}.
\end{definition}

\begin{definition}
  \label{definition:combinatorial_1_SD_unen}
  Let $n \ge 0$ and $k \ge 1$.
  An \emph{unparametrized enumerated combinatorial $1$-Sullivan diagram} $\Sigma$ of degree $n$ is a quadruple $(\lambda, \{ S_1, \ldots, S_k \}, \beta_1, \beta_2)$ consisting of:
  \begin{enumerate}[label={(\arabic*)}]
    \item the \emph{ribbon structure} $\lambda\in \SymGr([n])$,
    \item the set of cycles of $\lambda$ is denoted by $\Lambda := \cycles(\lambda)$,
    \item the \emph{outgoing boundary cycles} given by the permutation
      $\rho = \lambda^{-1} \langle 0\ 1\ \ldots\ n \rangle$,
    \item the \emph{ghost surfaces} given by triples $S_i = (g_i, m_i, A_i)$ where
      $A_i \subset \Lambda$ and $g_i, m_i \in \bN$,
    \item the \emph{enumerating data} given by injections
      \begin{align}
        \beta_1 \colon \cycles(\rho)                      & \xhr{} \{1,2,\ldots, m\} \\
        \beta_2 \colon \{1,2,\ldots, m\} - \imag(\beta_1) & \xhr{} \{S_1, S_2,\ldots, S_k\} \,.
      \end{align}
  \end{enumerate}
  Subject to the following conditions:
  \begin{enumerate}[label={(\roman*)}]
    \item \label{data_partition_unen}          The $A_i$'s form a partition of $\Lambda$, i.e., $A_i\cap A_j = \emptyset$ if $i\neq j$ and $\cup_i A_i = \Lambda$.
    \item[(iv)] \label{data_enumerattion_unen} For every $1 \leq i \leq k$ it holds that $|\beta_2^{-1}(i)| = m_i$.
  \end{enumerate}
  We denote by $C^\unen(n)$ the \defn{set of unparametrized enumerated combinatorial 1-Sullivan diagrams of degree $n$}.
\end{definition}
  
\begin{remark}
  \label{remark:non-degenerate_boundary}
  The names \emph{ribbon structure} and \emph{outgoing boundary cycles} come from the geometric interpretation of this description.
  Recall that each cycle $\lambda_k$ of $\lambda$ is contained in exactly ghost surface $S_i = (g_i, m_i, A_i)$
  and we think of every such cycle as a boundary component of $S_i$ that is attached to the ground circle, see Figure \ref{figure:combinatorial_1_SDpaen_example} for an example.
  
  In the parametrized case, $\Sigma \in \SDspaen_g(m,1)$, the \defn{outgoing boundary cycles} $\rho$ are in bijection with the cycles of $\Sigma$ which are not the admissible cycle and
  which have either at least one edge on the admissible cycle or consist of a single leaf $\rho(L_i) = L_i$.
  Note that a Sullivan diagram $\Sigma \in \SDspaen_g(m,1)$ is non-degenerate if and only if $\rho$ has $m$ cycles none of which consist of a single leave,
  see Definition \ref{definition:degenerate_SD} and Figure \ref{figure:combinatorial_1_SDpaen_example}.
  \begin{figure}[ht]
    \centering
    \inkpic[.6\columnwidth]{combinatorial_1_SDpaen_example}
    \caption{
      \label{figure:combinatorial_1_SDpaen_example}
      We interpret a combinatorial 1-Sullivan diagram $\Sigma = (\lambda, S_1, S_2, S_3)$ as a cell of $\SDspaen_g(m,1)$.
      The ribbon structure is
      $\lambda = \cycle{0\ L_2}\cycle{1\ 4}\cycle{2\ 6\ 5}\cycle{3\ L_5}$
      with fixed points $L_1, L_3, L_4$ and
      $\rho = \cycle{0\ 4\ 6\ L_2}\cycle{1\ 5\ 2\ L_5\ 3}$
      with the same fixed points.
      The ghost surfaces are
      $S_1 = (0,0,\{\cycle{0\ L_2}, \cycle{L_3}, \cycle{1\ 4}\})$,
      $S_2 = (1,0, \{\cycle{2\ 6 \ 5}, \cycle{L_1}, \cycle{L_4}\})$ and
      $S_3 = (0,0,\{\cycle{3\ L_5}\})$.
      Consequently, we consider a surface of genus $0$, no punctures and three boundary components for $S_1$,
      a surface of genus $1$, no punctures and three boundary components for $S_2$ and
      a surface of genus $0$, no punctures and one boundary component for $S_3$.
      The distribution of the leaves and the gluing of the surfaces to the ground cycle is prescribed by the ribbon structure $\lambda$.
      Moreover, we highlight one of the non-degenerate outgoing boundary curves.
    }
  \end{figure}
  
  In the unparametrized case, $\Sigma \in \SDsunen_g(m,1)$,
  the \defn{outgoing boundary cycles} $\rho$ are in bijection with the cycles of $\Sigma$ which are not the admissible cycle and have at least one edge on the admissible cycle.
  Note that a Sullivan diagram $\Sigma \in \SDsunen_g(m,1)$ is non-degenerate if and only if $\rho$ has $m$ cycles, see Definition \ref{definition:degenerate_SD}
  
  The maps $\beta_1$ and $\beta_2$ enumerate the non-degenerate outgoing boundaries and the degenerate outgoing boundaries respectively.
  In the case of $\SDspaen_g(m,1)$, the role of $\beta_1$ resp.\ $\beta_2$, enumerating the non-degenerate resp.\ degenerate outgoing boundaries, is implicit in $\lambda$ and the $A_i$'s.
\end{remark}

\begin{notation}
\label{notation:combinatorial_SD}
  We refer by a \emph{combinatorial 1-Sullivan diagram} to any of the two versions of given in Definitions
  \ref{definition:combinatorial_1_SD_paen} and \ref{definition:combinatorial_1_SD_unen}.
  Abusing notation, we denote any combinatorial 1-Sullivan by a tuple $\Sigma = (\lambda, S_1, \ldots, S_k)$ and omit writing the enumerating data unless it is specifically necessary.

  Consider two combinatorial 1-Sullivan diagrams $(\lambda, S_1, \ldots, S_k)$ and $(\lambda', S'_1, \ldots, S'_{k'})$ of degree $n$.
  In the parametrized enumerated case,
  the two Sullivan diagrams are equal if and only if $\lambda = \lambda'$, $k=k'$ and the ghost surfaces agree up to reordering.
  In the unparametrized enumerated case,
  the two Sullivan diagrams are equal if and only if $\lambda = \lambda'$, $k=k'$, the ghost surfaces agree up to reordering and the enumeration data agree.
\end{notation}

Regarding a combinatorial 1-Sullivan diagram as a ground circle to which the topological types of decorated surfaces are attached and
identifying a 1-Sullivan diagram (which is defined as equivalence class of graphs) with its fattening,
we obtain the following Proposition.

\begin{proposition}[{\cite[Proposition 3.8]{BoesEgas2017}}]
  \label{proposition:combinatorial_1_SD}
  Let $n \ge 0$, $m\geq 1$ and $L = \{ L_1, \ldots, L_m\}$.
  There exist bijections.
  \begin{align*}
    C^\paen_L(n) & \xlr{1:1} \{ \text{$n$-simplices of } \coprod_{g}   \SDs^\paen_g(m,1) \}\\
    C^\unen(n)   & \xlr{1:1} \{ \text{$n$-simplices of } \coprod_{g,r} \SDs^\unen_g(r,1) \}
  \end{align*}
\end{proposition}

Before discussing the faces and degeneracies in terms of the combinatorial representatives in Section \ref{section:combinatorial_SD_simplicial_structure}, we want to make two remarks.

\begin{remark}[Simplicial degenerate representatives]
  By Remark \ref{remark:simplicially_non_degenrate_is_closed} and Notation \ref{notation:discard_simplicially_degenerate_SD},
  we discard simplicially degenerate Sullivan diagrams from our notation.
  Consequently, we will discard simplicially degenerate combinatorial 1-Sullivan diagrams from our notation.
  Under the bijection of Proposition \ref{proposition:combinatorial_1_SD},
  a combinatorial 1-Sullivan diagram $\Sigma = (\lambda, S_1, \ldots, S_k)$ corresponds to a simplicially degenerate Sullivan diagram if and only if
  one of its decorated surfaces is of the form $S_i = (0,0, \cycle{r})$ with $r \neq 0$,
  c.f.\ \cite[Section 3]{BoesEgas2017} and Discussion \ref{discussion:degeneracies_geometrically}.
\end{remark}

\begin{remark}[Top degree and Euler characteristic]
  \label{remark:top_degree_of_a_SD}
  Consider a (simplicially non-de\-ge\-ne\-rate) Sullivan diagram $\Sigma \in \SDunen_g(m,1)$ and
  let $S_\Sigma$ be the surface with decorations obtained by fattening it as in Definition \ref{definition:fattening}.
  If $\Sigma$ is of top degree, then all ghost surfaces $S_i$ are all disks with exactly one or two attaching points at the boundary and no punctures or degenerate boundary.
  In other words, $\Sigma$ is a graph given by a system of chords attached on the ground circle where the vertex attached to the admissible leaf is of valence three.
  See Figure \ref{figure:SD_top_degree} (b).
  \begin{figure}[ht]
    \centering
    \inkpic[.6\columnwidth]{SD_top_degree}
    \caption{
      \label{figure:SD_top_degree}
      Two examples of top dimensional cells in $\SDspaen_g(2,1)$ and $\SDsunen_g(2,1)$.
      The parametrized enumerated case is seen in (a) and the unparametrized enumerated case is seen in (b).}
  \end{figure}
  Now, let $c$ be the number of chords of $\Sigma$.
  The Euler characteristic of the circle is $0$ and each time one adds a chord to the circle the Euler characteristic goes down by one.
  Therefore, we get
  \begin{align}
    -c = \chi(\Sigma) = \chi(S_\Sigma) \,.
  \end{align}
  Thus, the degree of $\Sigma$ is
  \begin{align}
    \deg(\Sigma) = -2\chi(S_\Sigma) \,.
  \end{align}

  If $\Sigma \in \SDpaen_g(m,1)$, then the top degree cell is a diagram with chords and leaves on the admissible cycles,
  exactly one per boundary cycle of $\Sigma$.
  See Figure \ref{figure:SD_top_degree} (a).
  Therefore, if $\Sigma$ is of top degree, we have
  \begin{align}
    \deg(\Sigma) = -2\chi(S_\Sigma) + m \,.
  \end{align}
\end{remark}


\subsection{The simplicial structure on the space of Sullivan diagrams}
\label{section:combinatorial_SD_simplicial_structure}
By Proposition \ref{proposition:combinatorial_1_SD}, a Sullivan diagram $\Sigma$ is identified with a combinatorial 1-Sullivan diagram.
We describe the face maps and the degeneracy maps both geometrically and algebraically.

\begin{discussion}[The face map geometrically]
  \label{discussion:faces_geometrically}
  Geometrically, the $i$-th face of a combinatorial Sullivan diagram $\Sigma = (\lambda, S_1, \ldots, S_t)$ is given by collapsing the $i$-th edge of the ground circle.
  Hereby, the surfaces $S_j$ and $S_k$ attached to $i$ and $i+1$ are merged, i.e., the resulting surface is the connected sum of $S_j$ and $S_k$ at the corresponding boundary components.
  There are four possible cases:
  \begin{figure}[!p]
    \centering
    \inkpic[.9\columnwidth]{face_merging_different_surfaces}
    \caption{
      \label{figure:face_merging_different_surfaces}
      The face map glues together the boundaries of two ghost surfaces $S_j$ and $S_k$ resulting in a ghost surface $\tilde{S}_j$.
    }
    \vspace{2ex}
    \inkpic[.9\columnwidth]{face_increase_genus}
    \caption{
      \label{figure:face_increase_genus}
      The face map glues together two different boundary components of a ghost surface $S_j$ resulting in a ghost surface $\tilde{S}_j$
      having the same number of punctures while the genus is increased by one and the number of boundary components is decreased by one.
    }
    \vspace{2ex}
    \inkpic[.9\columnwidth]{face_puncture}
    \caption{
      \label{figure:face_puncture}
      The non-degenerate outgoing boundary cycle $\cycle{i}$ of $\rho$ degenerates to a puncture of $\tilde{S}_j$ which we indicate by a gray dot.
    }
    \vspace{2ex}
    \inkpic[.9\columnwidth]{face_split_boundaries}
    \caption{
      \label{figure:face_split_boundaries}
      The face map splits the boundary of a ghost surface into two parts.
    }
  \end{figure}
  
  \textbf{Case (1):} Assume that two different boundary components $B$ and $C$ are attached to $i$ and $i+1$.
  There are two subcases.
  
  (1.a) If $B$ and $C$ belong to different surfaces $S_j$ and $S_k$, then $d_i$ glues together two different surfaces.
  The resulting surface $\tilde{S}_j$ has genus $g(S_j) + g(S_k)$, number of punctures $m(S_j) + m(S_k)$ and one boundary component less than the total number of boundaries of $S_j$ and $S_k$.
  See Figure \ref{figure:face_merging_different_surfaces}.
  
  (1.b) If $B$ and $C$ belong to the same surface $S_j$, then $d_j$ glues together two different boundary components of the same surface.
  The resulting surface $\tilde{S}_j$ has genus $g(S_j) + 1$, number of punctures $m(S_j)$ and one boundary component less than $S_j$.
  See Figure \ref{figure:face_increase_genus}
  
  \textbf{Case (2):} Assume that the same boundary $B$ of a single surface $S_j$ is attached to both $i$ and $i+1$.
  There are two subcases:
  
  (2.a) If $S_j$ is attached to $i+1$ right after it is attached to $i$, i.e.,
  $\lambda(i) = i+1$ or equivalently $\rho(i) = i$,
  then $d_i$ collapses the boundary cycle $\cycle{i}$ to a puncture.
  The resulting surface $\tilde{S}_j$ has genus $g(S_j)$, number of punctures $m(S_j) + 1$ and one boundary component less than $S_j$.
  See Figure \ref{figure:face_puncture}.
  
  (2.b) If $S_j$ is attached to $k \neq i+1$ right after $i$, i.e., $\lambda(i) \neq i+1$ or equivalently $\rho(i) \neq i$,
  then $d_i$ splits the boundary of $S_j$ into two parts.
  The resulting surface $\tilde S_j$ has genus $g(S_j)$, number of punctures $m(S_j)$ and one boundary component more than $S_j$.
  See Figure \ref{figure:face_split_boundaries}
\end{discussion}

\begin{discussion}[The degeneracy map geometrically]
  \label{discussion:degeneracies_geometrically}
  Geometrically speaking, the $i$-th degeneracy of $\Sigma = (\lambda, S_1, \ldots, S_t)$ is given by splitting the $i$-th edge of the ground circle into two pieces.
  This is achieved by glueing a disc $D$ to the splitting point.
  See Figure \ref{figure:degeneracy}
  \begin{figure}[ht]
    \centering
    \inkpic[\columnwidth]{degeneracy}
    \caption{
      \label{figure:degeneracy}
      The degeneracy map splits the $i$-th edge of the ground circle into two pieces and glues a disc $D$ to the splitting point.
    }
  \end{figure}
\end{discussion}

Before discussion the faces and degeneracies algebraically we need some notations.
Analogously to Section \ref{section:polygon_spaces}, we define the face maps and degeneracy maps of symmetric groups as follows.

\begin{definition}
  \label{definition:cylic_structure_on_SymGr_with_L}
  Let $L$ be a finite set and $0 \le i \le n$.
  By Definition \ref{definition:symmetric_group_on_injective_maps},
  the induced map $F_i := \SymGr(\delta_i \sqcup \id_L) \colon \SymGr([n-1] \sqcup L) \hr \SymGr([n] \sqcup L)$
  regards a permutation $\sigma \in \SymGr([n-1] \sqcup L)$ as a permutation of $[n] \sqcup L$ that has $i$ as a fixed point.
  
  The $i$-th face map $D_i \colon \SymGr( [n] \sqcup L) \to \SymGr([n-1] \sqcup L)$ is
  \begin{align}
    D_i( \sigma ) := F_i^{-1}( \transp{\sigma(i)}{i} \cdot \sigma )
  \end{align}
  where $\transp{\sigma(i)}{i}$ is the permutation exchanging $\sigma(i)$ and $i$ (if $\sigma(i) \neq i$; and it is the identity otherwise).
  
  The $i$-th degeneracy map $S_i \colon \SymGr([n] \sqcup L) \to \SymGr([n-1] \sqcup L)$ is
  \begin{align}
    S_j( \sigma ) := \transp{j+1}{j} \cdot  F_j( \sigma ).
  \end{align}
  The cyclic operator $T \colon \SymGr([n] \sqcup L) \to \SymGr([n] \sqcup L)$ is
  \begin{align}
    T(\sigma) := \longcycle_n \cdot \sigma \cdot \longcycle_n^{-1}
  \end{align}
  with $\longcycle_n$ the long cycle $\longcycle_n = \cycle{0\ 1\ 2\ \ldots\ n}$.
\end{definition}

\begin{remark}
  Consider a (possibly empty) finite set $L$.
  The sets $\{ \SymGr([n] \sqcup L) \}_{n \in \bN}$ together with the face maps $D_i$, the degeneracy maps $S_j$ and the cyclic operators $T$ form a cyclic set.
\end{remark}

\begin{discussion}[The face map algebraically]
  \label{discussion:faces_algebraically}
  Let $\Sigma=(\lambda,S_1,S_2,\ldots,S_t)$ be a combinatorial 1-Sullivan diagram of degree $n$ and let $\rho = \lambda^{-1}\cycle{0\ 1\ \ldots\ n}$ be the outgoing boundary cycles.
  The $i$-th face $d_i\Sigma = \tilde\Sigma = (\tilde \lambda, \tilde S_1, \ldots, \tilde S_{\tilde t})$ is obtained as follows.
  The ribbon structure and the outgoing boundary cycles $\tilde \rho$ of $\tilde \Sigma$ are
  \begin{align}
    \label{discussion:faces_algebraically:permutations}
    \tilde{ \rho } = D_i(\rho)
    \quad\quad\text{ and } \quad\quad
    \tilde \lambda = D_i( \lambda \cdot \transp{a}{i} )
  \end{align}
  where $a = \rho(i)$.
  The ghost surfaces are obtained analogously to Discussion \ref{discussion:faces_geometrically}.
  There are four cases.
  
  \textbf{Case (1)}: Assume that $a = \rho(i)$ and $i$ belong to different cycles of $\lambda$.
  Then, $\tilde \lambda$ has one cycle less than $\lambda$ by \eqref{discussion:faces_algebraically:permutations}.
  Moreover $\lambda(a) = \lambda \rho(i) = \lambda \lambda^{-1} \cycle{0\ 1\ \ldots\ n}(i) = i+1$.
  Therefore, the map $D_i$ induces a bijection
  \begin{align}
    \Phi \colon
    \{ \alpha \in \cycles(\lambda) \mid i,a \notin \alpha \} \cong
    \{ \alpha \in \cycles(\tilde \lambda) \mid i \notin \alpha \} \,.
  \end{align}
  We have to distinguish two subcases.
  
  (1.a) Assume that $a$ and $i$ belong to different cycles of different surfaces $S_j$ and $S_k$.
  Then, $\tilde \Sigma$ has one surface less than $\Sigma$, i.e., $\tilde t = t-1$.
  For sake of simplicity, let us assume that $j = t-1$ and $k=t$.
  For $1 \le r < \tilde t$ the surface $\tilde S_r$ is defined as
  \begin{align}
    \tilde S_r &= (g_r, m_r, \tilde A_r)
    \intertext{where}
    \tilde A_r &= \{ \Phi(\alpha) \mid \alpha \in A_r \} \,.
  \end{align}
  The surface $\tilde S_{\tilde t}$ is defined as
  \begin{align}
    \tilde S_{\tilde t} &= (g_{t-1} + g_t , m_{t-1} + m_t, \tilde A_{\tilde t})
    \intertext{where}
    \tilde A_{\tilde t} &= \{ \Phi(\alpha) \mid a,i \notin \alpha \in A_{t-1} \cup A_{t} \} \cup \{ i \in \alpha \in \cycles(\tilde \lambda) \} \,.
  \end{align}
  
  (1.b) Assume further that $a$ and $i$ belong to different cycles of the same surface $S_j$.
  Then, $\tilde \Sigma$ has the same number of surfaces as $\Sigma$, i.e., $\tilde t = t$.
  For $r \neq j$ the surface $\tilde S_r$ is defined as
  \begin{align}
    \tilde S_r &= (g_r, m_r, \tilde A_r)
    \intertext{where}
    \tilde A_r &= \{ \Phi(\alpha) \mid \alpha \in A_r \} \,.
  \end{align}
  The surface $\tilde S_j$ is defined as
  \begin{align}
    \tilde S_j &= (g_j + 1, m_j, \tilde A_j)
    \intertext{where}
    \tilde A_j &= \{ \Phi(\alpha) \mid a,i \notin \alpha \in A_j \} \cup \{ i \in \alpha \in \cycles(\tilde \lambda) \} \,.
  \end{align}
  
  \textbf{Case (2)}: Assume that $a = \rho(i)$ and $i$ belong to the same cycle of $\lambda$.
  We distinguish two subcases.
  
  (2.a) Assume that $a$ and $i$ belong to the same cycle of $\lambda$ and that $\lambda(i) = i+1$.
  Observe that this is the case if and only if $a = i$.
  Therefore $\lambda\cycle{a\ i} = \lambda$ and $\tilde \lambda$ has the same number of cycles as $\lambda$ and
  the map $D_i$ induces an bijection
  \begin{align}
    \Phi \colon \Lambda \cong \tilde \Lambda = \cycles(\tilde\lambda)\,.
  \end{align}
  The $i$-th face $\tilde \Sigma$ has the same number of surfaces as $\Sigma$, i.e., $\tilde t = t$.
  For $r \neq j$ the surface $\tilde S_r$ is defined as
  \begin{align}
    \tilde S_r &= (g_r, m_r, \tilde A_r) \quad\text{where}\quad \tilde A_r = \{ \Phi(\alpha) \mid \alpha \in A_r \} \,.
  \end{align}
  The surface $\tilde S_j$ is defined as
  \begin{align}
    \tilde S_j &= (g_j, m_j + 1, \tilde A_j) \quad\text{where}\quad \tilde A_j = \{ \Phi(\alpha) \mid \alpha \in A_j \} \,.
  \end{align}
  
  (2.b) Assume that $a$ and $i$ belong to the same cycle of $\lambda$ and that $\lambda(i) \neq i+1$.
  Observe that this is the case if and only if $a \neq i$.
  It will follow that $\lambda\cycle{a\ i}$ and $\tilde \lambda$ have one cycle more than $\lambda$ and
  that the map $D_i$ induces an injection
  \begin{align}
    \Phi \colon \{ \alpha \in \Lambda \mid i,a \notin \alpha \} \xhr{} \tilde \Lambda = \cycles(\tilde\lambda)  \,.
  \end{align}
  The $i$-th face $\tilde \Sigma$ has the same number of surfaces as $\Sigma$, i.e., $\tilde t = t$.
  For $r \neq j$ the surface $\tilde S_r$ is defined as
  \begin{align}
    \tilde S_r &= (g_r, m_r, \tilde A_r) \quad\text{where}\quad \tilde A_r = \{ \Phi(\alpha) \mid \alpha \in A_r \} \,.
  \end{align}
  The surface $\tilde S_j$ is defined as
  \begin{align}
    \tilde S_j &= (g_j, m_j, \tilde A_j) \quad\text{where}\quad \tilde A_j = \{ \Phi(\alpha) \mid i,a \notin \alpha \in A_j \} \cup (\tilde\Lambda - \imag(\Phi)) \,.
  \end{align}
\end{discussion}

\begin{discussion}[The degeneracy map algebraically]
  \label{discussion:combinatorial_1_SD_degeneracies}
  Let $\Sigma=(\lambda,S_1,S_2,\ldots,S_t)$ be a combinatorial 1-Sullivan diagram of degree $n$ and let $\rho = \lambda^{-1}\cycle{0\ 1\ \ldots\ n}$ its outgoing boundary cycles.
  The $i$-th degeneracy $s_i\Sigma = \tilde\Sigma = (\tilde \lambda, \tilde S_1, \ldots, \tilde S_{\tilde t})$ is obtained as follows.
  The ribbon structure and the outgoing boundary cycles $\tilde \rho$ of $\tilde \Sigma$ are
  \begin{align}
    \tilde{ \rho } = S_i(\rho)
    \quad\quad\text{ and } \quad\quad
    \tilde \lambda = F_{i+1}( \lambda )
  \end{align}
  with $F_{i+1}$ as in Definition \ref{definition:cylic_structure_on_SymGr_with_L}.
  The ghost surfaces are obtained analogously to Discussion \ref{discussion:degeneracies_geometrically}.
  The map $F_{i+1}$ induces an injection
  \begin{align}
    \Phi \colon \Lambda \xhr{} \tilde \Lambda
  \end{align}
  whose complement consists only of the cycle $\cycle{i+1}$.
  The $i$-th degeneracy $\tilde \Sigma$ has one surface more than $\Sigma$, i.e., $\tilde t = t+1$.
  For $r \neq \tilde t$ the surface $\tilde S_r$ is defined as
  \begin{align}
    \tilde S_r &= (g_r, m_r, \tilde A_r) \quad\text{where}\quad \tilde A_r = \{ \Phi(\alpha) \mid \alpha \in A_r \} \,.
  \end{align}
  The surface $\tilde S_{\tilde t}$ is defined as
  \begin{align}
    \tilde S_{\tilde t} &= (0, 0, \tilde A_{\tilde t}) \quad\text{where}\quad \tilde A_{\tilde t} = \tilde \Lambda - \imag(\Phi) = \{ \cycle{i+1} \} \,.
  \end{align}
\end{discussion}

The above discussions of the faces and degeneracies yield the following Proposition.

\begin{proposition}[B., Egas]
  Let $L$ be a finite, non-empty set of cardinality $m$.
  The sets of combinatorial 1-Sullivan diagrams $\{ C^\paen_L(n) \}_{n \in \bN}$ together with the face maps $d_i$ and degeneracies $s_i$ define a simplicial set
  which is isomorphic to the simplicial set underlying $\sqcup_g\SDspaen_g(m,1)$.
  Similarly, the sets of combinatorial 1-Sullivan diagrams $\{ C^\unen(n) \}_{n \in \bN}$ together with the face maps $d_i$ and degeneracies $s_i$ define a simplicial set
  which is isomorphic to the simplicial set underlying $\sqcup_g\SDsunen_g(m,1)$.
\end{proposition}


\subsection{The inclusion of the moduli space into its harmonic compactification}
\label{section:inclusion_of_fM_into_SD_combinatorial}
In \cite{EgasKupers2014}, Egas--Kupers provide a cellular homotopy equivalence from the harmonic compactification to the space of Sullivan diagrams.
The restriction to the moduli space
\begin{align}
  \fM^\star_g(m,1) \to \ov{\fM}^\star_g(m,1) \xr{\simeq} \SDsstar_g(m,1)
\end{align}
is given by sending a Riemann surface $F$ to the union of its complete critical graph $K$ and the incoming boundaries $\del_{in}$.
Here, we describe this map in terms of radial slit configurations and combinatorial Sullivan diagrams, see also \cite[Remark 3.12]{BoesEgas2017}.

In Section \ref{section:compactification_bar}, the space of radial slit configurations $\Radstar_g(m,1) \cong \fM^\star_g(m,1)$
is given as relative bi-simplicial set, i.e., it is a bi-simplicial set with some faces missing.
An open cell of bi-degree $(p,q)$ determines a sequence of permutations
\begin{align}
  \Sigma = (\tau_1 | \ldots | \tau_q) \in \SymGr([p])^q
\end{align}
subject to certain conditions.
By Section \ref{section:cluster_filtration}, the connected components of the complete graph corresponding to $\Sigma$ defines partition of $[p]$ into clusters.
Let $\tau := \tau_q \cdots \tau_1$.
On the set of cycles $\Lambda := \cycles(\tau)$, the following relation
\begin{align}
  \alpha \sim \beta \text{ if there exists $\alpha \ni i \sim_{CL} j \in \beta$}
\end{align}
defines an equivalence relation by Lemma \ref{lemma:partition_of_tau}.
We denote the partition of $\Lambda$ into equivalence classes by
\begin{align}
  \Lambda = A_1 \sqcup \ldots \sqcup A_k \,.
\end{align}
The \emph{radial projection} of $\Sigma$ is the Sullivan diagram $\pi_{\SD}(\Sigma) = (\lambda, S_1, \ldots, S_k) \in \SDstar_g(m,1)$ with $\lambda = \tau^{-1}$ and $S_i = (0,0,A_i)$.
Using the cellular homotopy equivalence of Egas--Kupers, the next Proposition is an immediate consequence of the above.

\begin{proposition}[B., Egas--Kupers]
  \label{proposition:inclusion_of_fM_into_SD_combinatorial}
  Let $g \ge 0$, $m \ge 0$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
  By
  \begin{align}
    \Phi \colon \Radstar_g(m,1) \cong \fM^\star_g(m,1) \xhr{} \ov{\fM}^\star_g(m,1) \xr{\simeq} \SDsstar_g(m,1)
  \end{align}
  we denote the composition of the Hilbert uniformization map, the inclusion into the harmonic compactification and the cellular homotopy equivalence described by Egas--Kupers.
  The restriction of $\Phi$ to an open cell $\Sigma \times \interior{\Delta^p \times \Delta^q} \subset \Radstar_g(m,1)$ is induced by the radial projection and the projection to the left simplex
  \begin{align}
    \Phi|_{\Sigma \times \interior{\Delta^p \times \Delta^q}} =
    \pi_{\SD} \times pr_{\interior{\Delta^p}} \colon
    \Sigma \times \interior{\Delta^p \times \Delta^q} \to
    \pi_{\SD}(\Sigma) \times \interior{\Delta^p} \,.
  \end{align}
\end{proposition}


\subsection{The stabilization map}
\label{section:stabilization_map_on_SD_combinatorial}
Let $g \ge 0$, $m \ge 1$, $n \ge 1$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
The stabilization map $\varphi \colon \fM^\star_g(m,n) \to \fM_{g+1}(m,n)$ is given by
sewing a surface of genus one with two boundary components to the first incoming boundary of $F \in \fM^\star_g(m,n)$,
See Figure \ref{figure:stabilization_map_mod_spc}.
\begin{figure}[ht]
  \centering
  \inkpic[.8\columnwidth]{stabilization_map_mod_spc}
  \caption{
    \label{figure:stabilization_map_mod_spc}
    The stabilization map is induced by sewing surfaces.
  }
\end{figure}
In homology, the induced map in homology is an isomorphism for $g \ge \frac{3\ast + 2}{2}$, see \cite{Harer1985} and \cite{RandalWilliams2016}.
The stabilization map extends to the spaces of Sullivan diagrams as follows.

\begin{definition}
  \label{definition:stabilization_map_SD}
  Let $g \ge 0$, $m \ge 1$, $n \ge 1$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
  The \defn{stabilization map}
  \begin{align}
    \ov\varphi \colon \SDsstar_g(m,1) \to \SDsstar_{g+1}(m,1)
  \end{align}
  is the simplicial map that increases the genus of the ghost surface attached to the vertex $0$ by one,
  see Figure \ref{figure:stabilization_map_SD}.
  More precisely,
  if $\Sigma = (\lambda, S_1, \ldots, S_t)$ and $S_1 = (g_1, m_1, A_1)$ such that $A_1$ contains the cycle to which $0$ belongs, then
  $\ov\varphi(\Sigma) = (\lambda, (g_1+1, m_1, A_1), S_2, \ldots, S_t)$.
\end{definition}

\begin{figure}[ht]
  \centering
  \inkpic[.7\columnwidth]{stabilization_map_SD}
  \caption{
    \label{figure:stabilization_map_SD}
    The stabilization map increases the genus of the ghost surface attached to the vertex $0$.
  }
\end{figure}

\begin{remark}
  \label{remark:stabilization_map_as_inclusion_of_a_sub-complex}
  The stabilization map $\ov\varphi \colon \SDsstar_g(m,1) \to \SDsstar_g(m,1)$ is an injective simplicial map.
  In particular, it identifies $\SDstar_g(m,1)$ with a sub-complex of $\SDstar_{g+1}(m,1)$.
\end{remark}
