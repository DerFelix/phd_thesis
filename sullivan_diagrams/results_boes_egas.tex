\section{A summary of the results with Egas Santander}
\label{section:on_the_homotopy_type_of_the_space_of_sullivan_diagrams}
In this section, we provide the main results of our article with Egas Santander \cite{BoesEgas2017}.
Recall that the harmonic compactification $\ov\fM^\star_g(m,1)$ is cellularly homotopy equivalent to the space of Sullivan diagrams $\SDsstar_g(m,1)$ by \cite{EgasKupers2014},
see also Proposition \ref{proposition:inclusion_of_fM_into_SD_combinatorial}.
Therefore, the results presented below carry over to the harmonic compactifications.

The results on the stabilization map of the moduli spaces and its space of Sullivan diagrams are in Section \ref{section:boes_egas_stability}.
The results on structure of the homology of the space of Sullivan diagrams are in Section \ref{section:boes_egas_structure_of_homology}.
The results on infinite families of non-trivial string operations are in Section \ref{section:boes_egas_non_trivial_families}.
Lastly, we computed the homology of the space of Sullivan diagrams for small parameters $g$ and $m$, see Section \ref{section:boes_egas_computations}.


\subsection{Homological stability and high connectivity}
\label{section:boes_egas_stability}
Using our combinatorial description of the spaces of Sullivan diagrams,
we extend the stabilization map of the moduli spaces to their space of Sullivan diagrams.
\begin{theorem}[{\cite[Theorem 4.1]{BoesEgas2017}}]
  \label{theorem:boes_egas_stability}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \paun, \unen, \unun \}$.
  The stabilization map $\varphi \colon \fM^\star_g(m,1) \to \fM^\star_{g+1}(m,1)$ extends to the space of Sullivan diagrams, i.e.,
  there is a map $\ov\varphi \colon \SDsstar_g(m,1) \to \SDsstar_{g+1}(m,1)$ making the following diagram commutative.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      \fM^\star_g(m,1)      \ar{r}{\varphi} \ar{d} \& \fM^\star_{g+1}(m,1) \ar{d} \\
      \SDsstar_g(m,1) \ar{r}{\ov\varphi}     \& \SDsstar_{g+1}(m,1)
    \end{tikzcd}
  \end{align}
  The stabilization map $\ov{\varphi}$ induces an isomorphism in homology in the degrees $\ast \le g + m - 2$.
  Moreover, if $m > 2$, the stabilization map $\ov\varphi$ is $(g+m-2)$ connected.
\end{theorem}

The usual framework that is used to proof homological stability cannot be applied to our situation because the spaces of Sullivan diagrams are highly connected
(see Theorem \ref{theorem:boes_egas_connectivity}).
Our proof of the Theorem \ref{theorem:boes_egas_stability} is summarized as follows.
The stabilization map $\ov\varphi_g$ identifies the cellular chain complex $\SDstar_g(m,1)$ with a subcomplex of $\SDstar_{g+1}(m,1)$.
On the pair $( \SDstar_g(m,1), \SDstar_{g+1}(m,1))$, we construct a discrete Morse flow that is perfect in the degrees $\ast \le (g+m-1)$.
Consequently, the stabilization map induces an integral homology isomorphism in the degrees $\ast \le g + m - 2$.
By Theorem \ref{theorem:boes_egas_connectivity}, $\SDsstar_g(m,1)$ is $1$-connected if $m > 2$ and, in this case, it follows that $\ov\varphi_g$ is $(g+m-2)$-connected.
In Chapter \ref{chapter:homotopy_type_of_SDspaen}, we will show that, in the parametrized enumerated case, the stabilization map $\ov\varphi_g$ is $(g+m-2)$-connected for all $m$ and
we believe that this holds true also in the (un)parametrized (un)enumerated cases.

\begin{theorem}[{\cite[Theorem 4.2]{BoesEgas2017}}]
  \label{theorem:boes_egas_connectivity}
  Let $g \ge 0$ and $m > 2$.
  The spaces $\SDspaen_g(m,1)$, $\SDspaun_g(m,1)$, $\SDsunen_g(m,1)$ or $\SDsunun_g(m,1)$ are highly connected with respect to $m$, i.e.,
  \begin{align}
   \pi_\ast( \SDspaen_g(m,1) ) = 0 \mspc{and}{20} \pi_\ast( \SDsunen_g(m,1) ) = 0 \mspc{for}{20} \ast \le m-2
  \end{align}
  and
  \begin{align}
    \pi_\ast( \SDspaun_g(m,1) ) = 0 \mspc{and}{20} \pi_\ast( \SDsunun_g(m,1) ) = 0 \mspc{for}{20} \ast \le m'
  \end{align}
  where $m'$ is the largest even number strictly smaller than $m$.
\end{theorem}


\subsection{On the homology}
\label{section:boes_egas_structure_of_homology}
It is a non-trivial task to find non-trivial classes in the homology of the spaces of Sullivan diagrams.
We show that every homology class is represented by a chain of Sullivan diagrams without degenerate outgoing boundaries.
\begin{proposition}[{\cite[Proposition 4.3]{BoesEgas2017}}]
  \label{proposition:support_of_homology}
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \paun, \unen, \unun \}$.
  Let $\SD := \SDstar_g(m,1)$ and let $R$ be an arbitrary coefficient group.
  Denote the sub-complex of Sullivan diagrams with degenerate outgoing boundaries by $D$.
  Every homology class $x \in H_\ast(\SD; R)$ is represented by a chain $\sum \kappa_i c_i$ with $c_i \notin D$.
\end{proposition}
The proof of Proposition \ref{proposition:support_of_homology} follows from the properties of the discrete Morse flows which
we construct in the proofs of Theorem \ref{theorem:boes_egas_stability} and Theorem \ref{theorem:boes_egas_stability}.
In Chapter \ref{chapter:homotopy_type_of_NSD}, we obtain another desuspension theorem, relating $\SDstar_g(m,1) / D$ with an $(m-1)$-fold suspension of $\SDstar_g(m,1) - D$.

Forgetting the enumeration of the outgoing boundaries is not a covering for Sullivan diagrams having more then two degenerate outgoing boundaries.
Nonetheless, using Proposition \ref{proposition:support_of_homology}, the forgetful maps behave like coverings in homology, i.e., there are ``transfer maps''.
These ``trace maps'' are used to construct some of the non-trivial classes in Proposition \ref{proposition:boes_egas_non_trivial_families_via_string_topology}.
\begin{proposition}[{\cite[Proposition 5.13]{BoesEgas2017}}]
  \label{proposition:boes_egas_covering_in_homology}
  In homology there are maps
  \begin{align}
    tr \colon H_\ast( \SDpaun_g(m,1); \bZ ) &\to H_\ast( \SDpaen_g(m,1); \bZ )
    \intertext{respectively}
    tr \colon H_\ast( \SDunun_g(m,1); \bZ ) &\to H_\ast( \SDunen_g(m,1); \bZ )
  \end{align}
  where $(\pi^\paen_\paun)_\ast \circ tr$ respectively $(\pi^\unen_\unun)_\ast \circ tr$ is the multiplication by $m!$.
\end{proposition}


\subsection{Non-trivial families of higher string operations}
\label{section:boes_egas_non_trivial_families}
Denote the surface of genus $g$, one parametrized incoming and one parametrized outgoing boundary by $S_{g,2}$.
Glueing the $k$-th punctured disc to the outgoing boundary component of $S_{g,2}$ induces the canonical inclusion of the $k$-th Braid group resp.\ its classifying space:
\begin{align}
  Br_k \xhr{} \Gamma_{g,1}^k \quad \text{resp.} \quad \fM^\unun_0(k,1) \xhr{} \fM^\unun_g(k,1) \,.
\end{align}
By construction, these inclusions are compatible with respect to the stabilization of the genus.
By $\alpha_g$, we denote the image of the braid generator under the canonical map
\begin{align}
  Br_2 \cong \pi_1( \fM^\unun_0(2,1) ) \xhr{} \pi_1( \fM^\unun_g(2,1) ) \to \pi_1(\ov{\fM}^\unun_g(2,1)) \cong \pi_1( \SDsunun_g(2,1) ) \,.
\end{align}

\begin{proposition}[{\cite[Proposition 5.1]{BoesEgas2017}}]
  \label{proposition:boes_egas_pi_1_g_0_m_2}
  The fundamental group of the space of Sullivan diagrams $\SDsunun_0(2,1)$ is
  \begin{align}
    \pi_1( \SDsunun_0(2,1) ) \cong
    \begin{cases}
      \bZ  \langle \alpha_0 \rangle & g = 0 \\
      \bZ_2\langle \alpha_g \rangle & g > 0
    \end{cases} \,.
  \end{align}
  The homomorphism on fundamental groups induced by the stabilization map
  \begin{align}
    \varphi \colon \pi_1(\SDsunun_g(2,1)) \to \pi_1(\SDsunun_{g+1}(2,1))
  \end{align}
  sends $\alpha_g$ to $\alpha_{g+1}$.
\end{proposition}

Using the methods of \cite{Wahl2016} and Proposition \ref{proposition:boes_egas_covering_in_homology},
we construct infinite families of non-trivial classes of infinite order that correspond to non-trivial higher string operations.

\begin{proposition}[{\cite[Proposition 5.10 and Proposition 5.14]{BoesEgas2017}}]
  \label{proposition:boes_egas_non_trivial_families_via_string_topology}
  Let $m>0$, $1\leq i \leq m$, $c_i>1$ and $c=\sum_i c_i$.
  \begin{enumerate}
    \item[$(i)$]  There are classes of infinite order
      \begin{align}
        \widetilde{\Gamma}_m                      & \in H_{4m-1}(\SDspaen_m(m,1); \bZ)
      \intertext{and}
        \widetilde{\Omega}_{(c_1, \ldots, c_m)}   & \in H_{2c-1}(\SDspaen_0(c,1); \bZ) \,.
      \end{align}
      All these classes correspond to non-trivial higher string operations.
    \item[$(ii)$] There are classes of infinite order
      \begin{align}
        \Gamma_m                        & \in H_{4m-1}(\SDspaun_m(m,1); \bZ)
      \intertext{and}
        \Omega_{(c_1, \ldots, c_m)}     & \in H_{2c-1}(\SDspaun_0(c,1); \bZ)
      \intertext{and}
        \zeta_{2m}                      & \in H_{2m-1}(\SDspaun_0(2m,1); \bZ) \,.
      \end{align}
  \end{enumerate}
\end{proposition}

The classes $\tilde \Gamma$ and $\tilde \Omega$ are constructed as follows.
The PROP-structure of the surface PROP $\oplus_{g \ge 0, m \ge 1} H_\ast( \fM^\paen_g(m,1); \bZ )$ extends to $\oplus_{g \ge 0, m \ge 1} H_\ast( \SDspaen_g(m,1); \bZ )$ and,
roughly speaking, it is defined as follows.
For a Sullivan diagram $\Sigma \in \SDspaen_g(m,1)$ with no degenerate boundary cycles and
Sullivan diagrams $\Sigma_1, \ldots, \Sigma_m \in \sqcup_{g' \ge 0, m' \ge 1} \SDspaen_{g'}( m', 1)$,
we cut open the ground circle of each $\Sigma_i$ at the vertex $0$ to obtain a ``ground interval with decorated surfaces attached''.
These intervals are glued to the admissible edges of the $i$-th outgoing boundary of $\Sigma$.
For a precise definition, see e.g.\ \cite[Definition 2.17]{BoesEgas2017}.
Using this PROP-structure, the classes $\tilde \Gamma$ and $\tilde \Omega$ are the products defined in \eqref{equation:defn_tilde_gamma} and \eqref{equation:defn_tilde_omega}.
We begin with the basic building blocks.
\begin{figure}[ht]
  \centering
  \inkpic[.75\columnwidth]{building_blocks}
  \caption{The building blocks, where we abbreviate the name of a leaf $l_i$ by $i$.}
  \label{figure:building_blocks}
\end{figure}
See Figure \ref{figure:building_blocks} for examples for small $m$.
\begin{enumerate}
\item For $m>0$ consider the permutation $\widetilde{\lambda}_\zeta = \cycle{0\ l_1\ 1\ l_2\ \ldots\ l_{m-1}\ m-1\ l_m}$ and let $\Lambda_\zeta$ denote its unique cycle.
  We define the chain $\widetilde{\zeta}_m := (\widetilde{\lambda}_{\zeta}, (0, 0, \Lambda_\zeta)) \in \SDpaen_0(m,1)$.
\item
  For $m>1$ we define the chain $\widetilde{\omega}_m := \widetilde{\omega}_{m,1} - \widetilde{\omega}_{m,2}\in \SDpaen_0(m,1)$, where
  $\widetilde{\omega}_{m,1} = (\lambda_1,S_{0,1},S_{1,1},S_2,\ldots, S_{m})$ and
  $\widetilde{\omega}_{m,2} = (\lambda_2,S_{0,2},S_{1,2},S_2,\ldots, S_{m})$
  are given by the following data:
  \begin{align*}
    \lambda_{0,1} &= \cycle{0} \text{ and } \lambda_{0,2} = \cycle{0\ l_1} \\
    \lambda_{1,1} &= \cycle{1\ 3\ \ldots \ 2m-1 \ l_1} \text{ and } \lambda_{1,2} = \cycle{1\ 3\ \ldots \ 2m-1} \\
    \lambda_i     &= \cycle{2i - 2\ l_i} \text{ for } 2 \le i \le m \\
    \lambda_1     &= \lambda_{0,1} \lambda_{1,1} \lambda_2 \cdots \lambda_m \\
    \lambda_2     &= \lambda_{0,2} \lambda_{1,2} \lambda_2 \cdots \lambda_m \\
    S_{i,j}       &= (0,0,\lambda_{i,j}) \text{ for } i =0,1 \text{ and } j = 1,2 \\
    S_i           &= (0,0,\lambda_i) \text{ for } 2\leq i \leq m
  \end{align*}
\item
  We define the chain $\widetilde{\gamma} := \widetilde{\gamma}_1+\widetilde{\gamma}_2 -\widetilde{\gamma}_3\in \SDpaen_1(m,1)$ where for $1\leq i \leq 3$,
  $\widetilde{\gamma}_i = (\lambda_i,S_{1,i}, S_{2,i})$ and these are given by the following data:
  \begin{align*}
    \lambda_{1,j} &= \cycle{0} \text{ for } j=1,2\\
    \lambda_{1,3} &= \cycle{0 \ l_1} \\
    \lambda_{2,1} &= \cycle{l_1 \ 1 \ 3 \ 2} \\
    \lambda_{2,2} &= \cycle{1 \ 3 \ l_1 \ 2} \\
    \lambda_{2,3} &= \cycle{1 \ 3 \ 2}\\
    \lambda_{j} &= \lambda_{1,j} \lambda_{2,j} \text{ for } j=1,2,3 \\
    S_{i,j} &= (0,0,\lambda_{i,j}) \text{ for } i = 1,2 \text { and } j=1,2,3
  \end{align*}
\end{enumerate}
The chains $\widetilde{\omega}_{m}$ and $\widetilde{\gamma}$ are cycles in the chain complex of Sullivan diagrams.
However, the chain $\widetilde{\zeta}_m$ is not a cycle.

Now, let $m > 0 $ and let $(c_1, \ldots , c_m)$ be a tuple of integers with $c_i > 1$ for $1 \leq i \leq m$ and set $c := \sum_{i=1}^m c_i$.
We define the chain $\widetilde{\Omega}_{(c_1, \ldots, c_m)}\in \SDpaen_0(c,1)$ to be
\begin{align}
  \label{equation:defn_tilde_omega}
  \widetilde{\Omega}_{(c_1, \ldots, c_m)} :=
  \widetilde{\zeta}_m \circ
  (\widetilde{\omega}_{c_1}\otimes
  \widetilde{\omega}_{c_2}\otimes
  \ldots \otimes
  \widetilde{\omega}_{c_m})
\end{align}
where $\circ$ denotes the PROPeradic multiplication.
Note that $\widetilde{\Omega}_{(c)} = \widetilde{\zeta}_1\circ \widetilde{\omega}_c = \widetilde{\omega}_c$.
See Figure \ref{figure:generator_omega} for another example.
\begin{figure}[ht]
  \centering
  \inkpic[.9\columnwidth]{generator_omega}
  \caption{\label{figure:generator_omega}The chain $\widetilde{\Omega}_{(3,3)}$.}
\end{figure}

For $m> 0$ we define the chain $\widetilde{\Gamma}_{m} \in \SDpaen_m(m,1)$ to be
\begin{align}
  \label{equation:defn_tilde_gamma}
  \widetilde{\Gamma}_{m}:=
  \widetilde{\zeta}_m \circ
  (\widetilde{\gamma}^{\otimes m}) \,
\end{align}
Note that $\widetilde{\Gamma}_1=\widetilde{\gamma}$.

The chains $\tilde \Gamma$ and $\tilde \Omega$ are cycles in the chain complex of Sullivan diagrams.
Using the Kontsevich--Soibelman recipe (made explicit in \cite[Section 6.2]{WahlWesterland2016}),
our classes act non-trivially on the Hochschild homology of the Frobenius algebra $H^\ast( \bS^2; \bQ)$.
This proofs part \emph{(i)} of Proposition \ref{proposition:boes_egas_non_trivial_families_via_string_topology}.
Part \emph{(ii)} follows the study of the string operations associated to the transfer of $\Omega_{(c_1, \ldots, c_m)}$, $\Gamma_m$ and $\zeta_{2m}$.


\subsection{Computational results for small parameters \texorpdfstring{$g$ and $m$}{g and m}}
\label{section:boes_egas_computations}
We obtain the integral homology of $\SDsunun_g(m,1)$ and $\SDspaun_g(m,1)$ using computer software.
\begin{proposition}[{\cite[Proposition A.1]{BoesEgas2017}}]
  \label{proposition:boes_egas_computations_unun}
  The integral homology of $\SDsunun_g(m,1)$, for small parameters $2g+m$, is given by the tables \ref{result:genus_0}, \ref{result:genus_1}, \ref{result:genus_2} and \ref{result:genus_3}.
\end{proposition}

\begin{proposition}[{\cite[Proposition A.2]{BoesEgas2017}}]
  \label{proposition:boes_egas_computations_paun}
  The integral homology of $\SDspaun_g(m,1)$, for small parameters $2g+m$, is given by the tables \ref{result:para_genus_0}, \ref{result:para_genus_1} and \ref{result:para_genus_2}.
\end{proposition}

Our program is mainly written in \emph{Python 2.7}.
Given parameters $g$ and $m$ it produces the integral chain complexes $\SDunun_g(m,1)$ and $\SDpaun_g(m,1)$.
Computing its integral homology is by far the most time consuming task.
There is a zoo of programs and libraries for this purpose.
We use a modified version of \emph{The Original CHomP Software} by Pawe\l{} Pilarczyk \cite{chomp_orig}.
Even small parameter $2g+m$ lead to integer overflows in the current version of CHomP and
we work around this issue by forcing CHomP to use the \emph{GNU Multiple Precision Arithmetic Library} \cite{GMP}.
All results were produced on an \emph{Intel i7-2670QM} and \emph{Intel i5-4570} running \emph{Debian Sid} and \emph{Debian Jessie} with \emph{Linux Kernel 4.2.0-1-amd64}.
For the source code of our program, see \cite{hosd}.

In \cite{EgasComputations2014}, Egas Santander used a separate program in order to compute the integral homology of
the genus zero case using the fact that every Sullivan diagram $\Sigma \in \SDunun_0(m,1)$ is the same as a weighted, non-crossing partition.
Her program is implemented in \emph{Magma} \cite{Magma} and our results agree with these computations.

\begin{landscape}
  \begin{table}[ht]
    \centering
    \scalebox{.9}{\begin{tabular}{C{3ex}L{3ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}}
      \hline
      m & H_0 & H_1 & H_2 & H_3 & H_4 & H_5 & H_6 & H_7 & H_8 & H_9 & H_{10} \\ \hline
      1 & \bZ &     &     &     &     &     &     &     &     &     & \\
      2 & \bZ & \bZ &     &     &     &     &     &     &     &     & \\
      3 & \bZ &     &     & \bZ &     &     &     &     &     &     & \\
      4 & \bZ &     &     & \bZ &     &     &     &     &     &     & \\
      5 & \bZ &     &     &     &     & \bZ &     &     &     &     & \\
      6 & \bZ &     &     &     &     & \bZ &     & \bZ & \bZ &     & \\
      7 & \bZ &     &     &     &     &     &     & \bZ &     &     & \\
      8 & \bZ &     &     &     &     &     &     & \bZ &     & \bZ & \bZ \\ \hline
    \end{tabular}}
    \caption{\label{result:genus_0}The integral homology $H_\ast( \SDsunun_0(m,1); \bZ )$.}
    \vspace{2ex}
    \scalebox{.9}{\begin{tabular}{C{3ex}L{3ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}}
      \hline
      m & H_0 & H_1 & H_2 & H_3 & H_4 & H_5 & H_6 & H_7 & H_8 & H_9 \\ \hline
      1 & \bZ &     &     & \bZ &     &     &     &     &     & \\
      2 & \bZ & \bZ_2 &   & \bZ &     &     &     &     &     & \\
      3 & \bZ &     &     & \bZ_3 &   & \bZ^2 & \bZ &   &     & \\
      4 & \bZ &     &     & \bZ_2 &   & \bZ\oplus \bZ_2 & \bZ_2 & \bZ^2 & \bZ^2 & \\
      5 & \bZ &     &     &     &     &     & \bZ & \bZ^5 & \bZ^3 & \bZ_2 \\ \hline
    \end{tabular}}
    \caption{\label{result:genus_1}The integral homology $H_\ast( \SDsunun_1(m,1); \bZ )$.}
    \vspace{2ex}
    \scalebox{.9}{\begin{tabular}{C{3ex}L{3ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{13ex}L{9ex}L{9ex}}
      \hline
      m & H_0 & H_1 & H_2 & H_3 & H_4 & H_5 & H_6 & H_7 & H_8 & H_9 & H_{10} \\ \hline
      1 & \bZ &     & \bZ & \bZ_5 &   & \bZ^2 & \bZ_3 & &     &     & \\
      2 & \bZ & \bZ_2 &   & \bZ_2 &   & \bZ \oplus \bZ_2 & \bZ\oplus \bZ_2 & \bZ^2 & \bZ\oplus \bZ_2 & \bZ_2 & \\
      3 & \bZ &     &     & \bZ_3 & \bZ_2 & & \bZ^4 & \bZ^9\oplus \bZ_2 & \bZ^4\oplus \bZ_2 \oplus \bZ_3^2 & \bZ\oplus \bZ_2 & \bZ \\ \hline
    \end{tabular}}
    \caption{\label{result:genus_2}The integral homology $H_\ast( \SDsunun_2(m,1); \bZ )$.}
    \vspace{2ex}
    \scalebox{.9}{\begin{tabular}{C{3ex}L{3ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{13ex}L{9ex}L{9ex}L{9ex}}
      \hline
      m & H_0 & H_1 & H_2 & H_3 & H_4 & H_5 & H_6 & H_7 & H_8 & H_9 & H_{10} & H_{11} \\ \hline
      1 & \bZ &     & \bZ &     & \bZ & \bZ_{35} & \bZ & \bZ^5 & \bZ\oplus \bZ_2^2 \oplus \bZ_3 & & \bZ_2 & \bZ_2 \\ \hline
    \end{tabular}}
    \caption{\label{result:genus_3}The integral homology $H_\ast( \SDsunun_3(m,1); \bZ )$.}
  \end{table}

  \begin{table}[ht]
    \centering
    \scalebox{.9}{\begin{tabular}{C{3ex}L{3ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}}
      \hline
      m & H_0 & H_1 & H_2 & H_3 & H_4 & H_5 & H_6 & H_7 & H_8 & H_9 & H_{10} \\ \hline
      1 & \bZ & \bZ &     &     &     &     &     &     &     &     & \\
      2 & \bZ & \bZ & \bZ & \bZ &     &     &     &     &     &     & \\
      3 & \bZ &     &     & \bZ^3 & \bZ^2 & \bZ & \bZ &  &    &     & \\
      4 & \bZ &     &     & \bZ & \bZ & \bZ^6 & \bZ^5 & \bZ^2 & \bZ^2 & & \\
      5 & \bZ &     &     &     &     & \bZ^7 & \bZ^{10} & \bZ^{13} & \bZ^{11} & \bZ^5 & \bZ^3 \\ \hline
    \end{tabular}}
    \caption{\label{result:para_genus_0}The integral homology $H_\ast( \SDspaun_0(m,1); \bZ )$.}
    \vspace{2ex}
    \scalebox{.9}{\begin{tabular}{C{3ex}L{3ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}L{5ex}}
      \hline
      m & H_0 & H_1 & H_2 & H_3 & H_4 & H_5 & H_6 & H_7 & H_8 & H_9 & H_{10} \\ \hline
      1 & \bZ & \bZ &     & \bZ & \bZ &     &     &     &     &     & \\
      2 & \bZ &     &     & \bZ^2 & & \bZ^3 & \bZ^6 & \bZ^2 & &     & \\
      3 & \bZ &     &     &     & \bZ^2 & \bZ^{12} & \bZ^{11} & \bZ^9 & \bZ^{14} & \bZ^8 & \bZ \\ \hline
    \end{tabular}}
    \caption{\label{result:para_genus_1}The integral homology $H_\ast( \SDspaun_1(m,1); \bZ )$.}
    \vspace{2ex}
    \scalebox{.9}{\begin{tabular}{C{3ex}L{3ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}L{9ex}}
      \hline
      m & H_0 & H_1 & H_2 & H_3 & H_4 & H_5 & H_6 & H_7 & H_8 & H_9 & H_{10} & H_{11} \\ \hline
      1 & \bZ &     &     & \bZ &     & \bZ^2 & \bZ^2 & \bZ_3 &  &     &     & \\ \hline
      2 & \bZ &     &     &     & \bZ & \bZ^3 & \bZ^2 & \bZ^{12} & \bZ^{18} & \bZ^{13} \oplus \bZ_3^2 & \bZ^{10} & \bZ^4 \\ \hline
    \end{tabular}}
    \caption{\label{result:para_genus_2}The integral homology $H_\ast( \SDspaun_2(m,1); \bZ )$.}
  \end{table}
\end{landscape}
