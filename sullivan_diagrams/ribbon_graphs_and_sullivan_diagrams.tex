\section{Ribbon graphs and Sullivan diagrams}
\label{section:ribbon_graphs_and_sullivan_diagrams}
For the convenience of the reader, we recall the notions of combinatorial graphs, ribbon graphs, their boundary cycles, Sullivan diagrams and their thickenings in this section.

\begin{definition}
  \label{definition:combinatorial_graph}
  A \defn{combinatorial graph} $G$ is a tuple $G=(V,H,s,i)$, with a finite set of vertices $V$,
  a finite set of \defn{half-edges} $H$, a source map $s\colon H\to V$ and an involution $i\colon H\to H$.
  The source map $s$ ties each half-edge to its source vertex and the involution $i$ attaches half-edges together.
  The \defn{valence} of a vertex $v\in V$ is the cardinality of the set $s^{-1}(v)$ and we denote it by $|v|$.
  A \defn{leaf} of a graph is a fixed point of $i$.
  An \defn{edge} of the graph is an orbit of $i$.
  Note that leaves are also edges.
  We call both leaves and edges connected to a vertex of valence one \defn{outer-edges} all other edges are called \defn{inner-edges}.
  See Figure \ref{figure:ribbon_graph_fattening}.
\end{definition}

\begin{definition}
  A \defn{ribbon graph} $\Gamma=(G,\sigma)$ is a combinatorial graph together with a cyclic orderings $\sigma_v$ of the half edges incident at each vertex $v$.
  The cyclic orderings $\sigma_v$ define a permutation $\sigma \colon H \to H, \sigma(h) := \sigma_{s(h)}(h)$ which satisfies $s\sigma = s$.
  This permutation is called the \defn{ribbon structure} of the graph.
  Figure \ref{figure:ribbon_graph_example} shows some examples of ribbon graphs.
\end{definition}

\begin{figure}[ht]
  \centering
  \inkpic[.6\columnwidth]{ribbon_graph_simple_example}
  \caption{
    \label{figure:ribbon_graph_example}
    The thick lines show two different ribbon graphs with the same underlying combinatorial graph.
    The  ribbon structure is given by the clockwise orientation of the plane.
    The dotted lines indicate the boundary cycles.
    We make this graphical description precise using the names of the half edges indicated in the picture.
    (a) ribbon structure: $\sigma = (ABC)(\ov A \ov C \ov B)$.
    Boundary cycles: $\omega = (A\ov C)(B\ov A)(C \ov B)$.
    (b) ribbon structure: $\sigma = (ABC)(\ov A \ov B \ov C)$.
    Boundary cycles: $\omega = (A \ov B C \ov A B \ov C)$.
  }
\end{figure}

\begin{definition}
  \label{definition:boundary_cycles_of_a_ribbon_graph}
  The \defn{boundary cycles} of a ribbon graph $\Gamma = (G,\sigma)$ are the orbits of the permutation $\omega := \sigma\circ i \colon H \to H$.
  Let $c = (h_1, \ldots, h_k)$ be a boundary cycle of $\Gamma$.
  The \defn{boundary cycle sub-graph} of $c$ the sub-graph of $\Gamma$ uniquely defined by the set of half-edges $\{ h_1, i(h_1), h_2, i(h_2), \ldots \mid h_j \neq i(h_j) \}$.
  See Figure \ref{figure:ribbon_graph_example} for an example.
  When clear from the context, we will refer to a boundary cycle sub-graph simply as boundary cycle.
\end{definition}

\begin{definition}[Surface with decorations]
  \label{definition:fattening}
  Given a ribbon graph $\Gamma = (V,H,s,i,\sigma)$, we construct a surface with marked points at the boundary by ``fattening" the ribbon graph.
  More precisely, we start with a collection of oriented disks for every vertex
  \begin{align}
   \{ D_v \mid v \in V \}
  \end{align}
  and a collection of oriented strips for every half edge
  \begin{align}
    \{ I_h := [0,{\textstyle\frac{1}{2}}] \times [-1,1]\mid h\in H\} \,.
  \end{align}
  Preserving the orientations of the disks and the strips, we glue the boundary $\{0\} \times [-1,1]$ of every strip $I_h$ to
  the disk $D_{s(h)}$ in the cyclic order given by the ribbon structure of $\Gamma$.
  For each edge which is not a leaf, say $e=\{h, \overline{h}\}$,
  we glue $\{\frac1 2\}\times[-1,1]$ of $I_h$ to $\{\frac1 2\} \times[-1,1]$ of $I_{\overline{h}}$ via $-\id$ in the second factor
  (see Figure \ref{figure:ribbon_graph_fattening} (b)).
  
  We end our construction by collapsing each boundary component, which is not connected to a leaf or to a vertex of valence one, to a puncture.
  This procedure gives a surface with boundary, where each boundary component is connected to at least one leaf or one vertex of valence one.
  We interpret these as marked points on the boundary (see Figure \ref{figure:ribbon_graph_fattening} (c)).
  
  We call this surface together with the combinatorial data of the marked points at the boundary a \defn{surface with decorations} and denote it by $S_\Gamma$.
  The \defn{topological type} of a surface with decorations $S_\Gamma$ is its genus, number of boundary components and punctures
  together with the cyclic ordering of the marked points at the boundary.
\end{definition}

\begin{figure}[ht]
  \inkpic[.85\columnwidth]{ribbon_graph_fattening}
  \caption{
    \label{figure:ribbon_graph_fattening}
    The picture shows
    (a) a ribbon graph with 4 outer-edges and 7 inner-edges;
    (b) the surface obtained from the fattening procedure;
    (c) the surface with decorations obtained from (b) by labeling points at the boundary and collapsing unmarked boundary components to a puncture.
  }
\end{figure}

\begin{remark}
  \label{remark:fattening_graph}
  The topological type of $S_\Gamma$ is completely determined by $\Gamma$.
  Moreover, given an inner-edge $e$ of $\Gamma$ which is not a loop,
  collapsing $e$ gives a homotopy equivalence $S_\Gamma \xhr{\simeq} S_{\Gamma/e}$ and does not change the number of boundary cycles or their decorations.
  Thus, the surfaces $S_{\Gamma}$ and $S_{\Gamma/e}$ have the same topological type.
\end{remark}

\begin{remark}
  Usually, spaces of Sullivan diagrams are defined by geometric realization of certain semi-multi-simplicial sets.
  However, we will define the spaces of Sullivan diagrams as geometric realizations of certain multi-simplicial sets.
  In order to take simplicially degenerate Sullivan diagrams into account, we need to change the usual definition of ''admissible fat graphs`` by allowing vertices of valence two.
\end{remark}

\begin{definition}
  \label{definition:admissible_graph}
  An \defn{$n$-admissible ribbon graph} $\Gamma$ consists of:
  \begin{enumerate}
    \item an (isomorphism class) of connected ribbon graphs such that all vertices have valence at least $2$
    \item an integer $k \ge n$ and an enumeration of a subset of the set of leaves by $\{1,2,\ldots, k\}$
  \end{enumerate}
  such that:
  \begin{enumerate}
   \item each of the first $n$ leaves $L_1, \ldots, L_n$ is the only leaf in its boundary cycle $c_i$ for $1\leq i \leq n$,
   \item their corresponding boundary cycle sub-graphs
    \begin{align}
      \Gamma_{c_1}, \Gamma_{c_2}, \ldots, \Gamma_{c_n}
    \end{align}
    are disjointly embedded circles in $\Gamma$.
  \end{enumerate}
  We will refer to the first $n$ leaves as \defn{admissible leaves} and to their corresponding boundary cycles as \defn{admissible cycles}.
  Figure \ref{figure:ribbon_graph_fattening} (a) shows a ribbon graph that is not 1-admissible because the leaf with number 1 is not the only leaf in its boundary cycle.
  Figure \ref{figure:admissible_example} shows an example of a 3-admissible ribbon graph.
\end{definition}

\begin{figure}[ht]
  \centering
  \inkpic[.5\columnwidth]{3_admissible_ribbon_graph}
  \caption{\label{figure:admissible_example}An example of a 3-admissible ribbon graph.}
\end{figure}

\begin{definition}
  Let $\Gamma_1$ and $\Gamma_2$ be $n$-admissible ribbon graphs.
  We say $\Gamma_1 \sim_{\scriptscriptstyle{SD}} \Gamma_2$ if
  $\Gamma_1$ and $\Gamma_2$ are connected by a zigzag of edge collapses where we only collapse inner edges that do not belong to the admissible cycles and which are not loops.
  Equivalently, we have $\Gamma_1 \sim_{\scriptscriptstyle{SD}} \Gamma_2$ if $\Gamma_2$ can be obtained from $\Gamma_1$ by sliding vertices along edges that do not belong to the admissible cycles.
  Figure \ref{figure:sullivan_example} shows some examples of equivalent 1-admissible ribbon graphs.
\end{definition}

\begin{figure}[ht]
  \centering
  \inkpic[\columnwidth]{SD_equivalence_relation}
  \caption{
    \label{figure:SD_equivalence_relation}
    Four equivalent 1-admissible ribbon graphs.
    Since there is only one admissible cycle, instead of labeling edges by $e^1_j$ (as described in Definition \ref{definition:n_SD_multisimplicial}) they are just labeled by $e_j$.
  }
  \label{figure:sullivan_example}
\end{figure}

\begin{remark}
  Note that $\sim_{\scriptscriptstyle{SD}}$ is an equivalence relation.
\end{remark}

\begin{definition}
  \label{definition:SD_eq_relation}
  An \defn{$n$-Sullivan diagram} $\Sigma$ is an equivalence class of $n$-admissible ribbon graphs under the equivalence relation $\sim_{\scriptscriptstyle{SD}}$.
\end{definition}
