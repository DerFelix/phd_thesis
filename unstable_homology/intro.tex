\noindent
This chapter is an excerpt of the article \cite{BoedigheimerBoes2018}
Here, we construct three homology classes in the unstable range, using various techniques.
Our classes live in the moduli space with at most one parametrized incoming boundary curve and $m \ge 0$ unenumerated punctures.
To simplify our notation, we write $\fM_{g,n}^m$ instead of $\fM^\unun_g(m,n)$ and we drop the symbol $n$ resp.\ $m$ from the notation if $n=0$ resp.\ $m=0$.
The corresponding mapping class group is denoted by $\Gamma_{g,n}^m$ and we drop the the symbol $n$ resp.\ $m$ from the notation if $n=0$ resp.\ $m=0$.

Using Bödigheimer's space of parallel slit configurations \cite{Boedigheimer19901}, the integral homology of $\fM_{2,1}$ has been computed by Ehrenfried in \cite{Ehrenfried1997}.
Based on Visy's results on factorable groups in \cite{Visy201011}, Mehner applied discrete Morse theory to the Bödigheimer's model in \cite{Mehner201112} and detected some of the generators of
$H_\ast( \fM_{2,1}; \bZ)$ and $H_\ast( \fM_{1,1}^2; \bZ_2)$, see also Appendix \ref{appendix:unstable_homology_of_a_single_moduli_space}.
Adding our generators to their findings, we obtain the following result.
\begin{theorem}[B., Bödigheimer, Ehrenfried, Mehner]
  \label{theorem:unstable_homology_for_small_g_and_m}
  The integral homology of $\fM_{2,1}$ and $\fM_{1,1}^2$ is as follows.
  \begin{align*}
    H_\ast( \fM_{2,1}; \bZ) =
    \begin{cases}
      \bZ\cycle{c^2} & \ast = 0\\
      \bZ_{10}\cycle{cd} & \ast = 1\\
      \bZ_2\cycle{d^2} & \ast = 2\\
      \bZ\cycle{\lambda s} \oplus \bZ_2\cycle{T(e)} & \ast = 3\\
      \bZ_3\cycle{w_3} \oplus \bZ_2 \cycle{?}& \ast = 4 \\
      0 & \ast \ge 5
    \end{cases}
    \quad
    H_\ast( \fM_{1,1}^2; \bZ) =
    \begin{cases}
      \bZ\cycle{a^2c} & \ast = 0\\
      \bZ\cycle{a^2 d} \oplus \bZ_2\cycle{bc} & \ast = 1\\
      \bZ_2\cycle{a^2 e} \oplus \bZ_2\cycle{bd} & \ast = 2\\
      \bZ_2\cycle{f} & \ast = 3 \\
      0 & \ast \ge 4
    \end{cases}
  \end{align*}
  Here, the known generators $a$,$b$,$c$,$d$,$e$,$t$ and their products have been described in \cite[Kapitel 1]{Mehner201112} or \cite[Chapter 4]{BoesHermann2014},
  see also \cite{BoedigheimerBoes2018}.
  The generators $s \in H_3( \fM^1_{2,1}; \bQ)$, $w_3 \in H_4( \fM^1_{2,1}; \bZ)$ respectively $f \in H_3(\fM^2_{1,1}; \bZ)$ will be described in
  Section \ref{section:the_generator_s_is_non_trivial}, Section \ref{section:the_generator_w_3_is_non_trivial} respectively Section \ref{section:the_generator_f_is_non_trivial}.
  The only unknown generator is denoted by the symbol $?$.
\end{theorem}

In Section \ref{section:the_generator_s_is_non_trivial}, we study $\fM_{2,1}$ as the total space of the fibration $U \xhr{} \fM_{2,1} \to \fM_{2,0}$
whose fiber is the unit tangent bundle of the closed oriented surface of genus two.
We show that the image of the rational fundamental class $[U]$ is a rational generator denoted by $s \in H_3( \fM_{2,1}; \bQ)$.

In Section \ref{section:the_generator_w_3_is_non_trivial}, we study certain maps between the braid groups, spherical braid groups and mapping class groups.
Sending the braid generators $\sigma_1, \ldots, \sigma_5 \in Br_6$ to a certain system of five Dehn twists defines a Segal--Tillmann map $ST \colon \Conf^6(\bD^2) \to \fM_{2,1}$.
By a result of Cohen, see \cite[Theorem A.1.b]{CohenLadaMay1976}, we know that $H_4(\Conf^6(\bD^2); \bF_3) \cong \bF_3\langle \tilde w_3 \rangle$
where $\tilde w_3$ is the class of tree pairs of revolving particles that orbit the origin.
We denote the image of $\tilde w_3$ under $ST$ by $w_3$ and show $w_3$ generates the $3$-torsion of $H_4(\fM_{2,1}; \bZ)$.

Finally, in Section \ref{section:the_generator_f_is_non_trivial}, we define a homology class $f \in H_3( \fM^2_{1,1}; \bZ)$
by embedding a three dimensional torus $U(1)^3$ into Bödigheimer's space of parallel slit configurations $\Par^2_{1,1} \simeq \fM^2_{1,1}$.
The space of parallel slit configurations is a relative manifold in its bar compactification $Bar := Bar(\Par^2_{1,1})$.
We construct the Poincaré-dual $PD(f) \in H^9(Bar, Bar'; \cO)$ of $f$ and show that $H^9(Bar, Bar'; \cO) \cong \bZ_2\langle PD(f) \rangle$.
