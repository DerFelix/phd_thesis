\section{The generator \texorpdfstring{$f \in H_3(\fM^2_{1,1}; \bZ)$}{f in H\_3(M\^{}2\_\{1,1\}; Z)}}
\label{section:the_generator_f_is_non_trivial}

The integral class $f \in H_3( \fM^2_{1,1}; \bZ )$ is defined as the embedded three dimensional torus $U(1)^3 \subset \Par^2_{1,1}$ that is shown in Figure \ref{figure:the_class_f}.
\begin{proposition}
  \label{proposition:the_class_f_non_trivial}
  It is $H_3( \fM^2_{1,1}; \bZ ) \cong \bZ_2\cycle{f}$ where $f$ is represented by the embedded $3$-dimensional torus seen in Figure \ref{figure:the_class_f}.
\end{proposition}
We show that $f$ is non-trivial as follows.
Firstly, we construct its Poincaré-dual $PD(f) \in H^{12-3}(Bar, Bar'; \cO)$, with $Bar := Bar(\Par^2_{1,1})$ the bar compactification of $\Par^2_{1,1}$.
Then, we compute the cohomology of this finitely generated cochain complex explicitly and compare the generators.
For the convenience of the reader, we describe the construction of a Poincaré-dual of an embedded manifold below.

Denote the bar compactification of $\Par^2_{1,1}$ by $Bar$ and recall that
the space of parallel slit configurations $\Par^2_{1,1}$ is a relative manifold, i.e., $\Par^2_{1,1} \subset Bar$ is open and dense in the finite complex $Bar$ and
its complement $Bar' = Bar - \Par^2_{1,1}$ is a subcomplex of codimension one.
The Poincaré dual of an embedded closed, $d$-dimensional manifold $M \subset \Par^2_{1,1}$ is constructed as follows.
We assume that $M$ intersects every open cell $e \subset \Par^2_{1,1}$ transversely.
In particular, $M$ does not meet any cell of codimension more then $d$.
The intersection with an arbitrary cell $e \subset \Par^2_{1,1}$ of codimension $d$ is a finite number of points and
for each $x \in M \cap e$, we set $\fo(e) = \pm 1$ according to the orientation of $M$ at $x$ relative $e$ and $\Par^2_{1,1}$.
With this at hand, the Poincaré-dual of $M$ is the cochain $PD(M) \in C^{12-d}(Bar, Bar'; \cO)$ defined by evaluation:
\begin{align}
  PD(M)(e) = \sum_{x \in M \cap e} \fo(x) \,.
\end{align}

We define $f \in H_3(\Par^2_{1,1})$ by the embedded torus $U(1)^3 \xhr{} \Par^2_{1,1}$ that is shown in Figure \ref{figure:the_class_f}.
\begin{figure}[ht]
  \centering
  \begin{subfigure}[t]{0.30\textwidth}
    \centering
    \begin{tikzpicture}[yscale=.8, xscale=.8, x=1cm, y=1cm, line width=1pt]
      \draw (0,0.5) -- (5,0.5);
      \draw (0,5.5) -- (5,5.5);
      \draw (0,0.5) -- (0,5.5);
      \draw (5,0.5) -- (5,5.5);
      
      \draw[line width=2pt, color=red]  (2,3.00) to (2,4.00);
      \draw[line width=2pt, color=blue] (2,1.00) to (2,2.00);
      
      \draw[line width=1pt] (0,5.00) to (1,5.00);
      \draw[line width=1pt] (0,4.50) to (1,4.50);
      
      \draw[line width=1pt] (0,3.25) to (2,3.25);
      \draw[line width=1pt] (0,1.70) to (2,1.70);
      
      \draw[line width=1pt] (0,4.00) to (3,4.00);
      \draw[line width=1pt] (0,3.00) to (3,3.00);
      
      \draw[line width=1pt] (0,2.00) to (4,2.00);
      \draw[line width=1pt] (0,1.00) to (4,1.00);
    \end{tikzpicture}
  \end{subfigure}
  \hspace{1ex}
  \begin{subfigure}[t]{0.30\textwidth}
    \centering
    \begin{tikzpicture}[yscale=.8, xscale=.8, x=1cm, y=1cm, line width=1pt]
      \draw (0,0.5) -- (5,0.5);
      \draw (0,5.5) -- (5,5.5);
      \draw (0,0.5) -- (0,5.5);
      \draw (5,0.5) -- (5,5.5);
      
      \draw[line width=1pt, dashed] (0.7,5.2) to (1.3,5.2) to (1.3,4.3) to (0.7,4.3) to (0.7, 5.2);
      \draw[line width=2pt, color=green] (1, 3.00) to (1, 2.50) to (3.5, 2.50) to (3.5, 4.75) to (1, 4.75) to (1, 4.00);
      
      \draw[line width=1pt] (0,5.00) to (1,5.00);
      \draw[line width=1pt] (0,4.50) to (1,4.50);
      
      \draw[line width=1pt] (0,3.25) to (2,3.25);
      \draw[line width=1pt] (0,1.70) to (2,1.70);
      
      \draw[line width=1pt] (0,4.00) to (3,4.00);
      \draw[line width=1pt] (0,3.00) to (3,3.00);
      
      \draw[line width=1pt] (0,2.00) to (4,2.00);
      \draw[line width=1pt] (0,1.00) to (4,1.00);
    \end{tikzpicture}
  \end{subfigure}
  \hspace{1ex}
  \begin{subfigure}[t]{0.30\textwidth}
    \centering
    \begin{tikzpicture}[yscale=.8, xscale=.8, x=1cm, y=1cm, line width=1pt]
      \draw (0,0.5) -- (5,0.5);
      \draw (0,5.5) -- (5,5.5);
      \draw (0,0.5) -- (0,5.5);
      \draw (5,0.5) -- (5,5.5);
      
      \draw[line width=2pt, color=red]  (2,3.00) to (2,4.00);
      \draw[line width=2pt, color=blue] (2,1.00) to (2,2.00);
      
      \draw[line width=1pt, dashed] (0.7,5.2) to (1.3,5.2) to (1.3,4.3) to (0.7,4.3) to (0.7, 5.2);
      \draw[line width=2pt, color=green] (1, 3.00) to (1, 2.50) to (3.5, 2.50) to (3.5, 4.75) to (1, 4.75) to (1, 4.00);
      
      \draw[line width=1pt] (0,5.00) to (1,5.00);
      \draw[line width=1pt] (0,4.50) to (1,4.50);
      
      \draw[line width=1pt] (0,3.25) to (2,3.25);
      \draw[line width=1pt] (0,1.70) to (2,1.70);
      
      \draw[line width=1pt] (0,4.00) to (3,4.00);
      \draw[line width=1pt] (0,3.00) to (3,3.00);
      
      \draw[line width=1pt] (0,2.00) to (4,2.00);
      \draw[line width=1pt] (0,1.00) to (4,1.00);
    \end{tikzpicture}
  \end{subfigure}
  
  \caption{
    \label{figure:the_class_f}
    On the left, we describe the embedded torus $U(1)^2 \times \{ 1 \} \subset U(1)^3 \subset \Par^2_{1,1}$.
    The first factor defines the rotation of a slit along the red line and the second factor defined the rotation of a slit along the blue line.
    In the middle, we describe the embedded circle $\{1\}^2 \times U(1) \subset U(1)^3 \subset \Par^2_{1,1}$.
    It is the closed loop moving the pair of slits sitting in the dotted box along the green line.
    On the right, we describe the embedded torus $U(1)^3 \subset \Par_1(2,1)$ as a combination of two independent movements shown on the left and in the middle.
  }
\end{figure}
Observe that $U(1)^3$ intersects all cells transversely.
Observe further, that $U(1)^3$ intersects exactly four cells of codimension three non-trivially and
the intersection with each of these cells is a single point, see Figure \ref{figure:the_class_f_intersecting_codimension_three_cells}.
\begin{figure}[ht]
  \centering
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \begin{tikzpicture}[yscale=.8, xscale=1.3, x=1cm, y=1cm, line width=1pt]
      \draw (0,0.5) -- (5,0.5);
      \draw (0,5.5) -- (5,5.5);
      \draw (0,0.5) -- (0,5.5);
      \draw (5,0.5) -- (5,5.5);
      
      \draw[line width=1pt] (0,5.00) to (1,5.00);
      \draw[line width=1pt] (0,4.05) to (1,4.05);
      
      \draw[line width=1pt] (0,3.05) to (2,3.05);
      \draw[line width=1pt] (0,1.05) to (2,1.05);
      
      \draw[line width=1pt] (0,3.95) to (3,3.95);
      \draw[line width=1pt] (0,2.95) to (3,2.95);
      
      \draw[line width=1pt] (0,2.00) to (4,2.00);
      \draw[line width=1pt] (0,0.95) to (4,0.95);
    \end{tikzpicture}
    \caption*{The intersection of $U(1)^3$ with cell of combinatorial type $( \transp{5}{4} | \transp{3}{1} | \transp{4}{3} | \transp{2}{1} )$.}
  \end{subfigure}
  \hspace{3ex}
  \begin{subfigure}[t]{.45\textwidth}  
    \begin{tikzpicture}[yscale=.8, xscale=1.3, x=1cm, y=1cm, line width=1pt]
      \draw (0,0.5) -- (5,0.5);
      \draw (0,5.5) -- (5,5.5);
      \draw (0,0.5) -- (0,5.5);
      \draw (5,0.5) -- (5,5.5);
      
      \draw[line width=1pt] (0,5.05) to (1,5.05);
      \draw[line width=1pt] (0,3.00) to (1,3.00);
      
      \draw[line width=1pt] (0,4.05) to (2,4.05);
      \draw[line width=1pt] (0,1.05) to (2,1.05);
      
      \draw[line width=1pt] (0,4.95) to (3,4.95);
      \draw[line width=1pt] (0,3.95) to (3,3.95);
      
      \draw[line width=1pt] (0,2.00) to (4,2.00);
      \draw[line width=1pt] (0,0.95) to (4,0.95);
    \end{tikzpicture}
    \caption*{The intersection of $U(1)^3$ with cell of combinatorial type $( \transp{5}{3} | \transp{4}{1} | \transp{5}{4} | \transp{2}{1} )$.}
  \end{subfigure}

  \vspace{3ex}

  \begin{subfigure}[t]{.45\textwidth}
    \centering
    \begin{tikzpicture}[yscale=.8, xscale=1.3, x=1cm, y=1cm, line width=1pt]
      \draw (0,0.5) -- (5,0.5);
      \draw (0,5.5) -- (5,5.5);
      \draw (0,0.5) -- (0,5.5);
      \draw (5,0.5) -- (5,5.5);
      
      \draw[line width=1pt] (0,4.10) to (1,4.10);
      \draw[line width=1pt] (0,1.05) to (1,1.05);
      
      \draw[line width=1pt] (0,5.00) to (2,5.00);
      \draw[line width=1pt] (0,4.00) to (2,4.00);
      
      \draw[line width=1pt] (0,2.00) to (3,2.00);
      \draw[line width=1pt] (0,0.95) to (3,0.95);
      
      \draw[line width=1pt] (0,3.90) to (4,3.90);
      \draw[line width=1pt] (0,3.00) to (4,3.00);
    \end{tikzpicture}
    \caption*{The intersection of $U(1)^3$ with cell of combinatorial type $( \transp{4}{1} | \transp{5}{4} | \transp{2}{1} | \transp{4}{3} )$.}
  \end{subfigure}
  \hspace{3ex}
  \begin{subfigure}[t]{.45\textwidth}
    \begin{tikzpicture}[yscale=.8, xscale=1.3, x=1cm, y=1cm, line width=1pt]
      \draw (0,0.5) -- (5,0.5);
      \draw (0,5.5) -- (5,5.5);
      \draw (0,0.5) -- (0,5.5);
      \draw (5,0.5) -- (5,5.5);
      
      \draw[line width=1pt] (0,3.05) to (1,3.05);
      \draw[line width=1pt] (0,1.05) to (1,1.05);
      
      \draw[line width=1pt] (0,5.05) to (2,5.05);
      \draw[line width=1pt] (0,2.95) to (2,2.95);
      
      \draw[line width=1pt] (0,2.00) to (3,2.00);
      \draw[line width=1pt] (0,0.95) to (3,0.95);
      
      \draw[line width=1pt] (0,4.95) to (4,4.95);
      \draw[line width=1pt] (0,4.00) to (4,4.00);
    \end{tikzpicture}
    \caption*{The intersection of $U(1)^3$ with cell of combinatorial type $( \transp{3}{1} | \transp{5}{3} | \transp{2}{1} | \transp{5}{4} )$.}
  \end{subfigure}
  \caption{
    \label{figure:the_class_f_intersecting_codimension_three_cells}
    The torus $U(1)^3 \subset \Par^2_{1,1}$ described in Figure \ref{figure:the_class_f} intersects exactly four cells of codimension three non-trivially.
    We draw the intersection points schematically here.
  }
\end{figure}
An explicit computation of the orientations at the intersection points yields the Poincaré-dual of $f \colon U(1)^3 \xhr{} \Par^2_{1,1}$:
\begin{align}
  \begin{aligned}
  PD(f) &= ( \transp{5}{4} | \transp{3}{1} | \transp{4}{3} | \transp{2}{1} )
         - ( \transp{5}{3} | \transp{4}{1} | \transp{5}{4} | \transp{2}{1} ) \\
        &- ( \transp{4}{1} | \transp{5}{4} | \transp{2}{1} | \transp{4}{3} )
         + ( \transp{3}{1} | \transp{5}{3} | \transp{2}{1} | \transp{5}{4} )
  \end{aligned}
\end{align}
The Poincaré-dual is a cochain in the finitely generated chain complex $C^\ast( Bar, Bar'; \cO)$.
Using a computer program based on \cite{BoesHermann2014}, we verify
\begin{align}
  H^9( Bar(\Par^2_{1,1}), Bar'(\Par^2_{1,1}); \cO) ) \cong \bZ_2\cycle{PD(f)} \,.
\end{align}
This proves Proposition \ref{proposition:the_class_f_non_trivial}.


Our program is mainly written in \emph{C++11} and makes use of the \emph{GNU Multiple Precision Arithmetic Library} \cite{GMP} and the \emph{Boost C++ Libraries} \cite{boost}.
Given parameters $g$, $m$ and a coefficient ring, it produces the corresponding chain complexes of the Ehrenfried complex associated to $\Radunun_g(m,1)$.
Then, we compute its homology and the projection maps from the cycles to the homology.
This task consumes by far the most time and memory.
Using the projections, we test if our cycles are generators.
The above results were produced on an \emph{Intel i7-2670QM} and \emph{Intel i5-4570} running \emph{Debian Sid} and \emph{Debian Jessie} with \emph{Linux Kernel 4.2.0-1-amd64}.
For the source code of our program, see \cite{kappa}.
