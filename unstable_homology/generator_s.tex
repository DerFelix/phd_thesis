\section{The generator \texorpdfstring{$s \in H_3(\fM_{2,1}; \bQ)$}{s in H\_3(M\_\{2,1\}; Q)}}
\label{section:the_generator_s_is_non_trivial}

Let us recall some well known facts (whose proofs can be found in \cite[Section 4.2.5 and Section 12.6]{FarbMargalitMCG}).
The moduli space $\fM_{2,1}$ is homotopy equivalent to the moduli space of all closed oriented surfaces of genus two together with a choice of tangential direction at some base point.
Forgetting the tangential direction but keeping the base point defines a surjective map $\fM_{2,1} \to \fM_2^1$ to
the moduli space of all closed oriented surfaces of genus two together with a choice of base point.
The base is a rational classifying space for the mapping class group $\Gamma_2^1$, i.e.,
its rational homology is the rational homology of $\Gamma_2^1$.
Performing only rational computations, we are safe to assume that $\fM_{2,1} \to \fM_2^1$ is a circle fibration.
Forgetting the tangential direction and the base point, defines a surjective map $\fM_{2,1} \to \fM_2$ to the moduli space of all closed oriented surfaces of genus two.
The base is a rational classifying space for the mapping class group $\Gamma_2$ and, performing only rational computations, we are safe to assume that $\fM_{2,1} \to \fM_2$ is a fibration
whose fiber is the total space $U$ of the unit tangent bundle $\bS^1 \to U \to F$ over the closed oriented surface $F$ of genus two.
\begin{definition}
  \label{definition:generator_s}
  The image of the (rational) fundamental class $[U]$ in $H_3(\fM_{2,1}; \bQ)$ is denoted by $s$.
\end{definition}

A careful study of the rational spectral sequences associated to the diagram below will lead to Proposition \ref{proposition:the_class_s_non_trivial}.
\begin{align}
  \begin{tikzcd}[ampersand replacement=\&]
      \&                                \& 3                                    \& 4 \\
    1 \& \bS^1 \arrow{d}{\id} \arrow{r} \& U          \arrow{d} \arrow{r}       \& F       \arrow{d} \\
    2 \& \bS^1                \arrow{r} \& \fM_{2,1}  \arrow{d} \arrow{r}       \& \fM_2^1 \arrow{d} \\
      \&                                \& \fM_2                \arrow{r}{\id}  \& \fM_2
  \end{tikzcd}
\end{align}

\begin{proposition}
  \label{proposition:the_class_s_non_trivial}
  The inclusion of the fiber $U \hr{} \fM_{2,1}$ induces an isomorphism
  \begin{align}
    H_3(U; \bQ) \xr{\cong} H_3(\fM_{2,1}; \bQ) \cong \bQ \langle s \rangle \,.
  \end{align}
\end{proposition}

\begin{proof}
  Let us investigate the four spectral sequences.

  \subsubsection*{Spectral sequence 1}
  The first spectral sequence computes the homology of the unit tangent bundle $\mathbb S^1 \to U \to F$ of a surface of genus two.
  The action of $\pi_1(F)$ on $H_1(\mathbb S^1)$ is trivial because the bundle is oriented.
  The second page consists of two rows with three columns and a single non-trivial differential, see Figure \ref{figure:spectral_sequence_for_the_unit_tangent_bundle}.
  It is the multiplication with the Euler characteristic $\chi(F) = -2$.
  \begin{figure}[ht]
    \centering
    \begin{tikzcd}[ampersand replacement=\&, row sep=.5em, column sep=.8em]
      \bQ \& \bQ^4 \& \bQ \\
      \bQ \& \bQ^4 \& \bQ \arrow{llu}
    \end{tikzcd}
    \hspace{1cm}
    \begin{tikzcd}[ampersand replacement=\&, row sep=.5em, column sep=.8em]
      0   \& \bQ^4 \& \bQ \\
      \bQ \& \bQ^4 \& 0
    \end{tikzcd}
    \caption{\label{figure:spectral_sequence_for_the_unit_tangent_bundle}The second and third page of spectral sequence 1.}
  \end{figure}


  \subsubsection*{Spectral sequence 2}
  The second spectral sequence comes from a central extension of the mapping class groups $\Gamma_2^1$ by the integers.
  It is used in \cite[p.~33]{Harer1991} to compute certain rational Betti numbers.
  The second page has exactly two non-trivial columns and a single non-trivial differential, see Figure \ref{figure:spectral_sequence_central_extension_of_mcg}.
  Therefore, we obtain rational Betti numbers
  \begin{align}
    \label{ss:betti_gamma_2,1} \beta( \fM_2^1 ) = (1,0,1) \quad\text{ and }\quad \beta( \fM_{2,1} ) = (1,0,0,1) \,.
  \end{align}
  \begin{figure}[ht]
    \centering
    \begin{tikzcd}[ampersand replacement=\&, row sep=.5em, column sep=.8em]
      \bQ  \& 0   \& \bQ  \\
      \bQ  \& 0   \& \bQ \arrow{llu}
    \end{tikzcd}
    \caption{\label{figure:spectral_sequence_central_extension_of_mcg}The second page of spectral sequence two.}
  \end{figure}

  \subsubsection*{Spectral sequence 3}
  The first page of the third spectral sequence is $E^1_{p,q} \cong C_p( \fM_2; \cH_q(U ; \bQ))$ with $\cH_q(U; \bQ)$ the canonical $\pi_1(\fM_2)$-module.
  We have $\pi_1( \fM_2 ) \cong \Gamma_2$, so we need to understand how an orientation preserving diffeomorphism $f$ of the closed surface $F$ acts on $H_\ast(U; \bQ)$.
  Clearly, $f_\ast$ acts on $H_0(U; \bQ) \cong \bQ \cong H_3(U; \bZ)$ via the identity.
  By the discussion of spectral sequence 1, see Figure \ref{figure:spectral_sequence_for_the_unit_tangent_bundle}, we have isomorphisms
  \begin{align}
    H_1(U; \bQ) &\cong H_1(F; H_0(\bS^1; \bQ)) \cong H_1(F; \bQ)
    \intertext{and}
    H_2(U; \bQ) &\cong H_1(F; H_1(\bS^1; \bQ)) \cong H_1(F; \bQ)\,.
  \end{align}
  Note that these isomorphisms are $\Gamma_2$-equivariant.
  Therefore, the second page of spectral sequence 3 consists of four rows; rows 0 and 3 are isomorphic and rows 1 and 2 are isomorphic,
  see the left hand side of Figure \ref{figure:ss_3_4}.


  \subsubsection*{Spectral sequence 4}
  The discussion of this spectral sequence is similar to the discussion of spectral sequence 3.
  The second page of spectral sequence 4 consists of three rows:
  Row number 0 (resp.\ 1, resp.\ 2) of spectral sequence $4$ is identified with row 0 (resp.\ 2, resp.\ 3) of spectral sequence $3$,
  see the right hand side of Figure \ref{figure:ss_3_4}


  \subsubsection*{Rational computations}
  Denote $M_\ast = H_\ast(\Gamma_2; V)$ with $V$ the symplectic representation.
  By the above, the middle row(s) of spectral sequences 3 and 4 are rationally isomorphic to $M_\ast$.
  In \cite[Corollary 5.2.3]{LeeWeintraub1985}, the authors show that the reduced integral homology of $\Gamma_2$ is all $p$-torsion for $p=2,3,5$.
  Thus, the top and bottom rows are rationally trivial except for the trivial $\Gamma_2$-module $\bQ$ sitting in column zero.
  \begin{figure}[ht]
    \centering
    \begin{minipage}{.4\textwidth}
      \begin{align*}
      \begin{tikzcd}[ampersand replacement=\&, row sep=.5em, column sep=.8em]
        \bQ  \& 0   \& 0   \& 0   \& \ldots \\
        M_0 \& M_1 \& M_2 \& M_3 \& \ldots \\
        M_0 \& M_1 \& M_2 \& M_3 \& \ldots \\
        \bQ  \& 0   \& 0   \& 0   \& \ldots
      \end{tikzcd}
    \end{align*}
    \end{minipage}
    \hspace{1cm}
    \begin{minipage}{.4\textwidth}
      \begin{align*}
        \begin{tikzcd}[ampersand replacement=\&, row sep=.5em, column sep=.8em]
          \bQ  \& 0   \& 0   \& 0   \& \ldots \\
          M_0 \& M_1 \& M_2 \& M_3 \& \ldots \\
          \bQ  \& 0   \& 0   \& 0   \& \ldots
        \end{tikzcd}
      \end{align*}
    \end{minipage}
    \caption{
      \label{figure:ss_3_4}
      The second page of the spectral sequence $E^r_{\ast,\ast}( U \to \fM_{2,1} \to \fM_2 )$ is seen on the left.
      The second page of the spectral sequence $E^r_{\ast,\ast}(F \to \fM_2^1 \to \fM_2)$ is seen on the right.
    }
  \end{figure}
  With \eqref{ss:betti_gamma_2,1} it follows that $M_k = 0$ for all $k$.
  Therefore, $H_3(\fM_{2,1}; \bQ) \cong E^2_{0,3} \cong \bQ$ which is generated by the image of the fundamental class of the fiber $U$ in $\fM_{2,1}$.
  This ends the proof of Proposition \ref{proposition:the_class_s_non_trivial}.
\end{proof}
