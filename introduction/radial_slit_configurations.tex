\section{The moduli spaces of Riemann surfaces}
\label{section:moduli_spaces_of_riemann_surfaces}
On a closed (orientable) surface, every conformal structure defines a unique complex structure and vice versa.
Therefore, such a surface is a Riemann surface.
More generally, we define a Riemann surface to be a smooth and compact surface with a fixed conformal structure such that,
near the boundary, the metric is the product metric.
Below we impose certain conditions on some of the boundaries components:
We allow the boundary components to be enumerated and each boundary is allowed to come with a parametrization (which is a distinguished point on the boundary).

By $\fM^\paen_{g,k}$ we denote the moduli space of Riemann surfaces of genus $g \ge 0$ with $k \ge 0$ parametrized and enumerated boundary components.
For each $g \ge 0$, $m \ge 1$ and $n \ge 1$,
we consider the moduli space of conformal bordisms of genus $g$ with $n$ parametrized and enumerated \defn{incoming} boundaries and with $m$ additional \defn{outgoing} boundaries, namely
\begin{enumerate}
  \item $m$ parametrized and enumerated additional boundaries, denoted by $\fM^\paen_g(m,n)$;
  \item $m$ unparametrized and enumerated additional boundaries, denoted by $\fM^\unen_g(m,n)$;
  \item $m$ parametrized and unenumerated additional boundaries, denoted by $\fM^\paun_g(m,n)$;
  \item $m$ unparametrized and unenumerated additional boundaries, denoted by $\fM^\unun_g(m,n)$.
\end{enumerate}
These moduli spaces are related by the diagram below.
The horizontal maps are $m!$-fold coverings and the vertical map on the left is an $m$-dimensional torus bundle.
\begin{align*}
  \begin{tikzcd}[ampersand replacement=\&]
    \fM^\paen_g(m,n) \ar{r} \ar{d} \& \fM^\paun_g(m,n) \ar{d} \\
    \fM^\unen_g(m,n) \ar{r}        \& \fM^\unun_g(m,n) 
  \end{tikzcd}
\end{align*}
More precisely, on $\fM^\paen_g(m,n) = \fM^\paen_{g, m+n}$, the $m$-dimensional torus acts freely and properly discontinous by rotating around the parametrization of the $m$ outgoing boundaries.
The orbit space is $\fM^\unen_g(m,n)$.
On $\fM^\paen_g(m,n)$ resp.\ $\fM^\unen_g(m,n)$ the $m$-th symmetric group acts faithfully by permuting the $m$ outgoing boundaries.
The orbit space is $\fM^\paun_g(m,n)$ resp.\ $\fM^\unun_g(m,n)$.

These moduli spaces admit various (relative) cellular models and, in this thesis, we study two models in detail.
The theory of quadratic differentials yields a space of metric ribbon graphs, see Section \ref{section:metric_ribbon_graphs} and,
using harmonic potentials, Bödigheimer constructs spaces of slit configurations in \cite{Boedigheimer19901} and \cite{Boedigheimer2006},
see Section \ref{section:models:radial_slit_configurations} and Chapter \ref{chapter:radial_slit_configurations}.
Both models admit a canonical compactification and the study of the homology of these compactifications has interesting applications in the field of string topology, see Section \ref{section:string_topology}.
In Sections \ref{section:homological_stability} and \ref{section:unstable_homology}, we briefly recall what is known about the homology of the moduli spaces.


\subsection{Metric ribbon graphs}
\label{section:metric_ribbon_graphs}
Ribbon graph models play a prominent role in the study of moduli spaces and rely on the works of
\cite{StrebelQuadDiff}, \cite{Penner1987}, \cite{BowditchEpstein1988}, \cite{Harer1986}, \cite{Igusa2002} and \cite{Godin2004}.
For convenience of the reader, we review some facts from the theory of Strebel differentials and discuss a space of metric ribbon graphs.
A detailed discussion can be found in \cite{Looijenga1995}.

A quadratic differential $\phi$ on a Riemann surface $F$ (with $k$ parametrized and enumerated boudaries) is a section of $(TF^\ast)^{\otimes 2}$, i.e.,
it is of the form $\phi(z) dz^2$ in local coordinates.
The horizontal trajectories of $\phi$ are the closed curves along which $\phi(z) dz^2$ is real and positive.
The Jenkins--Strebel quadratic differentials (short: JS differential) are the holomorphic quadratic differentials for which the union of the non-closed horizontal trajectories have measure zero.
A JS differential $\phi$ defines a flat metric away from the discrete set of its zeros.
The non-closed (horizontal) trajectories of a JS differential cut the surface into finitely many closed cylinders.
Each of the cylinders has exactly one boundary component that is a union of non-closed trajectories;
the cylinder is exhausted by closed trajectories whose length is the circumference of the cylinder and
its other boundary component agrees with a boundary component of $F$.
Therefore, the union of the non-closed trajectories define an embedded graph $G_\phi \subset F$ such that
the edges are equiped with a positive length and, using the orientation of the surface $F$, the set of edges at each vertex is ordered cyclically.
Such a (connected) graph is called metric ribbon graph.
In $G_\phi$, consider a closed path $\gamma = (v_0, e_0, v_1, \ldots, v_{n-1}, e_n, v_0)$ that constists of egdes $e_i$ starting in the vertex $v_{i-1}$ and ending in the vertex $v_i$.
A closed path $\gamma = (v_0, e_1, \ldots, e_n, v_0)$ is called boundary component of the ribbon graph if,
for each $i=0,\ldots,n$, the succesor of the edge $e_i$ with respect to the cyclic operation at $v_{i+1}$ is $e_{i+1}$.
The boundary components of the metric ribbon graph are in one-to-one correpondence with the boundary components of $F$.
See Figure \ref{figure:ribbon_graph} for an example.
\begin{figure}[ht]
  \centering
  \inkpic[.7\columnwidth]{ribbon_graph}
  \caption{\label{figure:ribbon_graph}
    On the left, we  sketch the trajectories of a JS differential $\phi$ on a surface $S$ of genus zero with three boundary components.
    One of the boundary component is called $C$.
    The non-closed trajectories are thick.
    On the right, we sketch the metric ribbon graph $G_\phi$ corresponding to the non-closed trajectories.
    Hereby, the boundary component $C$ of $S$ corresponds to the boundary component $C'$ of $G_\phi$.}
\end{figure}

The parametrization of every boundary component $C \subset F$ is a choice of basepoint $p_C \in C$.
Following the vertical trajectory of $p_C \in C$ towards the embedded graph $G_\phi$ yields a marked point in every boundary component of $G_\phi$.
A metric ribbon graph with a distinguished point per boundary component is called marked metric ribbon graph.
Given an arbitrary marked metric ribbon graph $G$, we can replace its edges by thin stripes (respecting the cyclic orientation at each vertex) to obtain
a conformal surface $F_G$ of genus $g$ with $k$ parametrized boundary components.
Consequently, we say that $G$ has genus $g$ and $k$ boundary components.
The space of marked metric ribbon graphs of genus $g$ and $k$ parametrized and enumerated boundaries is denoted $Rib^\paen_{g,k}$ and
its topology is induced by the lenghts of the edges of its graphs.
It is homotopy equivalent to the respective moduli space, see \cite{Godin2004}:
\begin{align}
  \label{rib_simeq_modspc}
  Rib^\paen_{g,k} \simeq \fM^\paen_{g,k} \,.
\end{align}
Selecting a partition $k=m+n$ of the boundaries into $n$ incoming and $m$ outgoing components, we denote the space of marked metric ribbon graphs by $Rib^\paen_g(m,n) := Rib^\paen_{g,n+m}$.
The $m$-dimensional torus acts on $Rib^\paen_g(m,n)$ by sliding each parametrization point along its boundary component.
The orbit space is a space of metric ribbon graphs with $n$ parametrized enumerated boundaries and $m$ unparametrized enumerated boundaries denoted by $Rib^\unen_g(m,n)$.
Variations of these spaces of ribbon graphs are used in the study of string topology, see Section \ref{section:string_topology}.


\subsection{Radial slit configurations}
\label{section:models:radial_slit_configurations}
In this section, we briefly discuss Bödigheimer's spaces of radial slit configurations \cite{Boedigheimer19901,Boedigheimer2006} which model the moduli spaces
$\fM^\paen_g(m,n)$, $\fM^\unen_g(m,n)$, $\fM^\paun_g(m,n)$ or $\fM^\unun_g(m,n)$.
We postpone the presentation of some technical details of this model to Chapter \ref{chapter:radial_slit_configurations}.

Given a Riemann surface $F$ with $n \ge 1$ incoming and $m \ge 1$ outgoing boundaries,
we choose a harmonic function $u \colon F \to \bR_{\le 0}$ that is zero on the incoming boundaries and constant on each outgoing boundary.
The function $u$ is a solution to a Dirichlet problem.
It always exists and is uniquely determined by $F$ and its values on the outgoing boundaries (see e.g.\ \cite[Theorem 22.17]{Forster1981} or \cite[Theorem I.25]{Tsuji1959}).
The union of all gradient flow lines that leave a critical point define a possibly disconnected graph in $F$, called the critical graph.
After removing the critical graph, we are left with an open possibly disconnected subsurface of $F$.
Pulling the remaining flow lines straight defines a biholomorphic map whose image is a disjoint union of $n$ annuli with pairs of radial slits removed,
see Figure \ref{figure:harmonic_function} for an example.
\begin{figure}[ht]
  \centering
  \inkpic[.7\columnwidth]{harmonic_function}
  \caption{\label{figure:harmonic_function}
    On the left, we sketch the flow lines of a harmonic function defined on a Riemann surface $F$ of genus one with one incoming and one outgoing boundary curve.
    The critical points are black, the flow lines leaving a critical point are red and all other flow lines are blue.
    On the right, we sketch the image of $F$ under the process described above.
    It is an annulus with red pairs of radial slit removed and one pair is shorter than the other.}
\end{figure}
The distribution of these pairs over the annuli define a combinatorial datum.
The end points of the slits define barycentric coordinates in the interior of a multi-simplex.
Sending a Riemann surface to its radial slit configuration defines the Hilbert uniformization $\Phi \colon \fM^\paen_g(m,n) \times \bR^m_{< 0} \to R_g(m,n)$.
The codomain is a multi-simplicial space, the image of $\Phi$ is the complement of a subcomplex $R'_g(m,n) \subset R_g(m,n)$ and
the Hilbert uniformization restricts to a homeomorphism $\fM^\paen_g(m,n) \times \bR^m_{< 0} \cong R_g(m,n) - R'_g(m,n)$.

We remark that there are at least two variations of the complex $R_g(m,n)$:
In \cite[Section 8]{Boedigheimer2006}, the complex $R_g(m,n)$ is obtained by adding certain degenerate surfaces to $\fM^\paen_g(m,n)$.
It is called the harmonic compactification  of the moduli space because it is the space of (possibly degenerate) surfaces admitting a real function that is harmonic on the smooth points.
In Section \ref{section:string_topology}, we discuss the role of the harmonic compactification in the studies of string topology.
Most importantly, by \cite{EgasKupers2014}, it has the homotopy type of a space of Sullivan diagrams and therefore it approximates the space of all higher string operations by \cite{Wahl2016}.

In \cite{Boedigheimer2007}, another compactification is introduced.
It admits a more elegant presentation as it is made from subspaces of bar resulutions of symmetric groups.
This description is used by several authors to compute the integral homology of the moduli spaces for small parameters $g$ and $m$,
see Section \ref{section:unstable_homology}, Section \ref{section:the_generator_f_is_non_trivial} and Appendix \ref{appendix:unstable_homology_of_a_single_moduli_space}.

Albeit we have two versions of $R_g(m,n)$, their differences shall not play any role here.
Most importantly, the moduli space $\fM^\paen_g(m,n)$ is a relative manifold, i.e.,
it is an oriented manifold that is homeomorphic to an open and dense subspace of a compact cell complex $R_g(m,n)$ and its boundary is a subcomplex of codimension one.
Therefore, $H_\ast(\fM^\paen_g(m,n); \bZ)$ is Poincaré-dual to $H^\ast(R_g(m,n), R'_g(m,n); \bZ)$.


\subsection{Homological stability}
\label{section:homological_stability}
Here, we state the most important facts about homological stability and the Madsen--Weiss theorem.
For an introduction to the field, we refer the reader to \cite{Hatcher2011}, \cite{Wahl2013} and \cite{Galatius2013}.

Given a compact surface $F_{g,k}$ of genus $g$ with $k$ enumerated and parametrized boundary components,
the mapping class group of $F_{g,k}$ is defined to be the group of all orientation preserving diffeomorphisms (that restrict to the identity near each boundary component) up to isotopy,
i.e., $\Gamma_{g,k} := \Diff^+(F_{g,k}, \del F_{g,k})/_{\mathit{isotopy}}$.
For $k \ge 1$ the mapping class group $\Gamma_{g,k}$ acts freely and properly discontinous via isometries on its corresponding contractible Teichmüller space and the orbit space is $\fM^\paen_{g,k}$.
Therefore, there is a natural isomorphism $H_\ast(\fM^\paen_{g,k}) \cong H_\ast( \Gamma_{g,k})$.

Sewing a surface of genus one with two boundary components to a surface with $k \ge 1$ boundary components identifies $\Gamma_{g,k}$ with
a subgroup of $\Gamma_{g+1,k}$ and the union of these is the stable mapping class group $\Gamma_{\infty,k}$.
By \cite{Harer1985}, the inclusion $\Gamma_{g,k} \xhr{} \Gamma_{g+1,k}$ induces an isomorphism in homology in a range of the homological degree $\ast$ depending on $g$.
The slope of this range has been improved in the last years and it is known to be at least $\ast \le \frac{2(g-1)}{3}$ by \cite{RandalWilliams2016}.
Consequently, the inclusion of groups induces an isomorphism $H_\ast( \fM^\paen_{g,k} ) \cong H_\ast( \Gamma_{g,k} ) \cong H_\ast( \Gamma_{\infty, k})$ in this range.

The stable moduli space $\fM^\paen_{\infty, 1} := B\Gamma_{\infty, 1}$ is an infinite loop space by \cite{Tillmann1997} (after applying the Quillen plus construction) and,
as an infinite loop space, it is homotopy equivalent to the loop space of a certain Thom-spectrum by the famous Madsen--Weiss theorem,
see \cite{MadsenWeiss2005} and also \cite{GalatiusTillmannMadsenWeiss2009}.
The homology of the latter has been computed rationally by Madsen and Weiss in \cite{MadsenWeiss2005} and with coefficients in finite fields by Galatius in \cite{Galatius2004}.

Adding a point near the boundary of a surface induces maps $\fM^\unun_g(m,n) \to \fM^\unun_g(m+1,n)$.
This leads to another result in terms of homological stability:
In homology, the induced map is split injective by \cite{BoedigheimerTillmann2001} and
by \cite{Tillmann2016} it an isomorphism in a range where the homological degree $\ast$ does not exceed $\ast \le \frac{m}{2}$.

\subsection{Unstable homology}
\label{section:unstable_homology}
The unstable homology of the moduli spaces is almost unkown.
Here, we summarize what is known about the unstable homology, see also Appendix \ref{appendix:homology_results_by_others}.

The virtual cohomological dimension of the moduli spaces has been determined by Harer in \cite[Theorem 4.1]{Harer1986}.
For example, if $g > 0$ and $k > 0$, then $\vcd(\Gamma_{g,k}) = 4(g-1) + 2k$.
Moreover, the rational homology vanishes on its virtual cohomological dimension by \cite{ChurchFarbPutman2012}.

Using various techniques, the integral or rational homology groups of the moduli spaces $\fM^\paen_{g,k}$ for arbitrary $g$ and $k$ are well understood in homological degrees of at most three.
Among others, there are results by Mumford in
\cite{Mumford1967}, Powell in \cite[Theorem 1]{Powell1978} and Korkmaz--McCarthy in \cite[Theorem 3.12]{KorkmazMcCarthy2000}
on the first integral homology;
results on the second integral homology are due to Harer in \cite[Theorem 0.a]{Harer1991} and Korkmaz--Stipsicz in \cite{KorkmazStipsicz2003};
and the third rational homology is trivial for $g \ge 6$ by \cite[Theorem 0.b]{Harer1991}.
For the convenience of the reader, we spell out their results in Appendix \ref{appendix:homology_results_by_others}.

The moduli space of a disc with $m \ge 1$ (un)permutable punctures is the classifying space of the $m$-th (pure) braid group.
A complete description of the integral homology of the pure braid group is due to Arnold in \cite{Arnold1970}.
Denoting the field with $p$ elements by $\bF_p$ for $p$ a prime, the $\bF_p$-homology of the braid groups has been determined by
Fuks in \cite{Fuks1970} for $p = 2$ and by Cohen in \cite{CohenLadaMay1976} for $p > 2$.

The following results on the homology groups $H_\ast(\fM_{g,k})$ for small parameters $g$ and $k$ and
with coefficients in the integers or a field have been computed using Bödigheimer's models for the moduli spaces.
In his Ph.D.\ thesis \cite{Ehrenfried1997}, Ehrenfried determines the complete integral homology $H_\ast(\fM^\unun_1(2,1), \bZ)$ and $H_\ast(\fM_{2,1};\bZ)$,
see also \cite{Abhau200501, AbhauBoedigheimerEhrenfried2008}.
Using Visy's theory of factorable groups introduced in \cite{Visy201011}, Mehner computes the integral homology $H_\ast(\fM^\unun_g(m,1); \bZ)$ for $2g+m \le 5$ in \cite{Mehner201112} and
provides a list of generators for roughly half of the homology groups.
Let us mention two examples.
\begin{proposition*}[Bödigheimer, Ehrenfried, Mehner]
  The integral homology of $\fM_{2,1}$ and $\fM^\unun_1(2,1)$ is as follows.
  \begin{align*}
    H_\ast( \fM_{2,1}; \bZ) =
    \begin{cases}
      \bZ\langle c^2 \rangle & \ast = 0\\
      \bZ_{10}\langle cd \rangle & \ast = 1\\
      \bZ_2\langle d^2 \rangle & \ast = 2\\
      \bZ\langle ? \rangle \oplus \bZ_2\langle t \rangle & \ast = 3\\
      \bZ_6\langle ? \rangle & \ast = 4 \\
      0 & \ast \ge 5
    \end{cases}
    \quad
    H_\ast( \fM^\unun_1(2,1); \bZ) =
    \begin{cases}
      \bZ\langle a^2c \rangle & \ast = 0\\
      \bZ\langle a^2 d \rangle \oplus \bZ_2\langle bc \rangle & \ast = 1\\
      \bZ_2\langle a^2 e \rangle \oplus \bZ_2\langle bd \rangle & \ast = 2\\
      \bZ_2\langle ? \rangle & \ast = 3 \\
      0 & \ast \ge 4
    \end{cases}
  \end{align*}
  Here, the known generators $a$, $b$, $c$, $d$, $e$, $t$ and their products are described in \cite[Kapitel 1]{Mehner201112} or \cite[Chapter 4]{BoesHermann2014}
  see also \cite{BoedigheimerBoes2018}.
  The other generators are denoted by $?$.
\end{proposition*}
To their findings, we will add three new generators, see Section \ref{section:results} and Chapter \ref{chapter:unstable_homology}.

For genus three, computations with coefficients in finite field $H_\ast( \fM_{3,1}; \bF_p )$ where carried out by Wang in \cite{Wang201102}.
In \cite{BoesHermann2014}, the author and Anna Hermann determine $H_\ast( \fM^\unun_g(m,1); \bF)$ for coefficients in finite fields and the rationals for $2g+m \le 6$.

Let us mention more results.
In \cite[Corollary 5.2.3]{LeeWeintraub1985}, Lee--Weintraub show that $\tilde H_\ast( \Gamma_{2,0}; \bZ)$ is all $p$-torsion with $p=2$,$3$ or $5$ and,
in \cite{BensonCohen1991} Benson--Cohen, derive the Poincaré-series for $H^\ast(\Gamma_{2,0}; \bF_p)$ with $p=2$, $3$ and $5$.
In her Ph.D.\ thesis \cite{Godin2004}, Godin uses a ribbon graph model of the moduli space to compute the integral homology
$H_\ast( \fM^\paen_{1,1}; \bZ)$, $H_\ast( \fM^\paen_{2,1}; \bZ)$ (agreeing with Ehrenfried's results) and $H_\ast( \fM^\paen_{1,2}; \bZ)$.
The mapping class group is perfect for $g \ge 3$ by \cite[Theorem 1]{Powell1978}, hence $H_1( \fM^\paen_{3,1}; \bZ) = 0$.
The second homology group $H_2(\fM^\paen_{3,1}; \bZ) \cong \bZ \oplus \bZ_2$ has been determined by \cite[Theorem 4.9]{Sakasai2012}.
The rational cohomology of $\fM^\paen_{4,0}$ resp.\ $\fM^\unun_3(2,0)$ is determined by Tommasi in \cite{Tommasi2005} resp.\ \cite{Tommasi2007}.
