\section{Maps to \texorpdfstring{$BU(1)^m$}{BU(1)\^{}m}}
\label{section:maps_to_bu_1_m}
Forgetting the parametrization of the $m$ enumerated parametrized outgoing boundaries defines an $m$-dimensional torus bundle $U(1)^m \to \fM^\paen_g(m,n) \to \fM^\unen_g(m,n)$.
The classifying map to the universal bundle $EU(1)^m \to BU(1)^m$ has been studied and
we recall an important result by Looijenga \cite[Proposition 2.1]{Looijenga1995} concerning the stable cohomology of moduli spaces:
\begin{proposition}
  \label{proposition:looijenga_torus_bundle}
  There is a graded isomorphism of algebras over $H^\ast( \fM^\paen_\infty(m,n); \bZ )$
  \begin{align}
    H^\ast( \fM^\paen_\infty(m,n); \bZ )[ u_1, \ldots, u_m] \cong H^\ast( \fM^\unen_\infty(m,n); \bZ ) \,,
  \end{align}
  where $u_i \in H^\ast( \fM^\unen_\infty(m,n); \bZ )$ is the pullback of
  $e_i \in H^\ast(BU(1)^m; \bZ) \cong \bZ[ e_1, \ldots, e_m ]$
  along the classifying map of the $m$-dimensional torus bundle $\fM^\paen_g(m,n) \to \fM^\unen_g(m,n)$.
\end{proposition}

Using explicit (rational) models, both for the moduli spaces and the universal bundle $EU(1)^m \to BU(1)^m$,
the classifying map has been made explicit by Kontsevich in \cite{Kontsevich1992} and Bödigheimer in \cite{Boedigheimer1993}.
We use their ideas to construct yet another model for the classifying space in Chapter \ref{chapter:polygon_spaces}.
Our model plays a role in the study of the harmonic compactifications of the moduli spaces,
see Section \ref{section:results}, Chapter \ref{chapter:homotopy_type_of_NSD} and Chapter \ref{chapter:homotopy_type_of_SDspaen}.
For convenience of the reader, we discuss the main ideas of the three models here.


\subsection{Finite subset spaces, polygon bundles and metric ribbon graphs}
\label{section:finite_subspaces_polygon_bundles_and_metric_ribbon_graphs}
In this section, we discuss the relations between the space of finite subset space in the circle $\bS^1 = U(1)$, Kontsevich's polygon bundles and spaces of (marked) metric ribbon graphs.
More precisely, we recall the definitions of the spaces not yet introduced and discuss the following known fact.

\begin{proposition*}[Kontsevich, Tuffley]
  \label{proposition:exp_and_Kontsevich}
  There diagram below is commutative.
  The maps in the top row are $U(1)$-equivariant and the vertical maps are the orbit projections.
  The projection $Rib^\paen_g(1,n) \to Rib^\unen_g(1,n)$ is a circle bundle.
  \begin{align*}
    \begin{tikzcd}[ampersand replacement=\&]
      Rib^\paen_g(1,n)  \ar{r} \ar{d}  \& \KoEU \ar{r}{\cong} \ar{d}  \& \bR_{> 0} \times \expS \ar{d}  \\
      Rib^\unen_g(1,n)  \ar{r}         \& \KoBU \ar{r}{\cong}         \& \bR_{> 0} \times \expS / U(1)
    \end{tikzcd}
  \end{align*}
\end{proposition*}

Given a topological space $X$ and an integer $N > 0$, the space of non-empty subsets of $X$ of cardinality at most $N$ is denoted by $exp_N(X)$.
It is topologised as a quotient of the $N$-fold product $X^N$.
Elementary properties and relations to configurations spaces are derived in \cite{Handel2000}.
For example,
(1) the configuration spaces $\Conf^n(X)$ are open subspaces of $exp_N(X)$;
(2) for $X$ a compact Hausdorff space, $exp_N(X)$ is a compactification of $\Conf^N(X)$ and
(3) for any $N \ge 1$ the subset spaces are homotopy functors.

The subset spaces $\expNS$ and variations of those are of particular interest for us.
We know that $\expNS$ is homotopy equivalent to an odd dimensional sphere of dimension $N$ respectively $N-1$ if $N$ is odd respectively even,
see \cite[Theorem 4]{Tuffley2002}.
Therefore the union
\begin{align*}
  \expS := \bigcup_{N \ge 1} \expNS
\end{align*}
is contractible.
In \cite[Section 2.2]{Kontsevich1992}, Kontsevich introduces a contractible space with a faithful circle action called $\KoEU$.
It is homeomorphic to $\bR_{> 0} \times \expS$ via an $U(1)$-equivariant map.
Observe that the $U(1)$-action is faithful but not free: the vertices of a regular $k$-gon define a point in $\KoEU$ with stabilizer $\bZ_k \subset U(1)$.
The orbit space is $\KoBU \cong \bR_{> 0} \times \expS / U(1)$.

Consider a metric ribbon graph $G \in Rib^\paen_g(1,n)$ of genus $g$ with one parametrized outgoing boundary and $n$ enumerated parametrized incoming boundary components.
The parametrized outgoing boundary is obviously isometric to an oriented parametrized circle consisting of $k$ edges for some integer $k > 0$.
The endpoints of these edges define a set $C(G)$ of $k$ points on this circle, i.e., $C(G) \in \bR_{>0} \times \expNS$.
Sending $G$ to $C(G)$ defines a continous map $Rib^\paen_g(1,n) \to \KoEU$.
This map is $U(1)$-equivariant and 
therefore, it induces a map of orbit spaces $Rib^\unen_g(1,n) \to \KoBU$.


\subsection{Interval exchange spaces}
\label{section:interval_exchange_spaces}
In \cite{Boedigheimer1993} Bödigheimer introduces several spaces of interval exchanges as a device to
study moduli spaces of Riemann surfaces using his model of parallel slit configurations \cite{Boedigheimer19901, Boedigheimer19902}.
Here, we discuss the space of interval exchanges $\Er(\infty) = \cup_n \Er(n)$ and $\cE(\infty)$.

Geometrically speaking, an interval exchange of the real line is given by cutting $\bR$ into finitely many intervals and
reglueing the bounded pieces in a different order.
More pedantically, an interval exchange is an orientation preserving local isometry defined on the real line minus a finite number of points
(that restricts to the identity on both ends of the real line).
An interval exchange is determined by a sequence of points $y_0 \le y_1 \le \ldots \le y_{n-1}$ and a permutation $\pi$ of the $n-2$ bounded intervals
$Y_i := [y_i, y_{i+1}]$, $0 \le i \le n-2$.
Fixing the number of (bounded) intervals to be at most $n-2$, this space of interval exchanges retracts onto the subspace of normalized interval exchanges denoted by
\begin{align*}
  \Er(n) := \{ [y_0, \ldots, y_{n-1}; \pi] \mid y_0 = 0, y_{n-1} = 1 \} \,.
\end{align*}
It is a semi-simplical space of dimension $n-2$, its top cells are indexed by the symmetric group on $n-1$ letters and
it is homotopy equivalent to a bouquet of spheres of dimension $n-2$ by \cite[Theorem 5.1]{Boedigheimer1993}.
Moreover, the family of spaces $\Er(n)$ form an H-monoid with multiplication
\begin{align*}
  \Er(n) \times \Er(m) \to \Er(n+m) \,.
\end{align*}
Using his parallel model of the moduli spaces of Riemann surfaces, c.f.\ \cite{Boedigheimer19901, Boedigheimer19902},
Bödigheimer constructs a map of H-monoids
\begin{align*}
  \bigsqcup_{g \ge 0} \fM_{g,1} \to \bigsqcup_{g \ge 0} \Er(4g) \,.
\end{align*}
Furthermore, we have an inclusion $\iota \colon \Er(n) \to \Er(n+1)$ by sending $[y_0, \ldots, y_{n-1}; \pi]$ to $[y_0, \ldots, y_{n-1}, y_{n-1}; S_n(\pi)]$,
where $S_n(\pi)$ is the permutation of the intervals $Y_0, \ldots, Y_n$ that sends the degenerate interval $Y_n = [y_{n-1}, y_{n-1}]$ to itself and
whose action on $Y_0, \ldots, Y_{n-1}$ agrees with the action of $\pi$.
The union $\Er(\infty) = \cup_{n} \Er(n)$ is contractible.

By construction, the space $\Er(\infty)$ is the space of all normalized interval exchanges.
It has the same homotopy type as the space of all (unnormalized) interval exchanges $\cE(\infty)$ which is a space of all $L^2$-classes of functions
$\phi \colon \bR \to \bR$ that
have finitely many points of discontinuity $C_\phi$,
that restrict to  an orientation preserving isometry on $\bR-C_\phi$ and
that restrict to the identity near $\pm \infty$.
Observe that the composition of functions make $\cE(\infty)$ into a group.
The real line acts by translation on $\cE(\infty)$ and
it follows that $\cE(\infty)$ is a model for the total space of the universal bundle $E\bR \to B\bR$.

In this thesis, we show that an analogously defined space of orientation preserving local isometries from $n \ge 1$ enumerated circles to $m \ge 1$ enumerated circles
leads to an elegant model for $EU(1)^m \to BU(1)^m$ and
we obtain an explicit description of the classifying map of the bundle $\fM^\paen(m,n) \to \fM^\unen(m,n)$.


\subsection{The spaces of polygons}
\label{section:the_spaces_of_polygons_intro}
For each $m \ge 1$ and $n \ge 1$, we define a model $\Polspcpaen(m,n) \to \Polspcunen(m,n)$ for the universal torus bundle $EU(1)^m \to BU(1)^m$.
We see our construction as a natural generalization of Bödigheimer's space of interval exchanges discussed in Section \ref{section:interval_exchange_spaces}.
We give the basic idea of the construction of $\Polspcpaen(m,n) \to \Polspcunen(m,n)$ here.
The details are found in Chapter \ref{chapter:polygon_spaces}.

Let us treat the case $m=n=1$ at first.
Following Bödigheimer, a point in $\Polspcpaen(1,1)$ is an $L^2$ class of orientation preserving isometries $\phi$ between
two circles that have a finite number of points of discontinuity.
The domain is the ``incoming circle'' and the target is the ``outgoing circle''.
On $\Polspcpaen(1,1)$, there is a canonical $U(1)$-action by postcomposing isometries with a given rotation.
Let us study the orbit space $\Polspcunen(1,1)$.
The incoming circle is $\bS^1_{in} = [0,1]/_\sim$ and the points of discontinuity are $C_\phi \subset \bS^1_{in}$.
Note that $C_\phi$ is invariant under the $U(1)$-action, i.e., $C_\phi = C_{\alpha\phi}$ for every rotation $\alpha \in U(1)$ of the outgoing circle.
For technical reasons, let us assume that $0 \in C_\phi$.
After removing $C_\phi$ from $\bS^1_{in}$, we are left with a sequence of oriented intervals $e_0 = (0, t_1), e_1 = (t_0, t_0+t_1), \ldots, e_k = (1-t_k, 1)$ of total length $\sum t_i = 1$.
By construction, $\phi$ maps the intervals isometrically into the outgoing circle.
Using the orientation of the outgoing circle, we regard $\phi$ as a cyclic permutation $\sigma_\phi$ of the intervals.
Observe that the combinatorial datum $\sigma = \sigma_\phi$ together with the lengths $t = (t_0, \ldots, t_k)$ of the intervals defines a point in the orbit space $(\sigma;t) \in \Polspcunen(1,1)$.
This leads to a simplicial structure on $\Polspcunen(1,1)$.
In Section \ref{section:poly_spaces_homotopy_type} we show that the orbit map $\Polspcpaen(1,1) \to \Polspcunen(1,1)$
is a universal circle bundle, c.f.\ Theorem \ref{theorem:polspcpaenN_is_ETn}.

The case $m \ge 1$ and $n=1$ is treated similarly:
The space $\Polspcpaen(m,1)$ is the space of all orientation preserving isometries from one circle to $m$ enumerated circles
(having possibly different circumferences) up to a finite set of points.
The domain is the ``incoming circle'' and the circles in the target are the ``outgoing circles''.
The action of the $m$-dimensional torus $U(1)^m$ on $\Polspcpaen(m,1)$ is by postcomposing isometries with a given sequence of $m$ rotations.
As before, by removing the points of discontinuity, the orbit projection sends $\phi \in \Polspcpaen(m,1)$ to a point
$(\sigma, t) \in \Polspcunen(m,1)$ where $t \in \Delta^k$ and $\sigma \in \SymGrn[k]$ is a permutation with exactly $m$ enumerated cycles.
Again, the orbit projection $\Polspcpaen(1,1) \to \Polspcunen(1,1)$ is a universal $m$-dimensional torus bundle by Theorem \ref{theorem:polspcpaenN_is_ETn}.

More generally, for each $m \ge 1$ and $n \ge 1$, there is an universal $m$-dimensional torus bundle $\Polspcpaen(m,n) \to \Polspcunen(m,n)$,
with $\Polspcpaen(m,n)$ the space of all orientation preserving isometries from $n$ enumerated incoming circles to $m$ enumerated outgoing circles up to a finite set of points and
with $\Polspcunen(m,n)$ made from points $(\sigma, t)$ with $t \in \Delta^{k_1} \times \ldots \times \Delta^{k_n}$ and $\sigma$ a permutation with exactly $m$ enumerated cycles.


\subsection{From moduli to polygons}
\label{section:from_moduli_to_polygons}
Following \cite{Boedigheimer2006}, a Riemann surface $F \in \fM^\paen_g(m,n)$ admits a unique harmonic potential $f \colon F \to \bR$ that is zero on
the incoming boundaries and one on the outgoing boundaries.
Let $\Phi = \grad(f)$ denote the gradient flow of $f$.
We define the complete critical graph $K \subset F$ to consists of all gradient flow lines that enter or leave a critical point of $u$ or that
enter or leave a parametrization point in the outgoing or incoming boundaries.
Most importantly, the complement $F-K$ is a disjoint union of half open rectangles $R = [0,1] \times (a,b)$.
Such a rectangle $R$ has exactly two boundary components $\del R = \{ 0 \} \times (a,b) \sqcup \{1 \} \times (a,b)$.
One boundary component is part of an incoming boundary while the other boundary component is part of an outgoing boundary.
Moreover, these boundary components are isometric (flowing the one boundary through the rectangle defines an isotopy
from the inclusion of the one boundary component to the inclusion of the other boundary component).
Consequently, these pairs define an orientation preserving isometry from the incoming boundaries of $F$ to the outgoing boundaries of $F$ up to a finite set of points.
Sending a Riemann surface to this isometry defines a continous map $\fM^\paen_g(m,n) \to \Polspcpaen(m,n)$ that commutes with the map forgetting the parametrizations, i.e.,
we have the following map of $m$-dimensional torus bundles.
\begin{align}
  \label{diagram:maps_to_bu_1_m:modspc_to_pol}
  \begin{tikzcd}[ampersand replacement=\&]
    \Pol^\paen_{\fM} \colon \fM^\paen_g(m,n) \ar{r} \ar{d} \& \Polspcpaen(m,n) \simeq EU(1)^m \ar{d} \\
    \Pol^\unen_{\fM} \colon \fM^\unen_g(m,n) \ar{r}        \& \Polspcunen(m,n) \simeq BU(1)^m
  \end{tikzcd}
\end{align}
In particular, $\Pol^\unen_{\fM} \colon \fM^\unen_g(m,n) \to \Polspcunen(m,n)$ classifies the torus bundle $\fM^\paen(m,n) \to \fM^\unen(m,n)$
which is a highly non-trivial bundle (see \cite[Proposition 2.1]{Looijenga1995} or Proposition \ref{proposition:looijenga_torus_bundle}).

The inclusion of the moduli space into its harmonic compactification is of particular interest in the study of string topology, see Section \ref{section:string_topology}.
Our spaces of polygons turn out to be useful in understanding these inclusions.
The moduli space $\fM^\paen(m,n)$ deformation retracts onto the subspace $N\fM^\paen(m,n)$ where all incoming resp.\ outgoing boundaries have equal lengths.
The space of polygons $\Polspcpaen(m,n)$ deformation retracts onto a corresponding subspace denoted by $\PolspcpaenN(m,n)$.
Both retractions are equivariant with respect to the torus action.
In Chapter \ref{chapter:homotopy_type_of_NSD}, we show that the classifying map factors through the inclusion into the harmonic compactification of $N\fM^\paen(m,n)$, i.e.,
we have the following maps of $m$-dimension torus bundles.
\begin{align}
  \begin{tikzcd}[ampersand replacement=\&]
    N\fM^\paen_g(m,n) \ar{r} \ar{d} \& \ov{N\fM}^\paen_g(m,n) \ar{r} \ar{d} \& N\Polspcpaen(m,n) \simeq EU(1)^m \ar{d} \\
    N\fM^\unen_g(m,n) \ar{r}        \& \ov{N\fM}^\unen_g(m,n) \ar{r}        \& N\Polspcunen(m,n) \simeq BU(1)^m
  \end{tikzcd}
\end{align}
In Chapter \ref{chapter:homotopy_type_of_NSD} and Chapter \ref{chapter:homotopy_type_of_SDspaen}, we compare these bundle maps with the stabilization maps of the moduli spaces and their harmonic compactifications,
see also Section \ref{section:results}.
