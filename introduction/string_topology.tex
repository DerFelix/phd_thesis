\section{Applications to string topology}
\label{section:string_topology}
The subject of string topology is the study of algebraic properties of the based loop space $\Omega M$, the free loop space $LM = \Map(\bS^1, M)$ and,
more generally, the space of paths in a manifold $M$.
In this article, we restrict our attention to the rational homology $H_\ast( - ) := H_\ast( - ; \bQ)$
of the free loop space of a closed, oriented, $d$-dimensional, simply connected manifold $M$.

The study of moduli spaces of Riemann surfaces and their compactifications is strongly related to its applications to string topology.
In this section, we give a brief overview of this relation.
An introduction to the aspects of string topology presented here can be found in \cite{CohenHessVoronov2006,Abbaspour2015,Felix2015}.
For a broad overview that covers more aspects of string topology we refer the reader to \cite{Sullivan2007}.


\subsection{String operations}
\label{section:string_topology:string_operations}
In \cite{ChasSullivan1999}, Chas and Sullivan discovered rich algebraic structures in the homology of the free loop space $LM$.
There are several approaches and conjectures connecting these algebraic structures with algebraic structures on the Hochschild cohomology of algebras related to $M$, see e.g.\ 
\cite{Jones1987, CohenJones2002, Vaintrob2007, KaufmannModSpcActions1, KaufmannModSpcActions2, Kaufmann2010}.

To begin with, Chas--Sullivan construct the loop product
\begin{align}
  \circ \colon H_i(LM) \otimes H_j(LM) \to H_{i+j-d}(LM) \,.
\end{align}
In order to give the geometric construction of this map we consider the bouquet $\bS^1 \vee \bS^1$ (which is often called ``figure-8''),
the canonical map $\bS^1 \sqcup \bS^1 \to \bS^1 \vee \bS^1$ and the pinch map $\bS^1 \to \bS^1 \vee \bS^1$.
The loop product of two homology classes $x$ and $y$ is the transverse intersection of $x \times y \in \Map( \bS^1, M) \times \Map(\bS^1, M) \cong \Map( \bS^1 \sqcup \bS^1, M)$
with the codimension $d$ submanifold $\Map( \bS^1 \vee \bS^1, M )$ followed by the restriction $\rho^{out}$ that is induced by the pinch map, c.f.\ \cite{Chataur2005}.
Equivalently, the restriction $\rho^{in}$ to the two ``inner'' circles of the figure-$8$ admits an Umkehr (or wrong way) map $\rho^{in}_!$ and the loop product is the composition
\begin{align}
  \circ \colon H_i(LM) \otimes H_j(LM) \xr{ \rho^{in}_! } H_{i+j-d}( \Map(  \bS^1 \vee \bS^1, M) ) \xr{\rho^{out}_\ast} H_{i+j-d}( LM ) \,.
\end{align}
After adjusting the grading $\bH_\ast = H_{\ast -d}$, the loop product turns $\bH_\ast( LM )$ into an associative, commutative algebra.
Moreover, the evaluation map $LM \to M$, $\gamma \mapsto \gamma(1)$ induces a homomorphism of algebras where $\bH_\ast(M)$ is the intersection algebra
\begin{align}
  ev_\ast \colon \bH_\ast (LM) \to \bH_\ast(M) \,.
\end{align}
The circle action $\bS^1 \times LM \to LM$ induces an operator
\begin{align}
  \Delta \colon \bH_\ast(LM) \to \bH_{\ast + 1}(LM)
\end{align}
that turns $\bH_\ast(LM)$ into a Batalin--Vilkovisky algebra (short: BV-algebra), i.e.,
$\bH_\ast(LM)$ is graded commutative,
$\Delta^2 = 0$ and
the binary operator, called bracket,
\begin{align}
  \{ x,y \} := (-1)^{|x|} \Delta ( x \circ y ) - (-1)^{|x|} \Delta(x) \circ y - x \circ \Delta(y)
\end{align}
is a derivation in each variable.
The bracket satisfies the (graded) Jacobi identities and therefore $\bH_\ast(LM)$ is also a graded Lie algebra.
Over a field of characteristic different from 2, the category of BV-algebras is equivalent to
the category of algebras over the (homology of the) operad of little framed 2-discs $\cD_2$ by \cite[Theorem 4.5]{Getzler1994}.
In other words, $\bH_\ast(LM)$ is an algebra over the operad $H_\ast( \cD_2)$.
Voronov gives an explicit description of this action, c.f.\ \cite[Theorem 2.3]{Voronov2005}.

In \cite{CohenJones2002}, Cohen and Jones give a homotopy theoretic realization of the structures found by Chas--Sullivan and Voronov.
The loop operation and the operadic action are both realized on the Thom spectrum of a certain virtual bundle defined over $LM$.
In particular, the above structures are present on all generalized homology theories.
Moreover, they establish an isomorphism of graded algebras between the Hochschild cohomology of the singular cochains of $M$ and $\bH_\ast(LM)$
\begin{align}
  \label{isomorphism_hochschild_cochains_and_loop_homology}
  HH^\ast( C^\bullet(M), C^\bullet(M) ) \cong \bH_\ast(LM) \,.
\end{align}
For finite dimensional symmetric Frobenius algebras%
\footnote{An algebra $A$ over $\bQ$ is a Frobenius algebra if it is a unital, associative algebra with symmetric, non-degenrate inner product
$\langle - , - \rangle$ satisfying $\langle a \cdot b , c \rangle = \langle a , b \cdot c\rangle$ for all $a,b,c \in A$, see Appendix \ref{appendix:symmetric_frobenius_algebras}.}
or, more generally, a possibly infinite dimensional $A_\infty$-Frobenius algebras $A$ over $\bQ$,
the Hochschild cohomology carries a natural BV-algebra structure, see \cite{Tradler2008} and \cite{Kaufmann2008}.
The cochains $C^\bullet(M)$ of a closed, oriented manifold $M$ carry a natural $A_\infty$-Frobenius algebra structure
that is compatible with the BV-algebra structure on the Hochschild cohomology and the loop homology.
With rational coefficients and for a simply connected manifold $M$, the isomorphism \eqref{isomorphism_hochschild_cochains_and_loop_homology}
of Cohen--Jones is an isomorphism of BV-algebras by \cite[Theorem 1]{FelixThomas2008}, \cite[Section 1.1]{LambrechtsStanley2008} and \cite[Corollary 20]{Menichi2009BVHH}.

The $H_\ast( \cD_2)$-algebra structure on $HH^\ast( C^\bullet(M), C^\bullet(M))$ depends only on the homotopy type of $M$,
whereas on $\bH_\ast( LM )$ the smooth structure of $M$ is involved.
However, the action of $H_\ast( \cD_2)$ on $\bH_\ast( LM )$ is independent up to orientation preserving homotopy equivalences,
see \cite[Theorem 1]{CohenKleinSullivan2008}.


\subsection{Higher string operations}
\label{section:string_topology:higher_string_operations}
Observe that $H_\ast(\cD_2)$ and $H_\ast( \sqcup_q \fM^\paen_0(q,1) )$ are isomorphic operads,
where the operadic action on the right is induced by sewing surfaces along parametrized boundaries.
Consequently, denoting the surface PROP by $\fM^\paen := \sqcup_{g,n,m} \fM^\paen_g(m,n)$, we have an inclusion of a suboperad $H_\ast( \cD_2 ) \subset H_\ast( \fM^\paen )$.
The homology of little framed 2-discs $H_\ast(\cD_2)$ acts on the homology of the free loop space $\bH_\ast(LM)$ and
on the Hochschild cohomology of an algebra $HH^\ast( A, A )$ in a natural way.
There are various extensions this action.
By an extension of the action we mean a factorization of the operadic action of $H_\ast( \cD_2 )$ on $\bH_\ast( LM )$ or $HH^\ast(A,A)$
through a PROPeradic action of $H_\ast(X)$, with $X$ a suitable subspace (or componentwise compactification) of $\fM^\paen$.
The extensions reviewed in this subsection are summarized in the following diagram,
where the labels on the vertical arrows are indicating in which subsection the extension is discussed.
\begin{align*}
  \begin{tikzcd}[ampersand replacement=\&, row sep=4ex, column sep=3cm]
    H_\ast( \cD_2)         \curvearrowright \bH_\ast(LM) \ar{d}{\ref{section:string_topology:extension:GodinCohen}} \arrow[leftrightarrow]{r}{\cong}[swap]{\eqref{isomorphism_hochschild_cochains_and_loop_homology}}        \& H_\ast( \cD_2)   \curvearrowright HH^\ast(C^\bullet(M), C^\bullet(M)) \ar{d}{\ref{section:string_topology:extension:CostelloKontsevichSoibelman}} \\
    H_\ast( U\fM^\paen)    \curvearrowright \bH_\ast(LM) \ar{d}{\ref{section:string_topology:extension:PoirierRounds}}                                      \& H_\ast( \fM^\paen)                                                                \curvearrowright HH^\ast(C^\bullet(M), C^\bullet(M)) \ar{d}{\ref{section:string_topology:extension:TradlerZeinalian} \text{ and } \ref{section:string_topology:extension:Kaufmann}} \\
    H_\ast( \ov\fM^\paen ) \curvearrowright \bH_\ast(LM)                                                                                                    \& H_\ast( \ov\fM^\paen )                                                            \curvearrowright HH^\ast(C^\bullet(M), C^\bullet(M))
  \end{tikzcd}
\end{align*}



\subsubsection{An extension by Godin--Cohen}
\label{section:string_topology:extension:GodinCohen}
In \cite{CohenGodin2004}, Cohen and Godin provide an extension of the action on the homology of the free loop space (up to a shift in the homological degree)
\begin{align}
  H_\ast( URib^\paen_g(m,n) ) \otimes H_\ast( LM )^{ \otimes n} \to H_\ast( LM )^{\otimes m}
\end{align}
for a subspace $URib^\paen_g(m,n) \subset Rib^\paen_g(m,n)$ that is defined as follows.
A marked metric ribbon graph $G \in Rib^\paen_g(m,n)$ is said to be of type $(g,m,n)$ if two conditions are met.
Firstly, we require the subgraph $G' \subset G$ corresponding to the $n$ incoming boundaries to be a disjoint union of $n$ circles; and
secondly, we require the complement of $G'$ in $G$ to be a disjoint union of trees $T_1, \ldots, T_k$.
The marked ribbon graphs of type $(g,m,n)$ define the subspace $URib^\paen_g(m,n) \subset Rib^\paen_g(m,n)$.
In order to define the action of $URib_g(m,n)$ in the loop homology, we contract each of the trees $T_i \subset G$ to obtain
a new marked metric ribbon graph $S(G)$ called the reduction of $G$.
The reduction of $G$ has the same genus as $G$ and there is a canonical decomposition of its boundary components into $n+m$ pieces.
However it is not of type $(g,m,n)$ any more.
In Section \ref{section:string_topology:string_operations}, we reviewed the geometric definition of the Chas--Sullivan loop product using
the transverse intersection with a certain submanifold followed by a restriction map.
Here, we also have a submanifold $\Map(S(G), M) \subset \Map(\sqcup_n\bS^1, M )$ of codimension $\chi(G)d$ and the transverse intersection with this induces an operation:
\begin{align}
  H_\ast(LM)^{\otimes n} \xr{ \rho^{in}_! } H_{\ast + \chi(G)d}( \Map( S(G), M) ) \xr{\rho^{out}_\ast} H_{\ast + \chi(G)d}( LM )^{\otimes m} \,.
\end{align}
Given two ribbon graphs $G_1$ and $G_2$, the induced operations agree whenever $[G_1] = [G_2] \in H_0( URib^\paen_g(m,n) )$ and
there is an operation for each homology class $H_\ast( URib^\paen_g(m,n))$.

Under the homotopy equivalence $Rib^\paen_g(m,n) \simeq \fM^\paen_g(m,n)$,
the subspace $URib^\paen_g(m,n)$ is sent to a subspace of the moduli space which we denote by $U\fM^\paen_g(m,n) \subset \fM^\paen_g(m,n)$.
The homology groups $H_\ast(U\fM^\paen) = H_\ast( \sqcup_{g,m,n} U\fM^\paen_g(m,n) )$ form a PROP and the PROPeradic composition is induced by sewing surfaces along boundaries.
Consequently, the inclusion $U\fM^\paen \subset \fM^\paen$ induces a map of PROPs $H_\ast( U\fM^\paen ) \to H_\ast( \fM^\paen )$.
Voronovs model for the operad of little framed 2-discs $\cD^2$ has the same homotopy type as $\sqcup_m U\fM^\paen_0(m,1)$ and the actions (of their homologies) on $H_\ast(LM)$ agree.
In other words, the operadic action of $H_\ast( \cD_2)$ is extended to a PROPeradic action of $H_\ast( U\fM^\paen)$.


\subsubsection{An extension by Costello and Kontsevich--Soibelman}
\label{section:string_topology:extension:CostelloKontsevichSoibelman}
Costello constructs a model for the moduli space $\fM$ using a dual version of the ribbon graph decomposition, see \cite{Costello2007Graphs} and \cite[Theorem B]{Egas2014}.
Following \cite{WahlWesterland2016}, we call his model the chain complex of black and white graphs and denote it by $\BW(\fM)$.
In \cite{Costello2007Frobenius} Costello constructs an action of the homology of $\BW(\fM)$ on the Hochschild cohomology of any $A_\infty$-Frobenius algebra.
This action is compatible with glueing surfaces along intervals and boundary components (see \cite{Costello2007Frobenius} and \cite[Theorem D]{Egas2014}).
Moreover, the action of $H_\ast( \fM^\paen )$ restricts to the usual action of $H_\ast( \cD_2)$.

Using yet another chain complex model of the moduli spaces, Kontsevich and Soibelman let these chains act on the Hochschild cohomology of
a finite dimensional $A_\infty$-algebra with scalar product in \cite[Section 11.6]{KontsevichSoibelman2009}.
The two actions agree in case of $A_\infty$-Frobenius algebras by \cite[Section 6]{WahlWesterland2016}.


\subsubsection{An extension by Tradler--Zeinalian}
\label{section:string_topology:extension:TradlerZeinalian}
In \cite{TradlerZeinalian2006}, Tradler and Zeinalian construct a PROP of chain complexes $\SD$ whose cells are
in one to one correpondence to certain combinatorial graphs called Sullivan diagrams.
The chain complex of Sullivan diagrams is a quotient of Costellos chain complex of black and white graphs, see \cite[Theorem 2.9]{WahlWesterland2016}.
The corresponding space of Sullivan diagrams has the homotopy type of Bödigheimer's harmonic compactification of the moduli spaces,
see \cite{Boedigheimer2006} and \cite[Proposition 5.1]{EgasKupers2014}.
The Hochschild homology of a finite dimensional Frobenius algebra $A$ is an algebra over the PROP $H_\ast(\SD)$ and
the dual action on the Hochschild cohomology of a Frobenius algebra extends the action of Costello and Kontsevich--Soibelman by \cite[Theorem 5.13]{WahlWesterland2016}.


\subsubsection{An extension by Kaufmann}
\label{section:string_topology:extension:Kaufmann}
Another extension of the action of $H_\ast( \cD_2)$ on the Hochschild cohomology of a Frobenius algebra has been provided by Kaufmann in
\cite{KaufmannModSpcActions1}, \cite{KaufmannModSpcActions2} and \cite{Kaufmann2010}.
He constructs a model for the moduli space by means of embedded arcs running from the incoming boundaries to the outgoing ones.
In his model, he allows open and closed boundaries.
The closed part is a model for spaces of Sullivan diagrams and the actions agree with the ones above.


\subsubsection{An extension by Drummond-Cole--Poirier--Rounds}
\label{section:string_topology:extension:PoirierRounds}
In \cite{PoirierRounds2011,DrummondPoirierRounds2015}, Drummond-Cole, Poirier and Rounds consider a compactification of the space of reduced ribbon graphs used by Cohen and Godin.
This compactification is homeomorphic to the space of Sullivan diagrams defined by Tradler and Zeinalian.
Using this identification, Poirer and Rounds define an action of the cellular chains $C_\ast(\SD)$ on (a tensor product of) the singular chains $C_\ast(LM)$.
After taking homology, the actions of Cohen--Godin and Poirer--Rounds agree in homological degree zero.
However, it is not known to us wether the action of Cohen--Godin factors through the action of $C_\ast(\SD)$ in all homological degrees.


\subsection{Open questions and partial answers}
\label{section:string_topology:questions}
In the above subsections, we gave a brief overview of the extensions of the action of the framed disc operad on the loop homology of a closed manifold $M$ and on the Hochschild cohomology of an $A_\infty$-Frobenius algebra.
Up to identifications, the action on the loop homology resp.\ the Hochschild cohomology comes from subspaces
$\cD_2 \subset U\fM^\paen \subset \ov{\fM}^\paen$ resp.\ $\cD_2 \subset \fM^\paen \subset \ov{\fM}^\paen$ of the harmonic compactification.
Moreover, there is an isomorphism of $H_\ast(\cD_2)$-algebras by \cite{CohenJones2002}.
\begin{align}
  \bH_\ast(LM) \cong HH^\ast( C^\bullet(M), C^\bullet(M) ) \,.
\end{align}
It is an open question wether or not the extension of the action of $H_\ast(\cD_2)$ to an action of $H_\ast(\ov{\fM}^\paen)$ is maximal or not;
we do not know if the isomorphism of Cohen--Jones is an isomorphism of $H_\ast(\ov{\fM}^\paen)$-algebras;
and a description of the homology of $H_\ast(\ov{\fM}^\paen)$ is also unkown.
In what follows, we discuss some partial answers to these interrelated questions.

The question wether or not the harmonic compactification of the moduli space is the largest PROP through which the action factors has been partially answered by Wahl in \cite{Wahl2016}:
All natural operations on the Hochschild homology of Frobenius algebras form a chain complex.
As an approximation of this, Wahl introduces her chain complex of all formal operations.
The chain complex of Sullivan diagrams $\SD$ is a subcomplex of the complex of all formal operations and the inclusion is a split-injective quasi-isomorphism, see \cite[Theorem 2.9]{Wahl2016}.
We believe this is a big step towards a complete answer of the first question.

The action of a homology class in $H_\ast(\SDs)$ on $HH_\ast(A,A)$ is made explicit in \cite[Section 6.2]{WahlWesterland2016}.
Moreover, the space of Sullivan diagrams $\SDs$ is homotopy equivalent to the (componentwise) harmonic compactification $\ov\fM^\paen$ of $\fM^\paen$ by \cite{EgasKupers2014}.
Therefore, a better understanding of the extension of actions is strongly related to deeper understandin of the homology of the moduli spaces, their harmonic compactifications and the inclusion
\begin{align}
  \fM^\paen \xhr{} \ov{\fM}^\paen \,.
\end{align}
Wahl and Westerland showed that every stable class of the moduli space of surfaces with
at least one parametrized outgoing boundary component vanishes in its harmonic compactification, see \cite[Theorem 2.19]{WahlWesterland2016}.
However, there are unstable classes that are detected in the harmonic compactification,
e.g., in case of a single incoming boundary and $m$ outgoing boundaries, the harmonic compactification can be identified with
a deformation retraction of its moduli space by the work of \cite{EgasKupers2014}.
In this thesis, we provide some insights on the homotopy type of $\ov{\fM}^\paen$.
