\section{Results}
\label{section:results}
In this section, we state the most important results of our thesis.
Some of the results in this thesis are published in the article \cite{BoesEgas2017} with Egas Santander.
Here, these results are marked as ``B.+Egas``.


\subsection{On the stable homotopy type of the harmonic compactification}
\label{section:results:stable_homotopyt_type}
Let $g \ge 0$, $m \ge 0$, $n = 1$ and $\star \in \{ \paen, \paun, \unen, \unun \}$.
Recall that the stabilization map of the moduli spaces $\varphi_g \colon \fM^\star_g(m,n) \to \fM^\star_{g+1}(m,n)$
induces isomorphisms in integral homology in the range $\ast \le 2(g-1)/3$.
We show that the stabilization map $\varphi_g$ extends to the harmonic compactification and
that this extension is highly connected connected with respect to the genus and the number of outgoing boundary curves.

\begin{theoremwithfixednumber}{\ref{theorem:boes_egas_stability} and Theorem \ref{theorem:stable_homotopy_type_of_harm_normalized}}[B.+Egas]
  Let $g \ge 0$, $m \ge 1$, $\star \in \{ \paen, \paun, \unen, \unun \}$ and
  denote the stabilization maps of the moduli spaces by $\varphi_g \colon \fM^\star_g(m,1) \to \fM^\star_{g+1}(m,1)$ resp.\ $\varphi_g \colon N\fM^\star_g(m,1) \to N\fM^\star_{g+1}(m,1)$.
  The stabilization maps extend to the harmonic compactification, i.e.,
  there is a maps $\ov\varphi_g \colon \ov{\fM}^\star_g(m,1) \to \ov{\fM}^\star_{g+1}(m,1)$ resp.\ $\ov\varphi_g \colon \ov{N\fM}^\star_g(m,1) \to \ov{N\fM}^\star_{g+1}(m,1)$
  making the following diagrams commutative.
  The diagram on the right hand side is the restriction of the diagram on the left hand side.
  \begin{center}
    \begin{tikzcd}[ampersand replacement=\&]
      \fM^\star_g(m,1)      \ar{r}{\varphi_g} \ar{d} \& \fM^\star_{g+1}(m,1) \ar{d} \\
      \ov{\fM}^\star_g(m,1) \ar{r}{\ov\varphi_g}     \& \ov{\fM}^\star_{g+1}(m,1)
    \end{tikzcd}
    \hspace{1cm}
    \begin{tikzcd}[ampersand replacement=\&]
      N\fM^\star_g(m,1)      \ar{r}{\varphi_g} \ar{d} \& N\fM^\star_{g+1}(m,1) \ar{d} \\
      \ov{N\fM}^\star_g(m,1) \ar{r}{\ov\varphi_g}     \& \ov{N\fM}^\star_{g+1}(m,1)
    \end{tikzcd}
  \end{center}
  The stabilization map $\ov\varphi_g$ induces an isomorphism in homology in the degrees $\ast \le g + m - 2$ resp.\ $\ast \le g - 1$.
  Moreover, if $m > 2$, the stabilization map $\ov\varphi_g$ is $(g+m-2)$-connected resp.\ $(g-1)$-connected.
\end{theoremwithfixednumber}

The usual framework that is used to proof homological stability cannot be applied to our situation because the harmonic compactifications are highly connected (see the theorem below).
Our proof of the above theorem in the case of $\ov{\fM}^\star_g(m,1)$ is as follows.
The stabilization map $\ov\varphi_g$ identifies the cellular chain complex $C_\ast(\ov{\fM}^\star_g(m,1))$ with a subcomplex of $C_\ast(\ov{\fM}^\star_{g+1}(m,1))$.
On the pair $(C_\ast( \ov{\fM}^\star_g(m,1), \ov{\fM}^\star_{g+1}(m,1))$, we construct a discrete Morse flow that is perfect in the degrees $\ast \le (g+m-1)$.
Consequently, the stabilization map induces an integral homology isomorphism in the degrees $\ast \le g + m - 2$.
By the Theorem below, $\fM^\star_g(m,1)$ is $(g+m-2)$-connected and so is $\ov\varphi_g$.
For $m = 1$ and $m=2$, we believe that the stabilization maps $\ov\varphi_g$ are also highly connected with respect to the genus.

The harmonic compactifications are highly connected with respect to the number of outgoing boundaries.
\begin{theoremwithfixednumber}{\ref{theorem:boes_egas_connectivity}}[B.+Egas]
  Let $g \ge 0$ and $m > 2$.
  The spaces $\ov{\fM}^\paen_g(m,1)$, $\ov{\fM}^\paun_g(m,1)$, $\ov{\fM}^\unen_g(m,1)$ or $\ov{\fM}^\unun_g(m,1)$ are highly connected with respect to $m$, i.e.,
  \begin{align*}
   \pi_\ast( \ov{\fM}^\paen_g(m,1) ) = 0 \mspc{and}{20} \pi_\ast( \ov{\fM}^\unen_g(m,1) ) = 0 \mspc{for}{20} \ast \le m-2
  \end{align*}
  and
  \begin{align*}
    \pi_\ast( \ov{\fM}^\paun_g(m,1) ) = 0 \mspc{and}{20} \pi_\ast( \ov{\fM}^\unun_g(m,1) ) = 0 \mspc{for}{20} \ast \le m'
  \end{align*}
  where $m'$ is the largest even number strickly smaller than $m$.
\end{theoremwithfixednumber}
In addition to the above theorem, let us discuss the connectivity of $\ov\fM^\star_g(2,1)$ and $\ov\fM^\star_g(1,1)$  for a fixed $g \ge 0$.
The space $\fM^\star_g(2,1)$ is not in general simply-connected, see Subsection \ref{section:results:infinite_families}.
From the construction, it is clear that the moduli spaces with one parametrized incoming boundary $\fM^\paen_g(1,1)$, $\fM^\paun_g(1,1)$, $N\fM^\paen_g(1,1)$ and $N\fM^\paun_g(1,1)$ are all equal and
so are their harmonic compactifications.
Analogously, the moduli spaces $\fM^\unen_g(1,1)$, $\fM^\unun_g(1,1)$, $N\fM^\unen_g(1,1)$ and $N\fM^\unun_g(1,1)$ are all equal and
so are their harmonic compactifications.
Therefore, it follows from the next Theorem, that $\ov{\fM}^\star_g(1,1)$ is also simply-connected.

The forgetful map $\ov{\fM}^\paen_g(m,1) \to \ov{\fM}^\unen_g(m,1)$ is not a fibration (the homotopy type of the fibres is not even constant).
However, the restriction to the harmonic compactification of $N\fM^\star_g(m,1)$ is an $m$-dimensional torus bundle $U(1)^m \to \ov{N\fM}^\paen_g(m,1) \to \ov{N\fM}^\unen_g(m,1)$.
The forgetful map commutes with the stabilization maps, the stabilization maps are highly connected and we determine its stable homotopy type of $\ov{N\fM}^\star_g(m,1)$.
\begin{theoremwithfixednumber}{\ref{theorem:stable_homotopy_type_of_harm_normalized}}[B.]
  Let $m \ge 1$, $g \ge 0$.
  Forgetting the parametrization of the outgoing boundaries defines $m$-dimensional torus fibrations
  \begin{align*}
    \begin{tikzcd}[ampersand replacement=\&]
        N\fM^\paen_g(m,1) \ar{r} \ar{d}{\pi^\paen_\unen} \& \ov{N\fM}^\paen_g(m,1) \ar[hook]{r}{\ov\varphi_g} \ar{d}{\pi^\paen_\unen} \& \ov{N\fM}^\paen_\infty(m,1) \ar{r}{\Pol} \ar{d}{\pi^\paen_\unen} \& \PolspcpaenN(m,1) \simeq EU(1)^m \ar{d}{\pi^\paen_\unen} \\
        N\fM^\unen_g(m,1) \ar{r}                         \& \ov{N\fM}^\unen_g(m,1) \ar[hook]{r}{\ov\varphi_g}                         \& \ov{N\fM}^\unen_\infty(m,1) \ar{r}{\Pol}                         \& \PolspcunenN(m,1) \simeq BU(1)^m
    \end{tikzcd}
  \end{align*}
  with $\ov\varphi_g$ the extension of the stabilization map of the moduli spaces.
  The maps $\ov\varphi_g$ are $(g-1)$-connected and the maps $\Pol$ are homotopy equivalences.
\end{theoremwithfixednumber}

The next theorem is of particular interest to string topologists, because the main result of \cite{Wahl2016} suggests that
all higher string operations are parametrized by the spaces $\ov{\fM}^\paen_g(m,n)$.
\begin{theoremwithfixednumber}{\ref{theorem:stable_homotopy_type_of_harm_paen}}[B.]
  Let $g \ge 2$ and $m \ge 1$ or $g \ge 0$ and $m \ge 3$.
  The harmonic compactification $\ov{\fM}^\paen_g(m,1)$ is $(g+m-2)$-connected.
  In particular, $\ov{\fM}^\paen_\infty(m,1)$ is contractible.
\end{theoremwithfixednumber}
As a corollary, we obtain not only the vanishing result by Tamanoi \cite[Theorem 4.4 (ii)]{Tamanoi2009} (stating that all stable string operations are trivial)
but the stronger statement that all string operations that are parametrized by the harmonic compactification $\fM^\paen_g(m,1)$ are trivial if their homological degrees do not exceed $(g+m-2)$.


\subsection{On the homology of the harmonic compactification}
\label{section:results:stable_unstable_homology_of_harm}
In this section, we present our results on the homology of the harmonic compactification.

We have the following generalization of the desuspension theorem by Klamt in \cite{Klamt2013}.
\begin{theoremwithfixednumber}{\ref{theorem:desuspension_theorem_SD}}[B.]
  Let $m \ge 1$ and $g \ge 0$.
  There are isomorphisms
  \begin{align*}
    H_\ast( \ov{N\fM}^\paen_g(m,1); \bZ)   &\cong \Sigma^{-m+1} H_\ast( \ov{\fM}^\paen_g(m,1), D\ov{\fM}^\paen_g(m,1); \bZ)   \\
    H_\ast( \ov{N\fM}^\unen_g(m,1); \bF_2) &\cong \Sigma^{-m+1} H_\ast( \ov{\fM}^\unen_g(m,1), D\ov{\fM}^\unen_g(m,1); \bF_2) \\
    H_\ast( \ov{N\fM}^\paun_g(m,1); \bF_2) &\cong \Sigma^{-m+1} H_\ast( \ov{\fM}^\paun_g(m,1), D\ov{\fM}^\paun_g(m,1); \bF_2) \\
    H_\ast( \ov{N\fM}^\unun_g(m,1); \bF_2) &\cong \Sigma^{-m+1} H_\ast( \ov{\fM}^\unun_g(m,1), D\ov{\fM}^\unun_g(m,1); \bF_2)
  \end{align*}
  where $D\ov{\fM}^\star(m,1)$ denotes the subspace of all moduli (or Sullivan diagrams) having at least one degenerate outgoing boundary.
\end{theoremwithfixednumber}
Roughly speaking, the theorem follows from the following observation.
The subspace $D\ov{\fM}^\paen_g(m,1) \subset \ov{\fM}^\paen_g(m,1)$ is the closed subspace of all moduli with at least one degenerate outgoing boundary.
The harmonic compactification of the moduli space with normalized outgoing boundaries $\ov{N\fM^\paen}_g(m,1)$ is a deformation retract of $\ov{\fM}^\paen_g(m,1) - D\ov{\fM}^\paen_g(m,1)$.
The retraction together with measuring the circumference of the outgoing boundaries defines a homeomorphism
$\ov{\fM}^\paen_g(m,1) - D\ov{\fM}^\paen_g(m,1) \cong \ov{N\fM}^\paen_g(m,1) \times \interior{\Delta^{m-1}}$
that extends to the quotient $\ov{\fM}^\paen_g(m,1) / D\ov{\fM}^\paen_g(m,1) \cong \Sigma^{m-1}\ov{N\fM}^\paen_g(m,1)$.

It is a non-trivial task to find non-trivial classes in the homology of the spaces of the harmonic compactification.
We show that every homology class is represented by a chain of moduli without degenerate outgoing boundaries.
\begin{propositionwithfixednumber}{\ref{proposition:support_of_homology}}[B.+Egas]
  Let $g \ge 0$, $m \ge 1$ and $\star \in \{ \paen, \paun, \unen, \unun \}$.
  Denote the cellular chain complex of $\ov{\fM}^\star_g(m,1)$ by $C$ and let $R$ be an arbitrary coefficient group.
  Denote the sub-complex of moduli with at least $1$ degenerate boundaries by $D \subset C$.
  Every homology class $x \in H_\ast(\ov{\fM}; R)$ is represented by a chain $\sum \kappa_i c_i$ with $c_i \notin D$.
\end{propositionwithfixednumber}

Forgetting the enumeration of the outgoing boundaries is not a covering for moduli having more then two degenerate outgoing boundaries.
Nontheless, using the above theorem, the forgetful maps behave like coverings in homology.
\begin{propositionwithfixednumber}{\ref{proposition:boes_egas_covering_in_homology}}[B.+Egas]
  In homology there are maps
  \begin{align*}
    tr \colon H_\ast( \ov{\fM}^\paun_g(m,1); \bZ ) &\to H_\ast( \ov{\fM}^\paen_g(m,1); \bZ )
    \intertext{respectively}
    tr \colon H_\ast( \ov{\fM}^\unun_g(m,1); \bZ ) &\to H_\ast( \ov{\fM}^\unen_g(m,1); \bZ )
  \end{align*}
  where $(\pi^\paen_\paun)_\ast \circ tr$ respectively $(\pi^\unen_\unun)_\ast \circ tr$ is the multiplication by $m!$.
\end{propositionwithfixednumber}


\subsection{Non-trivial families in the homology of the harmonic compactification}
\label{section:results:infinite_families}
In this section, we present our non-trivial families in the homology of the harmonic compactification.

Denote the surface of genus $g$, with one incoming and one outgoing boundary by $S_g(1,1)$.
Glueing the $k$-th punctured disc to the outgoing boundary induces the canonical inclusion of the $k$-th group into the mapping class group
\begin{align*}
  Br_k \xhr{} \Gamma_{g,1}^k \quad \text{or equivalently} \quad \fM^\unun_0(k,1) \xhr{} \fM^\unun_g(k,1) \,.
\end{align*}
By construction, these inclusion are compatible with respect to the stabilization of the genus.
\begin{propositionwithfixednumber}{\ref{proposition:boes_egas_pi_1_g_0_m_2}}[B.+Egas]
  The fundamental group of $\ov{\fM}^\unun_0(2,1)$ is
  \begin{align*}
    \pi_1( \ov{\fM}^\unun_0(2,1) ) \cong
    \begin{cases}
      \bZ  \langle \alpha_0 \rangle & g = 0 \\
      \bZ_2\langle \alpha_g \rangle & g > 0
    \end{cases} \,.
  \end{align*}
  The homomorphism on fundamental groups induced by the stabilization map
  \begin{align*}
    \pi_1(\ov\varphi_g) \colon \pi_1(\ov{\fM}^\unun_g(2,1)) \to \pi_1(\ov{\fM}^\unun_{g+1}(2,1))
  \end{align*}
  sends $\alpha_g$ to $\alpha_{g+1}$.
  Furthermore, the class $\alpha_g$ is in the image of the braid generator under the canonical map
  \begin{align*}
    \pi_1( \fM^\unun_0(2,1) ) \xhr{} \pi_1( \fM^\unun_g(2,1) ) \to \pi_1( \ov{\fM}^\unun_g(2,1) ) \,.
  \end{align*}
\end{propositionwithfixednumber}

Using the methods of \cite{WahlWesterland2016} and \cite{Wahl2016}, we construct infinite families of non-trivial classes of infinite order that correspond to non-trivial higher string operations.
\begin{propositionwithfixednumber}{\ref{proposition:boes_egas_non_trivial_families_via_string_topology}}[B.+Egas]
  Let $m>0$, $1\leq i \leq m$, $c_i>1$ and $c=\sum_i c_i$.
  \begin{enumerate}
    \item[$(i)$]  There are classes of infinite order
      \begin{align*}
        \widetilde{\Gamma}_m                      & \in H_{4m-1}(\ov{\fM}^\paen_m(m,1); \bZ)
      \intertext{and}
        \widetilde{\Omega}_{(c_1, \ldots, c_m)}   & \in H_{2c-1}(\ov{\fM}^\paen_0(c,1); \bZ) \,.
      \end{align*}
      All these classes correspond to non-trivial higher string topology operations.
    \item[$(ii)$] There are classes of infinite order
      \begin{align*}
        \Gamma_m                        & \in H_{4m-1}(\ov{\fM}^\paun_m(m,1); \bZ)
      \intertext{and}
        \Omega_{(c_1, \ldots, c_m)}     & \in H_{2c-1}(\ov{\fM}^\paun_0(c,1); \bZ)
      \intertext{and}
        \zeta_{2m}                      & \in H_{2m-1}(\ov{\fM}^\paun_0(2m,1); \bZ) \,.
      \end{align*}
  \end{enumerate}
\end{propositionwithfixednumber}

Furthermore, we use a computer program to determine the integral homology of the harmonic compactifications $\ov{\fM}^\unun_g(m,1)$ and $\ov{\fM}^\paun_g(m,1)$ for small parameters $g$ and $m$.
\begin{propositionwithfixednumber}{\ref{proposition:boes_egas_computations_unun} and Proposition \ref{proposition:boes_egas_computations_paun}}[B.+Egas]
  For small parameters $2g+m$, the integral homology of  $\ov{\fM}^\unun_g(m,1)$ and $\ov{\fM}^\paun_g(m,1)$ is given by the tables
  \ref{result:genus_0}, \ref{result:genus_1}, \ref{result:genus_2}, \ref{result:genus_3},
  \ref{result:para_genus_0}, \ref{result:para_genus_1} and \ref{result:para_genus_2} in Section \ref{section:boes_egas_computations}.
\end{propositionwithfixednumber}


\subsection{On the unstable homology of the moduli spaces}
Recall that, in Section \ref{section:unstable_homology}, we stated the result by Ehrenfried and Mehner.
To their findings, we add three more generators.
\begin{theoremwithfixednumber}{\ref{theorem:unstable_homology_for_small_g_and_m}}[B., Bödigheimer, Ehrenfried, Mehner]
  The integral homology of $\fM_{2,1}$ and $\fM_{1,1}^2$ is as follows.
  \begin{align*}
    H_\ast( \fM_{2,1}; \bZ) =
    \begin{cases}
      \bZ\cycle{c^2} & \ast = 0\\
      \bZ_{10}\cycle{cd} & \ast = 1\\
      \bZ_2\cycle{d^2} & \ast = 2\\
      \bZ\cycle{\lambda s} \oplus \bZ_2\cycle{T(e)} & \ast = 3\\
      \bZ_3\cycle{w_3} \oplus \bZ_2 \cycle{?}& \ast = 4 \\
      0 & \ast \ge 5
    \end{cases}
    \quad
    H_\ast( \fM_{1,1}^2; \bZ) =
    \begin{cases}
      \bZ\cycle{a^2c} & \ast = 0\\
      \bZ\cycle{a^2 d} \oplus \bZ_2\cycle{bc} & \ast = 1\\
      \bZ_2\cycle{a^2 e} \oplus \bZ\cycle{bd} & \ast = 2\\
      \bZ_2\cycle{f} & \ast = 3 \\
      0 & \ast \ge 4
    \end{cases}
  \end{align*}
  Here, the known generators $a$, $b$, $c$, $d$, $e$, $t$ and their products have been described in \cite[Kapitel 1]{Mehner201112} or \cite[Chapter 4]{BoesHermann2014},
  see also \cite{BoedigheimerBoes2018}.
  The generators $s$, $w_3$ and $f$ will be described in Chapter \ref{chapter:unstable_homology}.
  The only generator remaining unknown is denoted by $?$.
\end{theoremwithfixednumber}
Firstly, we obtain a generator of the third rational homology of $\fM_{2,1}$ as follows.
\begin{propositionwithfixednumber}{\ref{proposition:the_class_s_non_trivial}}[B.]
  The moduli space $\fM_{2,1}$ is the total space of a rational fibration $U \to \fM_{2,1} \to \fM_{2,0}$ with $U$ the unit tangent bundle over the closed surface of genus two.
  The inclusion of the fibre $U \hr{} \fM_{2,1}$ induces an isomorphism
  \begin{align*}
    H_3(U; \bQ) \xr{\cong} H_3(\fM_{2,1}; \bQ)\,.
  \end{align*}
\end{propositionwithfixednumber}
We do not know, wether or not $[U]$ is an integral generator.

Secondly, we obtain the $3$-torsion of the fourth integral homology via what we call a Segal--Tillmann map.
\begin{propositionwithfixednumber}{\ref{proposition:the_class_w_3_is_non_trivial}}[B.]
  Sending the braid generators $\sigma_1, \ldots, \sigma_5 \in Br_6$ to a certain system of five Dehn twists along an interlocking chain of simply closed curves defines a Segal--Tillmann map $ST \colon Br_{2g+2} \to \Gamma_{g,1}$.
  This Segal--Tillman map induces an isomorphism
  \begin{align*}
    H_4( \Conf^6(\bD^2); \bF_3 ) \to H_4( \fM_{2,1}; \bF_3 ) \,.
  \end{align*}
\end{propositionwithfixednumber}

Lastly, we describe an integral class $f \in H_3( \fM^2_{1,1}; \bZ )$ by an embedded torus $U(1)^3 \subset \fM_{1,1}^2$.
Using a computer programm we show that this class is an integral generator.
\begin{propositionwithfixednumber}{\ref{proposition:the_class_f_non_trivial}}[B.]
  It is $H_3( \fM^2_{1,1}; \bZ ) \cong \bZ_2\cycle{f}$ and $f$ is represented by an embedded $3$-dimensional torus.
\end{propositionwithfixednumber}
