\section{Cellular and multi-cyclic structures of \texorpdfstring{$\PolspcstarN(m,1)$}{NPol(m,1)}}
\label{section:poly_spaces_cellular_structures}
In this section, we will use the projection to the left factor
\begin{align*}
  \Polspcpaen(m,1) \cong \PolspcpaenN(m,1) \times \interior{\Delta^{m-1}} \to \PolspcpaenN(m,1)
\end{align*}
to construct a multi-cyclic structure on $\PolspcpaenN(m,1)$, see Proposition \ref{proposition:multicyclic_structure_on_NPol}.
This will lead to a cellular structure on $\PolspcpaunN(m,1)$, $\PolspcunenN(m,1)$ respectively $\PolspcununN(m,1)$,
see Proposition \ref{proposition:cellular_decomposition_of_normalized_polygons}.

\begin{proposition}
  \label{proposition:multicyclic_structure_on_NPol}
  Let $m\ge 1$.
  The space $\PolspcpaenN(m,1)$ is homeomorphic to the realization of an $m$-fold multi-cyclic set $N\SymGr^\paen(m,1)$ whose
  multi-simplices of multi-degree $\underline{k} = (k_1, \ldots, k_m)$ and total degree $n$ are
  \begin{align}
    N\SymGr^\paen(m,1)_{\underline{k}}
      & = \{ \langle \sigma_{1,0}\ \ldots\ \sigma_{1,k_1} \rangle \cdots \langle \sigma_{m,0}\ \ldots\ \sigma_{m,k_m} \rangle  \} \\
      & \subset F_m\SymGrpaenn - F_{m-1}\SymGrpaenn \,.
  \end{align}
  The face maps $d_{j,i}$ remove the $i$-th symbol from the $j$-th cycle and renomalize the other symbols afterwards, i.e.,
  \begin{align}
    d_{j,i}(\sigma) = D_{\sigma_{j,i}}(\sigma) \,,
  \end{align}
  the degeneracy maps $s_{j,i}$ are
  \begin{align}
    s_{j,i}(\sigma) = S_{\sigma_{j,i}}(\sigma)
  \end{align}
  and the cyclic operator in the $j$-th component permutes the symbols in the $j$-th cycle cyclically.
\end{proposition}

\begin{proof}
  Let us denote the interior of an $n$-simplex by $\Delta^n$ to simplify our notation.
  In this notation, the normalization of an $n$-dimensional cell $\sigma \times \Delta^n$ is
  \begin{align}
    r(\sigma \times \Delta^n) = \sigma \times \{ (t_0, \ldots, t_n) \mid {\textstyle \sum_{j \in \sigma_i} t_j = \frac{1}{m}} \text{ for all $i$} \} \subset \sigma \times \Delta^n \,.
  \end{align}
  Consequently, denoting the norm of a permutation $\alpha$ by $N(\alpha)$, we obtain a canonical cell decomposition of $\PolspcpaenN$ with open cells
  \begin{align}
    \label{proof:proposition:multicyclic_structure_on_NPol:canonical_cell_decompisition}
    \Delta(\sigma) := r(\sigma \times \Delta^n) \cong \sigma \times \Delta^{N(\sigma_1)} \times \ldots \times \Delta^{N(\sigma_m)} \,.
  \end{align}
  The dimension of the multi-simplex $\Delta(\sigma)$ is $n-m+1$.
  Using the distinguished symbol of each cycle, we have canonical barycentric coordinates $(t_{j,0}, \ldots, t_{j,N(\sigma_j)})_{j=1,\ldots,m}$.
  From the construction it is clear that the $i$-th face in the $j$-coordinate is the cell $\Delta(D_{\sigma_{j,i}}(\sigma))$.
  With this at hand, it is easy to see that $\PolspcpaenN(m,1)$ is the realization of the multi-simplicial space
  $N\SymGr^\paen(m,1)$ with face maps $d_{j,i}(\sigma) = D_{\sigma_{j,i}}(\sigma)$ and
  with degeneracy maps $s_{j,i}(\sigma) = S_{\sigma_{j,i}}(\sigma)$.
  
  We define the $j$-th cyclic operator $t^j$ to act on $\sigma = \sigma_1 \cdots \sigma_m$ by $t^j\sigma_i = \sigma_i$ for $j \neq i$ and
  $t^j \langle \sigma_{j,0}\ \sigma_{j,1}\ \ldots\ \sigma_{j,c} \rangle = \langle \sigma_{j,c}\ \sigma_{j,0}\ \ldots\ \sigma_{j,c-1} \rangle$.
  A straight forward computation shows that these data equip $\PolspcpaenN(m,1)$ with the structure of an $m$-fold multi-cyclic space.
\end{proof}

\begin{discussion}
  \label{discussion:geometry_of_normalized_polygons_paen}
  We discuss our geometric interpretation of the points in $\PolspcpaenN(m,1)$.
  For simplicity, assume $m=1$ first.
  Given a point $x \in \PolspcpaenN(1,1)$, consider a representative $x = (\sigma; u)$ with $u \in \interior{\Delta^n}$.
  The permutation $\sigma$ consists of a single cycle $\sigma = \sigma_1 = \langle \sigma_{1,0}\ \sigma_{1,1}\ \ldots\ \rangle$.
  We dissect $\bS^1 = [0,1]/_\sim$ into $n+1$ intervals $[t_i, t_{i+1}]$ where $t_i = \sum_{k < i} u_k$.
  The $i$-th interval is denoted by $e_i := \sigma_{1,i}$.
  Each interval $e_i$ is oriented such that $t_i \in e_i$ is the starting point of $e_i$ and $t_{i+1} \in e_i$ is the end point.
  The starting point of the interval $e_0$ is the distinguished point $0 \sim 1 \in \bS^1$.
  See Figure \ref{figure:example_of_two_points_in_polspcpaen_1_1} for two examples.
  \begin{figure}[ht]
    \centering
    \inkpic[.5\textwidth]{example_of_two_points_in_polspcpaen_1_1}
    \caption{
      \label{figure:example_of_two_points_in_polspcpaen_1_1}
      In the left picture, we draw the point $x = (\sigma; u) \in \PolspcpaenN(1,1)$ with
      $\sigma = \langle4\ 0\ 1\ 3\ 2\rangle$ and $u = (\frac{1}{6}, \frac{2}{6}, \frac{1}{6}, \frac{1}{6}, \frac{1}{6})$.
      In the right picture, we draw the point $y = (\sigma; u) \in \PolspcpaenN(1,1)$ with
      $\sigma = \langle3\ 1\ 0\ 2\rangle$ and $u = (\frac{1}{6}, \frac{2}{6}, \frac{1}{4}, \frac{1}{4})$.
    }
  \end{figure}
  
  By construction, the lengths of the intervals are given by the barycentric coordinates $u$ of $x = (\sigma, u)$ and
  if $u$ tends towards the face $d_i\Delta^n$, the edge with label $i$ degenerates to a point.
  Thus, our geometric interpretation of a point in $\PolspcpaenN(1,1)$ behaves well with faces.

  The geometric interpretation of a point $x = (\sigma, u)$ with $\sigma = s_i(\sigma')$ is a circle having consecutive edges with labels $i$ and $i+1$.
  Regarding a point in $\PolspcpaenN(1,1)$ as an orientation preserving isometry between two circles up to an unspecified configuration of points,
  the cutpoint between the two edges is superfluous, i.e., instead of two edges with labels $i$ and $i+1$ we see a single edge with label $i$
  (and renormalize the labels of all other edges).
  With this identification, our geometric interpretation is independent of representatives $x = (\sigma, u) = (s_i\sigma', s_i(u')) = (\sigma', u')$,
  see also Figure \ref{figure:example_of_the_action_on_a_point_in_pol_paen_1} left and middle.
  
  Since $\PolspcpaenN(1,1)$ is the realization of a cyclic space, we have a circle action:
  It is (up to introducing or forgetting cut points) the rotation of the distinguished point $1 \in \bS^1$,
  see also Proposition \ref{proposition:multicyclic_circle_action_in_terms_of_representatives} and Figure \ref{figure:example_of_the_action_on_a_point_in_pol_paen_1}.
  \begin{figure}[ht]
    \centering
    \inkpic[.8\textwidth]{example_of_the_action_on_a_point_in_pol_paen_1}
    \caption{
      \label{figure:example_of_the_action_on_a_point_in_pol_paen_1}
      Consider the point $x = (\sigma; u) \in \PolspcpaenN(1,1)$ with
      $\sigma = \langle4\ 0\ 1\ 3\ 2\rangle$ and $u = (\frac{1}{6}, \frac{2}{6}, \frac{1}{6}, \frac{1}{6}, \frac{1}{6})$
      as it is seen on the left hand side on the left of this figure or of Figure \ref{figure:example_of_two_points_in_polspcpaen_1_1}.
      Let us demonstrate that the action of $z = \frac{1}{13} \in [0,1]/_\sim = \bS^1$.
      It will be the rotation of the distinguished point in the circle by $z$.
      According to Proposition \ref{proposition:multicyclic_circle_action_in_terms_of_representatives}, the new point $x.z$ is obtained as follows.
      In the first step we choose the degenerate representative $s_4\sigma$ of $\sigma$ with degenerate coordinates
      $u' = (\frac{1}{6}, \frac{2}{6}, \frac{1}{6}, \frac{1}{6}, \frac{1}{6} - \frac{1}{13}, \frac{1}{13})$.
      This is seen in the middle.
      Then, we compute the action of $t_5$ on $s_4\sigma$ and the coaction of $t_5^{-1}$ on $u'$.
      It is
      $t_5s_4\sigma = \langle 3\ 5\ 0\ 1\ 4\ 2\rangle$ and
      $t_5^{-1}u = (\frac{1}{13}, \frac{1}{6}, \frac{2}{6}, \frac{1}{6}, \frac{1}{6}, \frac{1}{6} - \frac{1}{13})$.
      As it is seen on the right hand side, this is exactly the rotation of the distinguished point by $z$.
    }
  \end{figure}
  
  A point $x \in \PolspcpaenN(m,1)$ is thought of cutting the circle into finitely many intervals and reglueing the pieces such that
  a collection of $m$ enumerated circles of equal circumference is obtained.
  By symmetry, we can think of a point $x \in \PolspcpaenN(m,1)$ as cutting $m$ enumerated circles of equal circumference into intervals that
  are reglued to give a single circle.
  From this point of view, it is not surprising that $\PolspcpaenN(m,1)$ admits not only a cyclic structure but also an $m$-fold multi-cyclic structure.
  Moreover, for arbitrary $m \ge 1$ and $n \ge 1$, it is straight forward to construct the spaces $\PolspcpaenN(m,n)$ together with
  canonical $n$-fold multi-cyclic structure and $m$-fold multi-cyclic structure.
  We leave this to the reader.
\end{discussion}

\begin{discussion}
  \label{discussion:geometry_of_normalized_polygons_other}
  Recall that the spaces $\PolspcpaunN(m,1)$, $\PolspcunenN(m,1)$ and $\PolspcununN(m,1)$ are quotients of $\PolspcpaenN(m,1)$.
  It is easy to see that $\pi^\paen_\paun$ and $\pi^\unen_\unun$ are $m!$-fold coverings whose
  group of Deck transformations is isomorphic to a symmetric group on $m$ elements.
  After introducing an $m$-fold multi-cyclic structure on $\PolspcpaenN(m,1)$,
  we give a full proof that $\pi^\paen_\unen$ is an $m$-dimensional torus bundle in Section \ref{section:poly_spaces_homotopy_type}.
  In particular, one should expect to see these facts in our geometric interpretation of the points of
  $\PolspcpaunN(m,1)$, $\PolspcunenN(m,1)$ and $\PolspcununN(m,1)$.
  
  Firstly, fix a point $x \in \PolspcpaunN(m,1)$.
  Consider a representative of $x = (\sigma, u)$ with $u \in \interior{\Delta^n}$.
  Choosing an enumeration of the cycles of $\sigma$ gives a point $\tilde x \in \PolspcunenN(m,1)$ with $\pi^\paen_\paun(\tilde x) = x$.
  Therefore, a point $x \in \PolspcpaunN(m,1)$ is thought of cutting the circle into finitely many intervals and reglueing the pieces such that
  a collection of $m$ unenumerated circles of equal circumference is obtained.
  
  Our geometric interpretation of a point $x \in \PolspcunenN(1,1)$ is as follows.
  Consider a representative of $x = (\sigma, u)$ with $u \in \interior{\Delta^n}$.
  We emphasize, that we allow $\sigma$ to be simplicially degenerated.
  By definition, $\sigma$ has one cycle.
  Choosing a distinguished element of this cycle yields a point $\tilde x \in \PolspcpaenN(1,1)$ with $\pi^\paen_\unen(\tilde x) = x$.
  By Discussion \ref{discussion:geometry_of_normalized_polygons_paen}, we view $\tilde x$ as a circle with decorated edges and
  the action of $\bS^1$ on $\tilde x$ is the the rotation the distinguished point of this circle.
  It is straight forward to check that the preimage of $x$ under $\pi^\paen_\unen$ is exactly the orbit of $\tilde x$ of this action,
  see also Proposition \ref{proposition:multicyclic_circle_action_in_terms_of_representatives}.
  Consequently, we interpret $x$ as cutting a given circle into finitely many intervals and reglueing the pieces in a different order such that
  we obtain a circle but only up to rotation.
  See Figure \ref{figure:example_of_a_point_in_pol_unen_1} for an example.
  \begin{figure}[ht]
    \centering
    \inkpic[.8\textwidth]{example_of_a_point_in_pol_unen_1}
    \caption{
      \label{figure:example_of_a_point_in_pol_unen_1}
      The three pictures show the very same point $x \in \PolspcunenN(1,1)$.
      It is the image of the points $\tilde x$ and $z.\tilde x \in \PolspcpaenN(1,1)$ seen in Figure \ref{figure:example_of_the_action_on_a_point_in_pol_paen_1}.
      The left picture represents the point $x = (\sigma; u)$ by
      $\sigma = \langle 4\ 0\ 1\ 3\ 2\rangle$ and $u = (\frac{1}{6}, \frac{2}{6}, \frac{1}{6}, \frac{1}{6}, \frac{1}{6})$.
      The middle picture represents the point $x = (\sigma'; u')$ by
      $\sigma' = \langle 5\ 0\ 1\ 4\ \ 2\ 3\rangle$ and
      $u' = (\frac{1}{6}, \frac{2}{6}, \frac{1}{6}, \frac{1}{6}, \frac{1}{6} - \frac{1}{13}, \frac{1}{13})$.
      The right picture represents the point $x = (\sigma''; u'')$ by
      $\sigma'' = \langle 3\ 5\ 0\ 1\ 4\ 2\rangle$ and
      $u'' = (\frac{1}{13}, \frac{1}{6}, \frac{2}{6}, \frac{1}{6}, \frac{1}{6}, \frac{1}{6} - \frac{1}{13})$.
    }
  \end{figure}
  More generally, a point $x \in \PolspcunenN(m,1)$ is thought of cutting a given circle into finitely many intervals and reglueing the pieces such that
  we obtain a collection of $m$ enumerated circles of equal circumference but only up to rotation of each individual circle.
  
  Lastly, a point $x \in \PolspcununN(m,1)$ is thought of cutting a given circle into finitely many intervals and reglueing the pieces such that
  we obtain a collection of $m$ unenumerated circles of equal circumference but only up to rotation of each individual circle.
\end{discussion}

\begin{proposition}
  \label{proposition:multicyclic_torus_action_on_poly_is_free}
  Let $m \ge 1$.
  The induced action of the $m$-dimensional torus $U(1)^m$ on the space $\PolspcpaenN(m,1)$ is free.
\end{proposition}

\begin{proof}
  From Discussion \ref{discussion:geometry_of_normalized_polygons_paen} and Proposition \ref{proposition:multicyclic_circle_action_in_terms_of_representatives}
  the action of the $i$-th factor of $U(1)^m$ on $\PolspcpaenN(m,1)$ can be carried out explicitly:
  Let $\alpha \in U(1)$ be an arbitrary angle and let $x \in \PolspcpaenN(m,1)$ be a collection of $m$ normalized enumerated polygons with base points.
  Then, $\alpha.x$ is the very same collection of $m$ enumerated polygons with the single exception that
  the base point of the $i$-th polygon has moved by the angle $\alpha$.
  This is enough to prove the claim.
\end{proof}

\begin{proposition}
  \label{proposition:cellular_decomposition_of_normalized_polygons}
  Let $m\ge 1$ and $\star \in \{ \paen, \paun, \unen, \unun \}$.
  The space of $m$ normalized polygons $\PolspcstarN(m,1)$ admits a cellular decomposition whose $n$ cells are
  in one-to-one correspondence to
  \begin{align}
    F_m\SymGrstar([n+m-1]) - F_{m-1}\SymGrstar([n+m-1])
  \end{align}
  and such that the quotient maps in Definition \ref{definition:forgetting_enumerations_or_parametrizations_on_normalized_polygons} are cellular.
\end{proposition}

\begin{proof}
  First of all, note that Proposition \ref{proposition:multicyclic_structure_on_NPol} implies the desired result for $\PolspcpaenN(m,1)$.
  
  For the general case, recall that the normalization $\Polspcpaen(m,1) \to \PolspcpaenN(m,1)$ is cellular and
  that it sends every open cell $\sigma \times \Delta^n \subset \Polspcpaen(m,1)$ onto the open multi-simplex
  $\sigma \times \Delta^{N(\sigma_1)} \times \ldots \times \Delta^{N(\sigma_m)} \subset \PolspcpaenN(m,1)$.
  By construction, the vertices of the multi-simplex are given by the enumerated and parametrized cycles of $\sigma$.
  Moreover, the normalization
  $\Polspcpaun(m,1) \to \PolspcpaunN(m,1)$, $\Polspcunen(m,1) \to \PolspcunenN(m,1)$ and $\Polspcunun(m,1) \to \PolspcununN(m,1)$
  commute with the projection maps in Diagram \ref{diagram:poly_length_functions}.
  Therefore, the image of a cell $\sigma \times \Delta^n$ in $\Polspcpaun(m,1)$, $\Polspcunen(m,1)$ or $\Polspcunun(m,1)$ is homeomorphic to
  a multi-simplex $\sigma \times \Delta^{N(\sigma_1)} \times \ldots \times \Delta^{N(\sigma_m)}$
  after choosing an enumeration of the cycles of $\sigma$ and a parametrization of each cycle.
  The attaching maps of these cells are uniquely given by the choices of the enumerations and parametrizations of the cycles and
  the attaching maps of $\Polspcpaun(m,1)$, $\Polspcunen(m,1)$ or $\Polspcunun(m,1)$.
\end{proof}
