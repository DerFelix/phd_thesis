\noindent
In this chapter, we introduce our spaces of polygons.
We see our construction as the natural generalization of Bödigheimer's spaces of interval exchanges, see \cite{Boedigheimer1993} or Section \ref{section:interval_exchange_spaces}.
The space $\Polspcpaen(m,n)$ is thought of as a space of (certain $L^2$ classes of) isometries
mapping $n \ge 1$ enumerated incoming circles to $m \ge 1$ enumerated outgoing circles (up to a finite set of points).
As such, $\Polspcpaen(m,n)$ admits a free action of the $m$-dimensional torus by postcomposing isometries with a given sequence of $m$ rotations.
The quotient map to the orbit space $\Polspcunen(m,n)$ is a universal $m$-torus bundle.
In Chapter \ref{chapter:radial_slit_configurations} we will construct maps of torus fibrations
\begin{align}
  \label{polyspc:intro:mod_spc_to_poly}
  \begin{tikzcd}[ampersand replacement=\&]
    \fM^\paen_g(m,1) \ar{r} \ar{d} \& \Polspcpaen(m,1) \simeq EU(1)^m \ar{d} \\
    \fM^\unen_g(m,1) \ar{r}        \& \Polspcunen(m,1) \simeq BU(1)^m
  \end{tikzcd}
\end{align}
and these fibrations commute (up to homotopy) with the stabilization maps of the moduli spaces.
Aside from these properties, we use (a deformation retraction of) our polygon spaces to study the harmonic compactifications of the moduli spaces in question,
see Chapter \ref{chapter:sullivan_diagrams}, Chapter \ref{chapter:homotopy_type_of_NSD} and Chapter \ref{chapter:homotopy_type_of_SDspaen}.
Let us mention the following variation of these spaces of polygons.
The maps in Diagram \eqref{polyspc:intro:mod_spc_to_poly} are all equivariant with respect to permuting the outgoing boundaries resp.\ outgoing circles.
The orbit projection of the spaces of polygons are denoted by $\Polspcpaun(m,n) \to \Polspcunun(m,n)$ and we obtain the following diagram.
\begin{align}
  \begin{tikzcd}[ampersand replacement=\&]
    \fM^\paun_g(m,n) \ar{r} \ar{d} \& \Polspcpaun(m,n) \ar{d} \\
    \fM^\unun_g(m,n) \ar{r}        \& \Polspcunun(m,n)
  \end{tikzcd}
\end{align}
Thus these spaces of polygons are suitable to study the moduli spaces of Riemann surfaces with unenumerated outgoing boundaries.

We will define the spaces of polygons as the realization of a corresponding simplicial set.
The simplicial structure of the spaces $\Polspcpaen(m,n)$, $\Polspcpaun(m,n)$, $\Polspcunen(m,n)$ and $\Polspcunun(m,n)$ is very similar.
However, the combinatorics of $\Polspcunun(m,n)$ is the least technical.

Let us discuss the simplicial structure of the space of unenumerated and unparametrized polygons $\Polspcunun = \cup_m \Polspcunun(m,1)$ here.
The $k$-dimensional simplices of $\Polspcunun$ are the permutations $\sigma \in \SymGrn[k]$ on the symbols $[k] = \{0, \ldots, k\}$.
Let us describe the points of an open $k$-simplex in the geometric realization before discussing the faces and degeneracies.
To this end, let $\sigma \in \SymGrn[k]$ with exactly $m$ cycles $\cycles(\sigma) = \{ \sigma_1, \ldots, \sigma_m \}$ and
fix a point $t = (t_0, \ldots, t_k) \in \interior{\Delta^k}$.
Cutting the circle $\bS^1 = [0,1]/_\sim$ into $k+1$ intervals $e_0 = [0, t_0], e_1 = [t_0, t_0+t_1], \ldots, e_n = [1-t_k, 1]$ of length $t_0, \ldots, t_k$,
we obtain a set of $m$ polygons by glueing the end of the edge $e_i$ to the start of the edge $e_{\sigma(i)}$.
Note that this defines an orientation preserving isomoetry from $\bS^1$ to $m$ unparametrized and unenumerated circles up to a finite set of points (which is the set of cut points).
Observe that each orientation preserving isomoetry from $\bS^1$ to $m$ unparametrized and unenumerated circles up to a finite set of points is obtained this way.
Removing $i$ from its cycle representation and renormalizing all other symbols afterwards, defines the $i$-th face of $\sigma$.
Geometrically speaking, the $i$-th face of $(\sigma, t)$ is obtained by collapsing the edge $e_i$.
In the special case, where $i$ is a fixed point of $\sigma$, the $i$-th face is regarded as isomoetry from $\bS^1$ to $m-1$ circles.
The $i$-th degeneracy of $\sigma$ is obtained by increasing all symbols $j \ge i$ by one in the cycle representation and making $i$ the predecessor of $i+1$.
Geometrically speaking, the $i$-th degeneracy splits the edge $e_i$ into two pieces introducing a superfluous cut point.
This space of polygons admits a stratification by the spaces of exactly $m$ polygons.
The $m$-th stratum is denoted $\Polspcunun(m,1)$.
The construction of the spaces $\Polspcpaen(m,n)$, $\Polspcpaun(m,n)$ and $\Polspcunen(m,n)$ will be similar.

\subsubsection*{Overview of the results of this chapter}
The space of $m$ polygons $\Polspcpaen(m,1)$ deformation retracts onto the subspace where all circumferences are equal,
see Proposition \ref{proposition:strong_deformation_retraction_to_normalized_polygons}.
This space is the space of $m$ normalized polygons $\PolspcpaenN(m,1)$ and the deformation retraction induces a homeomorphism
$\Polspcpaen(m,1) \cong \PolspcpaenN(m,1) \times \interior{\Delta^{m-1}}$, see Proposition \ref{proposition:Pol_as_NPol_times_delta}.
The space of $m$ normalized, enumerated and parametrized polygons $\PolspcpaenN(m,1)$ admits the structure of an $m$-fold multi-cyclic set,
see Proposition \ref{proposition:multicyclic_structure_on_NPol}.
The quotient by the $m$-fold torus action defines a $m$-fold torus bundle with base $\PolspcunenN(m,1)$.
\begin{theoremwithfixednumber}{\ref{theorem:polspcpaenN_is_ETn}}
  The $m$-fold torus bundles $\Polspcpaen(m,1) \to \Polspcunen(m,1)$ and $\PolspcpaenN(m,1) \to \PolspcunenN(m,1)$ are universal.
\end{theoremwithfixednumber}

Let us mention another result which is similar to \cite[Theorem C]{Klamt2013} and which is used in the study of the harmonic compactifications,
see Chapter \ref{chapter:homotopy_type_of_NSD} and Chapter \ref{chapter:homotopy_type_of_SDspaen}.
For each $n \ge 1$, the space $\Polspcpaen = \cup_k \Polspcpaen(k,n)$ is filtered by the closed subspaces
$F_m\Polspcpaen = \ov{\Polspcpaen}(m,n) = \cup_{k \le m} \Polspcpaen(k,n)$.
Roughly speaking, there is an extension of the homeomorphism
\begin{align*}
  F_m\Polspcpaen - F_{m-1}\Polspcpaen = \Polspcpaen(m,1) \cong \PolspcpaenN(m,n) \times \Delta^{m-1}
\end{align*}
to $F_m\Polspcpaen / F_{m-1}\Polspcpaen \to \Sigma^{m-1}\PolspcpaenN(m,n)$ which leads to our desuspension theorem for the spaces of polygons.
\begin{theoremwithfixednumber}{\ref{theorem:normalized_polygons_and_suspensions_mod_2} and \ref{theorem:normalized_polygons_and_suspensions}}
  There are isomorphisms of cellular chain complexes
  \begin{align*}
    C_\ast( \PolspcpaenN(m,1); \bZ)   &\cong \Sigma^{-m+1} C_\ast( F_m\Polspcpaen, F_{m-1}\Polspcpaen; \bZ) \\
    C_\ast( \PolspcpaunN(m,1); \bF_2) &\cong \Sigma^{-m+1} C_\ast( F_m\Polspcpaun, F_{m-1}\Polspcpaun; \bF_2) \\
    C_\ast( \PolspcunenN(m,1); \bF_2) &\cong \Sigma^{-m+1} C_\ast( F_m\Polspcunen, F_{m-1}\Polspcunen; \bF_2) \\
    C_\ast( \PolspcununN(m,1); \bF_2) &\cong \Sigma^{-m+1} C_\ast( F_m\Polspcunun, F_{m-1}\Polspcunun; \bF_2)
  \end{align*}
\end{theoremwithfixednumber}
The isomorphism is made explicit in Proposition \ref{proposition:NPol_and_suspensions} and
a variation of the arguments are used to prove our desuspension Theorem for the harmonic compactifications, see Theorem \ref{theorem:desuspension_theorem_SD}.
Lastly, in Section \ref{section:compare_bu_with_bucomb}, we construct an explicit rational homotopy equivalence between
our space of polygons $\Polspcunen(1,1)$ and Kontsevich's polygon bundle $\KoBU$.

\subsection*{Organization of the chapter}
In Section \ref{section:notation_symmetric_groups}, we fix our notations for the symmetric groups and define sets $\SymGrstar$ of
(un)enumerated and (un)parametrized permutations with $\star \in \{ \paen, \paun, \unen, \unun \}$.
In Section \ref{section:polygon_spaces} we define the spaces of (un)enumerated and (un)parametrized polygons $\Polspcstar$ as
the geometric realization of $(\SymGrstarn)_{n \in \bN}$.
It is stratified by the spaces of $m$ polygons $\Polspcstar(m,1)$.
For the sake of completeness, we make the definition of the four variations of the space of polygons explicit.
In Section \ref{section:polygon_spaces_normalized}, we define the space of normalized polygons $\PolspcstarN(m,1) \subset \Polspcstar(m,1)$,
discuss our interpretation of its points and
establish a canonical homeomorphism $\Polspcstar(m,1) \cong \PolspcstarN(m,1) \times \interior{\Delta^{m-1}}$ in the enumerated cases.
In Section \ref{section:poly_spaces_cellular_structures}, we use the projection
$\Polspcpaen(m,1) \cong \PolspcpaenN(m,1) \times \interior{\Delta^{m-1}} \to \PolspcpaenN(m,1)$ to define an $m$-multi-cyclic structure on $\PolspcpaenN(m,1)$ and
a cellular structure on the other spaces of $m$ normalized polygons $\PolspcstarN(m,1)$.
Moreover, we prove our desuspension theorem for the spaces of polygons in Section \ref{section:desuspension_theorem_pol}
In Section \ref{section:poly_spaces_homotopy_type}, we study the homotopy type of the spaces of polygons.
Most importantly, we prove that the $m$-dimensional torus bundle $\Polspcpaen(m,1) \to \Polspcunen(m,1)$ is universal.
In Section \ref{section:compare_bu_with_bucomb}, we construct a zig-zag of rational homotopy equivalence between
$\Polspcunen(1,1)$ and Kontsevich's polygon bundle $\KoBU$.
