\section{The space of normalized polygons \texorpdfstring{$\PolspcstarN(m,1)$}{NPol(m,1)}}
\label{section:polygon_spaces_normalized}
In this section, we introduce the space of $m$ normalized polygons $\PolspcstarN(m,1) \subset \Polspcstar(m,1)$,
see Definition \ref{definition:normalized_polygons}.
Most importantly, we will construct a homeomorphism
\begin{align}
  \Polspcpaen(m,1) \cong \PolspcpaenN(m,1) \times \interior{\Delta^{m-1}} \,,
\end{align}
see Proposition \ref{proposition:Pol_as_NPol_times_delta}.
This homeomorphism will be used Section \ref{section:poly_spaces_cellular_structures} to equip these spaces of normalized polygons with an $m$-fold multi-simplicial structure and,
in Section \ref{section:desuspension_theorem_pol}, it will be used to proof our desuspension theorem for our spaces of polygons.

\begin{definition}
  \label{definition:length_of_a_polygon}
  Let $m \ge 1$ and consider a (representative of a) point $(\sigma; t) \in \Polspcunen(m,1)$ or $(\sigma; t) \in \Polspcpaen(m,1)$ with
  $\cycles(\sigma) = \{ \sigma_1, \ldots, \sigma_m \}$.
  The \defn{length of the $i$-th outgoing circle} is $l_i := l_i(\sigma ,t) := \sum_{j \in \sigma_i} t_j$ and
  the \defn{length of} $(\sigma,t)$ is the tuple $l(\sigma, t) := (l_1, \ldots, l_m)$.
\end{definition}

\begin{remark}
  Observe that $l_i > 0$ for all $i$ and that $\sum_{1 \le i \le m} l_i = 1$.
\end{remark}

\begin{definition}
  \label{definition:length_function_enumerated}
  On the space of $m$ enumerated (un)parametrized polygons, the \defn{length} is the continous map
  \begin{align}
    l \colon \Polspcunen(m,1) \to \interior{\Delta^{m-1}}, \quad (\sigma, t) \mapsto l(\sigma, t)
  \intertext{respectively}
    l \colon \Polspcpaen(m,1) \to \interior{\Delta^{m-1}}, \quad (\sigma, t) \mapsto l(\sigma, t) \,.
  \end{align}
\end{definition}

\begin{remark}
  Clearly, the length does not depend on the parametrization of the outgoing circles.
  Therefore, the length commutes with the forgetful map $\pi^\paen_\unen \colon \Polspcpaen(m,1) \to \Polspcunen(m,1)$.
  
  In Remark \ref{remark:pol_m_covering} we saw that the map forgetting the enumeration of the $m$ cycles is an $m!$-fold covering whose
  group of Deck transformations is isomorphic to $\fS[m-1]$.
  Observe that the length $l$ is $\fS[m-1]$-equivariant (with respect to permuting the order of the cycles resp.\ the coordinates of the $(m-1)$-simplex).
\end{remark}

\begin{definition}
  \label{definition:length_function_unenumerated}
  On the space of $m$ unenumerated (un)parametrized polygons, the \defn{length} is the continous map
  \begin{align}
    l \colon \Polspcpaun(m,1) \to \interior{\Delta^{m-1}}/_{\fS[m-1]}
  \end{align}
  respectively
  \begin{align}
    l \colon \Polspcunun(m,1) \to \interior{\Delta^{m-1}}/_{\fS[m-1]}
  \end{align}
  making the following diagram commutative.
  \begin{align}
    \label{diagram:poly_length_functions}
    \begin{tikzcd}[ampersand replacement=\&]
      \Polspcpaen(m,1) \ar{r}{\pi^\paen_\unen} \ar{d}{/_{\fS[m-1]}}  \& \Polspcunen(m,1) \ar{r}{l} \ar{d}{/_{\fS[m-1]}}  \& \interior{\Delta^{m-1}} \ar{d}{/_{\fS[m-1]}} \\
      \Polspcpaun(m,1) \ar{r}{\pi^\paun_\unun}                       \& \Polspcunun(m,1) \ar[dashed]{r}{l}               \& \interior{\Delta^{m-1}}/_{\fS[m-1]}
    \end{tikzcd}
  \end{align}
\end{definition}

\begin{definition}
  \label{definition:normalized_polygons}
  Let $m \ge 1$.
  The space of $m$ \defn{normalized enumerated or parametrized polygons} is the closed subspace of $m$ polygons of the same length.
  \begin{align}
    \PolspcpaenN(m,1) &:= l^{-1}( {\textstyle \frac{1}{m}, \ldots, \frac{1}{m}} ) \subset \Polspcpaen(m,1) \\
    \PolspcpaunN(m,1) &:= l^{-1}( {\textstyle \frac{1}{m}, \ldots, \frac{1}{m}} ) \subset \Polspcpaun(m,1) \\
    \PolspcunenN(m,1) &:= l^{-1}( {\textstyle \frac{1}{m}, \ldots, \frac{1}{m}} ) \subset \Polspcunen(m,1) \\
    \PolspcununN(m,1) &:= l^{-1}( {\textstyle \frac{1}{m}, \ldots, \frac{1}{m}} ) \subset \Polspcunun(m,1)
  \end{align}
\end{definition}

\begin{definition}
  \label{definition:forgetting_enumerations_or_parametrizations_on_normalized_polygons}
  Abusing notation, the restrictions of the quotient maps in Definition \ref{definition:forgetting_enumerations_or_parametrizations_on_polspc}
  define quotient maps with the same names.
  \begin{align}
    \label{definition:forgetting_enumerations_or_parametrizations_on_normalized_polygons:diagram}
    \begin{tikzcd}[ampersand replacement=\&]
                                                                                                    \& \PolspcpaunN(m,1) \ar{dr}{\pi^\paun_\unun}  \& \\
      \PolspcpaenN(m,1) \ar{ur}{\pi^\paen_\paun} \ar{dr}{\pi^\paen_\unen} \ar{rr}{\pi^\paen_\unun}  \&                                             \& \PolspcununN(m,1) \\
                                                                                                    \& \PolspcunenN(m,1) \ar{ur}{\pi^\unen_\unun}  \&
    \end{tikzcd}
  \end{align}
\end{definition}

We will now show that $\PolspcstarN(m,1) \subset \Polspcstar(m,1)$ is a strong retract using a ``normalization process''.

\begin{definition}
  \label{definition:Pol_normalization_of_a_simplex}
  Let $m \ge 1$ and $n \ge 0$.
  Let $\sigma \in \SymGrpaenn$ with $m$ cycles and $t \in \interior{\Delta^n}$.
  Each symbol $i \in [n]$ belongs to exactly one cycle, say $\sigma_{j(i)}$, and
  we denote the length of the $j(i)$-th outgoing circle by $l(i) := l_{j(i)}(\sigma, t) = \sum_{k \in \sigma_{j(i)}} t_k$.
  The \defn{normalization of $(\sigma, t)$} is
  \begin{align}
    r(\sigma, t) := (\sigma, t'), \quad t'_i = \frac{t_i}{m \cdot l(i)} \text{ for } 0 \le i \le n\,.
  \end{align}
\end{definition}

\begin{remark}
  Note that the normalization of the simplices of $\Polspcpaen(m,1)$ induces a continous self map of $\Polspcpaen(m,1)$.
  Observe that $l(r(\sigma, t)) = ( {\textstyle \frac{1}{m}, \ldots, \frac{1}{m}} )$, i.e.,
  the normalization maps to $\PolspcpaenN(m,1) \subset \Polspcpaen(m,1)$.
  Observe further that the normalization of $\Polspcpaen(m,1)$ induces a normalization of the spaces of polygons $\Polspcunen(m,1)$, $\Polspcpaun(m,1)$ and $\Polspcunun(m,1)$.
\end{remark}

\begin{definition}
  \label{definition:Pol_normalization}
  Let $m \ge 1$ and $\star \in \{ \paen, \paun, \unen, \unun \}$.
  The \defn{normalization} is the continous retraction
  \begin{align}
    r \colon \Polspcstar(m,1) \to \PolspcstarN(m,1)
  \end{align}
  induced by the normalization of simplices.
\end{definition}

\begin{remark}
  By construction, the normalizations commute with the forgetful maps in
  Definition \ref{definition:forgetting_enumerations_or_parametrizations_on_normalized_polygons}.
\end{remark}

\begin{proposition}
  \label{proposition:strong_deformation_retraction_to_normalized_polygons}
  Let $m \ge 1$.
  The space of $m$ normalized (enumerated and / or parametrized) polygons is
  a strong deformation retract of the space of $m$ (enumerated and / or parametrized) polygons.
  The deformation retractions can be chosen to commute with the forgetful maps in
  Definition \ref{definition:forgetting_enumerations_or_parametrizations_on_normalized_polygons}.
\end{proposition}

\begin{proof}
  On $\Polspcpaen(m,1)$ we define the obvious homotopy on the level of $n$-dimensional simplices:
  On $\sigma \times \interior{\Delta^n}$ the homotopy is
  \begin{align}
    \label{proposition:strong_deformation_retraction_to_normalized_polygons:the_homotopy}
    h_s( \sigma , t) := (\sigma, t^s_0, \ldots, t^s_n), \quad t^s_i = (1-s) \cdot t_j + s \cdot \textstyle{ \frac{t_j}{m \cdot l(i)} } \quad \text{for } 0 \le i \le n.
  \end{align}
  By construction, $h_s$ induces strong deformation retractions
  $\Polspcpaun(m,1) \to \PolspcpaunN(m,1)$, $\Polspcunen(m,1) \to \PolspcunenN(m,1)$ and $\Polspcunun(m,1) \to \PolspcununN(m,1)$
  that commute with the forgetful maps.
\end{proof}

\begin{proposition}
  \label{proposition:Pol_as_NPol_times_delta}
  Let $m \ge 1$ and $\star \in \{ \paen, \unen\}$.
  The normalization and the length map induce a homeomorphism
  \begin{align}
    r \times l \colon \Polspcstar(m,1) \cong \PolspcstarN(m,1) \times \interior{\Delta^{m-1}} \,.
  \end{align}
\end{proposition}

\begin{proof}
  The map $r \times l$ is a continous bijection.
  Its inverse is the continous map
  \begin{align}
    (\sigma, t', l') \mapsto (\sigma, t), \quad t_i = t'_i \cdot m \cdot l'_{j(i)}
  \end{align}
  where $j(i)$ denotes the number of the cycle to which $i$ belongs, i.e., $i \in \sigma_{j(i)}$.
  By construction, the length of the $j$-th cycle of $(\sigma, t)$ is $l'_j$:
  \begin{align}
    l_j(\sigma, t) = \sum_{i \in \sigma_j} t_i = \sum_{i \in \sigma_j} t'_i \cdot m \cdot l'_j = \frac{m\cdot l'_j}{m} = l'_j \,.
  \end{align}
\end{proof}

