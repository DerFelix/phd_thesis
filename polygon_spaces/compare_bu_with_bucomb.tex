\section{Comparing \texorpdfstring{$\Polspcunen(1,1)$}{Pol\^{}circ(1,1)} with Kontsevich's \texorpdfstring{$\KoBU$}{BU\^{}comb(1)}}
\label{section:compare_bu_with_bucomb}
Let $m \ge 1$ and $\star \in \{ \paen, \unen, \paun, \unun \}$.
Recall that the set of permutations with at most $m$ cycles defines a simplicial subset $F_m\SymGrstar \le \SymGrstar$.
Note that, by definition, the realization of $F_1\SymGrstar$ is $\Polspcstar(1,1)$.
Denote the fat realization of $F_1\SymGrstar$ by $\FatPolspcstar(1,1)$ and the canonical quotient by $q \colon \FatPolspcstar(1,1) \xr{\simeq} \Polspcstar(1,1)$.
By definition of the fat realization, every point $x \in \FatPolspcstar(1,1)$ is uniquely represented by a pair $x = (\sigma, t) \in F_1\SymGrstarn \times \interior{\Delta^n}$ for some $n$.

Similarly to our geometric interpretation of points in $\Polspcunen(1,1)$, we associate to $(\sigma, t) \in F_1\SymGrunenn \times \interior{\Delta^n}$
an oriented polygon $P(x)$ with $n+1$ edges that are cyclically labeled by the symbols $0, \sigma(0), \ldots, \sigma^n(0)$ and whose lengths are $t_0, t_{\sigma(0)}, \ldots, t_{\sigma^n(0)}$.
This polygon is piecewise isometric to a circle and we regard the vertices of the polygons as set of $n+1$ points up to a rotation, i.e., $C(x) \in \expS/U(1)$.
This defines a continuous map
\begin{align}
  \theta \colon \FatPolspcunen(1,1) \to \expS/U(1) \,.
\end{align}

\begin{proposition}
  \label{proposition:comparing_pol_and_bu_comb}
  The zig-zag
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      \Polspcunen(1,1) \& \FatPolspcunen(1,1) \ar{l}{\simeq}[swap]{q} \ar{r}{\theta}[swap]{\simeq_\bQ} \& \expS/U(1)
    \end{tikzcd}
  \end{align}
  induces a rational homotopy equivalence.
\end{proposition}

\begin{proof}
  Clearly, $\expS$ is filtered by the closed subspaces $\expNS = \{ C \subset \bS^1 \mid \# C \le N \}$.
  By \cite[Theorem 4]{Tuffley2002}, the spaces $\expNS[2k-1]$ and $\expNS[2k]$ are odd dimensional spheres of dimension $2k-1$.
  Moreover, by \cite[Theorem 5]{Tuffley2002}, the inclusion $\expNS[2k-1] \subset \expNS[2k]$ induces the multiplication by two in $H_{2k-1}$.
  From the long exact sequence of homotopy groups induced by the Borel construction
  \begin{align}
    \expNS[2k-1] \to EU(1) \times_{U(1)} \expNS[2k-1] \to BU(1)
  \end{align}
  it follows that
  \begin{align}
    EU(1) \times_{U(1)} \expNS[2k-1] \to BU(1)
  \end{align}
  is $(2k-2)$-connected.
  Similarly, the inclusion $\expNS[2k-1] \subset \expS$ induces a $(2k-2)$-connected map
  \begin{align}
    EU(1) \times_{U(1)} \expNS[2k-1] \to EU(1) \times_{U(1)} \expS \,.
  \end{align}
  The stabilizers of the action of $U(1)$ on $\expNS[2k-1]$ and $\expS$ are finite and it follows that both quotient maps
  \begin{align}
    EU(1) \times_{U(1)} \expNS[2k-1] &\to \expNS[2k-1]/U(1)
    \intertext{and}
    EU(1) \times_{U(1)} \expS        &\to \expS/U(1)
  \end{align}
  are rational equivalences.
  Consequently, the zig-zag
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      BU(1) \& EU(1) \times_{U(1)} \expS \ar{l} \ar{r} \& \expS/U(1)
    \end{tikzcd}
  \end{align}
  induces a rational equivalence and the inclusion $\expNS[2k-1]/U(1) \subset \expS/U(1)$ is rationally a $(2k-2)$-connected map.
  
  Observe that $\theta \colon \FatPolspcunen(1,1) \to \expS/U(1)$ maps the $k$-skeleton $\FatPolspcunen_k(1,1)$ to $\expNS[k+1]$.
  It is readily checked that $\FatPolspcunen_2(1,1)$ is a sphere made a single $0$-cell, a single $1$-cell and two $2$-simplices $\langle 0\ 1\ 2\rangle \times \Delta^2$ and $\langle 0\ 2\ 1\rangle \times \Delta^2$.
  Comparing the long exact sequences of the pair $(\FatPolspcunen_3(1,1), \FatPolspcunen_2(1,1))$ with the cellular chain complex it follows that
  the inclusion $\FatPolspcunen_2(1,1) \subset \FatPolspcunen_3(1,1) \subset \FatPolspcunen(1,1) \simeq \CP^\infty$ induces an isomorphism in the second (co)homology.
  
  By the above, we have the following commuting diagram.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      H^2( \expS/U(1); \bQ )     \ar{r}{\theta^\ast} \ar{d}{\cong} \& H^2( \FatPolspcunen(1,1); \bQ ) \ar{d}{\cong} \\
      H^2( \expNS[3]/U(1); \bQ ) \ar{r}{\theta^\ast}               \& H^2( \FatPolspcunen_2(1,1); \bQ )
    \end{tikzcd}
  \end{align}
  Since both $H^\ast( \expS/U(1); \bQ )$ and $H^\ast( \FatPolspcunen(1,1) \bQ )$ are polynomial algebras in one generator in degree two,\
  it is enough to show that $\theta$ induces a rational isomorphism
  \begin{align}
    \label{proposition:comparing_pol_and_bu_comb:theta_2}
    \theta_\ast \colon H_2(\FatPolspcunen_2(1,1); \bQ) \cong H_2( \expNS[3]/U(1); \bQ) \,.
  \end{align}
  To this end, we choose the following cellular decomposition of $\expNS[3]/U(1)$.
  \begin{figure}[ht]
    \centering
    \inkpic[.4\columnwidth]{cellular_decomposition_exp3_S1}
    \caption{
      \label{figure:cellular_decomposition_exp3_S1}
      The space $\expNS[3]/\bS^3$ is homeomorphic to a quotient of $\Delta^2$.
      As such, it admits the following cellular decomposition.
      The barycenter $(\frac{1}{3}, \frac{1}{3}, \frac{1}{3})$, the point $(1,0,0)$ and the point $(\frac{1}{2}, \frac{1}{2}, 0)$ represent the three 0-cells.
      The segments $\{ (t, \frac{1-t}{2}, \frac{1-t}{2}) \mid t \in [\frac{1}{3}, 1] \}$ and $\{ (t, 1-t, 0) \mid t \in [\frac{1}{2}, 1] \}$ represent to two 1-cells and
      the 2-cell is represented by $\{ (t_0, t_1, t_2) \mid t_0, t_1 \ge t_2 \}$.
    }
  \end{figure}
  Note that, given three distinct points on $\bS^1$ up to rotation, measuring the distance of each pair of consecutive points defines a sequence
  $(t_0, t_1, t_2) \in \interior{\Delta^2}/ \bZ_3$ up to cyclic permutation of the entries.
  This defines a homeomorphism $\expNS[3]/U(1) - \expNS[2]/U(1) \cong \interior{\Delta^2}/ \bZ_3$.
  A fundamental domain of the action of $\bZ_3$ on $\interior{\Delta^2}$ is $\{ (t_0, t_1, t_2) \in \interior{\Delta^2} \mid t_0, t_1 \ge t_2 \}$.
  From this, we obtain $\expNS[3]$ as a quotient of $\Delta^2$ with the cellular decomposition shown in Figure \ref{figure:cellular_decomposition_exp3_S1}.
  With this identification, the map $\theta$ sends a point
  $(t_0, t_1, t_2) \in \langle 0\ 1\ 2\rangle \times \Delta^2 \subset \FatPolspcunen_2(1,1)$ to $(t_0, t_1, t_2) \in \expNS[3]/U(1)$ and
  a point $(t_0, t_1, t_2) \in \langle 0\ 2\ 1 \rangle \times \Delta^2 \subset \FatPolspcunen_2(1,1)$ is send to $(t_0, t_2, t_1)$.
  Observe that $\theta$ is cellular and that its degree on $\langle 0\ 1\ 2 \rangle$ is $+3$ whereas on $\langle 0\ 2\ 1 \rangle$ it is $-3$.
  Observe further that $H_2(\FatPolspcunen_2(1,1); \bZ)$ is generated by $\langle 0\ 1\ 2 \rangle - \langle 0\ 2\ 1 \rangle$.
  Therefore, \eqref{proposition:comparing_pol_and_bu_comb:theta_2} is a rational isomorphism which was left to show.
\end{proof}

