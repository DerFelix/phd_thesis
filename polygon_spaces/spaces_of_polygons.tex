\section{The space of polygons as simplicial spaces}
\label{section:polygon_spaces}
In this section, we equip the sets of (un)enumerated and (un)parametrized permutations on $[n]$ with the structure of a cyclic set.
For convenience of the reader, we provide the definitions and facts about (multi)cyclic sets used in this thesis in Appendix \ref{appendix:multicyclic_sets}.
Let us describe informally the faces, degeneracies and the action of the cyclic operator on a permutation $\sigma \in \SymGrn$.
To this end, we represent $\sigma$ as a product of cycles
$\sigma= \langle \sigma_{1,0}\ \ldots\ \rangle \cdots \langle \sigma_{m,0}\ \ldots \rangle$.
The $i$-th face of $\sigma$ deletes the symbol $i$ from its cycle representation and renormalizes the other symbols afterwards.
The $i$-th degeneracy map increases all symbols $j \ge i$ by one in the cycle representation and makes $i$ the predecessor of $i+1$ afterwards.
The cyclic operator increases all symbols in the cycle representation by one (with the convention that $n+1=0$).
For a precise definition, see Definitions \ref{definition:sym_unun_structure_maps},
\ref{definition:sym_unen_structure_maps}, \ref{definition:sym_paun_structure_maps} and \ref{definition:sym_paen_structure_maps}.

The spaces of (un)enumerated and (un)parametrized polygons is the geometric realization of the simplicial set underlying the cyclic set.
This space is stratified by the number of cycles of a permutation and the $m$-th stratum is the space of $m$ (un)enumerated and (un)parametrized polygons.
Let us repeat that we make the definitions of the four variations of these spaces of polygons explicit for the sake of completeness.
After reading Subsection \ref{section:polygon_spaces_unun} carefully,
it is enough to skim the Subsections \ref{section:polygon_spaces_unen}, \ref{section:polygon_spaces_paun} and \ref{section:polygon_spaces_paen}.


\subsection{The space of unenumerated unparametrized polygons}
\label{section:polygon_spaces_unun}
\begin{definition}
  \label{definition:sym_unun_structure_maps}
  Denote the $i$-th face map in the simplicial category by $\delta_i \colon [n-1] \to [n]$.
  By Definition \ref{definition:symmetric_group_on_injective_maps},
  the induced map $F_i := \SymGrunun(\delta_i) \colon \SymGrununn[n-1] \hr \SymGrununn$ regards a permutation
  $\sigma \in \SymGrununn[n-1]$ as a permutation of $[n]$ that has $i$ as a fixed point.
  
  The $i$-th face map $D_i \colon \SymGrununn \to \SymGrununn[n-1]$ is
  \begin{align}
    D_i( \sigma ) := F_i^{-1}( \transp{\sigma(i)}{i} \cdot \sigma )
  \end{align}
  where $\transp{\sigma(i)}{i}$ is the permutation exchanging $\sigma(i)$ and $i$ (if $\sigma(i) \neq i$; and it is the identity otherwise).
  The $i$-th degeneracy map $S_i \colon \SymGrununn \to \SymGrununn[n+1]$ is
  \begin{align}
    S_j( \sigma ) := \transp{j+1}{j} \cdot  F_j( \sigma ).
  \end{align}
  The cyclic operator $T \colon \SymGrununn \to \SymGrununn$ is
  \begin{align}
    T(\sigma) := \longcycle_n \cdot \sigma \cdot \longcycle_n^{-1}
  \end{align}
  with $\longcycle_n$ the long cycle $\longcycle_n = \cycle{0\; 1\; 2\; \ldots\; n}$.
\end{definition}

\begin{lemma}
  \label{lemma:cycles_of_a_permutation_under_structure_maps}
  Let $\sigma \in \SymGrununn$ and consider its set of cycles $\cycles(\sigma) =  \{ \sigma_1, \ldots, \sigma_k \}$.
  Let $0 \le i \le n$ and assume that $i$ belongs to $\sigma_j$.
  Then
  \begin{align}
    \cycles(D_i(\sigma)) =
      \begin{cases}
        \{ D_i(\sigma_1), \ldots, D_i(\sigma_k) \}                                               & \text{ if $\sigma(i) \neq i$} \\
        \{ D_i(\sigma_1), \ldots, D_i(\sigma_{j-1}), D_i(\sigma_{j+1}), \ldots, D_i(\sigma_k) \} & \text{ if $\sigma(i) = i$}
      \end{cases}
  \end{align}
  and
  \begin{align}
    \cycles(S_i(\sigma)) = \{ S_i(\sigma_1), \ldots, S_i(\sigma_k) \}
  \end{align}
  and
  \begin{align}
    \cycles(T(\sigma)) = \{ T(\sigma_1), \ldots, T(\sigma_k) \}.
  \end{align}
\end{lemma}

We do not give a proof of this Lemma because the proof is straight forward.

\begin{definition}
  \label{definition:sym_unun_as_cylic_set}
  The sets $\{ \SymGrununn \}_{n \in \bN}$ together with the face maps $D_i$, the degeneracy maps $S_j$ and the cyclic operators $T$ form a cyclic set denoted by $\SymGrunun$.
\end{definition}

\begin{definition}
  \label{definition:pol_unun}
  The \defn{space of unenumerated unparametrized polygons} $\Polspcunun$ is the geometric realization of the (underlying simplicial set of the) cyclic set
  \begin{align}
    \Polspcunun := | \SymGrunun |.
  \end{align}
\end{definition}

\begin{definition}
  \label{definition:pol_unun_m}
  The cyclic set $\SymGrunun$ has for each $m \ge 1$ a cyclic subset $F_m \SymGrunun \subset \SymGrunun$
  consisting in degree $n$ of all permutations $\sigma \in \SymGrununn$ that have at most $m$ cycles (where fixed points count as cycles).
  This induces a filtration of the space of polygons $\Polspcunun$ by closed subspaces $F_m\Polspcunun := |F_m\SymGrunun| \subset \Polspcunun$.
  
  Let $m \ge 1$.
  The \defn{space of $m$ unenumerated unparametrized polygons} is the $m$-th stratum
  \begin{align}
    \Polspcunun(m,1) := F_m\Polspcunun - F_{m-1}\Polspcunun.
  \end{align}
\end{definition}


\subsection{The space of enumerated unparametrized polygons}
\label{section:polygon_spaces_unen}
\begin{definition}
  \label{definition:sym_unen_structure_maps}
  Let $\sigma\in \SymGrunenn$ and consider its ordered set of cycles $\cycles(\sigma) = \{ \sigma_1 < \ldots < \sigma_k \}$.
  Let $0 \le i \le n$ and $1 \le j \le k$ such that $i$ belongs to $\sigma_j$.
  Then the structure maps $D_i$, $S_i$ or $T$ applied to $\sigma$ give the permutations $D_i(\sigma)$, $S_i(\sigma)$ or $T(\sigma)$ and
  the enumeration of the cycles is as follows.
  \begin{align}
    \cycles(D_i(\sigma)) =
      \begin{cases}
        \{ D_i(\sigma_1) < \ldots < D_i(\sigma_k) \}                                                  & \text{ if $\sigma(i) \neq i$} \\
        \{ D_i(\sigma_1) < \ldots < D_i(\sigma_{j-1}) < D_i(\sigma_{j+1}) < \ldots < D_i(\sigma_k) \} & \text{ if $\sigma(i) = i$}
      \end{cases}
  \end{align}
  and
  \begin{align}
    \cycles(S_i(\sigma)) = \{ S_i(\sigma_1) < \ldots < S_i(\sigma_k) \}
  \end{align}
  and
  \begin{align}
    \cycles(T(\sigma)) = \{ T(\sigma_1) < \ldots < T(\sigma_k) \}.
  \end{align}
\end{definition}

\begin{definition}
  \label{definition:sym_unen_as_cylic_set}
  The sets $\{\SymGrunenn\}_{n \in \bN}$ together with the face maps $D_i$, the degeneracy maps $S_j$ and the cyclic operators $T$ form a cyclic set, denoted by $\SymGrunen$.
\end{definition}

\begin{definition}
  \label{definition:pol_unen}
  The \defn{space of enumerated unparametrized polygons} $\Polspcunen$ is the geometric realization of the (underlying simplicial set of the) cyclic set
  \begin{align}
    \Polspcunen := | \SymGrunen |.
  \end{align}
\end{definition}

\begin{definition}
  \label{definition:pol_unen_m}
  The cyclic set $\SymGrunen$ has for each $m \ge 1$ a cyclic subset $F_m \SymGrunen \subset \SymGrunen$
  consisting in degree $n$ of all permutations $\sigma \in \SymGrunenn$ that have at most $m$ cycles (where fixed points count as cycles).
  This induces a filtration of the space of polygons $\Polspcunen$ by closed subspaces $F_m\Polspcunen := |F_m\SymGrunen| \subset \Polspcunen$.
  
  Let $m \ge 1$.
  The \defn{space of $m$ enumerated unparametrized polygons} is the $m$-th stratum
  \begin{align}
    \Polspcunen(m,1) := F_m\Polspcunen - F_{m-1}\Polspcunen.
  \end{align}
\end{definition}


\subsection{The space of unenumerated parametrized polygons}
\label{section:polygon_spaces_paun}
\begin{definition}
  \label{definition:sym_paun_structure_maps}
  Let $\sigma\in \SymGrpaunn$ and consider its set of cycles $\cycles(\sigma) = \{ \sigma_1, \ldots, \sigma_k \}$.
  Let $0 \le i \le n$ and $1 \le j \le k$ such that $i$ belongs to $\sigma_j = \langle \sigma_{j,0}\ \sigma_{j,1}\ \ldots\ \rangle$.
  Then the structure maps $D_i$, $S_i$ or $T$ applied to $\sigma$ give the permutations $D_i(\sigma)$, $S_i(\sigma)$ or $T(\sigma)$ and
  the set of cycles is
  \begin{align}
    \cycles(D_i(\sigma_j)) =
      \begin{cases}
        \{ D_i(\sigma_1) ,\ldots ,D_i(\sigma_k) \}                                               & \text{ if $\sigma(i) \neq i$} \\
        \{ D_i(\sigma_1) ,\ldots ,D_i(\sigma_{j-1}) ,D_i(\sigma_{j+1}) ,\ldots ,D_i(\sigma_k) \} & \text{ if $\sigma(i) = i$}
      \end{cases}
  \end{align}
  and
  \begin{align}
    \cycles(S_i(\sigma)) = \{ S_i(\sigma_1), \ldots, S_i(\sigma_k) \}
  \end{align}
  and
  \begin{align}
    \cycles(T(\sigma)) = \{ T(\sigma_1), \ldots, T(\sigma_k) \}.
  \end{align}
  
  The parametrization of the permutations $D_i(\sigma)$, $S_i(\sigma)$ or $T(\sigma)$ is (up to possible renormalization) as follows:
  The first symbol of $D_i( \langle \sigma_{j,0}\ \sigma_{j,1}\ \ldots\ \rangle )$ is $\sigma_{j,0}$ if $i \neq \sigma_{j,0}$ and it is $\sigma_{j,1}$ otherwise.
  The first symbol of $S_i( \langle \sigma_{j,0}\ \sigma_{j,1}\ \ldots\ \rangle )$ is $\sigma_{j,0}$.
  The first symbol of $T( \langle \sigma_{j,0}\ \sigma_{j,1}\ \ldots\ \rangle ) = \langle \longcycle_n(\sigma_{j,0})\ \longcycle_n(\sigma_{j,0})\ \ldots\ \rangle$ is $\longcycle_n(\sigma_{j,0})$.
\end{definition}

\begin{definition}
  \label{definition:sym_paun_as_cylic_set}
  The sets $\{\SymGrpaunn\}_{n \in \bN}$ together with the face maps $D_i$, the degeneracy maps $S_j$ and the cyclic operators $T$ form a cyclic set, denoted by $\SymGrpaun$.
\end{definition}

\begin{definition}
  \label{definition:pol_paun}
  The \defn{space of unenumerated parametrized polygons} $\Polspcpaun$ is the geometric realization of the (underlying simplicial set of the) cyclic set
  \begin{align}
    \Polspcpaun := | \SymGrpaun |.
  \end{align}
\end{definition}

\begin{definition}
  \label{definition:pol_paun_m}
  The cyclic set $\SymGrpaun$ has for each $m \ge 1$ a cyclic subset $F_m \SymGrpaun \subset \SymGrpaun$
  consisting in degree $n$ of all permutations $\sigma \in \SymGrpaunn$ that have at most $m$ cycles (where fixed points count as cycles).
  This induces a filtration of the space of polygons $\Polspcpaun$ by closed subspaces $F_m\Polspcpaun := |F_m\SymGrpaun| \subset \Polspcpaun$.
  
  Let $m \ge 1$.
  The \defn{space of $m$ unenumerated parametrized polygons} is the $m$-th stratum
  \begin{align}
    \Polspcpaun(m,1) := F_m\Polspcpaun - F_{m-1}\Polspcpaun.
  \end{align}
\end{definition}


\subsection{The space of enumerated parametrized polygons}
\label{section:polygon_spaces_paen}
\begin{definition}
  \label{definition:sym_paen_structure_maps}
  Let $\sigma\in \SymGrpaen$ and consider its set of cycles $\cycles(\sigma) = \{ \sigma_1 < \ldots < \sigma_k \}$.
  Let $0 \le i \le n$ and $1 \le j \le k$ such that $i$ belongs to $\sigma_j = \langle \sigma_{j,0}\ \sigma_{j,1}\ \ldots\ \rangle$.
  Then the structure maps $D_i$, $S_i$ or $T$ applied to $\sigma$ give the permutations $D_i(\sigma)$, $S_i(\sigma)$ or $T(\sigma)$ and
  the enumeration of the cycles is as follows.
  \begin{align}
    \cycles(D_i(\sigma_j)) =
      \begin{cases}
        \{ D_i(\sigma_1) < \ldots < D_i(\sigma_k) \}                                                  & \text{ if $\sigma(i) \neq i$} \\
        \{ D_i(\sigma_1) < \ldots < D_i(\sigma_{j-1}) < D_i(\sigma_{j+1}) < \ldots < D_i(\sigma_k) \} & \text{ if $\sigma(i) = i$}
      \end{cases}
  \end{align}
  and
  \begin{align}
    \cycles(S_i(\sigma)) = \{ S_i(\sigma_1) < \ldots < S_i(\sigma_k) \}
  \end{align}
  and
  \begin{align}
    \cycles(T(\sigma)) = \{ T(\sigma_1) < \ldots < T(\sigma_k) \}.
  \end{align}
  
  The parametrization of the permutations $D_i(\sigma)$, $S_i(\sigma)$ or $T(\sigma)$ is (up to possible renormalization) as follows:
  The first symbol of $D_i( \langle \sigma_{j,0}\ \sigma_{j,1}\ \ldots\ \rangle )$ is $\sigma_{j,0}$ if $i \neq \sigma_{j,0}$ and it is $\sigma_{j,1}$ otherwise.
  The first symbol of $S_i( \langle \sigma_{j,0}\ \sigma_{j,1}\ \ldots\ \rangle )$ is $\sigma_{j,0}$.
  The first symbol of $T( \langle \sigma_{j,0}\ \sigma_{j,1}\ \ldots\ \rangle ) = \langle \longcycle_n(\sigma_{j,0})\ \longcycle_n(\sigma_{j,0})\ \ldots\ \rangle$ is $\longcycle_n(\sigma_{j,0})$.
\end{definition}

\begin{definition}
  \label{definition:sym_paen_as_cylic_set}
  The sets $\{\SymGrpaenn\}_{n \in \bN}$ together with the face maps $D_i$, the degeneracy maps $S_j$ and the cyclic operators $T$ form a cyclic set, denoted by $\SymGrpaen$.
\end{definition}

\begin{definition}
  \label{definition:pol_paen}
  The \defn{space of enumerated parametrized polygons} $\Polspcpaen$ is the geometric realization of the (underlying simplicial set of the) cyclic set
  \begin{align}
    \Polspcpaen := | \SymGrpaen |.
  \end{align}
\end{definition}

\begin{definition}
  \label{definition:pol_paen_m}
  The cyclic set $\SymGrpaen$ has for each $m \ge 1$ a cyclic subset $F_m \SymGrpaen \subset \SymGrpaen$
  consisting in degree $n$ of all permutations $\sigma \in \SymGrpaenn$ that have at most $m$ cycles (where fixed points count as cycles).
  This induces a filtration of the space of polygons $\Polspcpaen$ by closed subspaces $F_m\Polspcpaen := |F_m\SymGrpaen| \subset \Polspcpaen$.
  
  Let $m \ge 1$.
  The \defn{space of $m$ enumerated parametrized polygons} is the $m$-th stratum
  \begin{align}
    \Polspcpaen(m,1) := F_m\Polspcpaen - F_{m-1}\Polspcpaen.
  \end{align}
\end{definition}


\subsection{Forgetful maps}
\begin{definition}
  \label{definition:forgetting_enumerations_or_parametrizations_on_polspc}
  The forgetful maps from Definition \ref{definition:forgetting_enumerations_or_parametrizations_on_sym} induce quotient maps denoted as follows.
  \begin{align}
      \begin{tikzcd}[ampersand replacement=\&]
                                                                                              \& \Polspcpaun \ar{dr}{\pi^\paun_\unun}  \& \\
      \Polspcpaen \ar{ur}{\pi^\paen_\paun} \ar{dr}{\pi^\paen_\unen} \ar{rr}{\pi^\paen_\unun}  \&                                       \& \Polspcunun \\
                                                                                              \& \Polspcunen \ar{ur}{\pi^\unen_\unun}  \&
    \end{tikzcd}
  \end{align}
\end{definition}

\begin{remark}
  \label{remark:pol_m_covering}
  Let $m \ge 1$.
  Observe that both maps $\pi^\paen_\paun \colon \Polspcpaen \to \Polspcpaun$ and $\pi^\unen_\unun \colon \Polspcunen \to \Polspcunun$ restrict to an $m!$-fold covering
  \begin{align}
    \Polspcpaen(m,1) \to \Polspcpaun(m,1)
  \end{align}
  and
  \begin{align}
    \Polspcunen(m,1) \to \Polspcunun(m,1)
  \end{align}
  whose group of Deck transformation is a symmetric group on $m$ symbols.
  In Section \ref{section:poly_spaces_homotopy_type}, we show that
  $\pi^\paen_\unen$ restricts to the $m$-dimensional torus fibration
  \begin{align}
     \colon \Polspcpaen(m,1) \to \Polspcunen(m,1) \,.
  \end{align}
\end{remark}
