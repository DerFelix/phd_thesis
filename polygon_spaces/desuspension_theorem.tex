\section{The desuspension theorem for the spaces of polygons}
\label{section:desuspension_theorem_pol}
The space of all enumerated and parametrized polygons $\Polspcpaen$ is stratified by the spaces $\Polspcpaen(m,1)$, i.e.,
(1) $\Polspcpaen = \cup_k \Polspcpaen(m,1)$ and
(2) the closure of $\Polspcpaen(m,1)$ in $\Polspcpaen$ is $\ov{\Polspcpaen}(m,1) = \cup_{k \le m} \Polspcpaen(k,1)$ and
(3) $\Polspcpaen(m,1)$ is open in $\ov{\Polspcpaen}(m,1)$.
In particular, $\Polspcpaen$ is filtered by closed subspaces $F_m\Polspcpaen = \ov{\Polspcpaen}(m,1)$.
Therefore, after fixing $m \ge 1$, we think of a point in $\ov{\Polspcpaen}(m,1)$ as being non-degenerate if and only if it lies in $\Polspcpaen(m,1)$.
This idea of non-degenerate points is reflected by the moduli spaces and their harmonic compactifications in a precise sense,
see Section \ref{section:compactification_bar} and Section \ref{section:compactification_harmonic}.

The integral desuspension for the spaces of polygons $\PolspcpaenN(m,1)$ relates the complement of the degenerate points
$F_m\Polspcpaen - F_{m-1}\Polspcpaen = \Polspcpaen(m,1) \cong \PolspcpaenN(m,1) \times \interior{\Delta^{m-1}}$
with the quotient by the degenerate points $F_m\Polspcpaen/ F_{m-1}\Polspcpaen$.
\begin{theorem}
  \label{theorem:normalized_polygons_and_suspensions}
  Let $m \ge 1$.
  There is an isomorphism
  \begin{align}
    H_\ast( \PolspcpaenN(m,1); \bZ) \cong \Sigma^{-m+1} H_\ast( F_m\Polspcpaen, F_{m-1}\Polspcpaen; \bZ) \,.
  \end{align}
\end{theorem}
Moreover, without taking signs into account, we have a desuspension theorem for the spaces of polygons $\Polspcstar(m,1)$.
\begin{theorem}
  \label{theorem:normalized_polygons_and_suspensions_mod_2}
  There are isomorphisms
  \begin{align}
    H_\ast( \PolspcpaenN(m,1); \bF_2) &\cong \Sigma^{-m+1} H_\ast( F_m\Polspcpaen, F_{m-1}\Polspcpaen; \bF_2) \\
    H_\ast( \PolspcpaunN(m,1); \bF_2) &\cong \Sigma^{-m+1} H_\ast( F_m\Polspcpaun, F_{m-1}\Polspcpaun; \bF_2) \\
    H_\ast( \PolspcunenN(m,1); \bF_2) &\cong \Sigma^{-m+1} H_\ast( F_m\Polspcunen, F_{m-1}\Polspcunen; \bF_2) \\
    H_\ast( \PolspcununN(m,1); \bF_2) &\cong \Sigma^{-m+1} H_\ast( F_m\Polspcunun, F_{m-1}\Polspcunun; \bF_2)
  \end{align}
  Moreover, these isomorphisms commute with the forgetful maps
  (see Definitions \ref{definition:forgetting_enumerations_or_parametrizations_on_polspc} and \ref{definition:forgetting_enumerations_or_parametrizations_on_normalized_polygons}).
\end{theorem}
Geometrically speaking, both theorems follow from an extension of the homeomorphism
\begin{align*}
  F_m\Polspcpaen - F_{m-1}\Polspcpaen \cong \PolspcpaenN(m,1) \times \interior{\Delta^{m-1}}
\end{align*}
to the quotient
\begin{align*}
  F_m\Polspcpaen/ F_{m-1}\Polspcpaen \cong \PolspcpaenN(m,1) \wedge \bS^{m-1} \,.
\end{align*}
However, we will give an algebraic proof, see also Proposition \ref{proposition:NPol_and_suspensions}.
The author would like to thank Andrea Bianchi for pointing out a closed formula for the signs,
see \eqref{definition:block_and_set_of_symbols_left_of_i:parity_of_a_multisimplex} and \eqref{definition:block_and_set_of_symbols_left_of_i:sign_of_a_multisimplex}.

Before going into the proofs, let us adumbrate how our desuspension theorem is related to the desuspension theorem of Klamt \cite[Theorem C]{Klamt2013}.
She introduces a chain complex of certain loop diagrams that approximates the chain complex of all natural operations of certain commutative algebras.
It is therefore related to the harmonic compactification of moduli spaces in a very precise meaning, see \cite[Section 2.4]{Klamt2013}.
For sake of simplicity let us assume that every point in the harmonic compactification yields some sort of loop diagram and that a loop diagram is called degenerate, if it comes from a degenerate moduli.
She shows that, in homology, the quotient of all loop diagrams by the subcomplex of degenerate loop diagrams is isomorphic to a (twisted) desuspension of the cacti operad.

\begin{proof}[Proof of Theorem \ref{theorem:normalized_polygons_and_suspensions_mod_2} and Theorem \ref{theorem:normalized_polygons_and_suspensions}]
  The desuspension theorem with coefficients in $\bF_2$ follows immediately from the construction of the multi-cyclic structure on $\PolspcpaenN(m,1)$ and
  the cell structures of $\Polspcpaun(m,1)$, $\Polspcunen(m,1)$ and $\Polspcunun(m,1)$.
  Up to shift in degrees, the cellular complex of the left hand side has the same cells and faces as the relative simplicial complex of the right hand side.

  Introducing the correct signs, Theorem \ref{theorem:normalized_polygons_and_suspensions} follows immediately from Proposition \ref{proposition:NPol_and_suspensions}.
\end{proof}

In order to define the correct signs mentioned in the proof of Theorem \ref{theorem:normalized_polygons_and_suspensions}, we need to introduce some notation.

\begin{definition}
  \label{definition:symbols_left_of_i}
  Let $m \ge 1$ and $\sigma = \langle \sigma_{1,0}\ \ldots\ \rangle \cdots \langle \sigma_{m,0}\ \ldots\ \rangle \in \SymGrpaenn[k]$ be a multi-simplex of $\PolspcpaenN(m,1)$
  of multi-degree $(k_1, \ldots, k_m)$.
  
  Less pedantically, we say that \defn{$l$ is left of $u$ in $\sigma$}, if $l$ appears left of $u$ in the parametrized enumerated cycle decomposition of $\sigma$.
  
  More pedantically:
  For every $s \in [k]$, we have unique numbers $1 \le j(s) \le m$ and $0 \le i(s) \le k_{j(s)}$ with $u = \sigma_{j(s),i(s)}$ and
  we say that \defn{$l$ is left of $u$ in $\sigma$}, in symbols $l \leftof_\sigma u$, if the following condition is satisfied:
  \begin{align}
    l \leftof_\sigma u \quad:\Leftrightarrow\quad l \neq u \text{ and } \big( j(l) < j(u) \text{ or } j(l) = j(u) \text{ and } i(l) < i(u) \big) \,.
  \end{align}
  The \defn{set of symbols left of $u$ in $\sigma$} is
  \begin{align}
    \{ l \in [k] \mid l \leftof_\sigma u \} \,.
  \end{align}
\end{definition}

\begin{example}
  Consider the parametrized enumerated permutation $\sigma = \cycle{3\ 1\ 0}\cycle{2\ 5}\cycle{4}$.
  Then, $3$ is left of $0$, $1$, $2$, $4$ and $5$.
  The set of symbols left of $5$ is $\{0, 1, 2, 3\}$.
\end{example}

\begin{definition}
  \label{definition:parity_and_sign}
  Let $m \ge 1$ and $\sigma\in \SymGrpaenn[k]$ be a multi-simplex of $\PolspcpaenN(m,1)$.
  The \defn{parity of $i$ in $\sigma$} is
  \begin{align}
    \parity(\sigma, i) := \#\{ l \in [k] \mid l \leftof_\sigma i\} \in \bF_2 \,.
  \end{align}
  Having the inversion number of a permutation in mind, the \defn{parity of $\sigma$} is
  \begin{align}
    \label{definition:block_and_set_of_symbols_left_of_i:parity_of_a_multisimplex}
    \parity(\sigma) := \#\{ (l,u) \in [k]^2 \mid l < u, u \leftof_\sigma l \} \in \bF_2
  \end{align}
  and the \defn{sign of $\sigma$} is
  \begin{align}
    \label{definition:block_and_set_of_symbols_left_of_i:sign_of_a_multisimplex}
    \sign(\sigma) := (-1)^{\parity(\sigma)} \in \bF_2 \,.
  \end{align}
\end{definition}

\begin{example}
  Consider the parametrized enumerated permutation $\sigma = \cycle{3\ 1\ 0}\cycle{2\ 5}\cycle{4}$.
  Then, the parity of $5$ in $\sigma$ is
  \begin{align}
    \parity(\sigma, 5) = \#\{ 0, 1, 2, 3 \} = 0 \,.
  \end{align}
  The parity of $\sigma$ is the number of pairs $(l,u)$ with $l < u$ and $u \leftof_\sigma l$:
  \begin{align}
    \parity(\sigma) = \# \{ (0,1), (0,3), (1,3), (2,3), (4,5) \} = 1 \,.
  \end{align}
  Therefore, the sign of $\sigma$ is
  \begin{align}
    \sign(\sigma) = (-1)^{\parity(\sigma)} = -1 \,.
  \end{align}
\end{example}

\begin{proposition}
  \label{proposition:NPol_and_suspensions}
  Let $m \ge 1$.
  Denote the cellular complex of the multi-simplicial space $\PolspcpaenN(m,1)$ by $C_\ast( \PolspcpaenN(m,1) )$ and
  the relative simplicial complex associated to the simplicial pair $(F_m\Polspcpaen, F_{m-1}\Polspcpaen)$ by $C_\ast(F_m\Polspcpaen, F_{m-1}\Polspcpaen)$.
  
  The signs in \eqref{definition:block_and_set_of_symbols_left_of_i:sign_of_a_multisimplex} define an isomorphism of chain complexes.
  \begin{align}
    C_\ast( F_m\Polspcpaen, F_{m-1}\Polspcpaen) \to \Sigma^{-m+1} C_\ast( \PolspcpaenN(m,1)), \quad \sigma \mapsto \sign(\sigma) \cdot \sigma
  \end{align}
\end{proposition}

\begin{remark}
  \label{remark:symmetric_difference}
  The symmetric difference of two sets $A$ and $B$ is denoted by $A \Delta B$ and is clear that
  \begin{align}
    \#A + \#B = \#(A \Delta B) \in \bF_2 \,.
  \end{align}
  We will use this fact in the proof of Proposition \ref{proposition:NPol_and_suspensions} implicitly.
\end{remark}

\begin{proof}
  Let $m \ge 1$ and $\sigma \in C_\ast(\PolspcpaenN(m,1))$ a generator of multi-degree $(k_1, \ldots, k_m)$.
  Usually, the sign of a face $d_{j,i}\sigma$ in the totalization of the multi-simplical complex is defined to be $(-1)^{k_1 + \ldots + k_{j-1} + i}$.
  However, we obtain an isomorphic chain complex by using the sign $(-1)^{ \parity(\sigma, i) }$:
  An isomorphism is given by sending $\sigma$ to $(-1)^{p(\sigma)}\sigma$ with $p(\sigma) = \sum_{k_j \text{ odd}} j-1$.
  
  With this convention, we will verify that $\sigma \mapsto \sign(\sigma)\sigma$ is a chain map:
  To this end, let $\sigma \in C_\ast( F_m\Polspcpaen, F_{m-1}\Polspcpaen)$ and assume $d_i \sigma$ is non-trivial.
  It is enough to show that the coefficients $\sign(d_i\sigma) \cdot (-1)^i$ and $(-1)^{ \parity(\sigma, i) \} } \cdot \sign(\sigma)$ agree.
  This is the case if and only if the following expression vanishes in $\bF_2$.
  \begin{align}
    &\hideeq \parity(\sigma) + \parity(d_i\sigma) + \#\{ l \mid l < i \} + \#\{ l \mid l \leftof_\sigma i \}
  \intertext{
    In what follows, we abbreviate $\{l \in [k] \mid \ldots \}$ by $\{ l \mid \ldots \}$  and $\{ (l,u) \in [k]^2 \mid \ldots \}$ by $\{ l,u \mid \ldots \}$.
    The symbols in $d_i\sigma$ correspond to the symbols in $\sigma$ that are different from $i$.
    We use this correspondence to rewrite the parity of $d_i\sigma$.}
    &= \#\{ l,u \mid l < u, u \leftof_\sigma l \} + \#\{ l,u \mid l < u, u \leftof_\sigma l, i \notin \{l,u\} \} + \#\{ l \mid l < i \} + \#\{ l \mid l \leftof_\sigma i \} \\
  \intertext{Computing the symmetric difference of the first two terms yields the following.}
    &= \#\{ l,u \mid l < u, u \leftof_\sigma l, i \in \{ l, u \} \} + \#\{ l \mid l < i \} + \#\{ l \mid l \leftof_\sigma i \} \\
    &= \#\{ l \mid l < i, i \leftof_\sigma l \} + \#\{ l \mid i < l, l \leftof_\sigma i \} + \#\{ l \mid l < i \} + \{ l \mid l \leftof_\sigma i \}
  \intertext{Computing the symmetric difference of the first and the third term yields the following.}
    &= \#\{ l \mid l < i, l \leftof_\sigma i \} +\#\{ l \mid i < l, l \leftof_\sigma i \} + \#\{ l \mid l \leftof_\sigma i \}
    \intertext{Now we compute the symmetric difference of the first two terms.}
    &= \#\{ l \mid l \leftof_\sigma i \} + \#\{ l \mid l \leftof_\sigma i \} \\
    &= 0
  \end{align}
\end{proof}
