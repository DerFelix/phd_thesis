\section{The symmetric group}
\label{section:notation_symmetric_groups}
In this section we fix our notation.
The definitions and propositions in this section are well known.

\begin{definition}
  \label{definition:symmetric_group}
  We denote the category of finite sets with injective maps by $\FinInj$.
  The \defn{symmetric group} on a finite set $A$ is the group of automorphisms $\SymGr(A) := \Aut_{\FinInj}(A)$.
  Consequently, in our notation, the product $\beta \cdot \alpha$ agrees with the composition $\beta \circ \alpha$ of maps.
\end{definition}

\begin{definition}
  \label{definition:symmetric_group_on_injective_maps}
  Given an injection $f \colon A \to B$, the set $A$ is identified with a subset of $B$ and so
  every permutation on $A$ is extended to a permutation of $B$ by fixing $B -f(A)$ pointwise.
  More pedantically, the induced group homomorphism $\SymGr(f) \colon \SymGr(A) \hr \SymGr(B)$ is defined by
  \begin{align}
    \SymGr(f)(\sigma)(b) =
      \begin{cases}
        f(\sigma(s)) & \text{ if $b = f(s)$ for some  $s \in A$}, \\
        b            & \text{ else.}
      \end{cases}
  \end{align}
\end{definition}

\begin{proposition}
  \label{proposition:symmetric_groups_define_a_functor}
  The symmetric groups define a faithful functor into the category of groups with monomorphisms
  \begin{align}
    \SymGr \colon \FinInj \to \GrpInj.
  \end{align}
\end{proposition}

\begin{definition}
  The \defn{support} of a permutation $\sigma \in \SymGr(A)$ is the set of non-trivially permuted symbols
  \begin{align}
    \supp(\sigma) = \{ s \in A \mid \sigma(s) \neq s \} \,.
  \end{align}
  The \defn{set of fixed points} of a permutation $\sigma \in \SymGr(A)$ is
  \begin{align}
    \fix(\sigma) = \{ s \in A \mid \sigma(s) = s \} = A - \supp(\sigma) \,.
  \end{align}
\end{definition}

\begin{definition}
  \label{definition:long_cycle}
  The \defn{long cycle of degree $n \ge 0$} is the permutation $\omega_n \in \SymGrn$ sending $k$ to $k+1$ modulo $n+1$.
  We write $\omega_n = \langle 0\ \ldots\ n \rangle$ up to cyclic permutation of the symbols.
\end{definition}

\begin{definition}
  \label{definition:cycle_representation}
  A permutation $\id_A \neq \sigma \in \SymGr(A)$ is \defn{cyclic} if there exists $0 \le n \le \#A$ and an injective map $f \in \FinInj([n],A)$ such that $\sigma = \SymGr(f)(\omega_n)$.
  In this case, the \defn{cycle representation} of $\sigma$ is the word $\langle f(0)\ f(1)\ \ldots\ f(n)\rangle$ up to cyclic permutation of the entries.
  Abusing notation, a non-trivial cyclic permutation is identified with its cycle representation.
\end{definition}

\begin{definition}
  \label{definition:cycle_representation_inverse}
  Let $A$ be a finite set and $n \ge 0$.
  Given a word $s = \langle s_0\ \ldots\ s_n \rangle$ up to cyclic permutation and without reoccurring letters $s_i \in A$,
  the permutation associated with $s$ is $\SymGr(f)(\omega_n)$ where $f(i) = s_i$.
  Abusing notation, such a word is identified with its associated permutation.
\end{definition}

\begin{remark}
  \label{remark:cycle_representation_and_fixedpoints}
  Clearly, a non-trivial cyclic permutation is uniquely given by its cycle representation.
  Observe that, in Definition \ref{definition:cycle_representation}, the cycle representation is only defined for non-trivial permutations.
  However, in Definition \ref{definition:cycle_representation_inverse}, we are more generous and allow words of length one.
  In this case, the associated permutation is the identity and
  we regard a word of length one as a chosen fixed point of the identity.
  In other words, a fixed point $b$ is seen as a trivial cyclic permutation,
  its cycle representation is $\langle b \rangle$ and
  the product of a permutation $\sigma$ with the trivial cyclic permutation $\langle b \rangle$ is $\sigma$.
\end{remark}

\begin{definition}
  \label{definition:set_of_non_trivial_cycles}
  The \defn{set of non-trivial cycles} of a permutation $\sigma \in \SymGr(A)$ is the set of non-trivial cyclic permutations
  $\cycles^\dagger(\sigma)= \{\sigma_1, \ldots, \sigma_t\}$ with
  $\sigma = \sigma_1 \cdots \sigma_t$ and $\supp(\sigma_i) \cap \supp(\sigma_j) = \emptyset$ for all $i \neq j$.
\end{definition}

\begin{definition}
  \label{definition:cycles_of_a_permutation}
  The \defn{set of cycles} of a permutation $\sigma \in \SymGr(A)$ is $\cycles(\sigma) = \cycles^\dagger(\sigma)\sqcup \fix(\sigma)$,
  where each fixed point $i$ is regarded as cycle $\cycle{i}$.
  The elements of $\cycles(\sigma)$ are the \defn{cycles of $\sigma$}.
  The \defn{number of cycles} of a permutation $\sigma$ is the cardinality of $\cycles(\sigma)$.
\end{definition}

\begin{definition}
  \label{definition:s_belongs_to_sigma}
  Let $\sigma \in \SymGr(A)$ and $\cycles(\sigma) = \{ \sigma_1, \ldots, \sigma_k \}$ be its set of cycles.
  The symbol $s \in A$ \defn{belongs to $\sigma_k$} if it occurs in the cycle representation of $\sigma_k$.
\end{definition}

\begin{remark}
  Observe that, by Remark \ref{remark:cycle_representation_and_fixedpoints} and Definition \ref{definition:s_belongs_to_sigma}, each symbol $s \in A$ belongs to exactly one cycle of a given permutation $\sigma$.
  Here, we demonstrate that each fixed point belongs to exactly one cycle.
  By definition, the set of cycles $\cycles(\sigma) = \{ \sigma_1, \ldots, \sigma_k \}$ is the union of the set of non-trivial cycles and the set of fixed points.
  Following Remark \ref{remark:cycle_representation_and_fixedpoints}, we emphasize that we identify a fixed point $s \in \fix(\sigma)$ with the cycle representation $\langle s\rangle$ and
  therefore, we write $\sigma_i = \langle s \rangle$, for a unique $i$.
  In particular, $s$ occurs in the cycle representation of $\sigma_i$ and, according to Definition \ref{definition:s_belongs_to_sigma}, we say that $s$ belongs to $\sigma_i$.
\end{remark}

\begin{definition}
  \label{definition:enumerated_permutations}
  A permutation $\sigma$ is \defn{enumerated} if its set of cycles is equipped with a linear order.
  \begin{align}
    \cycles(\sigma) = \{ \sigma_1 < \cdots < \sigma_k \} \,.
  \end{align}
  
  The \defn{set of enumerated permutations on $A$} is the set $\SymGr^\unen(A)$.
\end{definition}

\begin{definition}
  \label{definition:parametrized_permutations}
  A permutation $\sigma$ is \defn{parametrized} if each cycle has a distinguished element.
  Given a parametrized permutation $\sigma$, denote the distinguished symbol of a cycle $\tau$ by $\tau_0$ and denote the $i$-th successor of $\tau_0$ by $\tau_i := \tau^i( \tau_0 )$.
  The \defn{parametrized cycle representation} of $\tau$ is
  $\tau = \langle \tau_0\ \tau_1 \ \ldots\ \rangle$.
  The set of parametrized permutations on $A$ is the set $\SymGr^\paun(A)$.
\end{definition}

\begin{remark}
  By definition, the cycle representation of an unparametrized permutation is unique up to cyclic permutation of its symbols.
  However, the parametrized cycle representation of a parametrized permutation is unique as the distinguished symbol comes first.
\end{remark}

\begin{definition}
  \label{definition:enumerated_and_parametrized_permutations}
  A permutation $\sigma$ is \defn{enumerated and parametrized},
  if its cycles are enumerated (in the sense of Definition \ref{definition:enumerated_permutations}) and
  if each cycle has a distinguished element (in the sense of Definition \ref{definition:parametrized_permutations}).
  
  The \defn{set of enumerated and parametrized permutations on $A$} is the set $\SymGr^\paen(A)$.
\end{definition}

\begin{notation}
  By abuse of notation, the term \defn{permutation} refers to an ordinary permutation, an enumerated permutation, a parametrized permutation or an enumerated and parametrized permutation.
\end{notation}

\begin{definition}
  \label{definition:forgetting_enumerations_or_parametrizations_on_sym}
  For each $A$, forgetting the enumeration or the parametrization of its permutations defines a commutative diagram of sets:
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
                                                                                                  \& \SymGr^\paun(A) \ar{dr}{\pi^\paun_\unun}  \& \\
      \SymGr^\paen(A) \ar{ur}{\pi^\paen_\paun} \ar{dr}{\pi^\paen_\unen} \ar{rr}{\pi^\paen_\unun}  \&                                           \& \SymGr^\unun(A) \\
                                                                                                  \& \SymGr^\unen(A) \ar{ur}{\pi^\unen_\unun}  \&
    \end{tikzcd}
  \end{align}
\end{definition}
