\section{On the homotopy type of the spaces of polygons}
\label{section:poly_spaces_homotopy_type}
In this section, we study the homotopy type of the spaces of polygons.
Most importantly, we will show that $\Pol^\paen$ is contractible, see Proposition \ref{proposition:polpaen_is_contractible} and
that the bundle $\Polspcpaen(m,1) \to \Polspcunen(m,1)$ is the universal torus bundle $EU(1)^m \to BU(1)^m$, see Theorem \ref{theorem:polspcpaenN_is_ETn}.


\begin{proposition}
  Let $m \ge 1$.
  The forgetful maps induce $m$-dimensional torus bundles.
  \begin{align}
    \begin{tikzcd}[ampersand replacement=\&]
      \Polspcpaen(m,1) \ar{r}{\cong} \ar{d}{\pi^\paen_\unen}  \& \PolspcpaenN(m,1) \times \interior{\Delta^{m-1}} \ar{r}{\simeq} \ar{d}{\pi^\paen_\unen}  \& \PolspcpaenN(m,1) \ar{d}{\pi^\paen_\unen} \\
      \Polspcunen(m,1) \ar{r}{\cong}                          \& \PolspcunenN(m,1) \times \interior{\Delta^{m-1}} \ar{r}{\simeq}                          \& \PolspcunenN(m,1)
    \end{tikzcd}
  \end{align}
\end{proposition}

\begin{proof}
  The commutativity of the diagram follows from Proposition \ref{proposition:strong_deformation_retraction_to_normalized_polygons}.
  The multi-cyclic structure of $\PolspcpaenN(m,1)$ induces a free action by the $m$-dimensional torus by Proposition \ref{proposition:multicyclic_torus_action_on_poly_is_free}.
  Therefore $\pi^\paen_\unen \colon \PolspcpaenN(m,1) \to \PolspcunenN(m,1)$ is a principle torus bundle.
  
  By Proposition \ref{proposition:Pol_as_NPol_times_delta}, the map $\pi^\paen_\unen \colon \Polspcpaen(m,1) \to \Polspcunen(m,1)$
  is the product of the torus fibration $\pi^\paen_\unen \colon \PolspcpaenN(m,1) \to \PolspcunenN(m,1)$ with $\id_{\interior{\Delta^{m-1}}}$.
  This finishes the proof.
\end{proof}

\begin{theorem}
  \label{theorem:polspcpaenN_is_ETn}
  The $m$-dimensional torus bundles
  \begin{align}
    \Polspcpaen(m,1) \to \Polspcunen(m,1) \quad\text{and}\quad \PolspcpaenN(m,1) \to \PolspcunenN(m,1)
  \end{align}
  are universal.
\end{theorem}

\begin{proof}
  The torus action is free by Proposition \ref{proposition:multicyclic_torus_action_on_poly_is_free} and so it is enough to prove that $\PolspcpaenN(m,1)$ is contractible.
  We show $\PolspcpaenN(m,1) \simeq \PolspcpaenN(m-1,1)$ for all $m \ge 1$ and $\PolspcpaenN(0,1) := \ast$.
  To this end, observe that the simplicial set $\PolspcpaenN(1,1)$ admits an extra degeneracy (see e.g.\ Appendix \ref{appendix:extra_degeneracies})
  defined by
  \begin{align}
    s_{-1}\langle i_0\ \ldots\ i_k \rangle = \langle-1 \ i_0 \ldots\ i_k\rangle
  \end{align}
  followed by a renormalization of the symbols, i.e., $s_{-1}\langle i_0\ \ldots\ i_k \rangle = \langle0 \ i_0+1 \ldots\ i_k+1\rangle$.
  Moreover, $\PolspcpaenN(1,1)$ has a single vertex $\langle0\rangle$ and therefore, it is contractible.
  For $m > 1$, there is an analogously constructed extra degeneracy on the first factor of the $m$-fold multi-simplicial set $\PolspcpaenN(m,1)$
  defined by
  \begin{align}
    s_{-1}\langle i_0\ \ldots\ i_k \rangle \sigma_2 \cdots \sigma_m = \langle -1 \ i_0\ \ldots\ i_k\rangle \sigma_2 \cdots \sigma_m
  \end{align}
  followed by a renormalization of the symbols.
  Consequently, $\PolspcpaenN(m,1)$ is homotopy equivalent to the subspace consisting of all non-degenerate simplices of the form $\sigma_1\cdots \sigma_m$ with $\sigma_1 = \langle 0\rangle$.
  This subspace is clearly homeomorphic to $\PolspcpaenN(m-1,1)$.
\end{proof}

\begin{proposition}
  \label{proposition:polpaen_is_contractible}
  The space $\Polspcpaen$ is contractible.
\end{proposition}

\begin{proof}
  Note that $\Polspcpaen$ is simply connected:
  It has a single zero cell $\langle 0\rangle$ and the only non-degenerate one-dimensional cells are $\langle 1\ 0\rangle$, $\langle 0 \rangle \langle 1 \rangle$ and $\langle 1 \rangle \langle 0 \rangle$.
  The first loop is filled by the two-dimensional cell $\langle 0\ 2\ 1\rangle$,
  the second loop is filled by $\langle 0 \rangle \langle 1 \rangle \langle 2 \rangle$ and
  the third loop is filled by $\langle 0 \rangle \langle 2 \rangle \langle 1 \rangle$.
  
  The filtration of $\Polspcpaen$ by $F_m\Polspcpaen$, see Definition \ref{definition:pol_paen_m},
  yields a spectral sequence with first page $E^1_{m,s} \cong H_{m+s}( F_m\Polspcpaen, F_{m-1}\Polspcpaen; \bZ)$.
  We have $E^1_{m,s} \cong H_{1+s}( \PolspcpaenN(m,1); \bZ)$ by Theorem \ref{theorem:normalized_polygons_and_suspensions}.
  But $\PolspcpaenN(m,1)$ is contractible by Theorem \ref{theorem:polspcpaenN_is_ETn}.
  Therefore, $E^1_{m,s}$ is concentrated in a single row where $s=-1$ and $E^1_{m,-1} \cong H_0( \PolspcpaenN(m,1); \bZ)$ is generated by the cell $\langle 0 \rangle \cdots \langle m-1 \rangle$.
  The differential $d^1$ is easily computed.
  \begin{align}
    d^1(\langle 0 \rangle \cdots \langle m-1 \rangle) = \sum_{i} (-1)^i d_i ( \langle 0 \rangle \cdots \langle m-1 \rangle ) = \sum_{i} (-1)^i \langle 0 \rangle \cdots \langle m-2 \rangle
  \end{align}
  Thus, $E^1_{m,-1}$ is acyclic and so is $\Polspcpaen$.
\end{proof}
